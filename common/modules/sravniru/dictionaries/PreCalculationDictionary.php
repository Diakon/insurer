<?php

namespace common\modules\sravniru\dictionaries;

/**
 * Class PreCalculationDictionary
 *
 * @package common\modules\sravniru\dictionaries
 */
class PreCalculationDictionary
{
    /**
     * Идентификация ТС по VIN, или кузову, или шасси
     */
    public const CAR_HAVE_VIN = 0;
    public const CAR_HAVE_BODY = 1;
    public const CAR_HAVE_CHASSIS = 2;
    const CAR_HAVE_LIST = [
        self::CAR_HAVE_VIN => "VIN номер ТС",
        self::CAR_HAVE_BODY => "Номер кузова",
        self::CAR_HAVE_CHASSIS => "Номер шасси",
    ];
}