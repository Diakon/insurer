<?php

namespace common\modules\sravniru\dictionaries;

/**
 * Class CarBrandModelDictionary
 * @package common\modules\sravniru\dictionaries
 */
class CarBrandModelDictionary
{
    /**
     * Типы поиска ТС
     */
    public const TYPE_SEARCH_BRAND = 0; // Поиск бренда
    public const TYPE_SEARCH_MODEL = 1; // Поиск модели
    public const TYPE_SEARCH_POWER = 3; // Поиск мощности
}