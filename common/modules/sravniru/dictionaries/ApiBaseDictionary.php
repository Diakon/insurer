<?php

namespace common\modules\sravniru\dictionaries;

/**
 * Class ApiBaseDictionary
 * @package common\modules\sravniru\dictionaries
 */
class ApiBaseDictionary
{
    public const TEST_HOST = 'http://apigateway.svc.master.qa.sravni-team.ru/';
    public const PROD_HOST = 'https://api.sravni.ru/';

}
