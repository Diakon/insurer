<?php

namespace common\modules\sravniru\dictionaries;

/**
 * Class OsagoSravniRuFormDictionary
 * @package common\modules\sravniru\dictionaries
 */
class OsagoSravniRuFormDictionary
{
    /**
     * Ключ названия сессии для записи предложений от страховой компании
     */
    public const SESSION_INSURE_DATA_NAME = 'sravniru-insure-offer';

    /**
     * Количество максимально возможных водителей
     */
    public const MAX_DRIVERS_COUNT = 5;

    /**
     * Указать предыдущее ВУ
     */
    public const ADD_PREV_DRIVER_INFO_YES = 1;
    public const ADD_PREV_DRIVER_INFO_NO = 0;

    /**
     * Тип расчета
     */
    public const PRE_CALCULATION_YES = 1; // Предварительный расчет
    public const PRE_CALCULATION_NO = 0; // Полный расчет
    const TYPE_CALC = [
        self::PRE_CALCULATION_YES => "Предварительный расчет (минимум данных)",
        self::PRE_CALCULATION_NO => "Полный расчет (с возможностью оформления полиса)",
    ];

    /**
     * Мультидрайв (ввод водителей)
     */
    public const IS_MULTI_DRIVE_YES = 1;
    public const IS_MULTI_DRIVE_NO = 0;

    /**
     * Есть предыдущий полис
     */
    public const PREV_POLICY_ISSET_YES = 1; // Есть предыдущий полис
    public const PREV_POLICY_ISSET_NO = 0; // Нет предыдущего полиса
}