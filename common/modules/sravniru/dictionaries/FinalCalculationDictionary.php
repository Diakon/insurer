<?php

namespace common\modules\sravniru\dictionaries;

use common\modules\cars\dictionaries\CarDictionary;

/**
 * Class FinalCalculationDictionary
 *
 * @package common\modules\sravniru\dictionaries
 */
class FinalCalculationDictionary extends PreCalculationDictionary
{
    /**
     * Типы документов автомобиля
     */
    public const CAR_DOC_TYPE_PTS = 1;
    public const CAR_DOC_TYPE_STS = 2;
    public const CAR_DOC_TYPE_EPTS = 4;
    const CAR_DOCS = [
        self::CAR_DOC_TYPE_PTS => 'ПТС',
        self::CAR_DOC_TYPE_STS => 'СТС',
        self::CAR_DOC_TYPE_EPTS => 'ЭПТС',
    ];
    /**
     * Соотношение id типа документов на ТС из таблицы cars с соответствующими значениями в сравни.ру
     */
    const CAR_TYPE_TO_SRAVNIRU = [
        CarDictionary::TYPE_DOCUMENT_PTS => self::CAR_DOC_TYPE_PTS,
        CarDictionary::TYPE_DOCUMENT_STS => self::CAR_DOC_TYPE_STS,
        CarDictionary::TYPE_DOCUMENT_EPTS => self::CAR_DOC_TYPE_EPTS,
    ];

    /**
     * Страхователь и владелец автомобиля одно лицо
     */
    public const ONE_FACE_YES = 1;
    public const ONE_FACE_NO = 0;
    const ONE_FACES = [
        self::ONE_FACE_YES => 'Да',
        self::ONE_FACE_NO => 'Нет',
    ];
}