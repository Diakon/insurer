<?php

namespace common\modules\sravniru\traits;

use common\modules\sravniru\services\AuthService;
use yii\helpers\Json;
use yii\httpclient\Client;

trait RequestApiTrait
{
    /**
     * Отправляет curl запрос
     * @param string $url
     * @param array|Json $data
     * @param int $format
     * @param string $method
     * @return \yii\httpclient\Response
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function request(string $url, $data, $format, string $method)
    {
        $client = new Client();
        return $client->createRequest()
            ->setFormat($format)
            ->setMethod($method)
            ->setUrl($url)
            ->addHeaders($this->getHeader())
            ->setData($data)
            ->send();
    }

    /**
     * Возвращает заголовок для отправки в АПИ сравни.ру
     * @param string $contentType
     * @return array
     * @throws \yii\base\ErrorException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function getHeader(string $contentType = 'application/x-www-form-urlencoded')
    {
        return ['Authorization' => $this->getAuthToken(), 'Content-Type' => $this->getContentType($contentType)];
    }

    /**
     * @return mixed|string
     * @throws \yii\base\ErrorException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function getAuthToken() {
        return AuthService::getToken();
    }

    /**
     * @param string $contentType
     * @return string
     */
    public function getContentType(string $contentType)
    {
        return $contentType;
    }
}