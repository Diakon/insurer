<?php

namespace common\modules\sravniru\interfaces;

/**
 * Interface AuthInterface
 * @package common\modules\sravniru\interfaces
 */
interface AuthInterface
{
    /**
     * Возвращает URL для отправки запроса в АПИ сравни.ру
     * @return mixed
     */
    public function getApiUrl();

    /**
     * Метод возвращающий данные для отправки в АПИ сравни.ру
     * @return mixed
     */
    public function getApiData();

    /**
     * Метод возвращающий формат curl запроса для отправки в АПИ сравни.ру
     * @return mixed
     */
    public function getApiCurlFormat();

    /**
     * Метод возвращает curl формат запрос в АПИ сравни.ру
     * @return mixed
     */
    public function getApiCurlMethod();
}