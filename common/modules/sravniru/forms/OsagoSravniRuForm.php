<?php
namespace common\modules\sravniru\forms;

use common\models\Model;
use common\modules\sravniru\dictionaries\PreCalculationDictionary;
use common\modules\sravniru\dictionaries\FinalCalculationDictionary;
use common\modules\sravniru\dictionaries\OsagoSravniRuFormDictionary;
use yii\helpers\ArrayHelper;

/**
 * OSAGO SravniRu form
 */
class OsagoSravniRuForm extends Model
{
    public $preCalculation;

    // Аттрибуты для раздела "Автомобиль"
    public $vehicleBrand;
    public $vehicleModel;
    public $vehiclePower;
    public $vehicleYear;
    public $vehicleVin;
    public $vehicleBodyNum;
    public $vehicleChassisNum;
    public $vehicleLicensePlate; // гос. номер
    public $vehicleIdentity = PreCalculationDictionary::CAR_HAVE_VIN;
    public $vehicleDocType;
    public $vehicleDocSerial;
    public $vehicleDocNumber;
    public $vehicleDocDate;

    // Аттрибуты для раздела "Водители"
    public $multiDrive;
    public $driverLicenseSerial = [];
    public $driverLicenseNumber = [];
    public $driverLastName = [];
    public $driverFirstName = [];
    public $driverMiddleName = [];
    public $driverExpDate = []; // Начало стажа ЛДУ
    public $driverBirthDate = []; // Дата рождения водителя
    public $driverPrevAddInfo = [];
    public $driverPrevLicenseSerial = [];
    public $driverPrevLicenseNumber = [];
    public $driverPrevLicenseDate = [];
    public $driverPrevLastName = [];
    public $driverPrevMiddleName = [];
    public $driverPrevFirstName = [];

    // Аттрибуты для раздела "Собственник"
    public $ownerCity; // Город прописки собственника (код)
    public $ownerDateOSAGOStart; // Дата начала действия полиса
    public $ownerDateOSAGOEnd; // Дата окончания действия полиса
    public $ownerPassportSerial;
    public $ownerPassportNumber;
    public $ownerPassportDate;
    public $ownerPassportIssuedBy;
    public $ownerLastName;
    public $ownerFirstName;
    public $ownerMiddleName;
    public $ownerBirthDate;
    public $ownerDadataAddress;
    public $ownerPhone;
    public $ownerEmail;
    public $insurerLastName;
    public $insurerFirstName;
    public $insurerMiddleName;
    public $insurerBirthDate;
    public $insurerPhone;
    public $insurerEmail;
    public $insurerDadataAddress;
    public $insurerPassportSerial;
    public $insurerPassportNumber;
    public $insurerPassportDate;
    public $insurerPassportIssuedBy;
    public $insurerIsOwner;

    // Атрибуты для раздела "Полис"
    public $prevPolicyIsset;
    public $prevPolicySerial;
    public $prevPolicyNumber;
    public $prevPolicyEndDate;

    // Атрибуты для оплаты
    public $searchId;
    public $companyId;
    public $companyName;
    public $paymentUrl;
    public $redirectPaymentUrlSravniRu; // Ссылка на которую редиректить для оплаты
    public $hash;

    const SCENARIO_STEP_TYPE_CALC = 'type_calc';
    const SCENARIO_STEP_CAR_INFO = 'car';
    const SCENARIO_STEP_DRIVER = 'driver';
    const SCENARIO_STEP_OWNER = 'owner';
    const SCENARIO_STEP_CALCULATE = 'calc';
    const SCENARIO_STEP_POLICY = 'policy';
    const SCENARIO_STEP_PAYMENT = 'payment';
    const SCENARIO_VALIDATE_CAR_NUMBER = 'validate_car_number';

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();

        $scenarios[self::SCENARIO_VALIDATE_CAR_NUMBER] = ['vehicleLicensePlate'];
        $scenarios[self::SCENARIO_STEP_TYPE_CALC] = ['preCalculation'];
        $scenarios[self::SCENARIO_STEP_CAR_INFO] = [
            'preCalculation',
            'vehicleBrand',
            'vehicleModel',
            'vehiclePower',
            'vehicleYear',
            'vehicleLicensePlate',
            'vehicleIdentity',
            'vehicleVin',
            'vehicleBodyNum',
            'vehicleChassisNum',
            'vehicleDocType',
            'vehicleDocSerial',
            'vehicleDocNumber',
            'vehicleDocDate',
        ];

        $scenarios[self::SCENARIO_STEP_DRIVER] = [
            'multiDrive',
            'preCalculation',
            'driverLicenseSerial',
            'driverLicenseNumber',
            'driverLastName',
            'driverFirstName',
            'driverMiddleName',
            'driverExpDate',
            'driverBirthDate',
            'driverPrevLicenseSerial',
            'driverPrevLicenseNumber',
            'driverPrevLicenseDate',
            'driverPrevAddInfo',
            'driverPrevLastName',
            'driverPrevMiddleName',
            'driverPrevFirstName',
        ];
        $scenarios[self::SCENARIO_STEP_OWNER] = [
            'preCalculation',
            'ownerCity',
            'ownerDateOSAGOStart',
            'ownerDateOSAGOEnd',
            'ownerPassportSerial',
            'ownerPassportNumber',
            'ownerPassportDate',
            'ownerLastName',
            'ownerFirstName',
            'ownerMiddleName',
            'ownerPassportIssuedBy',
            'ownerBirthDate',
            'ownerDadataAddress',
            'ownerPhone',
            'ownerEmail',
            'insurerLastName',
            'insurerFirstName',
            'insurerMiddleName',
            'insurerBirthDate',
            'insurerPhone',
            'insurerEmail',
            'insurerDadataAddress',
            'insurerPassportSerial',
            'insurerPassportNumber',
            'insurerPassportDate',
            'insurerPassportIssuedBy',
            'insurerIsOwner'
        ];
        $scenarios[self::SCENARIO_STEP_POLICY] = [
            'preCalculation',
            'prevPolicySerial',
            'prevPolicyNumber',
            'prevPolicyEndDate',
            'prevPolicyIsset'
        ];
        $scenarios[self::SCENARIO_STEP_PAYMENT] = ['searchId', 'companyId', 'companyName', 'paymentUrl', 'redirectPaymentUrlSravniRu', 'hash'];

        // Сценарий расчета - объединяет предыдущие шаги
        $scenarios[self::SCENARIO_STEP_CALCULATE] = [];
        foreach (
            [
                self::SCENARIO_STEP_CAR_INFO,
                self::SCENARIO_STEP_DRIVER,
                self::SCENARIO_STEP_OWNER,
                self::SCENARIO_STEP_POLICY
            ] as $scenarioName) {
            $scenarios[self::SCENARIO_STEP_CALCULATE] = ArrayHelper::merge($scenarios[self::SCENARIO_STEP_CALCULATE], $scenarios[$scenarioName]);
        }

        return $scenarios;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        $rules = [];

        // Сценарий валидации гос. номера авто
        if (in_array($this->scenario, [self::SCENARIO_VALIDATE_CAR_NUMBER])) {
            $rules[] = [['vehicleLicensePlate'], 'required'];
            $rules[] = [['vehicleLicensePlate'], 'validateCarNumber'];
        }

        // Тип расчета - должен быть выбран для всех сценариев, от этого зависит валидация полей (валидация для полного или предварительного расчета)
        $rules[] = [['preCalculation'], 'required'];
        $rules[] = [['preCalculation'], 'integer'];
        $rules[] = [['preCalculation'], 'in', 'range' => array_keys($this->getTypeCalc())];

        // Сценарий проверки даных об авто и расчета
        if (in_array($this->scenario, [self::SCENARIO_STEP_CAR_INFO, self::SCENARIO_STEP_CALCULATE])) {
           $rules = ArrayHelper::merge($rules, $this->getCarInfoRules());
        }

        // Сценарий проверки даных о водителе (водителях) и расчета
        if (in_array($this->scenario, [self::SCENARIO_STEP_DRIVER, self::SCENARIO_STEP_CALCULATE])) {
            $rules = ArrayHelper::merge($rules, $this->getDriversRules());
        }

        // Сценарий продления полиса и расчета
        if (in_array($this->scenario, [self::SCENARIO_STEP_POLICY, self::SCENARIO_STEP_CALCULATE])) {
            $rules = ArrayHelper::merge($rules, $this->getPolicyRules());
        }

        // Сценарий проверки даных о собственнике и расчета
        if (in_array($this->scenario, [self::SCENARIO_STEP_OWNER, self::SCENARIO_STEP_CALCULATE])) {
            $rules = ArrayHelper::merge($rules, $this->getOwnerRules());
        }

        // Сценарий оплаты
        if (in_array($this->scenario, [self::SCENARIO_STEP_PAYMENT])) {
            $rules = ArrayHelper::merge($rules, $this->getPaymentRules());
        }

        return $rules;
    }

    /**
     * Валидация ввода гос. номера
     * @param $attribute
     * @param $params
     * @param $items
     */
    public function validateCarNumber($attribute, $params, $items)
    {
        if (!preg_match('/^[АВЕКМНОРСТУХ]\d{3}(?<!000)[АВЕКМНОРСТУХ]{2}\d{2,3}$/ui', $this->{$attribute})) {
            $this->addError($attribute, 'Ошибка ввода гос. номера. Введите номер в формате а123яя45');
        }
    }

    /**
     * Валидация номера телефона
     * @param $attribute
     * @param $params
     * @param $items
     */
    public function validatePhone($attribute, $params, $items)
    {
        $phone = preg_replace("/[^0-9]/", '', $this->{$attribute});
        if (!empty($phone) && substr($phone, 0, 2) != '79') {
            $this->addError($attribute, 'Неверно указан номер телефона. Номер телефона должен начинаться с +79');
        }
    }

    /**
     * Проверяет, есть ли введенный адрес в АПИ dadata
     * @param $attribute
     * @param $params
     * @param $items
     */
    public function validateDaData($attribute, $params, $items)
    {
        $search = \Yii::$app->api->requestWithInfo('address/search/detail', 'GET', ['query' => $this->{$attribute}]);
        if (empty($search['data']['items'])) {
            $this->addError($attribute, "Адрес не найден в базе данных адресов. Пожалуйста, начните вводить адрес вручную и выберите нужный из выпадающего списка.");
        }
    }

    /**
     * Проверяет, что дата выдачи полиза больше текузей даты хотя бы на 1 день
     * @param $attribute
     * @param $params
     * @param $items
     */
    public function validateOSAGOStartDate($attribute, $params, $items)
    {
        $date = $this->{$attribute};
        $dateTime = strtotime("$date 12:00:00");
        $minDate = date('Y-m-d', strtotime("+1 day"));
        $minDateTime = strtotime($minDate . " 00:00:00");
        if ($minDateTime > $dateTime) {
            $this->addError($attribute, 'Дата выдачи полиса должна быть больше текущей');
        }
    }

    /**
     * Валидирует даты
     * @param $attribute
     * @param $params
     * @param $items
     * @throws \Exception
     */
    public function validateDates($attribute, $params, $items)
    {
        $value = $this->{$attribute};
        if (!empty($value)) {
            switch ($attribute) {
                // Проверяю, что страхователю / собственнику 18 и более лет
                case "ownerBirthDate":
                case "insurerBirthDate":
                $value = \Yii::$app->formatter->getAgeByDate($value);
                    if ((int)$value < 18) {
                        $this->addError($attribute, 'Возраст не может быть меньше 18 лет.');
                        continue;
                    }
                    break;
                // Проверяю, что паспорт выдали минимум в 14 лет
                case "insurerPassportDate":
                case "ownerPassportDate":
                    // Получаю дату рождения
                    $field = $attribute == 'insurerPassportDate' ? 'insurerBirthDate' : 'ownerBirthDate';
                    $birthDate = $this->{$field};
                    if (empty($birthDate)) {
                        $this->addError($field, 'Вы не указали дату рождения');
                        continue;
                    }

                    $firstDateTimeObject = new \DateTime($birthDate);
                    $secondDateTimeObject = new \DateTime($value);
                    $delta = $secondDateTimeObject->diff($firstDateTimeObject);
                    if ((int)$delta->format('%y') < 14) {
                        $this->addError($attribute, 'Дата выдачи паспорта не может быть менее 14 лет со дня рождения');
                        continue;
                    }
                    break;
            }
        }
    }

    /**
     * Проверяет, что дата выдачи документа на авто не больше текущей даты
     * @param $attribute
     * @param $params
     * @param $items
     */
    public function validateVehicleDocDate($attribute, $params, $items)
    {
        if (!empty($this->vehicleDocDate)) {
            $dateTime = strtotime($this->vehicleDocDate . ' 00:00:00');
            if ($dateTime > time()) {
                $this->addError($attribute, 'Дата выдачи документа на автомобиль не может быть больше текущей даты');
            }
        }
    }

    /**
     * Валидация данных о водителях
     * @param $attribute
     * @param $params
     * @param $items
     */
    public function validateDrivers($attribute, $params, $items)
    {
        $countDrivers = 0;
        $labels = $this->attributeLabels();

        // Проверяю, что все массивы в drivers заполненны
        if (is_array($this->{$attribute}) && $this->multiDrive == OsagoSravniRuFormDictionary::IS_MULTI_DRIVE_NO) {
            foreach ($this->{$attribute} as $key => $value) {
                ++$countDrivers;
                if ($countDrivers > OsagoSravniRuFormDictionary::MAX_DRIVERS_COUNT) {
                    $this->addError($attribute . '_' . $key,
                        'Превышено максимально возможное количество добавляемых водителей. Мкасимально возможное количество водителей: ' . OsagoSravniRuFormDictionary::MAX_DRIVERS_COUNT);
                    continue;
                }
                if (is_null($this->{$attribute}[$key]) || $this->{$attribute}[$key] == "") {
                    // Если это данные о предыдущем ВУ - проверять, только если driverPrevAddInfo == 1
                    if (strripos($attribute, 'driverPrev') !== false
                        &&
                        $this->driverPrevAddInfo[$key] != OsagoSravniRuFormDictionary::ADD_PREV_DRIVER_INFO_YES)
                    {
                        continue;
                    }
                    $this->addError($attribute . '_' . $key, 'Пожалуйста, заполните поле "' . $labels[$attribute] . '".');
                    continue;
                }

                // Валидация дат
                if (
                    in_array($attribute, ['driverPrevLicenseDate', 'driverExpDate', 'driverBirthDate']) &&
                    !empty($value) &&
                    !is_numeric(strtotime($value))
                ) {
                    $this->addError($attribute . '_' . $key, 'Неверно указана дата');
                }

                // Кастомная валидация для некоторых полей водителя
                switch ($attribute) {
                    case "driverBirthDate":
                        // Получаю возраст водителя
                        $value = \Yii::$app->formatter->getAgeByDate($value);
                        if ((int)$value < 18 || (int)$value > 90) {
                            $this->addError($attribute . '_' . $key, 'Возраст водителя должен быть от 18 до 90');
                        }
                        break;
                }
            }
        }
    }

    /**
     * Возвращает правила для сценария "Информация о автомобиле"
     * @return array
     */
    private function getCarInfoRules()
    {
        $rules = [];
        $rules[] = [['vehicleBrand', 'vehicleModel', 'vehiclePower', 'vehicleYear'], 'required'];
        // Поля необходимые к заполнению при полном расчете
        $rules[] = [
            [
                'vehicleDocType',
                'vehicleDocSerial',
                'vehicleDocNumber',
                'vehicleLicensePlate',
                'vehicleDocDate',
            ], 'required', 'when' => function ($model) {
                return $model->preCalculation == OsagoSravniRuFormDictionary::PRE_CALCULATION_NO;
            }];
        $rules[] = [['vehicleDocDate'], 'date', 'format' => 'php:Y-m-d'];
        $rules[] = [['vehicleDocDate'], 'validateVehicleDocDate'];
        $rules[] = [['vehicleIdentity'], 'in', 'range' => array_keys($this->getTypeCarIdentity())];
        $rules[] = [['vehicleDocType'], 'in', 'range' => array_keys($this->getTypeCarDocs())];
        $rules[] = [['vehicleIdentity'], 'required', 'when' => function ($model) {
            return $model->preCalculation == OsagoSravniRuFormDictionary::PRE_CALCULATION_NO;
        }];
        $rules[] = [['vehicleDocSerial', 'vehicleDocNumber'], 'string'];
        $rules[] = [['vehicleYear'], 'integer', 'max' => (int)date('Y', time()), 'min' => 1950];
        $rules[] = [['vehiclePower'], 'integer', 'max' => 1000, 'min' => 1, 'message' => 'Мощность должна быть указана как число от 1 до 1000.'];
        $rules[] = [['vehiclePower', 'vehicleYear', 'vehicleDocType'], 'integer'];
        $rules[] = [['vehicleLicensePlate'], 'validateCarNumber'];
        $rules[] = [['vehicleVin', 'vehicleBodyNum', 'vehicleChassisNum'], 'string'];
        $rules[] = [['vehicleVin'], 'required', 'when' => function ($model) {
            return $model->vehicleIdentity == PreCalculationDictionary::CAR_HAVE_VIN;
        }];
        $rules[] = [['vehicleBodyNum'], 'required', 'when' => function ($model) {
            return $model->vehicleIdentity == PreCalculationDictionary::CAR_HAVE_BODY;
        }];
        $rules[] = [['vehicleChassisNum'], 'required', 'when' => function ($model) {
            return $model->vehicleIdentity == PreCalculationDictionary::CAR_HAVE_CHASSIS;
        }];

        return $rules;
    }

    /**
     * Возвращает правила для сценария "Информация о водителях"
     * @return array
     */
    private function getDriversRules()
    {
        $rules = [];
        $rules[] = [
            [
                'driverLicenseSerial',
                'driverLicenseNumber',
                'driverLastName',
                'driverFirstName',
                'driverMiddleName',
                'driverExpDate',
                'driverBirthDate'
            ],
            'required', 'when' => function ($model) {
                return $model->multiDrive == OsagoSravniRuFormDictionary::IS_MULTI_DRIVE_NO;
            }
        ];
        $rules[] = [
            [
                'driverPrevLicenseSerial',
                'driverPrevLicenseNumber',
                'driverPrevLicenseDate',
                'driverPrevLastName',
                'driverPrevMiddleName',
                'driverPrevFirstName',
            ], 'required', 'when' => function ($model) {
            return $model->driverPrevAddInfo == OsagoSravniRuFormDictionary::ADD_PREV_DRIVER_INFO_YES;
        }];
        $rules[] = [[
            'driverLicenseSerial',
            'driverLicenseNumber',
            'driverLastName',
            'driverFirstName',
            'driverMiddleName',
            'driverExpDate',
            'driverBirthDate',
            'driverPrevLicenseSerial',
            'driverPrevLicenseNumber',
            'driverPrevLicenseDate',
            'driverPrevFirstName',
            'driverPrevMiddleName',
            'driverPrevLastName',
            'driverPrevAddInfo',
        ], 'validateDrivers', 'skipOnEmpty' => false, 'skipOnError' => false];

        return $rules;
    }

    /**
     * Возвращает правила для сценария "Информация о полисе"
     * @return array
     */
    private function getPolicyRules()
    {
        $rules = [];
        $rules[] = [['prevPolicySerial'], 'required', 'when' => function ($model) {
            return !empty($model->prevPolicyIsset);
        }, 'message' => 'Пожалуйста, укажите "{attribute}"'];
        $rules[] = [['prevPolicyNumber'], 'required', 'when' => function ($model) {
            return !empty($model->prevPolicyIsset);
        }, 'message' => 'Пожалуйста, укажите "{attribute}"'];
        $rules[] = [['prevPolicyEndDate'], 'required', 'when' => function ($model) {
            return !empty($model->prevPolicyIsset);
        }, 'message' => 'Пожалуйста, укажите "{attribute}"'];

        $rules[] = [['prevPolicyEndDate'], 'date', 'format' => 'php:Y-m-d'];
        $rules[] = [['prevPolicySerial', 'prevPolicyNumber'], 'string'];
        $rules[] = [['prevPolicySerial', 'prevPolicyNumber'], 'trim'];
        $rules[] = [['prevPolicyIsset'], 'in', 'range' => [OsagoSravniRuFormDictionary::PREV_POLICY_ISSET_NO, OsagoSravniRuFormDictionary::PREV_POLICY_ISSET_YES]];

        return $rules;
    }

    /**
     * Возвращает правила для сценария "Владелец авто"
     * @return array
     */
    private function getOwnerRules()
    {
        $rules = [];
        $rules[] = [['ownerCity', 'ownerDateOSAGOStart'], 'required'];
        $rules[] = [['ownerCity'], 'string'];
        $rules[] = [['ownerDateOSAGOStart',], 'validateOSAGOStartDate', 'skipOnEmpty' => false, 'skipOnError' => false];
        // Требуется только при полном расчете и если мультидрайв
        $rules[] = [[
            'ownerPassportSerial',
            'ownerPassportNumber',
            'ownerPassportDate',
            'ownerLastName',
            'ownerFirstName',
            'ownerMiddleName',
            'ownerPassportIssuedBy',
            'ownerBirthDate',
            'ownerDadataAddress',
            'ownerPhone',
            'ownerEmail',

        ], 'required', 'when' => function ($model) {
            return $model->preCalculation == OsagoSravniRuFormDictionary::PRE_CALCULATION_NO || $model->multiDrive == OsagoSravniRuFormDictionary::IS_MULTI_DRIVE_YES;
        }];
        $rules[] = [[
            'insurerLastName',
            'insurerFirstName',
            'insurerMiddleName',
            'insurerDadataAddress',
            'insurerPassportSerial',
            'insurerPassportNumber',
            'insurerPassportIssuedBy',
            'insurerEmail',
            'insurerPhone',
            'insurerPassportDate'
        ], 'required', 'when' => function ($model) {
            return $model->preCalculation == OsagoSravniRuFormDictionary::PRE_CALCULATION_NO && $model->insurerIsOwner == FinalCalculationDictionary::ONE_FACE_NO;
        }];
        $rules[] = [[
            'ownerPassportSerial',
            'ownerPassportNumber',
            'ownerLastName',
            'ownerFirstName',
            'ownerMiddleName',
            'ownerPassportIssuedBy',
            'ownerDadataAddress',
            'insurerLastName',
            'insurerFirstName',
            'insurerMiddleName',
            'insurerDadataAddress',
            'insurerPassportSerial',
            'insurerPassportNumber',
            'insurerPassportIssuedBy',
        ], 'safe'];
        $rules[] = [[
            'ownerPassportSerial',
            'ownerPassportNumber',
            'ownerLastName',
            'ownerFirstName',
            'ownerMiddleName',
            'ownerPassportIssuedBy',
            'ownerDadataAddress',
            'insurerLastName',
            'insurerFirstName',
            'insurerMiddleName',
            'insurerDadataAddress',
            'insurerPassportSerial',
            'insurerPassportNumber',
            'insurerPassportIssuedBy',
        ], 'trim'];
        $rules[] = ['insurerEmail', 'email'];
        $rules[] = ['ownerEmail', 'email'];
        $rules[] = [['insurerPhone', 'ownerPhone'], 'match', 'pattern' => '/[0-9]/i'];
        $rules[] = [['insurerPhone', 'ownerPhone'], 'string', 'min' => 9, 'max' => 20];
        $rules[] = [['insurerPhone', 'ownerPhone'], 'validatePhone'];
        $rules[] = [['ownerPassportDate', 'ownerBirthDate', 'insurerBirthDate', 'insurerPassportDate', 'ownerDateOSAGOEnd'], 'date', 'format' => 'php:Y-m-d'];
        $rules[] = [['ownerPassportDate', 'ownerBirthDate', 'insurerBirthDate', 'insurerPassportDate'], 'validateDates'];
        $rules[] = [['insurerIsOwner'], 'in', 'range' => array_keys($this->getOneFaces())];
        $rules[] = [[
            'insurerIsOwner',
        ], 'required', 'when' => function ($model) {
            return $model->preCalculation == OsagoSravniRuFormDictionary::PRE_CALCULATION_NO;
        }];

        return $rules;
    }

    /**
     * Возвращает правила для сценария оплаты
     * @return array
     */
    private function getPaymentRules()
    {
        $rules = [];
        $rules[] = [['searchId', 'companyId', 'paymentUrl', 'hash', 'redirectPaymentUrlSravniRu'], 'required'];
        $rules[] = [['paymentUrl', 'hash', 'companyName', 'redirectPaymentUrlSravniRu'], 'trim'];
        $rules[] = [['searchId', 'companyId'], 'integer'];
        $rules[] = [['paymentUrl', 'hash', 'companyName', 'redirectPaymentUrlSravniRu'], 'string'];

        return $rules;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'preCalculation' => 'Тип расчета',

            'vehicleBrand' => 'Марка',
            'vehicleModel' => 'Модель',
            'vehiclePower' => 'Мощность',
            'vehicleYear' => 'Год выпуска',
            'vehicleLicensePlate' => 'Гос. номер',
            'vehicleIdentity' => 'Идентифицировать ТС по',
            'vehicleVin' => 'VIN',
            'vehicleBodyNum' => 'Номер кузова',
            'vehicleChassisNum' => 'Номер шасси',
            'vehicleDocType' => 'Документ на авто',
            'vehicleDocSerial' => 'Серия документа',
            'vehicleDocNumber' => 'Номер документа',
            'vehicleDocDate' => 'Дата выдачи документа',

            'driverLicenseSerial' => 'Серия водительского удостоверения',
            'driverLicenseNumber' => 'Номер водительского удостоверения',
            'driverLastName' => 'Фамилия',
            'driverFirstName' => 'Имя',
            'driverMiddleName' => 'Отчество',
            'driverExpDate' => 'Дата выдачи первого водительского удостоверения (если нет точных данных, укажите 31.12.Год выдачи)',
            'driverBirthDate' => 'Дата рождения водителя',
            'driverPrevLicenseSerial' => 'Серия водительского удостоверения',
            'driverPrevLicenseNumber' => 'Номер водительского удостоверения',
            'driverPrevLicenseDate' => 'Дата выдачи водительского удостоверения',
            'driverPrevLastName' => 'Фамилия',
            'driverPrevMiddleName' => 'Отчество',
            'driverPrevFirstName' => 'Имя',


            'ownerCity' => 'Город прописки',
            'ownerDateOSAGOStart' => 'Дата начала действия полиса',
            'ownerDateOSAGOEnd' => 'Дата окнчания действия полиса',
            'ownerPassportDate' => 'Дата выдачи паспорта',
            'ownerPassportSerial' => 'Серия паспорта',
            'ownerPassportNumber' => 'Номер паспорта',
            'ownerLastName' => 'Фамилия',
            'ownerFirstName' => 'Имя',
            'ownerMiddleName' => 'Отчество',
            'ownerPassportIssuedBy' => 'Кем выдан',
            'ownerBirthDate' => 'Дата рождения',
            'ownerDadataAddress' => 'Адрес регистрации',
            'insurerLastName' => 'Фамилия',
            'insurerFirstName' => 'Имя',
            'insurerMiddleName' => 'Отчество',
            'insurerBirthDate' => 'Дата рождения',
            'insurerEmail' => 'Адрес электронной почты',
            'insurerPhone' => 'Телефон',
            'ownerPhone' => 'Телефон',
            'ownerEmail' => 'Адрес электронной почты',
            'insurerDadataAddress' => 'Адрес регистрации',
            'insurerPassportDate' => 'Дата выдачи паспорта',
            'insurerPassportSerial' => 'Серия паспорта',
            'insurerPassportNumber' => 'Номер паспорта',
            'insurerPassportIssuedBy' => 'Кем выдан',
            'insurerIsOwner' => 'Страхователь и владелец автомобиля одно лицо',

            'prevPolicySerial' => 'Серия полиса',
            'prevPolicyNumber' => 'Номер полиса',
            'prevPolicyEndDate' => 'Дата окончания действия полиса',
            'prevPolicyIsset' => 'Есть предыдущий полис',

            'searchId' => 'Идентификатор оплаты в сравни.ру',
            'companyId' => 'ID страховой компании',
            'companyName' => 'Страховая компания',
            'paymentUrl' => 'Ссылка перехода на страницу оплаты на нашем сайте',
            'redirectPaymentUrlSravniRu' => 'URL перехода на оплату',
            'hash' => 'Код оплаты для подтверждения в SMS',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        // Оставляю в гос.номере авто только цифры и русские буквы
        $this->vehicleLicensePlate = !empty($this->vehicleLicensePlate)
            ? preg_replace('/[^а-яё\d]/ui', '', $this->vehicleLicensePlate)
            : $this->vehicleLicensePlate;

        // Оставляю в телефонном номере только цифры
        if (!empty($this->insurerPhone)) {
            $this->insurerPhone = preg_replace("/[^0-9]/", '', $this->insurerPhone);
        }

        // Преобразование дат в формат Y-m-d
        foreach (
            [
                'vehicleDocDate',
                'prevPolicyEndDate',
                'ownerDateOSAGOStart',
                'ownerPassportDate',
                'ownerBirthDate',
                'insurerBirthDate',
                'insurerPassportDate',
            ] as $field) {
            if (empty($this->{$field})) {
                continue;
            }
            if (!is_numeric(strtotime($this->{$field}))) {
                $this->addError($field, 'Неверно указана дата.');
                continue;
            }
            $this->{$field} = \Yii::$app->formatter->asDate($this->{$field}, 'php:Y-m-d');

            // Вычисляю дату окончания действия полиса
            if ($field == 'ownerDateOSAGOStart') {
                $this->ownerDateOSAGOEnd = date('Y-m-d', strtotime('+12 MONTH', strtotime($this->{$field})));
            }
        }

        return parent::beforeValidate();
    }

    /**
     * Тип расчета
     * @return string[]
     */
    public function getTypeCalc()
    {
        return OsagoSravniRuFormDictionary::TYPE_CALC;
    }

    /**
     * Возвращает список марок ТС
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function getBrands()
    {
        return ArrayHelper::map(\common\modules\sravniru\services\CarBrandModelService::getBrands(), 'id', 'name');
    }

    /**
     * Возвращает список моделей ТС
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function getModels()
    {
        return !empty($this->vehicleBrand)
            ? ArrayHelper::map(\common\modules\sravniru\services\CarBrandModelService::getModels($this->vehicleBrand), 'id', 'name')
            : [];
    }

    /**
     * Возвращает список мощности двигателя ТС
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function getEnginePower()
    {
        return !empty($this->vehicleBrand)
            ? ArrayHelper::map(\common\modules\sravniru\services\CarBrandModelService::getEnginePower(
                $this->vehicleBrand, $this->vehicleModel, $this->vehicleYear), 'id', 'name')
            : [];
    }

    /**
     * Тип идентификации ТС (по VIN, кузову, шасси)
     * @return string[]
     */
    public function getTypeCarIdentity()
    {
        return PreCalculationDictionary::CAR_HAVE_LIST;
    }

    /**
     * Типы локументов ТС
     * @return string[]
     */
    public function getTypeCarDocs()
    {
        return FinalCalculationDictionary::CAR_DOCS;
    }

    /**
     * Стразователь и владелец одно лицо
     * @return string[]
     */
    public function getOneFaces()
    {
        return FinalCalculationDictionary::ONE_FACES;
    }
}
