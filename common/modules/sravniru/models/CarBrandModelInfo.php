<?php
namespace common\modules\sravniru\models;

use common\models\Model;
use common\modules\sravniru\dictionaries\CarBrandModelDictionary;

/**
 * Информация об марке автомобиля
 * Class CarInfo
 * @package common\modules\sravniru\models
 * @param int $searchType
 * @param int $brandId
 * @param int $yearFrom
 */
class CarBrandModelInfo extends Model
{
    /**
     * Тип поиска - бренд или модель
     * @var int
     */
    public $searchType;

    /**
     * Год с которгого возвращать бренды ТС
     * @var int
     */
    public $yearFrom;

    /**
     * ID бренда
     * @var int
     */
    public $brandId;

    /**
     * ID модели
     * @var int
     */
    public $modelId;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['typeSearch'], 'required'],
            [['brandId', 'modelId'], 'integer'],
            [['yearFrom'], 'integer', 'min' => 1950],
            [['yearFrom'], 'required', 'when' => function ($model) {
                return $model->searchType == CarBrandModelDictionary::TYPE_SEARCH_BRAND;
            }],
            [['brandId'], 'required', 'when' => function ($model) {
                return $model->searchType == CarBrandModelDictionary::TYPE_SEARCH_MODEL;
            }],
            [['brandId', 'modelId', 'yearFrom'], 'required', 'when' => function ($model) {
                return $model->searchType == CarBrandModelDictionary::TYPE_SEARCH_POWER;
            }],
        ];
    }
}