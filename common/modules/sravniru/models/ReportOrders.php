<?php
namespace common\modules\sravniru\models;

use common\models\Model;

/**
 * Информация об оплате заказа в сравни.ру
 * Class StatusOrder
 * @package common\moduls\sravniru\models
 */
class ReportOrders extends Model
{
    // http://apigateway.svc.master.qa.sravni-team.ru/osago/v1.0/reports/orders?encryptId=yCsQUsqEux5ipkiYjJ9Hbw%3D%3D&partnerId=67&dateFrom=2021-06-07&dateTo=2021-08-09&isAccepted=true&affSub3=50040

    /**
     * Дата заказов с
     * @var string
     */
    public $dateFrom;

    /**
     * Дата заказов по
     * @var string
     */
    public $dateTo;

    /**
     * Возвращать только оплаченные заказы
     * @var bool
     */
    public $isAccepted = true;

    /**
     * Токен заказа
     * @var string
     */
    public $orderToken; // affSub3
    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['orderToken', 'dateTo', 'dateFrom'], 'required'],
            [['orderToken'], 'trim'],
            ['orderToken', 'string'],
            [['dateFrom', 'dateTo'], 'date', 'format' => 'php:Y-m-d'],
            ['isAccepted', 'boolean'],
        ];
    }
}