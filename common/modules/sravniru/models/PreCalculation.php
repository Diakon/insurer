<?php
namespace common\modules\sravniru\models;

use common\modules\sravniru\dictionaries\PreCalculationDictionary;
use common\modules\sravniru\dictionaries\OsagoSravniRuFormDictionary;
use common\models\Model;

/**
 * Предварительный расчет стоимости ОСАГО
 * Class PreCalculation
 * @package common\moduls\sravniru\models
 */
class PreCalculation extends Model
{
    /**
     * бренд ТС
     * @var int
     */
    public $vehicleBrand;

    /**
     * Модель ТС
     * @var int
     */
    public $vehicleModel;

    /**
     * Год выпуска автомобиля
     * @var int
     */
    public $vehicleYear;

    /**
     * мощность двигателя автомобиля
     * @var int
     */
    public $vehiclePower;

    /**
     * гос. номер автомобиля
     * @var string
     */
    public $vehicleLicensePlate;

    /**
     * Тип определения ТС (по VIN, шасси или кузову)
     * @var int
     */
    public $vehicleIdentity;

    /**
     * VIN номер автомобиля.
     * @var string
     */
    public $vehicleVin;

    /**
     * Номер кузова. Обязательный параметр в случае отсутствия VIN
     * @var string
     */
    public $vehicleBodyNum;

    /**
     * Номер шасси. Обязательный параметр в случае отсутствия VIN
     * @var string
     */
    public $vehicleChassisNum;

    /**
     * Мультидрайв
     * @var int
     */
    public $multiDrive;

    /**
     * Дата начала стажа
     * @var array
     */
    public $driverExpDate = [];

    /**
     * Серия водительского удостоверения
     * @var array
     */
    public $driverLicenseSerial = [];

    /**
     * Номер водительского удостоверения
     * @var array
     */
    public $driverLicenseNumber = [];

    /**
     * Дата начала стажа водителя
     * @var array
     */
    public $driversExpDate = [];

    /**
     * Фамилия водителя
     * @var array
     */
    public $driverLastName = [];

    /**
     * Имя водителя
     * @var array
     */
    public $driverFirstName = [];

    /**
     * Отчество водителя
     * @var array
     */
    public $driverMiddleName = [];

    /**
     * Дата рождения водителя
     * @var array
     */
    public $driverBirthDate = [];

    /**
     * Город регистрации владельца автомобиля
     * @var string
     */
    public $ownerCity;

    /**
     * Дата начала действия полиса
     * @var string
     */
    public $ownerDateOSAGOStart;

    /**
     * Серия паспорта собственника
     * @var string
     */
    public $ownerPassportSerial;

    /**
     * Номер паспорта собственника
     * @var string
     */
    public $ownerPassportNumber;

    /**
     * Дата паспорта собственника
     * @var string
     */
    public $ownerPassportDate;

    /**
     * Кем выдан паспорт
     * @var string
     */
    public $ownerPassportIssuedBy;

    /**
     * Фамилия собственника
     * @var string
     */
    public $ownerLastName;

    /**
     * Имя собственника
     * @var string
     */
    public $ownerFirstName;

    /**
     * Отчество собственника
     * @var string
     */
    public $ownerMiddleName;

    /**
     * Дата рождения собственника
     * @var string
     */
    public $ownerBirthDate;

    /**
     * Аддрес собственника
     * @var string
     */
    public $ownerDadataAddress;

    /**
     * Телефон собственника
     * @var integer
     */
    public $ownerPhone;

    /**
     * Емайл собственника
     * @var string
     */
    public $ownerEmail;

    /**
     * Серия предыдущего (если есть) полиса
     * @var string
     */
    public $prevPolicySerial;

    /**
     * Номер предыдущего (если есть) полиса
     * @var string
     */
    public $prevPolicyNumber;

    /**
     * Дата окончания действия (если есть) полиса
     * @var string
     */
    public $prevPolicyEndDate;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [[
                'vehicleBrand',
                'vehicleModel',
                'vehiclePower',
                'vehicleYear',
                'ownerCity',
                'ownerDateOSAGOStart',
                'vehicleIdentity',
            ], 'required'],
            [[
                'vehicleVin',
            ], 'required', 'when' => function ($model) {
                return $model->vehicleIdentity == PreCalculationDictionary::CAR_HAVE_VIN;
            }],
            [[
                'vehicleBodyNum',
            ], 'required', 'when' => function ($model) {
                return $model->vehicleIdentity == PreCalculationDictionary::CAR_HAVE_BODY;
            }],
            [[
                'vehicleChassisNum',
            ], 'required', 'when' => function ($model) {
                return $model->vehicleIdentity == PreCalculationDictionary::CAR_HAVE_CHASSIS;
            }],
            [[
                'driverLicenseSerial',
                'driverLicenseNumber',
                'driverLastName',
                'driverFirstName',
                'driverMiddleName',
                'driverExpDate',
                'driverBirthDate'
            ], 'required', 'when' => function ($model) {
                return $model->multiDrive == OsagoSravniRuFormDictionary::IS_MULTI_DRIVE_NO;
            }],
            [['prevPolicyEndDate', 'ownerDateOSAGOStart'], 'date', 'format' => 'php:Y-m-d'],
            [[
                'multiDrive',
                'vehiclePower',
                'vehicleYear',
                'vehicleModel',
                'vehicleBrand',
                'vehicleIdentity',
            ], 'integer'],
            [['vehicleIdentity'], 'in', 'range' => array_keys(PreCalculationDictionary::CAR_HAVE_LIST)],
            [[
                'driverLicenseSerial',
                'driverLicenseNumber',
                'driversExpDate',
                'driverLastName',
                'driverFirstName',
                'driverMiddleName',
                'driverBirthDate'
            ], 'validateDrivers'],
            [[
                'vehicleVin',
                'vehicleBodyNum',
                'vehicleChassisNum',
                'prevPolicyNumber',
                'prevPolicySerial',
                'ownerCity',
                'vehicleLicensePlate'
            ], 'string'],
            [
                [
                    'ownerPassportSerial',
                    'ownerPassportNumber',
                    'ownerPassportIssuedBy',
                    'ownerLastName',
                    'ownerFirstName',
                    'ownerMiddleName',
                    'ownerDadataAddress',
                ], 'string'],
            ['ownerEmail', 'email'],
            ['ownerPhone', 'match', 'pattern' => "/[^0-9]/"],
            [['ownerPhone'], 'string', 'min' => 10, 'max' => 10],
            [
                [
                    'ownerPassportSerial',
                    'ownerPassportNumber',
                    'ownerPassportIssuedBy',
                    'ownerLastName',
                    'ownerFirstName',
                    'ownerMiddleName',
                    'ownerDadataAddress'
                ], 'trim'],
            [['ownerPassportDate', 'ownerBirthDate'], 'date', 'format' => 'php:Y-m-d'],
        ];
    }

    /**
     * Проверяет массив данных о водителях
     * @param $attribute
     * @param $params
     * @return bool
     */
    public function validateDrivers($attribute, $params)
    {
        if ($this->multiDrive == OsagoSravniRuFormDictionary::IS_MULTI_DRIVE_YES) {
            return true;
        }
        foreach (
            [
                'driverLicenseSerial',
                'driverLicenseNumber',
                'driversExpDate',
                'driverLastName',
                'driverFirstName',
                'driverMiddleName',
                'driverExpDate',
                'driverBirthDate',
            ] as $key) {
            if ($key == $attribute) {
                continue;
            }
            $count = count($this->{$attribute});
            if (
                $count != count($this->driverLicenseSerial) ||
                $count != count($this->driverLicenseNumber) ||
                $count != count($this->driversExpDate) ||
                $count != count($this->driverLastName) ||
                $count != count($this->driverFirstName) ||
                $count != count($this->driverMiddleName)
            ) {
                $this->addError($attribute, "Не для всех водителей(я) заполнены $attribute.");
            }
        }
    }
}