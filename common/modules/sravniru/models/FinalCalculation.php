<?php
namespace common\modules\sravniru\models;

use common\modules\sravniru\dictionaries\FinalCalculationDictionary;
use yii\helpers\ArrayHelper;

/**
 * Class FinalCalculation
 * @package common\modules\sravniru\models
 */
class FinalCalculation extends PreCalculation
{
    /**
     * Тип документа ТС
     * @var int
     */
    public $vehicleDocType;

    /**
     * Серия документа ТС
     * @var string
     */
    public $vehicleDocSerial;

    /**
     * Номер документа ТС
     * @var string
     */
    public $vehicleDocNumber;

    /**
     * дата документа ТС
     * @var string
     */
    public $vehicleDocDate;

    /**
     * серия предыдущих прав
     * @var array
     */
    public $driverPrevLicenseSerial = [];

    /**
     * Массив с флагами о том, какому водителю требуется указывать предыдущие водительские права
     * @var array
     */
    public $driverPrevAddInfo = [];

    /**
     * номер предыдущих прав
     * @var array
     */
    public $driverPrevLicenseNumber = [];

    /**
     * дата выдачи предыдущих прав
     * @var array
     */
    public $driverPrevLicenseDate = [];

    /**
     * Фамилия владельца предыдущих прав
     * @var array
     */
    public $driverPrevLastName = [];

    /**
     * Отчество владельца предыдущих прав
     * @var array
     */
    public $driverPrevMiddleName = [];

    /**
     * Имя владельца предыдущих прав
     * @var array
     */
    public $driverPrevFirstName = [];

    /**
     * Страхователь и владелец одно лицо
     * @var integer
     */
    public $insurerIsOwner;

    /**
     * Фамилия страхователя
     * @var string
     */
    public $insurerLastName;

    /**
     * Имя страхователя
     * @var string
     */
    public $insurerFirstName;

    /**
     * Отчество страхователя
     * @var string
     */
    public $insurerMiddleName;

    /**
     * Дата рождения страхователя
     * @var string
     */
    public $insurerBirthDate;

    /**
     * Телефон страхователя
     * @var integer
     */
    public $insurerPhone;

    /**
     * Емайл страхователя
     * @var string
     */
    public $insurerEmail;

    /**
     * Аддрес страхователя
     * @var string
     */
    public $insurerDadataAddress;

    /**
     * Серия паспорта страхователя
     * @var string
     */
    public $insurerPassportSerial;

    /**
     * Номер паспорта страхователя
     * @var string
     */
    public $insurerPassportNumber;

    /**
     * Дата паспорта страхователя
     * @var string
     */
    public $insurerPassportDate;

    /**
     * Кем выдан пасспорт
     * @var string
     */
    public $insurerPassportIssuedBy;


    /**
     * @return array
     */
    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [[
                    'vehicleLicensePlate',
                    'vehicleDocType',
                    'vehicleDocSerial',
                    'vehicleDocNumber',
                ], 'required'],
                ['insurerEmail', 'email'],
                [
                    [
                        'insurerFirstName',
                        'insurerMiddleName',
                        'insurerPassportSerial',
                        'insurerPassportNumber',
                        'insurerPassportIssuedBy',
                    ], 'string'],
                [[
                    'vehicleDocSerial',
                    'vehicleDocNumber',
                    'insurerLastName',
                    'insurerDadataAddress',
                ], 'string'],
                [['vehicleDocDate', 'insurerBirthDate', 'insurerPassportDate'], 'date', 'format' => 'php:Y-m-d'],
                [[
                    'vehicleDocType'
                ], 'integer'],
                [['vehicleDocType'], 'in', 'range' => array_keys(FinalCalculationDictionary::CAR_DOCS)],
                ['insurerPhone', 'match', 'pattern' => "/[^0-9]/"],
                [['insurerPhone'], 'string', 'min' => 10, 'max' => 10],
                [
                    [
                        'driverPrevLicenseSerial',
                        'driverPrevLicenseNumber',
                        'driverPrevLicenseDate',
                        'driverPrevLastName',
                        'driverPrevMiddleName',
                        'driverPrevFirstName',
                        'driverPrevAddInfo'
                    ], 'safe'],
                [['insurerIsOwner'], 'in', 'range' => array_keys(FinalCalculationDictionary::ONE_FACES)],
            ]);
    }
}