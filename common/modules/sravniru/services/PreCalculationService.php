<?php
namespace common\modules\sravniru\services;

use common\modules\sravniru\dictionaries\OsagoSravniRuFormDictionary;
use common\modules\sravniru\interfaces\AuthInterface;
use common\modules\sravniru\models\FinalCalculation;
use common\modules\sravniru\models\PreCalculation;
use common\modules\sravniru\traits\RequestApiTrait;
use yii\httpclient\Client;

/**
 * Class PreCalculationService
 * @package common\modules\sravniru\services
 */
class PreCalculationService extends ApiBaseService implements AuthInterface
{
    use RequestApiTrait;

    /**
     * @var $model PreCalculation|FinalCalculation
     */
    protected $model;

    protected $tokenOrder;

    /**
     * @param PreCalculation|FinalCalculation $model
     * @return mixed|void
     */
    public function __construct($model)
    {
        $this->model = $model;
    }

    /**
     * Возвращает URL sravni.ru для отправки запроса
     * @return string
     */
    public function getApiUrl()
    {
        return $this->host . 'osago/v1/calculate';
    }

    /**
     * Метод возвращающий данные для отправки в sravni.ru. В $tokenOrder передать токен заказа
     * @param false $returnAsJson
     * @return array|false|mixed|string
     * @throws \yii\base\InvalidConfigException
     */
    public function getApiData($returnAsJson = false)
    {
        $requestData = [];
        $requestData['brandId'] = (int)$this->model->vehicleBrand;
        $requestData['modelId'] = (int)$this->model->vehicleModel;
        $requestData['year'] = (int)$this->model->vehicleYear;
        $requestData['enginePower'] = (int)$this->model->vehiclePower;
        $requestData['getting'] = $requestData['registration'] = $this->model->ownerCity;
        $requestData['policyStartDate'] = \Yii::$app->formatter->asDate($this->model->ownerDateOSAGOStart, 'php:Y-m-d');
        if (!empty($this->model->vehicleLicensePlate)) {
            $requestData['carNumber'] = (string)$this->model->vehicleLicensePlate;
        }
        $isMultiDrive = !empty($this->model->multiDrive) ? OsagoSravniRuFormDictionary::IS_MULTI_DRIVE_YES : OsagoSravniRuFormDictionary::IS_MULTI_DRIVE_NO;
        $requestData['driversInfo'] = [];
        $requestData['driversInfo']['driverAmount'] = $isMultiDrive;
        if ($isMultiDrive == OsagoSravniRuFormDictionary::IS_MULTI_DRIVE_NO) {
            $drivers = [];
            foreach ($this->model->driverLastName as $key => $value) {
                $drivers[] = [
                    'lastName' => $value,
                    'middleName' => $this->model->driverMiddleName[$key] ?? "",
                    'firstName' => $this->model->driverFirstName[$key] ?? "",
                    'birthDate' => \Yii::$app->formatter->asDate($this->model->driverBirthDate[$key], 'php:Y-m-d'),
                    'license' => [
                        'series' => $this->model->driverLicenseSerial[$key] ?? null,
                        'number' => $this->model->driverLicenseNumber[$key] ?? null,
                        'date' => !empty($this->model->driverExpDate[$key]) ? \Yii::$app->formatter->asDate($this->model->driverExpDate[$key], 'php:Y-m-d') : null,
                    ],
                    // Предыдущее водительское удостоверение
                    'previousInfo' => !empty($this->model->driverPrevAddInfo[$key]) ? [
                        'lastName' => $this->model->driverPrevLastName[$key] ?? "",
                        'middleName' => $this->model->driverPrevMiddleName[$key] ?? "",
                        'firstName' => $this->model->driverPrevFirstName[$key] ?? "",
                        'date' => !empty($this->model->driverPrevLicenseDate[$key]) ? \Yii::$app->formatter->asDate($this->model->driverPrevLicenseDate[$key], 'php:Y-m-d') : null,
                        'number' => $this->model->driverPrevLicenseNumber[$key] ?? null,
                        'series' => $this->model->driverPrevLicenseSerial[$key] ?? null,
                    ] : null,
                ];
            }
            $requestData['driversInfo']['drivers'] = array_values($drivers);
        }
        if (!empty($this->model->vehicleVin)) {$requestData['vin'] = $this->model->vehicleVin;}
        if (!empty($this->model->vehicleBodyNum)) {$requestData['bodyNumber'] = $this->model->vehicleBodyNum;}
        if (!empty($this->model->vehicleChassisNum)) {$requestData['chassisNumber'] = $this->model->vehicleChassisNum;}

        // Для полного расчета
        if ($isMultiDrive == OsagoSravniRuFormDictionary::IS_MULTI_DRIVE_YES || $this->model::classNameShort() == FinalCalculation::classNameShort()) {
            // Данные о владельце ТС
            $requestData['owner'] = [
                'lastName' => $this->model->ownerLastName ?? "",
                'firstName' => $this->model->ownerFirstName ?? "",
                'middleName' => $this->model->ownerMiddleName ?? "",
                'birthDate' => !empty($this->model->ownerBirthDate) ? \Yii::$app->formatter->asDate($this->model->ownerBirthDate, 'php:Y-m-d') : null,
                'phone' => !empty($this->model->ownerPhone) ? preg_replace("/[^0-9]/", '', $this->model->ownerPhone) : null,
                'email' => $this->model->ownerEmail ?? "",
                'registrationAddress' => $this->model->ownerDadataAddress ?? "",
                // Паспорт
                'passport' => [
                    'series' => !empty($this->model->ownerPassportSerial) ? preg_replace("/[^0-9]/", '', $this->model->ownerPassportSerial) : "",
                    'number' => !empty($this->model->ownerPassportNumber) ? preg_replace("/[^0-9]/", '', $this->model->ownerPassportNumber) : "",
                    'issueDate' => !empty($this->model->ownerPassportDate) ? \Yii::$app->formatter->asDate($this->model->ownerPassportDate, 'php:Y-m-d') : null,
                    'issuedBy' => $this->model->ownerPassportIssuedBy ?? "",
                ],
            ];
        }
        if ($this->model::classNameShort() == FinalCalculation::classNameShort()) {
            // Данные о страхователе ТС
            $insurerLastName = !empty($this->model->insurerIsOwner) ? $this->model->ownerLastName : $this->model->insurerLastName;
            $insurerFirstName = !empty($this->model->insurerIsOwner) ? $this->model->ownerFirstName : $this->model->insurerFirstName;
            $insurerMiddleName = !empty($this->model->insurerIsOwner) ? $this->model->ownerMiddleName : $this->model->insurerMiddleName;
            $insurerBirthDate = !empty($this->model->insurerIsOwner)
                ? (!empty($this->model->ownerBirthDate) ? \Yii::$app->formatter->asDate($this->model->ownerBirthDate, 'php:Y-m-d') : null)
                : (!empty($this->model->insurerBirthDate) ? \Yii::$app->formatter->asDate($this->model->insurerBirthDate, 'php:Y-m-d') : null);
            $insurerRegistrationAddress = !empty($this->model->insurerIsOwner) ? $this->model->ownerDadataAddress : $this->model->insurerDadataAddress;
            $insurerPassportSeries = !empty($this->model->insurerIsOwner)
                ? (!empty($this->model->ownerPassportSerial) ? preg_replace("/[^0-9]/", '', $this->model->ownerPassportSerial) : "")
                : (!empty($this->model->insurerPassportSerial) ? preg_replace("/[^0-9]/", '', $this->model->insurerPassportSerial) : "");
            $insurerPassportNumber = !empty($this->model->insurerIsOwner)
                ? (!empty($this->model->ownerPassportNumber) ? preg_replace("/[^0-9]/", '', $this->model->ownerPassportNumber) : "")
                : (!empty($this->model->insurerPassportNumber) ? preg_replace("/[^0-9]/", '', $this->model->insurerPassportNumber) : "");
            $insurerPassportIssueDate = !empty($this->model->insurerIsOwner)
                ? (!empty($this->model->ownerPassportDate) ? \Yii::$app->formatter->asDate($this->model->ownerPassportDate, 'php:Y-m-d') : null)
                : (!empty($this->model->insurerPassportDate) ? \Yii::$app->formatter->asDate($this->model->insurerPassportDate, 'php:Y-m-d') : null);
            $insurerPassportIssuedBy = !empty($this->model->insurerIsOwner) ? $this->model->ownerPassportIssuedBy : $this->model->insurerPassportIssuedBy;
            $phone = !empty($this->model->insurerIsOwner)
                ? (!empty($this->model->ownerPhone) ? preg_replace("/[^0-9]/", '', $this->model->ownerPhone) : null)
                : (!empty($this->model->insurerPhone) ? preg_replace("/[^0-9]/", '', $this->model->insurerPhone) : null);
            $email = !empty($this->model->insurerIsOwner)
                ? (!empty($this->model->ownerEmail) ? trim($this->model->ownerEmail) : "")
                : (!empty($this->model->insurerEmail) ? trim($this->model->insurerEmail) : "");

            $requestData['insurer'] = [
                'lastName' => $insurerLastName ?? "",
                'firstName' => $insurerFirstName ?? "",
                'middleName' => $insurerMiddleName ?? "",
                'birthDate' => $insurerBirthDate,
                'phone' => $phone,
                'email' => $email,
                'registrationAddress' => $insurerRegistrationAddress ?? "",
                // Паспорт
                'passport' => [
                    'series' => $insurerPassportSeries,
                    'number' => $insurerPassportNumber,
                    'issueDate' => $insurerPassportIssueDate,
                    'issuedBy' => $insurerPassportIssuedBy,
                ],
            ];

            // Документы на авто
            $requestData['carDocument'] = [];
            $requestData['carDocument']['series'] = (string)$this->model->vehicleDocSerial;
            $requestData['carDocument']['number'] = (string)$this->model->vehicleDocNumber;
            $requestData['carDocument']['date'] = !empty($this->model->vehicleDocDate) ? \Yii::$app->formatter->asDate($this->model->vehicleDocDate, 'php:Y-m-d') : null;
            $requestData['carDocument']['documentType'] = (int)$this->model->vehicleDocType;
        }

        $requestData['partner'] = [
            'affSub3' => $this->tokenOrder,
            'affSub4' => null,
            'affSub5' => null,
            'medium' => 'api',
            'source' => 'offagent.5652663',
            'campaign' => 'api_osago',
        ];
        $requestData['platform'] = 'api';
        $requestData['usageMonthsPerYear'] = 10;

        return $returnAsJson ? json_encode($requestData, JSON_UNESCAPED_UNICODE) : $requestData;
    }

    /**
     * @return mixed|string
     */
    public function getApiCurlFormat()
    {
        return Client::FORMAT_RAW_URLENCODED;
    }

    /**
     * @return string
     */
    public function getApiCurlMethod()
    {
        return 'post';
    }

    /**
     * Запрашивает расчет по ID расчета (ID в sravni.ru)
     * @param $id
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function getDetailCalculationById($id)
    {
        $url = $this->getApiUrl() . '/' . $id;
        $client = new Client();
        $response = $client->createRequest()
            ->setFormat($this->getApiCurlFormat())
            ->setMethod('get')
            ->setUrl($url)
            ->send();

        return ['success' => $response->isOk, 'response' => $response->data];
    }

    /**
     * Возвращает результат предварительного расчета
     * @param string $tokenOrder
     * @param null  $id ID произведенного ранее расчтета в сравни.ру
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function getCalculateResult(string $tokenOrder, $id = null)
    {
        $this->tokenOrder = $tokenOrder;
        // Нет ID расчета в sravni.ru - формируем новый
        if (empty($id)) {
            $response = $this->request(
                $this->getApiUrl(),
                $this->getApiData(),
                $this->getApiCurlFormat(),
                $this->getApiCurlMethod()
            );

            $status = $response->isOk;
            $response = $response->data;
            if (!$status) {
                return ['success' => $status, 'response' => $response];
            }
            $id = $response;
        }

        // Получаю по ID расчета данные пока isCompleted == false каждые 5 сек (пробую максимум 1 минуту)
        for ($i = 0; $i < 15; ++$i) {
            $result = $this->getDetailCalculationById($id);
            if (!empty($result['response']['isCompleted'])) {
                break;
            }
            sleep(5);
        }

        return !empty($result) ? $result : [];
    }
}