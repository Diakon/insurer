<?php
namespace common\modules\sravniru\services;

use common\modules\orders\models\Order;
use common\modules\sravniru\models\ReportOrders;
use common\modules\sravniru\traits\RequestApiTrait;
use yii\httpclient\Client;
use common\modules\sravniru\interfaces\ApiInterface;

/**
 * Сервис получения данных о заказе в сравни.ру
 * Class CarNumberService
 * @package common\modules\sravniru\services
 */
class ReportOrdersService extends ApiBaseService implements ApiInterface
{
    use RequestApiTrait;

    /**
     * @var $model ReportOrders
     */
    protected $model;


    /**
     * @param ReportOrders $model
     * @return mixed|void
     */
    public function __construct(ReportOrders $model)
    {
        $this->model = $model;
    }

    /**
     * Возвращает URL для отправки запроса
     * @return string
     */
    public function getApiUrl()
    {
        return $this->host . 'osago/v1.0/reports/orders';
    }

    /**
     * @return mixed|string
     */
    public function getApiCurlFormat()
    {
        return Client::FORMAT_URLENCODED;
    }

    /**
     * @return string
     */
    public function getApiCurlMethod()
    {
        return 'get';
    }

    /**
     * Метод возвращающий данные для отправки в АПИ sravni.ru
     * @return array
     */
    public function getApiData()
    {
        $params = \Yii::$app->params;
        $requestData = [];
        $requestData['affSub3'] = $this->model->orderToken;
        $requestData['isAccepted'] = $this->model->isAccepted ? "true" : "false";
        if (!empty($this->model->dateFrom)) {
            $requestData['dateFrom'] = $this->model->dateFrom;
        }
        if (!empty($this->model->dateTo)) {
            $requestData['dateTo'] = $this->model->dateTo;
        }
        $requestData['encryptId'] = $params['encryptIdSravniRu'];
        $requestData['partnerId'] = $params['partnerIdSravniRu'];

        return $requestData;
    }

    /**
     * Возвращает данные о статусе заказа в сравни.ру по токену заказа
     * @param Order $order
     * @param $dateFrom
     * @param $dateTo
     * @param bool $onlySuccessPayment
     * @return array
     */
    public static function getOrderStatusInfo(Order $order, $dateFrom, $dateTo, bool $onlySuccessPayment = true)
    {
        $model = new ReportOrders();
        $model->orderToken = $order->token;
        $model->isAccepted = $onlySuccessPayment;
        $model->dateFrom = $dateFrom;
        $model->dateTo = $dateTo;
        if (!$model->validate()) {
            return ['status' => false, 'response' => []];
        }

        $service = new self($model);

        $params = $service->getApiData();
        $url = $service->getApiUrl() . '?';
        foreach ($params as $key => $value) {
            $url .= "&$key=$value";
        }
        $url = str_replace("?&", "?", $url);

        $response = $service->request(
            $url,
            $params,
            $service->getApiCurlFormat(),
            $service->getApiCurlMethod()
        );

        $status = $response->isOk;
        $response = $response->data ?? [];
        if (empty($response)) {
            $status = false;
        }

        return ['status' => $status, 'response' => $response ?? []];
    }
}