<?php
namespace common\modules\sravniru\services;

use common\modules\cars\dictionaries\CarDictionary;
use common\modules\cars\models\Car;
use common\modules\cars\services\CarService;
use common\modules\sravniru\models\CarNumber;
use common\modules\sravniru\traits\RequestApiTrait;
use yii\base\BaseObject;
use yii\httpclient\Client;
use common\modules\sravniru\interfaces\ApiInterface;

/**
 * Сервис получения данных об авто по гос номеру
 * Class CarNumberService
 * @package common\modules\sravniru\services
 */
class CarNumberService extends ApiBaseService implements ApiInterface
{
    use RequestApiTrait;

    /**
     * @var $model CarNumber
     */
    private $model;

    /**
     * @param CarNumber $model
     * @return mixed|void
     */
    public function __construct($model)
    {
        $this->model = $model;
    }

    /**
     * Возвращает URL для отправки запроса
     * @return string
     */
    public function getApiUrl()
    {
        return $this->host . 'prt/v1/autoinfo/number';
    }

    /**
     * @return mixed|string
     */
    public function getApiCurlFormat()
    {
        return Client::FORMAT_URLENCODED;
    }

    /**
     * @return string
     */
    public function getApiCurlMethod()
    {
        return 'get';
    }

    /**
     * Метод возвращающий данные для отправки в АПИ sravni.ru
     * @return array
     */
    public function getApiData()
    {
        $requestData = [];
        $requestData['number'] = trim($this->model->vehicleLicensePlate);

        return $requestData;
    }

    /**
     * Сохраняет данные об автомобиле полученные из АПИ
     * @param array $response
     * @return Car
     */
    public function setCarModelByApi(array $response)
    {
        $model = new Car();
        $response = $response['result'] ?? null;
        if (empty($response)) {
            return $model;
        }
        $category = $response['category'] ?? null;
        $category = array_search($category, CarDictionary::CATEGORY_CARS);

        $docs = $response['carDocument'] ?? [];

        $model->vin = $response['vin'] ?? null;
        $model->body_num = $response['bodyNumber'] ?? null;
        $model->chassis_num = $response['chassisNumber'] ?? null;
        $model->year = $response['year'] ?? null;
        $model->power = $response['power'] ?? null;
        $model->category = $category;
        $model->doc_type = CarDictionary::TYPES_DOCUMENTS_BY_ALIAS[$docs['documentType'] ?? null] ?? null;
        $model->doc_serial = $docs['series'] ?? null;
        $model->doc_number = $docs['number'] ?? null;
        $model->doc_date = null;
        $model->brand = $response['brand']['name'] ?? null;
        $model->brand_id = $response['brand']['id'] ?? null;
        $model->model = $response['model']['name'] ?? null;
        $model->model_id = $response['model']['id'] ?? null;
        $model->type = null;
        $model->model_brand_doc = null;
        $model->diag_card_number = null;
        $model->diag_card_date = null;
        $model->diag_card_issue_date = null;

        return $model;
    }

    /**
     * Метод возвращает данные об ТС по гос. номеру из сервиса сравни.ру
     * @param string $carNumber
     * @return array|Car
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public static function getCarDataFromApi(string $carNumber)
    {
        $model = new CarNumber();
        $model->vehicleLicensePlate = $carNumber;
        if (!$model->validate()) {
            return [];
        }
        $service = new self($model);
        $url = $service->getApiUrl() . "/" . urlencode($model->vehicleLicensePlate);
        $response = $service->request(
            $url,
            $service->getApiData(),
            $service->getApiCurlFormat(),
            $service->getApiCurlMethod()
        );
        if ($response->isOk) {
            /**
             * @var $car Car
             */
            return $service->setCarModelByApi($response->data);
        }

        return [];
    }
}