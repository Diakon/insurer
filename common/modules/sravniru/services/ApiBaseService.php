<?php
namespace common\modules\sravniru\services;

use common\modules\sravniru\dictionaries\ApiBaseDictionary;
use Yii;
use common\models\Service;

/**
 * Class ApiBaseService
 * @package common\modules\sravniru\services
 * @param string $host
 */
class ApiBaseService extends Service
{
    /**
     * Возвращает арес сервера куда следует слать запросы в зависимости от настройки
     * @return string
     */
    public function getHost()
    {
        $isTestApiServer = !empty(Yii::$app->params['isTestApiSravniRu']);
        return $isTestApiServer ? ApiBaseDictionary::TEST_HOST : ApiBaseDictionary::PROD_HOST;
    }

}