<?php
namespace common\modules\sravniru\services;

use common\modules\sravniru\models\FinalCalculation;

/**
 * Class FinalCalculationService
 * @package common\modules\sravniru\services
 */
class FinalCalculationService extends PreCalculationService
{
    /**
     * Возвращает URL sravni.ru для отправки запроса
     * @return string
     */
    public function getApiUrl()
    {
        return $this->host . 'osago/v1/calcthroughorders';
    }

    /**
     * Метод возвращающий данные для отправки в sravni.ru. В $tokenOrder передать токен заказа
     * @param false $returnAsJson
     * @return array|false|mixed|string
     * @throws \yii\base\InvalidConfigException
     */
    public function getApiData($returnAsJson = false)
    {
        /**
         * @var $model FinalCalculation
         */
        return parent::getApiData($returnAsJson);
    }
}