<?php
namespace common\modules\sravniru\services;

use frontend\modules\orders\traits\OrderTrait;
use Yii;
use common\modules\sravniru\dictionaries\CarBrandModelDictionary;
use common\modules\sravniru\models\CarBrandModelInfo;
use common\modules\sravniru\interfaces\ApiInterface;
use yii\httpclient\Client;

/**
 * Сервис для получении данных об авто из сравни.ру
 * Class AuthService
 * @package common\modules\sravniru\services
 */
class CarBrandModelService extends ApiBaseService implements ApiInterface
{
    use OrderTrait;

    /**
     * @var $model CarBrandModelInfo
     */
    private $model;

    /**
     * @param CarBrandModelInfo $model
     * @return mixed|void
     */
    public function __construct($model)
    {
        $this->model = $model;
    }

    /**
     * Возвращает URL для отправки запроса
     * @return string
     */
    public function getApiUrl()
    {
        switch ($this->model->searchType) {
            case CarBrandModelDictionary::TYPE_SEARCH_MODEL:
                return $this->host . 'auto/v1/brand/' . $this->model->brandId . '/models';
            case CarBrandModelDictionary::TYPE_SEARCH_POWER:
                return $this->host . 'auto/v1/brand/' . $this->model->brandId . '/years/' . $this->model->yearFrom . '/models/' .  $this->model->modelId . '/engine-powers';
            default:
                return $this->host . 'auto/v1/brands';
        }
    }

    /**
     * @return mixed|string
     */
    public function getApiCurlFormat()
    {
        return Client::FORMAT_RAW_URLENCODED;
    }

    /**
     * @return string
     */
    public function getApiCurlMethod()
    {
        return 'get';
    }

    /**
     * Метод возвращающий данные для отправки в АПИ sravni.ru
     * @return array
     */
    public function getApiData()
    {
        $requestData = [];
        if (!empty($this->model->yearFrom)) {
            $requestData[$this->model->searchType == CarBrandModelDictionary::TYPE_SEARCH_MODEL ? 'year' : 'yearFrom'] = (int)$this->model->yearFrom;
        }
        if (!empty($this->model->brandId)) {
            $requestData['brandId'] = $this->model->brandId;
        }
        if (!empty($this->model->modelId)) {
            $requestData['modelId'] = $this->model->modelId;
        }

        return $requestData;
    }

    /**
     * Возвращает бренды авто
     * @param int $year
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public static function getBrands(int $year = 1950)
    {
        $cacheKey = 'api-sraviru-car-brands-list';
        // Если токен есть в кеше - беру от туда
        $brandsList = Yii::$app->cache->get($cacheKey);
        if (empty($brandsList)) {
            $model = new CarBrandModelInfo();
            $model->searchType = CarBrandModelDictionary::TYPE_SEARCH_BRAND;
            $model->yearFrom = $year;
            $service = new self($model);
            $client = new Client();
            $response = $client->createRequest()
                ->setFormat($service->getApiCurlFormat())
                ->setMethod($service->getApiCurlMethod())
                ->setUrl($service->getApiUrl())
                ->setData($service->getApiData())
                ->send();
            if ($response->isOk) {
                $brandsList = $response->data;
                // Пишу в кеш что бы не делать постоянно запросы
                Yii::$app->cache->set($cacheKey, $brandsList, 3 * 86400);  // Время жизни кеша 3 деня
            }
        }

        return $brandsList;
    }

    /**
     * Возвращает модельный ряд бренда ($brandId - ID бренда в сраыни.ру)
     * @param int $brandId
     * @param int|null $year
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public static function getModels(int $brandId, int $year = null)
    {
        $cacheKey = "api-sraviru-car-brands-id-$brandId-year-" . ((int)$year) . "-models-list";
        // Если токен есть в кеше - беру от туда
        $modelsList = Yii::$app->cache->get($cacheKey);
        if (empty($modelsList)) {
            $model = new CarBrandModelInfo();
            $model->brandId = $brandId;
            $model->searchType = CarBrandModelDictionary::TYPE_SEARCH_MODEL;
            if (!empty($year)) {
                $model->yearFrom = $year;
            }
            $service = new self($model);
            $client = new Client();
            $response = $client->createRequest()
                ->setFormat($service->getApiCurlFormat())
                ->setMethod($service->getApiCurlMethod())
                ->setUrl($service->getApiUrl())
                ->setData($service->getApiData())
                ->send();
            if ($response->isOk) {
                $modelsList = $response->data;
                // Пишу в кеш что бы не делать постоянно запросы
                Yii::$app->cache->set($cacheKey, $modelsList, 3 * 86400);  // Время жизни кеша 3 деня
            }
        }

        return $modelsList;
    }

    /**
     * Мощность двигателя (список)
     * @param int $brandId
     * @param int $modelId
     * @param int $year
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public static function getEnginePower(int $brandId, int $modelId, int $year)
    {
        $cacheKey = "api-sraviru-car-brands-id-$brandId-year-$year-model-id-$modelId-engine-power";
        // Если токен есть в кеше - беру от туда
        $enginePowerList = Yii::$app->cache->get($cacheKey);
        if (empty($enginePowerList)) {
            $model = new CarBrandModelInfo();
            $model->brandId = $brandId;
            $model->yearFrom = $year;
            $model->modelId = $modelId;
            $model->searchType = CarBrandModelDictionary::TYPE_SEARCH_POWER;
            $service = new self($model);
            $client = new Client();
            $response = $client->createRequest()
                ->setFormat($service->getApiCurlFormat())
                ->setMethod($service->getApiCurlMethod())
                ->setUrl($service->getApiUrl())
                ->setData($service->getApiData())
                ->send();
            if ($response->isOk) {
                $enginePowerList = $response->data;
                // Пишу в кеш что бы не делать постоянно запросы
                Yii::$app->cache->set($cacheKey, $enginePowerList, 3 * 86400);  // Время жизни кеша 3 деня
            }
        }

        return $enginePowerList;
    }
}
