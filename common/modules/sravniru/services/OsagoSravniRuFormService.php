<?php
namespace common\modules\sravniru\services;

use common\modules\sravniru\dictionaries\OsagoSravniRuFormDictionary;
use Yii;
use common\models\Service;

/**
 * Class OsagoSravniRuFormService
 * @package common\modules\sravniru\services
 */
class OsagoSravniRuFormService extends Service
{
    /**
     * Сохраняет в сессию предложения от страховых компаний, что бы после выбора предложения взять от туда цену, комиссию и тд
     * @param array $requestResultsData
     */
    public static function setSessionInsureData(int $searchId, array $requestResultsData)
    {
        foreach ($requestResultsData as $orderResult) {
            $hash = $orderResult['hash'] ?? null;
            $companyId = $orderResult['companyId'] ?? null;
            $companyName = $orderResult['companyName'] ?? null;
            $price = $orderResult['price'] ?? 0;
            $commission = $orderResult['partnerProfit']['profit'] ?? 0;
            $paymentUrl = $orderResult['paymentUrl'] ?? null;

            $session = [
                'hash' => $hash,
                'companyId' => $companyId,
                'companyName' => $companyName,
                'price' => $price,
                'commission' => $commission,
                'paymentUrl' => $paymentUrl
            ];

            Yii::$app->session->set(self::getSessionInsureName($searchId, $companyId), $session);
        }
    }

    /**
     * Возвращает данные из сессии
     * @param int $searchId
     * @param string $companyId
     * @return mixed
     */
    public static function getSessionInsureData(int $searchId, string $companyId)
    {
        $session = self::getSessionInsureName($searchId, $companyId);
        return Yii::$app->session->get($session);
    }

    /**
     * Возвращает имя для сесии записи информации от страховых компаний
     * @param int $searchId
     * @param string $companyId
     * @return string
     */
    private static function getSessionInsureName(int $searchId, string $companyId)
    {
        return OsagoSravniRuFormDictionary::SESSION_INSURE_DATA_NAME . '-' . $searchId . $companyId;
    }
}