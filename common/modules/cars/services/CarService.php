<?php
namespace common\modules\cars\services;

use common\modules\cars\dictionaries\CarDictionary;
use common\modules\cars\models\Car;
use common\models\Service;
use common\modules\inguru\services\CarInfoService;
use common\modules\sravniru\services\CarNumberService;

/**
 * Class CarService
 * @package common\modules\cars\services
 */
class CarService extends Service
{
    /**
     * Возвращает модель авто по VIN
     *
     * @param $vin
     * @return array|\yii\db\ActiveRecord|null
     */
    public function getByVin($vin)
    {
        return Car::find()->where(['like', 'vin', trim($vin), false])->one();
    }

    /**
     * Возвращает модель авто по номеру
     * @param string $number
     * @param int $sourceId
     * @return array|\yii\db\ActiveRecord|null
     */
    public function getByNumber(string $number)
    {
        return Car::find()->where(['like', 'number', trim($number), false])->one();
    }

    /**
     * Сохраняет модель автомобиля
     *
     * @param Car $model
     * @param bool $validate
     * @return bool
     */
    public function save(Car $model, bool $validate = true)
    {
        $transaction = \Yii::$app->db->beginTransaction();

        // Удаляю ранее созданную (если есть) запись об этом авто для создания новой
        Car::deleteAll(['number' => $model->number]);
        if ($model->save($validate)) {
            $transaction->commit();
            return true;
        }
        $transaction->rollBack();

        return false;
    }

    /**
     * Ищет в таблице car указанный номер авто полученный от указанного в $sourceId сервиса (ИНГУРУ, сравни.ру и тд).
     * Если такой записи нет - пытается получить через АПИ сервиса заданного в $sourceId
     * @param string $carNumber
     * @param int $sourceId
     * @return array|Car
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public static function getCar(string $carNumber, int $sourceId)
    {
        $carNumber = trim($carNumber);
        // Если номер авто есть в БД номеров - беру оттуда
        $carService = new self();
        /**
         * @var $car Car
         */
        $car = $carService->getByNumber($carNumber);
        if (!empty($car) && $car->source_id == $sourceId) {
            return $car;
        }

        $car = [];
        // Номера авто нет в БД номеров или он имеет другой источник(получен, например, из ИНГУРУ) - тяну по указаному сервису
        switch ($sourceId) {
            // Тянем данные о авто из ИНГУРУ
            case CarDictionary::SOURCE_INGURU:
                $car = CarInfoService::getCarDataFromApi($carNumber);
                break;
            // Тянем данные о авто из сравни.ру
            case CarDictionary::SOURCE_SRAVNIRU:
                $car = CarNumberService::getCarDataFromApi($carNumber);
                break;
        }

        if (empty($car)) {
            return [];
        }

        $car->source_id = $sourceId;
        $car->number = trim($carNumber);
        // Обновляю / создаю запись об авто в таблице car
        if (!$carService->save($car)) {
            $car = [];
        }

        return $car;
    }
}