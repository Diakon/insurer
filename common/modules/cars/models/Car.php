<?php
namespace common\modules\cars\models;

use common\models\ActiveRecordModel;
use common\modules\cars\dictionaries\CarDictionary;
use common\modules\orders\models\CarProperty;
use common\modules\orders\models\DriverProperty;
use common\modules\orders\models\OrderProperty;
use common\modules\users\models\User;
use yii\behaviors\TimestampBehavior;

/**
 * Class Car
 * @package common\modules\cars\models\Car
 *
 * @property integer $id
 * @property string $number
 * @property string $vin
 * @property string $body_num
 * @property string $chassis_num
 * @property integer $power
 * @property integer $category
 * @property integer $year
 * @property integer $type
 * @property string $model
 * @property integer $model_id
 * @property string $brand
 * @property integer $brand_id
 * @property string $model_brand_doc
 * @property string $diag_card_number
 * @property string $diag_card_date
 * @property string $diag_card_issue_date
 * @property integer $doc_type
 * @property string $doc_serial
 * @property string $doc_number
 * @property string $doc_date
 * @property integer $updated_at
 * @property integer $created_at
 * @property integer $source_id
 *
 */
class Car extends ActiveRecordModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cars';
    }

    /**
     * Define rules for validation
     */
    public function rules()
    {
        return [
            [['number', 'vin', 'source_id'], 'required'],
            [['number'], 'string', 'max' => 20],
            [['number', 'vin'], 'unique'],
            [['vin', 'body_num', 'chassis_num', 'model', 'brand', 'diag_card_number', 'doc_serial', 'doc_number', 'model_brand_doc'], 'string', 'max' => 100],
            [['vin', 'body_num', 'chassis_num', 'model', 'brand', 'diag_card_number', 'doc_serial', 'doc_number', 'model_brand_doc'], 'trim'],
            [['id', 'power', 'category', 'type', 'doc_type', 'year', 'source_id', 'model_id', 'brand_id', 'updated_at', 'created_at'], 'integer'],
            [['diag_card_date', 'diag_card_issue_date', 'doc_date'], 'date', 'format' => 'php:Y-m-d'],
            [['source_id'], 'in', 'range' => array_keys(CarDictionary::TYPES_SOURCE)],
            [['type'], 'in', 'range' => array_keys(CarDictionary::TYPES_CARS)],
            [['doc_type'], 'in', 'range' => array_keys(CarDictionary::TYPES_DOCUMENTS)],
            [['category'], 'in', 'range' => array_keys(CarDictionary::CATEGORY_CARS)],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::class,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'number' => 'Гос. номер',
            'vin' => 'VIN',
            'body_num' => 'Номер кузова автомобиля',
            'chassis_num' => 'Номер шасси автомобиля',
            'year' => 'Год выпуска',
            'power' => 'Мощность автомобиля в лошадиных силах',
            'category' => 'Категория автомобиля',
            'type' => 'Тип автомобиля',
            'model' => 'Модель автомобиля',
            'model_id' => 'ID модели автомобиля в системе сервиса предоставившего данные',
            'brand' => 'Марка автомобиля',
            'brand_id' => 'ID марки автомобиля в системе сервиса предоставившего данные',
            'model_brand_doc' => 'Марка и модель автомобиля как записано в ПТС/СТС',
            'diag_card_number' => 'Номер диагностической карты',
            'diag_card_date' => 'Срок действия диагностической карты ',
            'diag_card_issue_date' => 'Дата выдачи диагностической карты в формате ',
            'doc_type' => 'Тип документа на автомобиль',
            'doc_serial' => 'Серия документа на автомобиль',
            'doc_number' => 'Номер документа на автомобиль',
            'doc_date' => 'Дата выдачи документа на автомобиль',
            'updated_at' => 'Дата редактирования',
            'created_at' => 'Дата создания',
            'source_id' => 'Сервис от которого были получены данные',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperties()
    {
        return $this->hasMany(OrderProperty::class, ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarProperty()
    {
        return $this->hasMany(CarProperty::class, ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDriverProperty()
    {
        return $this->hasMany(DriverProperty::class, ['order_id' => 'id']);
    }
}