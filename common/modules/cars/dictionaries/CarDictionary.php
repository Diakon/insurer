<?php

namespace common\modules\cars\dictionaries;

/**
 * Class CarDictionary
 *
 * @package common\modules\cars\dictionaries
 */
class CarDictionary
{
    /**
     * Из каких сервисов была получена запись об авто
     */
    public const SOURCE_INGURU = 1;
    public const SOURCE_SRAVNIRU = 2;
    const TYPES_SOURCE = [
        self::SOURCE_INGURU => 'Данные получены из ИНГУРУ',
        self::SOURCE_SRAVNIRU => 'Данные получены из спавни.ру',
    ];

    /**
     * Типы транспортных средств
     */
    public const TYPE_CAR_PASSENGER = 1;
    public const TYPE_CAR_TAXI = 2;
    public const TYPE_CAR_BUS = 3;
    public const TYPE_CAR_BUS_MORE_16 = 4;
    public const TYPE_CAR_TRUCK = 5;
    public const TYPE_CAR_TRUCK_MORE_16 = 6;
    public const TYPE_CAR_MOTO = 7;
    public const TYPE_CAR_TROLLEYBUS = 8;
    public const TYPE_CAR_TRAMS = 9;
    public const TYPE_CAR_SPEC = 10;
    public const TYPE_CAR_TAXI_BUS = 11;
    const TYPES_CARS = [
        self::TYPE_CAR_PASSENGER => 'Легковой автомобиль',
        self::TYPE_CAR_TAXI => 'Такси',
        self::TYPE_CAR_BUS => 'Автобусы (16 и менее мест)',
        self::TYPE_CAR_BUS_MORE_16 => 'Автобусы (более 16 мест)',
        self::TYPE_CAR_TRUCK => 'Грузовики (16 и менее тонн)',
        self::TYPE_CAR_TRUCK_MORE_16 => 'Грузовики (более 16 тонн)',
        self::TYPE_CAR_MOTO => 'Мотоциклы',
        self::TYPE_CAR_TROLLEYBUS => 'Троллейбусы',
        self::TYPE_CAR_TRAMS => 'Трамваи',
        self::TYPE_CAR_SPEC => 'Спец. техника',
        self::TYPE_CAR_TAXI_BUS => 'Маршрутные автобусы',
    ];

    /**
     * Тип документов на автомобиль
     */
    public const TYPE_DOCUMENT_PTS = 0;
    public const TYPE_DOCUMENT_STS = 1;
    public const TYPE_DOCUMENT_EPTS = 2;
    const TYPES_DOCUMENTS = [
        self::TYPE_DOCUMENT_PTS => 'ПТС',
        self::TYPE_DOCUMENT_STS => 'СТС',
        self::TYPE_DOCUMENT_EPTS => 'ЕПСТС',
    ];
    const TYPES_DOCUMENTS_BY_ALIAS = [
        'pts' => self::TYPE_DOCUMENT_PTS,
        'sts' => self::TYPE_DOCUMENT_STS,
        'ePts' => self::TYPE_DOCUMENT_EPTS,
    ];

    /**
     * Категория автомобиля
     */
    public const CATEGORY_CAR_A = 1;
    public const CATEGORY_CAR_B = 2;
    public const CATEGORY_CAR_C = 3;
    public const CATEGORY_CAR_D = 4;
    public const CATEGORY_CAR_BE = 5;
    public const CATEGORY_CAR_CE = 6;
    public const CATEGORY_CAR_DE = 7;
    public const CATEGORY_CAR_A1 = 8;
    public const CATEGORY_CAR_B1 = 9;
    public const CATEGORY_CAR_C1 = 10;
    public const CATEGORY_CAR_D1 = 11;
    const CATEGORY_CARS = [
        self::CATEGORY_CAR_A => 'A',
        self::CATEGORY_CAR_B => 'B',
        self::CATEGORY_CAR_C => 'C',
        self::CATEGORY_CAR_D => 'D',
        self::CATEGORY_CAR_BE => 'BE',
        self::CATEGORY_CAR_CE => 'CE',
        self::CATEGORY_CAR_DE => 'DE',
        self::CATEGORY_CAR_A1 => 'A1',
        self::CATEGORY_CAR_B1 => 'B1',
        self::CATEGORY_CAR_C1 => 'C1',
        self::CATEGORY_CAR_D1 => 'D1',
    ];
}