<?php
namespace common\modules\bonuses\services;

use common\modules\bonuses\models\Bonus;
use common\modules\bonuses\dictionaries\BonusDictionary;
use common\modules\bonuses\models\BonusLog;
use common\modules\orders\dictionaries\OrderDictionary;
use common\modules\orders\models\Order;
use common\models\Service;
use common\modules\orders\services\OrderPropertyService;
use yii\helpers\Json;

/**
 * Class BounsService
 * @package $bonus Bonus
 * @package common\modules\bonuses\services
 */
class BonusService extends Service
{
    /**
     * @var Bonus
     */
    public $bonus;

    public function getBonusesByParams(
        array $params,
        string $keyOrder = 'id',
        int $typeOrder = SORT_ASC,
        bool $returnOne = false)
    {
        $query = Bonus::find()->where($params)->orderBy([$keyOrder => $typeOrder]);

        return !$returnOne ? $query->all() : $query->one();
    }

    /**
     * Начисляет бонусы за заказ
     * @param Order $order
     * @param bool $runValidation
     * @return bool
     */
    public function save(Order $order, $runValidation = true)
    {
        // Проверяю, есть ли уже начисленный бонус для этого заказа. Если есть - обновеляю данные в нем
        $model = Bonus::find()->where(['order_id' => $order->id])->one();
        if (empty($model)) {
            $model = new Bonus();
            $model->status_send_store = BonusDictionary::STATUS_SEND_STORE_ERROR;
        }
        $model->user_id = $order->user_id;
        $model->order_id = $order->id;
        $model->bonus = self::calculatePrice($order->commission, false);
        $this->bonus = $model;

        return $model->save($runValidation);
    }

    /**
     * Вычисляет размер бонуса от цены
     * @param float $price Цена
     * @param bool $roundUp Если true - округляет до целого
     * @return float|int
     */
    public static function calculatePrice(float $price, bool $roundUp = true)
    {
        $price = $price * BonusDictionary::PERCENT_BONUS_OF_PRICE;
        return $roundUp ? round($price) : $price;
    }

    /**
     * Отправляет бонусы в стор
     * @param Bonus $bonus
     * @return bool|int
     */
    public static function sendStoreBonus(Bonus $bonus)
    {
        // Если уже была успешная отправка бонуса - выходим
        if ($bonus->status_send_store == BonusDictionary::STATUS_SEND_STORE_SUCCESS) {
            return true;
        }
        $order = $bonus->order;
        $servicePropertyService = new OrderPropertyService();
        $property = $order->properties;
        $comment = "Полис ОСАГО";
        switch ($order->type) {
            case OrderDictionary::TYPE_INGURU:
                $brandCar = $servicePropertyService->getOrderPropertyValue($property, 'vehicleBrand');
                $modelCar = $servicePropertyService->getOrderPropertyValue($property, 'vehicleModel');
                $plateCar = $servicePropertyService->getOrderPropertyValue($property, 'vehicleLicensePlate');
                $comment .= " для $brandCar $modelCar $plateCar";
                break;
        }

        $request = [
            'user_id' => $order->user_id,
            'sum' => $bonus->bonus,
            'created_at' => date('Y-m-d H:i:s', time()),
            'type' => BonusDictionary::STORE_TYPE,
            'comment' => trim($comment),
        ];

        $answer = \Yii::$app->api->requestWithInfo('store/bonus-charges', 'post', $request);
        $statusSend = !empty($answer['success']) ? BonusDictionary::STATUS_SEND_STORE_SUCCESS : BonusDictionary::STATUS_SEND_STORE_ERROR;
        $bonus->status_send_store = $statusSend;
        $bonus->save();

        $modelLog = new BonusLog();
        $modelLog->bonus_id = $bonus->id;
        $modelLog->status_send = $statusSend;
        $modelLog->request = Json::encode($request);
        $modelLog->answer = Json::encode($answer);
        $modelLog->save(false);

        return $statusSend;
    }
}