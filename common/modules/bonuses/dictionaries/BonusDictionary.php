<?php

namespace common\modules\bonuses\dictionaries;

/**
 * Class BonusDictionary
 *
 * @package common\modules\bonuses\dictionaries
 */
class BonusDictionary
{
    /**
     * Статус отправки данных в стор
     */
    public const STATUS_SEND_STORE_SUCCESS = 1;
    public const STATUS_SEND_STORE_ERROR = 0;
    const STATUSES_SEND_STORE = [
        self::STATUS_SEND_STORE_SUCCESS => 'Отправка прошла успешно',
        self::STATUS_SEND_STORE_ERROR => 'Отправка не удалась'
    ];

    public const PERCENT_BONUS_OF_PRICE = 0.75; // Процент бонуса составляет 75% от цены
    public const STORE_TYPE = 'ghost'; // Значение передаваемое в поле type при отправке бонусов в стор
}