<?php
namespace common\modules\bonuses\models;

use common\models\ActiveRecordModel;
use common\modules\bonuses\dictionaries\BonusDictionary;
use common\modules\orders\models\Order;
use common\modules\users\models\User;
use yii\behaviors\TimestampBehavior;

/**
 * Class Bonus
 * @package common\modules\bonuses\models
 * @property integer $id
 * @property integer $user_id
 * @property integer $order_id
 * @property integer $bonus
 * @property boolean $status_send_store
 * @property integer $updated_at
 * @property integer $created_at
 *
 * @property User $user
 * @property Order $order
 * @property BonusLog $logs[]
 */
class Bonus extends ActiveRecordModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bonuses';
    }

    /**
     * Define rules for validation
     */
    public function rules()
    {
        return [
            [['user_id', 'order_id', 'bonus'], 'required'],
            [['order_id'], 'unique'],
            [
                ['order_id'],
                'exist',
                'targetClass' => Order::class,
                'targetAttribute' => ['order_id' => 'id']
            ],
            [['user_id', 'order_id', 'updated_at', 'created_at', 'status_send_store'], 'integer'],
            [['bonus'], 'number'],
            [['status_send_store'], 'default', 'value' => BonusDictionary::STATUS_SEND_STORE_ERROR],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::class,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'Пользователь',
            'order_id' => 'Заказ',
            'bonus' => 'Размер бонуса',
            'status_send_store' => 'Статус отправки бонуса в стор',
            'updated_at' => 'Дата редактирования',
            'created_at' => 'Дата создания',
        ];
    }

    /**
     * @return User|\yii\web\IdentityInterface|null
     */
    public function getUser()
    {
        return User::findIdentity($this->user_id);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::class, ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogs()
    {
        return $this->hasMany(BonusLog::class, ['bonus_id' => 'id']);
    }
}