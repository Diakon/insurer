<?php
namespace common\modules\bonuses\models;

use common\models\ActiveRecordModel;
use common\modules\bonuses\dictionaries\BonusDictionary;
use yii\behaviors\TimestampBehavior;

/**
 * Class Bonus
 * @package common\modules\bonuses\models
 * @property integer $id
 * @property integer $bonus_id
 * @property integer $status_send
 * @property string $request
 * @property string $answer
 * @property integer $updated_at
 * @property integer $created_at
 *
 * @property Bonus $bonus
 */
class BonusLog extends ActiveRecordModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bonuses_logs';
    }

    /**
     * Define rules for validation
     */
    public function rules()
    {
        return [
            [['bonus_id'], 'required'],
            [
                ['bonus_id'],
                'exist',
                'targetClass' => Bonus::class,
                'targetAttribute' => ['bonus_id' => 'id']
            ],
            [['bonus_id', 'status_send', 'updated_at', 'created_at'], 'integer'],
            [['status_send'], 'default', 'value' => BonusDictionary::STATUS_SEND_STORE_ERROR],
            [['request', 'answer'], 'safe'],
            [['status_send'], 'default', 'value' => BonusDictionary::STATUS_SEND_STORE_ERROR],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::class,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'bonus_id' => 'Бонус',
            'request' => 'Запрос, отправленный в стор',
            'answer' => 'Ответ из стор',
            'status_send' => 'Статус отправки бонуса в стор',
            'updated_at' => 'Дата редактирования',
            'created_at' => 'Дата создания',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBonus()
    {
        return $this->hasOne(Bonus::class, ['id' => 'bonus_id']);
    }
}