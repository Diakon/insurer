<?php

namespace common\modules\bonuses\jobs;

use common\modules\bonuses\models\Bonus;
use common\modules\bonuses\services\BonusService;
use common\modules\orders\dictionaries\OrderDictionary;
use common\modules\orders\models\Order;
use yii\base\Object;

/**
 * Задание отправки бонусов в стор, через АПИ
 * Class SendStoreBonus
 * @package common\modules\bonuses\jobs
 */
class SendStoreBonus extends Object implements \yii\queue\Job
{
    /**
     * Модель заказа
     * @var Order
     */
    public $order;

    /**
     * Отправляет бонусы в стор
     * @param \yii\queue\Queue $queue
     * @return mixed|void
     */
    public function execute($queue)
    {
        $bonus = $this->getBonus();
        // Если записи в таблице бонус нет, а заказ оплачен - создаю запись
        if (empty($bonus) && $this->order->status == OrderDictionary::STATUS_COMPLETE) {
            $bonusService = new BonusService();
            if($bonusService->save($this->order)) {
                $bonus = $bonusService->bonus;
            }
        }
        // Начисляю бонусы
        if (!empty($bonus)) {
            BonusService::sendStoreBonus($bonus);
        }
    }

    /**
     * Возвращает модель таблицы bonus по заказу
     * @return array|\yii\db\ActiveRecord|\yii\db\ActiveRecord[]|null
     */
    private function getBonus()
    {
        $bonusService = new BonusService();
        return $bonusService->getBonusesByParams(['order_id' => $this->order->id], 'id', SORT_ASC, true);
    }
}