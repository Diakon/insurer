<?php
namespace common\modules\users\models;

use common\modules\users\dictionaries\UserDictionary;
use Yii;
use common\modules\users\services\UserService;
use yii\base\Model;
use yii\web\IdentityInterface;

/**
 * Пользователь ЕСА.
 */
class User extends Model implements IdentityInterface
{
    /** @var int */
    public $id;

    /** @var string */
    public $login;

    /** @var string */
    public $authKey;

    /**
     * @inheritDoc
     */
    public static function findIdentity($id)
    {
        $service = new UserService();
        $user = $service->getUserById($id);

        return $user ? self::create($user['id'], $user['login']) : null;
    }

    /**
     * Создание объекта User
     * @param int $id
     * @param string $login
     * @param string|null $accessToken
     * @return static
     */
    public static function create(int $id, string $login, ?string $accessToken = null): self
    {
        $user = new self;
        $user->id = $id;
        $user->login = $login;
        // Пробую получить токен из сессии
        if (empty($accessToken)) {
            $session = Yii::$app->session;
            $accessToken = $session->get(UserDictionary::ACCESS_TOKEN_SESSION_KEY, $accessToken);
        }
        $user->authKey = $accessToken;

        return $user;
    }

    /**
     * Вернуть модель пользователя по токену
     * @inheritDoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        $service = new UserService();
        $user = $service->getUserByAccessToken($token);

        return $user ? self::create($user['id'], $user['login'], $token) : null;
    }

    /**
     * @inheritDoc
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @inheritDoc
     */
    public function getAuthKey(): string
    {
        return $this->authKey;
    }

    /**
     * @inheritDoc
     */
    public function validateAuthKey($authKey): bool
    {
        return $this->authKey === $authKey;
    }

    /**
     * @inheritDoc
     * @return string[]
     */
    public function fields(): array
    {
        return [
            'access_token' => 'authKey',
        ];
    }
}