<?php

namespace common\modules\users\forms;

use common\modules\users\dictionaries\TimeDictionary;
use common\modules\users\dictionaries\UserDictionary;
use common\modules\users\services\UserService;
use Yii;
use common\modules\users\models\User;
use common\models\Model;

/**
 * Форма авторизации.
 *
 * Class LoginForm
 * @package common\modules\users\forms
 */
class LoginForm extends Model
{
    /** @var string логин */
    public $login;

    /** @var string пароль */
    public $password;

    /** @var User */
    private $user;

    /**
     * @var bool
     */
    public $rememberMe;

    /**
     * @return array the validation rules.
     */
    public function rules(): array
    {
        return [
            [['login', 'password'], 'required'],
            [['password'], 'validateUser'],
            ['rememberMe', 'boolean'],
        ];
    }

    public function validateUser(): void
    {
        if (!$this->getUser()) {
            $this->addError('password', 'Логин или пароль указан не верно.');
        }
    }

    /**
     * Возвращает юзера по данным из API.
     *
     * @return User|null
     */
    private function getUser(): ?User
    {
        $service = new UserService();
        if ($this->user === null) {
            $authData = $service->login($this->login, $this->password);
            if ($authData) {
                $this->user = User::create($authData['id'], $authData['login'], $authData['access_token']);
                // Пишу в ссессию токен пользователя
                $session = Yii::$app->session;
                $session->set(UserDictionary::ACCESS_TOKEN_SESSION_KEY, $authData['access_token']);
            }
        }


        return $this->user;
    }

    /**
     * Авторизация пользователя.
     *
     * @return bool
     */
    public function login(): bool
    {
        if (!$this->validate()) {
            return false;
        }

        return Yii::$app->user->login($this->getUser(), $this->rememberMe ? TimeDictionary::SECONDS_IN_A_MONTH : 0);
    }

    public function getUserData()
    {
        return $this->user;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'login' => 'Логин',
            'password' => 'Пароль',
            'rememberMe' => 'Заполмнить меня'
        ];
    }
}