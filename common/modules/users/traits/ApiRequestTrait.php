<?php

namespace common\modules\users\traits;

use Yii;

/**
 * Trait ApiRequestTrait
 * @package common\modules\users\traits
 */
trait ApiRequestTrait
{
    /**
     * Делает запрос в АПИ
     *
     * @param string $url
     * @param string $method
     * @param array $params
     * @return mixed|null
     */
    private function requestApi(string $url, string $method, array $params = []): ?array
    {
        $response = Yii::$app->api->requestWithInfo($url, $method, $params);

        return $response['success'] ? $response['data'] : null;
    }
}