<?php

namespace common\modules\users\services;

use common\models\Service;
use Exception;
use common\modules\users\traits\ApiRequestTrait;

/**
 * Сервис для работы с учетной запись пользователя.
 * Class UserService
 * @package common\modules\users\services
 */
class UserService extends Service
{
    use ApiRequestTrait;

    /**
     * Авторизация пользователя.
     * @param string $login
     * @param string $password
     *
     * @return array|null
     */
    public function login(string $login, string $password): ?array
    {
        return $this->requestApi('auth/users/login', 'POST', ['login' => $login, 'password' => $password]);
    }

    /**
     * Получение пользователя по $id ЕСА.
     *
     * @param int $id
     *
     * @return array|null
     * @throws Exception
     */
    public function getUserById(int $id): ?array
    {
        return $this->requestApi('auth/users/' . $id, 'GET', ['expand' => 'profile']);
    }

    /**
     * Получение пользователя по $accessToken ЕСА.
     *
     * @param string $accessToken
     *
     * @return array|null
     * @throws Exception
     */
    public function getUserByAccessToken(string $accessToken): ?array
    {
        return $this->requestApi('auth/users/access-token/' . $accessToken, 'GET', ['expand' => 'profile']);
    }

    /**
     * Возвращает данные провфиля пользователя по ID
     *
     * @param int $id
     * @return array|null
     */
    public function getUserProfileById(int $id): ?array
    {
        return $this->requestApi('auth/users/' . $id, 'GET', ['expand' => 'profile,personalData']);
    }
}