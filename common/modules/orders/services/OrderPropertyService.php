<?php
namespace common\modules\orders\services;

use common\modules\inguru\forms\OsagoInguruForm;
use common\modules\orders\dictionaries\OrderDictionary;
use common\modules\orders\models\Order;
use common\modules\orders\models\OrderProperty;
use common\models\Service;
use common\modules\sravniru\forms\OsagoSravniRuForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * Class OrderPropertyService
 * @package common\modules\orders\services
 */
class OrderPropertyService extends Service
{
    /**
     * Возвращает записи из OrderProperty по параметрам
     *
     * @param array $params
     * @param bool $returnOne
     * @return array|\yii\db\ActiveRecord|\yii\db\ActiveRecord[]|null
     */
    public function getOrdersByParams(array $params, bool $returnOne = false)
    {
        $query = OrderProperty::find()->where($params);

        return !$returnOne ? $query->all() : $query->one();
    }

    /**
     * Удаляет записи по ID заказа. Так же можно отфильтровать записи к удалению по типу $type
     * @param int $orderId
     * @param int|null $type
     * @return int
     */
    public function deleteByOrderId(int $orderId, int $type = null)
    {
        $param = [
            'order_id' => $orderId,
        ];
        if (!is_null($type)) {
            $param['type'] = $type;
        }
        return OrderProperty::deleteAll($param);
    }

    /**
     * Вставляет значение характеристик для заказа
     * @param Order $order
     * @param int $typeId
     * @param array $params
     */
    public function setProperty(Order $order, int $typeId, array $params)
    {
        foreach ($params as $key => $value) {
            $model = OrderProperty::find()
                ->where(['order_id' => $order->id])
                ->andWhere(['key' => $key])
                ->one();
            if (empty($model)) {
                $model = new OrderProperty();
                $model->order_id = $order->id;
            }
            $model->type = $typeId;
            $model->key = $key;
            $model->value = $value;
            $model->save();
        }
    }

    /**
     * @param array $params
     * @param array $condition
     */
    public function update(array $params, array $condition)
    {
        OrderProperty::updateAll(
            $params,
            $condition
        );
    }

    /**
     * Сохраняет модель параметров заказа
     *
     * @param $model
     * @param bool $validate
     * @return bool
     */
    public function save($model, bool $validate = true)
    {
        if (empty($model->type)) {
            $model->type = $model::typeId();
        }
        return $model->save($validate);
    }

    /**
     * Возвращает значение свойства
     *
     * @param OrderPropertyService[] $model
     * @param string $key
     * @return mixed|null
     */
    public function getOrderPropertyValue(array $model, string $key)
    {
        $property = ArrayHelper::map($model, 'key', 'value');
        $propertyValue = $property[$key] ?? null;
        if (empty($propertyValue)) {
            return $propertyValue;
        }

        return $this->isJson($propertyValue) ? Json::decode($propertyValue) : $propertyValue;
    }

    /**
     * Возвращает записи из OrderProperty по заданным параметрам
     * @param int $orderId
     * @param array $where
     * @param bool $returnOne
     * @param array $sort
     * @return array|\yii\db\ActiveRecord|\yii\db\ActiveRecord[]|null
     */
    public function getPropertyByParams(int $orderId, array $where = [], bool $returnOne = false, array $sort = [])
    {
        $query = OrderProperty::find();
        $query->where(['order_id' => $orderId]);
        if (!empty($where)) {
            $query->andWhere($where);
        }
        if (!empty($sort)) {
            $query->orderBy($sort);
        }

        return $returnOne ? $query->one() : $query->all();
    }

    /**
     * Возвращает список кастомно заданных для страховых компаний пар Бренд/Модель ТС
     * @param Order $order
     * @return array|mixed
     */
    public function getCarCustomBrandModel(Order $order)
    {
        $service = new self();
        $customCarBrandModel = $service->getOrderPropertyValue($order->properties, 'insurerCustomCarBrandModel');

        return !empty($customCarBrandModel) ? $customCarBrandModel : [];
    }

    /**
     * Возвращает массив с характеристиками выставленными для закзаа.
     * Если указан $typeProperty - отбирает характеристики только на определенноим шаге
     * @param Order $order
     * @param int|null $typeProperty
     * @param array $sort
     * @return array
     */
    public static function propertyInfo(Order $order, int $typeProperty = null, array $sort = ['id' => SORT_ASC])
    {
        $labels = $returnPropertyInfo = [];
        switch ($order->type) {
            // Если заказ в ИНГУРУ
            case OrderDictionary::TYPE_INGURU:
                $model = new OsagoInguruForm();
                $labels = $model->attributeLabels();
                break;
            // Если заказ в сравни.ру
            case OrderDictionary::TYPE_SRAVNIRU:
                $model = new OsagoSravniRuForm();
                $labels = $model->attributeLabels();
                break;
        }

        $service = new self();
        /**
         * @var $properties OrderProperty[]
         */
        $properties = $service->getPropertyByParams($order->id, !is_null($typeProperty) ? ['type' => $typeProperty] : [], false, $sort);
        foreach ($properties as $property) {
            $returnPropertyInfo[$property->type][] = [
                'key' => $property->key,
                'label' => $labels[$property->key] ?? "",
                'value' => $service->isJson($property->value) ? Json::decode($property->value) : $property->value
            ];
        }

        return $returnPropertyInfo;
    }
}