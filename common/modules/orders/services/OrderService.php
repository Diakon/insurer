<?php
namespace common\modules\orders\services;

use common\modules\inguru\forms\OsagoInguruForm;
use common\modules\inguru\models\OsagoDocuments;
use common\modules\inguru\services\OsagoDocumentsService;
use common\modules\inguru\services\PaymentsService;
use common\modules\orders\dictionaries\OrderDictionary;
use common\modules\orders\models\Order;
use common\modules\orders\models\OrderProperty;
use common\models\Service;
use yii\base\BaseObject;
use yii\helpers\Json;

/**
 * Class OrderService
 * @package common\modules\orders\services
 */
class OrderService extends Service
{
    /**
     * Возвращает заказы по параметрам (или один заказ, если $returnOne === true)
     *
     * @param array $params
     * @param string $keyOrder
     * @param int $typeOrder
     * @param bool $returnOne
     * @return array|\yii\db\ActiveRecord|\yii\db\ActiveRecord[]|null
     */
    public function getOrdersByParams(
        array $params,
        string $keyOrder = 'id',
        int $typeOrder = SORT_ASC,
        bool $returnOne = false)
    {
        $query = Order::find()->where($params)->orderBy([$keyOrder => $typeOrder]);

        return !$returnOne ? $query->all() : $query->one();
    }

    /**
     * Возвращает заказ по ID, если передан $userId - проверяет, что этот заказ закреплен за пользователем с ID = $userId
     *
     * @param int $orderId
     * @param int|null $userId
     * @return array|\yii\db\ActiveRecord|null
     */
    public function getOrderById(int $orderId, int $userId = null, $chekIsset = false)
    {
        $query = Order::find()->where(['id' => $orderId]);
        if (!is_null($userId)) {
            $query->andWhere(['user_id' => $userId]);
        }

        return $query->one();
    }

    /**
     * Возвращает количество заказов по параметрам
     *
     * @param array $params
     * @return bool|int|string|null
     */
    public function count(array $params)
    {
        return Order::find()->where($params)->count();
    }

    /**
     * Возвращает заказы пользователя. Если передан $status - то фильтрует по статусу заказа.
     * Так же можно задать параметры сортировки результата ($typeOrder и $keyOrder) и вернуть только 1 результат (если $returnOne === true)
     *
     * @param int $userId
     * @param int|null $status
     * @param string $keyOrder
     * @param int $typeOrder
     * @param bool $returnOne
     * @return array|\yii\db\ActiveRecord|\yii\db\ActiveRecord[]|null
     */
    public function getOrdersByUser(
        int $userId,
        int $status = null,
        string $keyOrder = 'id',
        int $typeOrder = SORT_ASC,
        bool $returnOne = false)
    {
        $query = Order::find()->where(['user_id' => $userId]);
        if (!is_null($status)) {
            $query->andWhere(['status' => $status]);
        }
        $query->orderBy([$keyOrder => $typeOrder]);

        return !$returnOne ? $query->all() : $query->one();
    }

    /**
     * Сохраняет модель заказа
     *
     * @param Order $model
     * @param bool $validate
     * @return bool
     */
    public function save(Order $model, bool $validate = true)
    {
        return $model->save($validate);
    }

    /**
     * Возвращает ссылку на оплату
     * @param Order $order
     * @return null
     */
    public function getPaymentLink(Order $order)
    {
        $serviceProperty = new OrderPropertyService();
        $property = $order->properties;
        switch ($order->type) {
            // Для ингуру
            case OrderDictionary::TYPE_INGURU:
                $servicePayment = new PaymentsService();
                $model = new OsagoInguruForm();
                $model->eId = $serviceProperty->getOrderPropertyValue($property, 'eId');
                $model->company = $serviceProperty->getOrderPropertyValue($property, 'company');
                $model->successUrl = $serviceProperty->getOrderPropertyValue($property, 'successUrl');
                $model->failUrl = $serviceProperty->getOrderPropertyValue($property, 'failUrl');

                return $servicePayment->getPaymentLinkResponse($model, $model->company);
            // Для сравни.ру
            case OrderDictionary::TYPE_SRAVNIRU:
                $redirectPaymentUrl = $serviceProperty->getOrderPropertyValue($property, 'redirectPaymentUrlSravniRu');
                return !empty($redirectPaymentUrl) ? ['url' => $redirectPaymentUrl] : null;
        }

        return null;
    }

    /**
     * Возвращает ссылки на документы ОСАГО
     * @param Order $order
     * @return array
     */
    public function getOsagoDocuments(Order $order)
    {
        $documents = [];
        switch ($order->type) {
            case OrderDictionary::TYPE_INGURU:
                /**
                 * @var $property OrderProperty[]
                 */
                $property = $order->properties;
                $orderPropertyService = new OrderPropertyService();
                $osagoDocumentsService = new OsagoDocumentsService();
                $model = new OsagoDocuments();
                $model->eId = $orderPropertyService->getOrderPropertyValue($property, 'eId');
                $documents = $osagoDocumentsService->getDocumentLink($model);
                $documents = !empty($documents['errors']) ? false : $documents['results'];

                break;
        }

        return $documents;
    }
}