<?php
namespace common\modules\orders\models;

use common\modules\orders\dictionaries\OrderPropertyDictionary;
use common\modules\orders\interfaces\OrderPropertyInterface;

/**
 * Class DriverProperty
 * @package common\modules\orders\models
 */
class DriverProperty extends OrderProperty implements OrderPropertyInterface
{
    /**
     * @inheritdoc
     */
    public static function find()
    {
        $query = self::find();
        $query->alias(self::classNameShort('id'));
        $query->andWhere([self::classNameShort('id') . '.type' => self::typeId()]);

        return $query;
    }

    /**
     * @return int
     */
    public static function typeId()
    {
        return OrderPropertyDictionary::TYPE_DRIVER_PROPERTY_ID;
    }
}