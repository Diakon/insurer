<?php
namespace common\modules\orders\models;

use common\models\ActiveRecordModel;
use yii\behaviors\TimestampBehavior;

/**
 * Class OrderProperty
 * @package common\modules\orders\models\OrderProperty
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $type
 * @property string $key
 * @property string $value
 * @property integer $updated_at
 * @property integer $created_at
 *
 * @property Order $order
 */
class OrderProperty extends ActiveRecordModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders_properties';
    }

    /**
     * Define rules for validation
     */
    public function rules()
    {
        return [
            [['order_id', 'key', 'type'], 'required'],
            [['key'], 'string', 'max' => 256],
            [['value'], 'safe'],
            [['id', 'order_id', 'type', 'updated_at', 'created_at'], 'integer'],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::class, 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::class,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'order_id' => 'Заказ',
            'type' => 'Типа свойства',
            'key' => 'Имя параметра',
            'value' => 'Значение параметра',
            'updated_at' => 'Дата редактирования',
            'created_at' => 'Дата создания',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::class, ['id' => 'order_id']);
    }
}