<?php
namespace common\modules\orders\models;

use common\modules\bonuses\models\Bonus;
use Yii;
use common\models\ActiveRecordModel;
use common\modules\orders\dictionaries\OrderDictionary;
use common\modules\users\models\User;
use yii\behaviors\TimestampBehavior;

/**
 * Class Order
 * @package common\modules\orders\models\Order
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $status
 * @property string $step
 * @property integer $type
 * @property string $token
 * @property integer $updated_at
 * @property integer $created_at
 * @property float $price
 * @property float $commission
 *
 * @property User $user
 * @property OrderProperty[] $properties
 * @property CarProperty[] $carProperty
 * @property DriverProperty[] $driverProperty
 * @property Bonus[] $bonuses
 */
class Order extends ActiveRecordModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * Define rules for validation
     */
    public function rules()
    {
        return [
            [['user_id', 'status', 'step', 'type', 'token'], 'required'],
            ['token', 'unique'],
            [['price', 'commission'], 'number'],
            [['step'], 'string', 'max' => 100],
            [['token'], 'string', 'max' => 250],
            [['status'], 'in', 'range' => array_keys(OrderDictionary::STATUSES)],
            [['type'], 'in', 'range' => array_keys(OrderDictionary::TYPES)],
            [['id', 'user_id', 'status', 'type', 'updated_at', 'created_at'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::class,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'Пользователь',
            'status' => 'Статус заказа',
            'step' => 'Текущий шаг',
            'type' => 'Тип АПИ оформления ОСАГО',
            'token' => 'Токен (уникальный ключ) для заказа',
            'price' => 'Стоимость заказа',
            'commission' => 'Комиссия',
            'updated_at' => 'Дата редактирования',
            'created_at' => 'Дата создания',
        ];
    }

    /**
     * @return User|\yii\web\IdentityInterface|null
     */
    public function getUser()
    {
        return User::findIdentity($this->user_id);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperties()
    {
        return $this->hasMany(OrderProperty::class, ['order_id' => 'id']);
    }

    /**
     * Генерирует токен для заказа
     *
     * @return string
     */
    public function generateToken()
    {
        return md5('token-order-' . \Yii::$app->user->id . '-' . time());
    }

    /**
     * Возвращает ссылку на страницу оплаты (для оплаты по выбранной компании)
     * @return string
     */
    public function generatePaymentLink()
    {
        return Yii::$app->request->hostInfo . '/payment-link/?token=' . $this->token;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBonuses()
    {
        return $this->hasMany(Bonus::class, ['order_id' => 'id']);
    }
}