<?php

namespace common\modules\orders\interfaces;

/**
 * Interface OrderPropertyInterface
 * @package common\modules\orders\interfaces
 */
interface OrderPropertyInterface
{
    /**
     * Переодпределяю find - добавлять type
     * @return mixed
     */
    public static function find();

    /**
     * Возвращает ID типа характеристики из словаря OrderPropertyDictionary
     * @return mixed
     */
    public static function typeId();
}