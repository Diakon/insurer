<?php

namespace common\modules\orders\dictionaries;

/**
 * Class OrderDictionary
 *
 * @package common\modules\orders\dictionaries
 */
class OrderDictionary
{
    /**
     * Типы систем оформоения ОСАГО
     */
    public const TYPE_INGURU = 1;
    public const TYPE_SRAVNIRU = 2;
    const TYPES = [
        self::TYPE_INGURU => 'Ингуру',
        self::TYPE_SRAVNIRU => 'Сравни.ру',
    ];

    /**
     * Статусы заказа
     */
    public const STATUS_NEW = 0;
    public const STATUS_AWAITING_PAYMENT = 1;
    public const STATUS_COMPLETE = 2;
    public const STATUS_CANCELED = 3;
    const STATUSES = [
        self::STATUS_NEW => 'Новый',
        self::STATUS_AWAITING_PAYMENT => 'Ожидает оплаты',
        self::STATUS_COMPLETE => 'Завершен',
        self::STATUS_CANCELED => 'Отменен',
    ];

}