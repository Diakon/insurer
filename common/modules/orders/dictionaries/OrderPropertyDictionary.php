<?php

namespace common\modules\orders\dictionaries;

/**
 * Class OrderPropertyDictionary
 *
 * @package common\modules\orders\dictionaries
 */
class OrderPropertyDictionary
{
    /**
     * ID типа характеристики
     */
    public const TYPE_CAR_PROPERTY_ID = 1;
    public const TYPE_DRIVER_PROPERTY_ID = 2;
    public const TYPE_OWNER_PROPERTY_ID = 3;
    public const TYPE_POLICY_PROPERTY_ID = 4;
    public const TYPE_CALCULATE_PROPERTY_ID = 5;
    public const TYPE_PAYMENT_PROPERTY_ID = 6;
    const TYPES_LIST = [
        self::TYPE_CAR_PROPERTY_ID => 'Данные об автомобиле',
        self::TYPE_DRIVER_PROPERTY_ID => 'Данные о водителях',
        self::TYPE_OWNER_PROPERTY_ID => 'Данные о владельце автомобиля',
        self::TYPE_POLICY_PROPERTY_ID => 'Данные о предыдущих страховках на автомобиль',
        self::TYPE_CALCULATE_PROPERTY_ID => 'Расчет стоимости полиса ОСАГО',
        self::TYPE_PAYMENT_PROPERTY_ID => 'Оплата стоимости полиса ОСАГО',
    ];
}