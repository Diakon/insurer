<?php
namespace common\modules\inguru\services;

use common\modules\inguru\dictionaries\OsagoDocumentsDictionary;
use common\modules\inguru\interfaces\ApiInterface;
use common\modules\inguru\models\OsagoDocuments;
use common\models\Service;
use yii\httpclient\Client;
use common\modules\inguru\traits\AuthTrait;
use common\modules\inguru\traits\RequestApiTrait;

/**
 * Class OsagoDocumentsService
 * @package common\modules\inguru\services
 */
class OsagoDocumentsService extends Service implements ApiInterface
{
    use AuthTrait;
    use RequestApiTrait;

    /**
     * Возвращает URL ИНГУРУ для отправки запроса
     *
     * @return string
     */
    public function getApiUrl()
    {
        return 'https://api.inguru.ru/eosago/6.0';
    }

    /**
     * Метод возвращающий данные для отправки в АПИ ИНГУРУ
     *
     * @param $model
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function getApiDataByModel($model)
    {
        $requestData = [];
        $requestData['q'] = OsagoDocumentsDictionary::API_CONS;
        $requestData['eId'] = trim($model->eId);

        return $requestData;
    }

    /**
     * @return mixed|string
     */
    public function getApiCurlFormat()
    {
        return Client::FORMAT_CURL;
    }

    /**
     * @return string
     */
    public function getApiCurlMethod()
    {
        return 'get';
    }

    /**
     * Возвращает ссылку на скачивание документа полича ОСАГО
     *
     * @param OsagoDocuments $model
     * @return false
     * @throws \yii\base\InvalidConfigException
     */
    public function getDocumentLink(OsagoDocuments $model)
    {
        $response = $this->request(
            $this->getApiUrl(),
            $this->getToken(),
            $this->getApiDataByModel($model),
            $this->getApiCurlFormat(),
            $this->getApiCurlMethod()
        );
        if ($response->isOk) {
            $response = $response->data;
            if (!empty($response['errors'])) {
                return false;
            }

            return $response;
        }

        return false;
    }
}