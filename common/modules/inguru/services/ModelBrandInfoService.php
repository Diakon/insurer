<?php
namespace common\modules\inguru\services;

use api\modules\inguru\models\ModelBrandInfo;
use common\modules\cars\models\Car;
use common\modules\cars\dictionaries\CarDictionary;
use common\modules\inguru\dictionaries\ModelBrandInfoDictionary;
use common\modules\inguru\interfaces\ApiInterface;
use common\models\Service;
use yii\httpclient\Client;

/**
 * Class ModelBrandInfoService
 * @package common\modules\inguru\services
 */
class ModelBrandInfoService extends Service implements ApiInterface
{
    /**
     * Возвращает URL ИНГУРУ для отправки запроса
     *
     * @return string
     */
    public function getApiUrl()
    {
        return 'https://api.inguru.ru/eosago/6.0';
    }

    /**
     * Метод возвращающий данные для отправки в АПИ ИНГУРУ
     *
     * @param ModelBrandInfo $model
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function getApiDataByModel($model)
    {
        $typeSearch = $model->searchType == ModelBrandInfoDictionary::SEARCH_TYPE_1
            ? ModelBrandInfoDictionary::SEARCH_BRAND
            : ModelBrandInfoDictionary::SEARCH_MODEL;
        if (!empty($model->companyId)) {
            $typeSearch = $model->searchType == ModelBrandInfoDictionary::SEARCH_TYPE_1
                ? ModelBrandInfoDictionary::SEARCH_BRAND_IN_COMPANY
                : ModelBrandInfoDictionary::SEARCH_MODEL_IN_COMPANY;
        }

        $requestData = [];
        $requestData['q'] = $typeSearch;
        $requestData['search'] = $model->search;
        if ($model->searchType == ModelBrandInfoDictionary::SEARCH_TYPE_2) {
            $requestData['brand'] = $model->brandName;
        }
        if (!empty($model->companyId)) {
            $requestData['sk'] = $model->companyId;
        }

        return $requestData;
    }

    /**
     * @return mixed|string
     */
    public function getApiCurlFormat()
    {
        return Client::FORMAT_CURL;
    }

    /**
     * @return string
     */
    public function getApiCurlMethod()
    {
        return 'get';
    }
}