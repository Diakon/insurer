<?php
namespace common\modules\inguru\services;

use common\modules\inguru\dictionaries\PreCalculationDictionary;
use common\modules\inguru\interfaces\ApiInterface;
use common\models\Service;
use yii\helpers\ArrayHelper;
use yii\httpclient\Client;

/**
 * Class PreCalculationService
 * @package common\modules\inguru\services
 */
class PreCalculationService extends Service implements ApiInterface
{
    /**
     * Возвращает URL ИНГУРУ для отправки запроса
     *
     * @return string
     */
    public function getApiUrl()
    {
        return 'https://api.inguru.ru/eosago/6.0';
    }

    /**
     * Метод возвращающий данные для отправки в АПИ ИНГУРУ
     *
     * @param $model
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function getApiDataByModel($model)
    {
        $requestData = [];
        $requestData['vehicle'] = [
            'type' => (int)$model->vehicleType,
            'power' => (int)$model->vehiclePower,
            'year' => (int)$model->vehicleYear,
        ];
        $requestData['multidrive'] = (int)$model->multiDrive;
        $requestData['exactСalculation'] = (int)$model->extractCalculation;

        $requestData['drivers'] = [];
        if (!empty($model->driverLastName)) {
            foreach ($model->driverLastName as $key => $value) {
                $requestData['drivers'][$key]['lastname'] = $value;
                $requestData['drivers'][$key]['middlename'] = $model->driverMiddleName[$key] ?? "";
                $requestData['drivers'][$key]['firstname'] = $model->driverFirstName[$key] ?? "";
                $requestData['drivers'][$key]['birthdate'] = !empty($model->driverBirthDate[$key]) ? \Yii::$app->formatter->asDate($model->driverBirthDate[$key], 'php:Y-m-d') : "";
                $requestData['drivers'][$key]['licenseSerial'] =  $model->driverLicenseSerial[$key] ?? "";
                $requestData['drivers'][$key]['licenseNumber'] = $model->driverLicenseNumber[$key] ?? "";
                $requestData['drivers'][$key]['licenseForeign'] = !empty($model->driverLicenseForeign[$key])
                    ? PreCalculationDictionary::DRIVER_LICENSE_TYPE_FOREIGN
                    : PreCalculationDictionary::DRIVER_LICENSE_TYPE_RU;
                $requestData['drivers'][$key]['lastname'] = $value;
                $requestData['drivers'][$key]['lastname'] = $value;
                $requestData['drivers'][$key]['lastname'] = $value;
                $requestData['drivers'][$key]['lastname'] = $value;
                $requestData['drivers'][$key]['lastname'] = $value;

                if (!empty($model->driverPrevLicenseSerial[$key])) {
                    foreach (
                        [
                            'prevLicenseSerial' => 'driverPrevLicenseSerial',
                            'prevLicenseNumber' => 'driverPrevLicenseNumber',
                            'prevLicenseDate' => 'driverPrevLicenseDate',
                            'prevLicenseLastname' => 'driverPrevLastName',
                            'prevLicenseFirstname' => 'driverPrevFirstName',
                            'prevLicenseMiddlename' => 'driverPrevMiddleName'
                        ] as $requestField => $driverPrevFieldName)
                    {
                        if (empty($model->{$driverPrevFieldName}[$key]))  {
                            $model->{$driverPrevFieldName}[$key] = null;
                        }
                        $value = $model->{$driverPrevFieldName}[$key];
                        switch ($driverPrevFieldName) {
                            case "driverPrevLicenseDate":
                                $value = \Yii::$app->formatter->asDate($value, 'php:Y-m-d');
                                break;
                        }
                        $requestData['drivers'][$key][$requestField] = $value;
                    }
                }
            }
        }
        foreach ($model->driversAge as $key => $value) {
            $requestData['drivers'][$key]['age'] = (int)$value;
            $requestData['drivers'][$key]['exp'] = !empty($model->driversExp[$key]) ? (int)$model->driversExp[$key] : null;
        }

        if (!empty($model->prevPolicySerial) && !empty($model->prevPolicyNumber)) {
            $requestData['prevPolicySerial'] = (string)$model->prevPolicySerial;
            $requestData['prevPolicyNumber'] = (string)$model->prevPolicyNumber;
        }

        $requestData['owner'] = [];
        $requestData['owner']['city'] = (string)$model->ownerCity;
        $requestData['usePeriod'] = (int)$model->usePeriod;
        $requestData['date'] = $model->dateContract;
        if (!is_null($model->useTrailer)) {
            $requestData['useTrailer'] = $model->useTrailer;
        }
        if (!is_null($model->applyKbm)) {
            $requestData['applyKbm'] = $model->applyKbm;
        }

        return $requestData;
    }

    /**
     * @return mixed|string
     */
    public function getApiCurlFormat()
    {
        return Client::FORMAT_JSON;
    }

    /**
     * @return string
     */
    public function getApiCurlMethod()
    {
        return 'post';
    }
}