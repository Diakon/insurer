<?php
namespace common\modules\inguru\services;

use common\modules\inguru\dictionaries\KladrDictionary;
use common\modules\inguru\interfaces\ApiInterface;
use common\models\Service;
use common\modules\inguru\models\Kladr;
use common\modules\inguru\traits\AuthTrait;
use common\modules\inguru\traits\RequestApiTrait;
use yii\httpclient\Client;

/**
 * Class KladrService
 * @package common\modules\inguru\services
 */
class KladrService extends Service implements ApiInterface
{
    use AuthTrait;
    use RequestApiTrait;

    /**
     * Возвращает URL ИНГУРУ для отправки запроса
     *
     * @return string
     */
    public function getApiUrl()
    {
        return 'https://api.inguru.ru/eosago/6.0';
    }

    /**
     * Метод возвращающий данные для отправки в АПИ ИНГУРУ
     *
     * @param $model
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function getApiDataByModel($model)
    {
        $requestData = [];
        $requestData['q'] = KladrDictionary::API_CONS;
        $requestData['name'] = trim($model->searchName);

        return $requestData;
    }

    /**
     * @return mixed|string
     */
    public function getApiCurlFormat()
    {
        return Client::FORMAT_CURL;
    }

    /**
     * @return string
     */
    public function getApiCurlMethod()
    {
        return 'get';
    }

    /**
     * Возвращает список городов найденый по названию в КЛАДР
     * @param string $cityName
     * @return array|mixed
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public static function getCityByName(string $cityName)
    {
        $model = new Kladr();
        $model->searchName = trim($cityName);
        $service = new self();
        $response = $service->request(
            $service->getApiUrl(),
            $service->getToken(),
            $service->getApiDataByModel($model),
            $service->getApiCurlFormat(),
            $service->getApiCurlMethod()
        );
        if ($response->isOk) {
            $response = $response->data;
        }

        return $response['results']['found'] ?? [];
    }
}