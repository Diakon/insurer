<?php
namespace common\modules\inguru\services;

use common\modules\inguru\dictionaries\FinalCalculationDictionary;

/**
 * Class FinalCalculationService
 * @package common\modules\inguru\services
 */
class FinalCalculationService extends PreCalculationService
{
    /**
     * Метод возвращающий данные для отправки в АПИ ИНГУРУ
     *
     * @param $model
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function getApiDataByModel($model)
    {
        $company = $model->insuranceCompanies;
        $companyId = current($company);
        // Если для страховой компании указали кастомное название модели - бренда ТС, беру его.
        // Если нет - тот что получили по гос. номеру от ИНГУРУ (у разных страхоавх компаний название модели-бренда ТС могут отличаться и не давать произвести расчет)
        $vehicleBrand = $model->insurerCustomCarBrandModel[$companyId]['vehicleBrand'] ?? $model->vehicleBrand;
        $vehicleModel = $model->insurerCustomCarBrandModel[$companyId]['vehicleModel'] ?? $model->vehicleModel;

        $requestData = parent::getApiDataByModel($model);
        $requestData['sk'] = $company;
        $requestData['useTrailer'] = !empty($model->useTrailer) ? FinalCalculationDictionary::USE_TRAILER_YES : FinalCalculationDictionary::USE_TRAILER_NO;
        $requestData['licensePlate'] = $model->vehicleLicensePlate;

        // Выставляю выбранное значение для идентификации ТС (по VIN, кузову или шасси)
        foreach (['vehicleVin' => 'vin', 'vehicleBodyNum' => 'bodyNum', 'vehicleChassisNum' => 'chassisNum'] as $identityCarParam => $requestDataKeyName) {
            if (empty($model->{$identityCarParam})) continue;
            $requestData['vehicle'][$requestDataKeyName] = $model->{$identityCarParam};
        }

        $requestData['vehicle']['docType'] = (int)$model->vehicleDocType;
        $requestData['vehicle']['docSerial'] = (string)$model->vehicleDocSerial;
        $requestData['vehicle']['docNumber'] = (string)$model->vehicleDocNumber;
        $requestData['vehicle']['docDate'] = \Yii::$app->formatter->asDate($model->vehicleDocDate, 'php:Y-m-d');
        $requestData['vehicle']['brand'] = (string)$vehicleBrand;
        $requestData['vehicle']['model'] = (string)$vehicleModel;
        $requestData['vehicle']['weight'] = (int)$model->vehicleWeight;
        $requestData['vehicle']['maxWeight'] = (int)$model->vehicleMaxWeight;
        $requestData['vehicle']['seats'] = (int)$model->vehicleSeats;
        $requestData['vehicle']['dc'] = (string)$model->vehicleDc;
        $requestData['vehicle']['dcDate'] = \Yii::$app->formatter->asDate($model->vehicleDcDate, 'php:Y-m-d');
        $requestData['vehicle']['dcIssueDate'] = \Yii::$app->formatter->asDate($model->vehicleIssueDate, 'php:Y-m-d');
        $requestData['owner']['passportForeign'] = !empty($model->ownerPassportType) ? FinalCalculationDictionary::PASSPORT_TYPE_FOREIGN : FinalCalculationDictionary::PASSPORT_TYPE_RU;
        $requestData['owner']['passportSerial'] = (string)$model->ownerPassportSerial;
        $requestData['owner']['passportNumber'] = (string)$model->ownerPassportNumber;
        $requestData['owner']['passportDate'] = \Yii::$app->formatter->asDate($model->ownerPassportDate, 'php:Y-m-d');
        $requestData['owner']['lastname'] = (string)$model->ownerLastName;
        $requestData['owner']['firstname'] = (string)$model->ownerFirstName;
        $requestData['owner']['middlename'] = (string)$model->ownerMiddleName;
        $requestData['owner']['birthdate'] = \Yii::$app->formatter->asDate($model->ownerBirthDate, 'php:Y-m-d');
        if ($model->ownerDadataAddress) {
            $search = \Yii::$app->api->requestWithInfo('address/search/detail', 'GET', ['query' => trim($model->ownerDadataAddress)]);
            if (!empty($search['data']['items'])) {
                $requestData['owner']['dadata'] = current($search['data']['items']);
            }
        }
        $requestData['email'] = $model->email;
        $requestData['purpose'] = $model->purpose;
        $requestData['name'] =  $model->ownerFirstName;

        $requestData['insurerIsOwner'] = (int)$model->insurerIsOwner;
        if ((int)$model->insurerIsOwner == FinalCalculationDictionary::ONE_FACE_NO) {
            $requestData['insurer']['lastname'] = $model->insurerLastName;
            $requestData['insurer']['firstname'] = $model->insurerFirstName;
            $requestData['insurer']['middlename'] = $model->insurerMiddleName;
            $requestData['insurer']['birthdate'] = \Yii::$app->formatter->asDate($model->insurerBirthDate, 'php:Y-m-d');
            $requestData['insurer']['passportForeign'] = !empty($model->insurerPassportType)
                ? FinalCalculationDictionary::PASSPORT_TYPE_FOREIGN
                : FinalCalculationDictionary::PASSPORT_TYPE_RU;
            $requestData['insurer']['passportSerial'] = (string)$model->insurerPassportSerial;
            $requestData['insurer']['passportNumber'] = (string)$model->insurerPassportNumber;
            $requestData['insurer']['passportDate'] = \Yii::$app->formatter->asDate($model->insurerPassportDate, 'php:Y-m-d');
            if ($model->insurerDadataAddress) {
                $search = \Yii::$app->api->requestWithInfo('address/search/detail', 'GET', ['query' => trim($model->insurerDadataAddress)]);
                if (!empty($search['data']['items'])) {
                    $requestData['insurer']['dadata'] = current($search['data']['items']);
                }
            }
        }

        // Формирую номер телефона по шаблону +7 (999) 111-11-11
        $phone = preg_replace("/[^0-9]/", '', $model->ownerPhone);
        $requestData['phone'] = "+" . substr($phone, 0, 1)
            . " (" . substr($phone, 1, 3) . ") "
            . substr($phone, 4, 3) . "-"
            . substr($phone, 7, 2) . "-"
            . substr($phone, 8, 2);

        $requestData['date'] = \Yii::$app->formatter->asDate($model->ownerDateOSAGOStart, 'php:Y-m-d');

        foreach ($model->driverExpDate as $key => $driverExpDate) {
            $requestData['drivers'][$key]['expdate'] = \Yii::$app->formatter->asDate($driverExpDate, 'php:Y-m-d');
        }

        return $requestData;
    }
}