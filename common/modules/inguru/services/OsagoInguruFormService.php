<?php
namespace common\modules\inguru\services;

use common\modules\inguru\dictionaries\OsagoInguruFormDictionary;
use Yii;
use common\models\Service;

/**
 * Class OsagoInguruFormService
 * @package common\modules\inguru\services
 */
class OsagoInguruFormService extends Service
{
    /**
     * Сохраняет в сессию предложения от страховых компаний, что бы после выбора предложения взять от туда цену, комиссию и тд
     * @param array $requestResultsData
     */
    public static function setSessionInsureData(array $requestResultsData)
    {
        foreach ($requestResultsData as $result) {
            $eId = $result['eId'];
            $sk = $result['sk'];
            $session = [
                'eId' => $eId,
                'sk' => $sk,
                'price' => $result['total'],
                'commission' => $result['commission']
            ];

            Yii::$app->session->set(self::getSessionInsureName($eId, $sk), $session);
        }
    }

    /**
     * Возвращает данные из сессии
     * @param int $eId
     * @param string $sk
     * @return mixed
     */
    public static function getSessionInsureData(int $eId, string $sk)
    {
        $session = self::getSessionInsureName($eId, $sk);
        return Yii::$app->session->get($session);
    }

    /**
     * Возвращает имя для сесии записи информации от страховых компаний
     * @param int $eId
     * @param string $sk
     * @return string
     */
    private static function getSessionInsureName(int $eId, string $sk)
    {
        return OsagoInguruFormDictionary::SESSION_INSURE_DATA_NAME . '-' . $eId . $sk;
    }
}