<?php
namespace common\modules\inguru\services;

use common\modules\inguru\dictionaries\FinalCalculationDictionary;
use common\modules\inguru\dictionaries\PaymentsDictionary;
use common\modules\inguru\forms\OsagoInguruForm;
use common\modules\inguru\interfaces\ApiInterface;
use common\modules\inguru\traits\RequestApiTrait;
use common\models\Service;
use yii\helpers\Json;
use yii\httpclient\Client;
use common\modules\inguru\traits\AuthTrait;
/**
 * Class PaymentsService
 * @package common\modules\inguru\services
 */
class PaymentsService extends Service implements ApiInterface
{
    use AuthTrait;
    use RequestApiTrait;

    /**
     * Возвращает URL ИНГУРУ для отправки запроса
     *
     * @return string
     */
    public function getApiUrl()
    {
        return 'https://api.inguru.ru/eosago/6.0';
    }

    /**
     * Метод возвращающий данные для отправки в АПИ ИНГУРУ
     *
     * @param $model
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function getApiDataByModel($model)
    {
        $requestData = [];
        $requestData['q'] = PaymentsDictionary::INGURU_CONST;
        $requestData['eId'] = $model->eId;
        $requestData['successUrl'] = $model->successUrl;
        $requestData['failUrl'] = $model->failUrl;
        $requestData['smsCode'] = $model->smsCode;

        return $requestData;
    }

    /**
     * @return mixed|string
     */
    public function getApiCurlFormat()
    {
        return Client::FORMAT_CURL;
    }

    /**
     * @return string
     */
    public function getApiCurlMethod()
    {
        return 'get';
    }

    /**
     * Возвращает ссылку на оплату или сообщение об ошибке
     * @param OsagoInguruForm $model
     * @param $company
     * @return mixed|null
     * @throws \yii\base\InvalidConfigException
     */
    public function getPaymentLinkResponse(OsagoInguruForm $model, $company)
    {
        $service = new self();
        $urlRequest = $service->getApiUrl();
        $paramsRequest = $service->getApiDataByModel($model);

        // Если это запрос в Абсолют Страхование - отправка через yii\httpclient\Client не работает.
        // ИНГУРУ говорят, что для Абсолют Страхование не принимает запрос икранированием. Пока делаю прямой запрос через curl
        if (in_array($company, [FinalCalculationDictionary::INSURANCE_COMPANY_ABSOLUT])) {
            $ch = curl_init();
            $paramRequestNum = 0;
            foreach ($paramsRequest as $keyRequest => $valueRequest) {
                $valueRequest = curl_escape($ch, $valueRequest);
                $urlRequest .= $paramRequestNum == 0 ? "?" : "&";
                $urlRequest .= "$keyRequest={$valueRequest}";
                ++$paramRequestNum;
            }

            $curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_URL => $urlRequest,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_HTTPHEADER => [
                    'Authorization:' . $this->getToken(),
                ],
            ]);
            $response = curl_exec($curl);
            curl_close($curl);
            $responseResult = !empty($response) && $this->isJson($response) ? Json::decode($response) : null;
        }
        else {
            $response = $this->request(
                $urlRequest,
                $this->getToken(),
                $paramsRequest,
                $service->getApiCurlFormat(),
                $service->getApiCurlMethod()
            );
            $responseResult = $response->isOk ? $response->data : null;
        }

        return $responseResult;
    }
}