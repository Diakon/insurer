<?php
namespace common\modules\inguru\services;

use common\modules\inguru\dictionaries\StatusOrderDictionary;
use common\modules\inguru\interfaces\ApiInterface;
use common\models\Service;
use yii\httpclient\Client;

/**
 * Class StatusOrderService
 * @package common\modules\inguru\services
 */
class StatusOrderService extends Service implements ApiInterface
{
    /**
     * Возвращает URL ИНГУРУ для отправки запроса
     *
     * @return string
     */
    public function getApiUrl()
    {
        return 'https://api.inguru.ru/eosago/6.0';
    }

    /**
     * Метод возвращающий данные для отправки в АПИ ИНГУРУ
     *
     * @param $model
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function getApiDataByModel($model)
    {
        $requestData = [];
        $requestData['q'] = StatusOrderDictionary::INGURU_CONS;
        $requestData['eId'] = $model->eId;

        return $requestData;
    }

    /**
     * @return mixed|string
     */
    public function getApiCurlFormat()
    {
        return Client::FORMAT_CURL;
    }

    /**
     * @return string
     */
    public function getApiCurlMethod()
    {
        return 'get';
    }
}