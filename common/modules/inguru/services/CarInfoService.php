<?php
namespace common\modules\inguru\services;

use common\modules\cars\models\Car;
use common\modules\cars\dictionaries\CarDictionary;
use common\modules\inguru\dictionaries\CarInfoDictionary;
use common\modules\inguru\interfaces\ApiInterface;
use common\models\Service;
use common\modules\inguru\models\CarInfo;
use common\modules\inguru\traits\AuthTrait;
use common\modules\inguru\traits\RequestApiTrait;
use yii\httpclient\Client;

/**
 * Class CarInfoService
 * @package common\modules\inguru\services
 */
class CarInfoService extends Service implements ApiInterface
{
    use AuthTrait;
    use RequestApiTrait;

    /**
     * Возвращает URL ИНГУРУ для отправки запроса
     *
     * @return string
     */
    public function getApiUrl()
    {
        return 'https://api.inguru.ru/eosago/6.0';
    }

    /**
     * Метод возвращающий данные для отправки в АПИ ИНГУРУ
     *
     * @param $model
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function getApiDataByModel($model)
    {
        $vehicleLicensePlate = preg_replace('/[^а-яё\d]/ui', '',  $model->vehicleLicensePlate);
        $vehicleLicensePlate = str_replace(' ', '', $vehicleLicensePlate);
        $vehicleLicensePlate = trim($vehicleLicensePlate);

        $requestData = [];
        $requestData['q'] = CarInfoDictionary::VEHICLE_CONS;
        $requestData['licensePlate'] = $vehicleLicensePlate;

        return $requestData;
    }

    /**
     * @return mixed|string
     */
    public function getApiCurlFormat()
    {
        return Client::FORMAT_CURL;
    }

    /**
     * @return string
     */
    public function getApiCurlMethod()
    {
        return 'get';
    }

    /**
     * Устанавливает параметры модели Car по данным из API запроса в Ингуру
     *
     * @param array $response
     * @return Car
     */
    public function setCarModelByApi(array $response)
    {
        $model = new Car();

        $response = $response['results'] ?? null;
        if (empty($response)) {
            return $model;
        }

        $docs = !empty($response['vehicle']['docs'][0]) ? $response['vehicle']['docs'][0] : [];

        $model->vin = $response['vehicle']['vin'] ?? null;
        $model->body_num = $response['vehicle']['bodyNum'] ?? null;
        $model->chassis_num = $response['vehicle']['chassisNum'] ?? null;
        $model->year = $response['vehicle']['year'] ?? null;
        $model->power = $response['vehicle']['power'] ?? null;
        $model->category = array_search($response['vehicle']['category'], CarDictionary::CATEGORY_CARS);
        $model->type = $response['vehicle']['type'] ?? null;
        $model->doc_type = $docs['docType'] ?? null;
        $model->doc_serial = $docs['docSerial'] ?? null;
        $model->doc_number = $docs['docNumber'] ?? null;
        $model->doc_date = $docs['docDate'] ?? null;
        $model->brand = $response['vehicle']['brand'] ?? null;
        $model->model = $response['vehicle']['model'] ?? null;
        $model->model_brand_doc = $response['vehicle']['brand_model'] ?? null;
        $model->diag_card_number = $response['vehicle']['dc'] ?? null;
        $model->diag_card_date = $response['vehicle']['dcDate'] ?? null;
        $model->diag_card_issue_date = $response['vehicle']['dcIssueDate'] ?? null;

        return $model;
    }

    /**
     * Возвращает данные об авто по гос. номеру от сервиса ИНГУРУ
     * @param string $carNumber
     * @return array|Car
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public static function getCarDataFromApi(string $carNumber)
    {
        $model = new CarInfo();
        $model->vehicleLicensePlate = $carNumber;
        if (!$model->validate()) {
            return [];
        }

        $service = new self();
        $response = $service->request(
            $service->getApiUrl(),
            $service->getToken(),
            $service->getApiDataByModel($model),
            $service->getApiCurlFormat(),
            $service->getApiCurlMethod()
        );
        if ($response->isOk) {
            $response = $response->data;
            if (empty($response['errors'])) {
                $car = $service->setCarModelByApi($response);
            }
        }

        return !empty($car) ? $car : [];
    }
}