<?php
namespace common\modules\inguru\models;

use common\modules\inguru\dictionaries\FinalCalculationDictionary;
use common\modules\inguru\dictionaries\PreCalculationDictionary;
use yii\helpers\ArrayHelper;

/**
 * Class FinalCalculation
 * @package common\modules\inguru\models
 */
class FinalCalculation extends PreCalculation
{
    /**
     * Массив идентификаторов страховых компаний, по которым проводить оформление полиса
     * @var array
     */
    public $insuranceCompanies = [];

    /**
     * VIN номер автомобиля.
     * @var string
     */
    public $vehicleVin;

    /**
     * Номер кузова. Обязательный параметр в случае отсутствия VIN
     * @var string
     */
    public $vehicleBodyNum;

    /**
     * Номер шасси. Обязательный параметр в случае отсутствия VIN
     * @var string
     */
    public $vehicleChassisNum;

    /**
     * Документ на автомобиль
     * @var integer
     */
    public $vehicleDocType;

    /**
     * Серия документа на автомобиль. Не требуется в случае ЭПТС.
     * @var string
     */
    public $vehicleDocSerial;

    /**
     * Номер документа на автомобиль.
     * @var string
     */
    public $vehicleDocNumber;

    /**
     * Дата выдачи документа на автомобиль в формате yyyy-mm-dd.
     * @var string
     */
    public $vehicleDocDate;

    /**
     * Наименование марки автомобиля из словаря страховой компании или словаря INGURU.
     * @var string
     */
    public $vehicleBrand;

    /**
     * Наименование модели автомобиля из словаря страховой компании или словаря INGURU
     * @var string
     */
    public $vehicleModel;

    /**
     * Масса без нагрузки, килограмм.
     * Требуется только для грузовиков, т.е. если vehicle.type=5 или vehicle.type=6
     * @var float
     */
    public $vehicleWeight;

    /**
     * Разрешенная максимальная масса, килограмм.
     * Требуется только для грузовиков, т.е. если vehicle.type=5 или vehicle.type=6
     * @var float
     */
    public $vehicleMaxWeight;

    /**
     * Число пассажирских мест.
     * Требуется только для автобусов, т.е. если vehicle.type=3 или vehicle.type=4
     * @var float
     */
    public $vehicleSeats;

    /**
     * Номер диагностической карты.
     * Не требуется для легковых автомобилей и мотоциклов в первые четыре года, включая год выпуска.
     * @var string
     */
    public $vehicleDc;

    /**
     * Срок действия диагностической карты в формате yyyy-mm-dd.
     * Не требуется для легковых автомобилей и мотоциклов в первые четыре года, включая год выпуска.
     * @var string
     */
    public $vehicleDcDate;

    /**
     * Дата выдачи диагностической карты в формате yyyy-mm-dd.
     * Требуется для оформления полиса в компаниях «РЕСО», «ВСК» и «Согласие», если заполнен параметр vehicle.dc
     * @var string
     */
    public $vehicleIssueDate;

    /**
     * Серия паспорта владельца автомобиля
     * @var string
     */
    public $ownerPassportSerial;

    /**
     * Номер паспорта владельца автомобиля.
     * @var string
     */
    public $ownerPassportNumber;

    /**
     * Тип паспорта (РФ или Иностранный) собственника ТС
     * @var int
     */
    public $ownerPassportType;

    /**
     * Тип паспорта (РФ или Иностранный) страхователя
     * @var int
     */
    public $insurerPassportType;

    /**
     * Дата выдачи паспорта владельца автомобиля в формате yyyy-mm-dd.
     * Требуется для оформления полиса в компаниях «Тинькофф Страхование», «ВСК», «Гайде», «Астро-Волга», «МАКС».
     * @var string
     */
    public $ownerPassportDate;

    /**
     * Адрес электронной почты страхователя, на который будет выслан полис.
     * @var string
     */
    public $email;

    /**
     * Цель использования автомобиля
     * @var string
     */
    public $purpose;

    /**
     * Фамилия владельца автомобиля
     * @var string
     */
    public $ownerLastName;

    /**
     * Имя владельца автомобиля
     * @var string
     */
    public $ownerFirstName;

    /**
     * Отчество владельца автомобиля
     * @var string
     */
    public $ownerMiddleName;

    /**
     * Дата рождения владельца автомобиля в формате yyyy-mm-dd.
     * @var string
     */
    public $ownerBirthDate;

    /**
     * Телефон страхователя в формате +7 (999) 111-11-11.
     * @var string
     */
    public $ownerPhone;

    /**
     * Дата начала действия полиса.
     * Дата должна быть не ранее чем на следующий день от текущей даты, например, если сегодня 10.12.18, то дата начала действия полиса может быть не ранее 11.12.18.
     * @var string
     */
    public $ownerDateOSAGOStart;

    /**
     * Признак того, что страхователь и владелец автомобиля одно лицо (0 или 1).
     * @var integer
     */
    public $insurerIsOwner;

    /**
     * Дата начала стажа водителя в формате yyyy-mm-dd
     * @var array
     */
    public $driverExpDate = [];

    /**
     * Адрес регистрации владельца авто от API стандартизации адресов dadata (как объект dadata)
     * @var object
     */
    public $ownerDadataAddress;


    /**
     * Адрес регистрации страхователя от API стандартизации адресов dadata (как объект dadata)
     * @var object
     */
    public $insurerDadataAddress;

    /**
     * Фамилия владельца автомобиля
     * @var string
     */
    public $insurerLastName;

    /**
     * Имя владельца автомобиля
     * @var string
     */
    public $insurerFirstName;

    /**
     * Отчество владельца автомобиля.
     * @var string
     */
    public $insurerMiddleName;

    /**
     * Дата рождения владельца автомобиля в формате yyyy-mm-dd.
     * @var string
     */
    public $insurerBirthDate;

    /**
     * Серия паспорта владельца автомобиля
     * @var string
     */
    public $insurerPassportSerial;

    /**
     * Номер паспорта владельца автомобиля.
     * @var string
     */
    public $insurerPassportNumber;

    /**
     * Дата выдачи паспорта владельца автомобиля в формате yyyy-mm-dd.
     * @var string
     */
    public $insurerPassportDate;

    /**
     * Гос. номер
     * @var string
     */
    public $vehicleLicensePlate;

    /**
     * Признак управления транспортным средством с прицепом
     * @var string
     */
    public $useTrailer;

    /**
     * Массив со списком пар Бренд/Модель ТС заданные для страховых компаний
     * @var array
     */
    public $insurerCustomCarBrandModel = [];

    /**
     * @return array
     */
    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [[
                    'insuranceCompanies',
                    'vehicleDocType',
                    'vehicleDocSerial',
                    'vehicleDocNumber',
                    'vehicleDocDate',
                    'vehicleBrand',
                    'vehicleModel',
                    'ownerPassportSerial',
                    'ownerPassportNumber',
                    'ownerPassportDate',
                    'email',
                    'purpose',
                    'ownerPhone',
                    'ownerDateOSAGOStart',
                    'driverExpDate',
                    'ownerLastName',
                    'ownerFirstName',
                    'ownerMiddleName',
                    'ownerBirthDate',
                    'insurerIsOwner',
                    'ownerDadataAddress',
                ], 'required'],
                [[
                    'insurerDadataAddress',
                    'insurerLastName',
                    'insurerFirstName',
                    'insurerMiddleName',
                    'insurerBirthDate',
                    'insurerPassportSerial',
                    'insurerPassportNumber',
                    'insurerPassportDate'
                ], 'required', 'when' => function ($model) {
                    return $model->insurerIsOwner == FinalCalculationDictionary::ONE_FACE_NO;
                }],

                [[
                    'vehicleVin',
                ], 'required', 'when' => function ($model) {
                    return empty($model->vehicleBodyNum) && empty($model->vehicleChassisNum);
                }],
                [[
                    'vehicleBodyNum',
                ], 'required', 'when' => function ($model) {
                    return empty($model->vehicleVin) && empty($model->vehicleChassisNum);
                }],
                [[
                    'vehicleChassisNum',
                ], 'required', 'when' => function ($model) {
                    return empty($model->vehicleBodyNum) && empty($model->vehicleVin);
                }],

                [[
                    'insurerLastName',
                    'insurerFirstName',
                    'insurerMiddleName',
                    'insurerPassportSerial',
                    'insurerPassportNumber',
                ], 'string'],
                [['insurerDadataAddress', 'insurerCustomCarBrandModel'], 'safe'],
                ['email', 'email'],
                [['useTrailer'], 'default', 'value' => FinalCalculationDictionary::USE_TRAILER_NO],
                [['purpose'], 'in', 'range' => FinalCalculationDictionary::CAR_PURPOSES],
                [['ownerDateOSAGOStart', 'ownerBirthDate', 'insurerBirthDate', 'insurerPassportDate'], 'date', 'format' => 'php:Y-m-d'],
                [['insuranceCompanies'], 'in', 'range' => FinalCalculationDictionary::INSURANCE_COMPANIES],
                [['vehicleWeight', 'vehicleMaxWeight'], 'required', 'when' => function ($model) {
                    return in_array($model->vehicleType, [PreCalculationDictionary::VEHICLE_TYPE_TRUCK, PreCalculationDictionary::VEHICLE_TYPE_TRUCK_MORE_16]);
                }],
                [['vehicleSeats'], 'required', 'when' => function ($model) {
                    return in_array($model->vehicleType, [PreCalculationDictionary::VEHICLE_TYPE_BUS, PreCalculationDictionary::VEHICLE_TYPE_BUS_MORE_16]);
                }],
                // Данные диагностической карты не требуется для легковых автомобилей и мотоциклов в первые четыре года, включая год выпуска
                [['vehicleDc', 'vehicleDcDate'], 'required', 'when' => function ($model) {
                    $isRequired = true;
                    if (in_array($model->vehicleType, [PreCalculationDictionary::VEHICLE_TYPE_CAR, PreCalculationDictionary::VEHICLE_TYPE_MOTO])) {
                        $year = (int)date('Y', time()) - (int)$model->vehicleYear;
                        $isRequired = $year > 4;
                    }
                    return $isRequired;
                }],
                // Требуется для оформления полиса в компаниях «РЕСО», «ВСК» и «Согласие», если заполнен параметр vehicle.dc
                [['vehicleIssueDate'], 'required', 'when' => function ($model) {
                    return in_array($model->insuranceCompanies, [
                        FinalCalculationDictionary::INSURANCE_COMPANY_RESO,
                        FinalCalculationDictionary::INSURANCE_COMPANY_VSK,
                        FinalCalculationDictionary::INSURANCE_COMPANY_SOGLASIE,
                    ]) && !empty($model->vehicleDc);
                }],
                [[
                    'vehicleVin',
                    'vehicleBodyNum',
                    'vehicleChassisNum',
                    'insuranceCompanies',
                    'vehicleDocType',
                    'vehicleDocSerial',
                    'vehicleDocNumber',
                    'vehicleDocDate',
                    'vehicleBrand',
                    'vehicleModel',
                    'ownerPassportSerial',
                    'ownerPassportNumber',
                    'ownerPassportDate',
                    'email',
                    'vehicleLicensePlate'
                ], 'trim'],
                ['email', 'email'],

                ['ownerPhone', 'match', 'pattern' => "/[^0-9]/"],
                [['ownerPhone'], 'string', 'min' => 10, 'max' => 10],
                [['ownerLastName', 'ownerMiddleName', 'ownerFirstName', 'vehicleLicensePlate'], 'string'],
                [['ownerPassportSerial'], 'string', 'min' => 4, 'max' => 4],
                [['ownerPassportNumber'], 'string', 'min' => 6, 'max' => 6],
                [['vehicleVin', 'vehicleModel',  'vehicleBrand', 'vehicleDc', 'vehicleBodyNum', 'vehicleChassisNum'], 'string'],
                ['insuranceCompanies', 'each', 'rule' => ['integer']],
                [['ownerPassportType'], 'in', 'range' => array_keys(FinalCalculationDictionary::PASSPORT_TYPES)],
                [['insurerPassportType'], 'in', 'range' => array_keys(FinalCalculationDictionary::PASSPORT_TYPES)],
                [['vehicleDocType'], 'in', 'range' => array_keys(FinalCalculationDictionary::CAR_DOCS)],
                [['insurerIsOwner'], 'in', 'range' => array_keys(FinalCalculationDictionary::ONE_FACES)],
                [['useTrailer'], 'in', 'range' => array_keys(FinalCalculationDictionary::USE_TRAILER)],
                [[
                    'vehicleDocType',
                    'vehicleDocSerial',
                    'vehicleDocNumber',
                    'vehicleWeight',
                    'vehicleMaxWeight',
                    'vehicleSeats',
                    'ownerPassportType',
                    'insurerPassportType'
                ], 'integer'],
                [
                    [
                        'driverExpDate',
                    ], 'safe'],
                [['vehicleDocDate', 'vehicleDcDate', 'vehicleIssueDate', 'ownerPassportDate'], 'date', 'format' => 'php:Y-m-d'],
            ]);
    }
}