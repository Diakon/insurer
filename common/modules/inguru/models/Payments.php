<?php
namespace common\modules\inguru\models;

use common\models\Model;
use common\modules\orders\models\Order;

/**
 * Оплата полиса
 * Class Payments
 * @package common\moduls\inguru\models
 */
class Payments extends Model
{
    /**
     * Идентификатор результата оформления полиса
     * @var string
     */
    public $eId;

    /**
     * Адрес, на который надо перенаправить пользователя в случае успешной оплаты.
     * Поддерживается не всеми страховыми компаниями. Адрес должен быть указан полностью, включая используемый протокол
     * При отсутствии параметра используется адрес по-умолчанию https://www.inguru.ru/kalkulyator_osago_pokupka#success=1
     * @var string
     */
    public $successUrl;

    /**
     * Адрес, на который надо перенаправить пользователя в случае неуспешной оплаты.
     * Поддерживается не всеми страховыми компаниями. Адрес должен быть указан полностью, включая используемый протокол.
     * При отсутствии параметра используется адрес по-умолчанию https://www.inguru.ru/kalkulyator_osago_pokupka#fail=1
     * @var string
     */
    public $failUrl;

    /**
     * Код подтверждения номера телефона.
     * Данный параметр следует передавать в повторном запросе только если в результате ответа на первый запрос
     * пришел флаг необходимости подтверждения номера {"needSmsCode": 1}.
     * Подтверждение номера телефона требуется только для компании «ВСК».
     * @var string
     */
    public $smsCode;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['eId', 'successUrl', 'failUrl'], 'required'],
            [['successUrl', 'failUrl', 'smsCode', 'eId'], 'trim'],
            [['successUrl', 'failUrl', 'smsCode', 'successUrl', 'failUrl', 'eId'], 'string'],
        ];
    }
}