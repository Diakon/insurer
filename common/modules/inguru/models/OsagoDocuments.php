<?php
namespace common\modules\inguru\models;

use common\models\Model;

/**
 * Получение документов по ОСАГО
 *
 * Class OsagoDocuments
 * @package common\moduls\inguru\models
 */
class OsagoDocuments extends Model
{
    /**
     * Идентификатор результата оформления полиса
     * @var string
     */
    public $eId;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['eId'], 'required'],
            [['eId'], 'trim'],
            [['eId'], 'string'],
        ];
    }
}