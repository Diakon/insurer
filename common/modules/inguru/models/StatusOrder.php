<?php
namespace common\modules\inguru\models;

use common\models\Model;

/**
 * Информация об оплате заказа в ИНГУРУ
 * Class StatusOrder
 * @package common\moduls\inguru\models
 */
class StatusOrder extends Model
{
    /**
     * ID заказа в системе ИНГУРУ
     * @var integer
     */
    public $eId;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['eId'], 'required'],
            [['eId'], 'trim'],
            ['eId', 'safe']
        ];
    }
}