<?php
namespace common\modules\inguru\models;

use common\models\Model;

/**
 * Код города в системе КЛАДР от Ингуру
 * Class Kladr
 * @package common\modules\inguru\models
 */
class Kladr extends Model
{
    /**
     * Строка с названием населенного пункта
     * @var string
     */
    public $searchName;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['searchName'], 'required'],
            [['searchName'], 'string', 'min' => 3],
        ];
    }
}