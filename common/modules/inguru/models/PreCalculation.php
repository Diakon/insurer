<?php
namespace common\modules\inguru\models;

use common\modules\inguru\dictionaries\PreCalculationDictionary;
use common\models\Model;

/**
 * Предварительный расчет стоимости ОСАГО
 * Class PreCalculation
 * @package common\moduls\inguru\models
 */
class PreCalculation extends Model
{
    /**
     * Идентификатор типа автомобиля из словаря «Типы ТС»
     * @var int
     */
    public $vehicleType;

    /**
     * Мощность автомобиля в лошадиных силах
     * @var int
     */
    public $vehiclePower;

    /**
     * Год выпуска автомобиля
     * @var int
     */
    public $vehicleYear;

    /**
     * Мультидрайв
     * @var int
     */
    public $multiDrive;

    /**
     * Признак точного расчета с запросом в РСА
     * @var int
     */
    public $extractCalculation;

    /**
     * Возраст водителя (в годах)
     * @var array
     */
    public $driversAge = [];

    /**
     * Стаж водителя (в годах)
     * @var array
     */
    public $driversExp = [];

    /**
     * Фамилия водителя. Требуется для точного расчета
     * @var array
     */
    public $driverLastName = [];

    /**
     * Имя водителя. Требуется для точного расчета
     * @var array
     */
    public $driverFirstName = [];

    /**
     * Отчество водителя. Требуется для точного расчета
     * @var array
     */
    public $driverMiddleName = [];

    /**
     * Дата рождения водителя
     * @var array
     */
    public $driverBirthDate = [];

    /**
     * Признак иностранных водительского удостоверения
     * @var array
     */
    public $driverLicenseForeign = [];

    /**
     * Серия водительского удостоверения
     * @var array
     */
    public $driverLicenseSerial = [];

    /**
     * Номер водительского удостоверения
     * @var array
     */
    public $driverLicenseNumber = [];

    /**
     * Серия предыдущего водительского удостоверения
     * @var array
     */
    public $driverPrevLicenseSerial = [];

    /**
     * Номер предыдущего водительского удостоверения
     * @var array
     */
    public $driverPrevLicenseNumber = [];

    /**
     * Дата выдачи предыдущего водительского удостоверения
     * @var array
     */
    public $driverPrevLicenseDate = [];

    /**
     * Фамилия водителя, которая использовалась в предыдущем водительском удостоверении
     * @var array
     */
    public $driverPrevLastName = [];

    /**
     * Имя водителя, которое использовалась в предыдущем водительском удостоверении
     * @var array
     */
    public $driverPrevFirstName = [];

    /**
     * Отчество водителя, которое использовалась в предыдущем водительском удостоверении
     * @var array
     */
    public $driverPrevMiddleName = [];

    /**
     * Код КЛАДР населенного пункта адреса регистрации владельца автомобиля
     * @var int
     */
    public $ownerCity;

    /**
     * Период использования (кол-во месяцев)
     * @var int
     */
    public $usePeriod;

    /**
     * Признак управления транспортным средством с прицепом
     *
     * @var bool
     */
    public $useTrailer;

    /**
     * Класс КБМ для применения к результатам предварительного расчета
     * @var string
     */
    public $applyKbm;

    /**
     * Дата заключения договора
     * Дата заключения договора.Дата заключения договора.
     * @var string
     */
    public $dateContract;

    /**
     * Серия предыдущего полиса
     * @var string
     */
    public $prevPolicySerial;

    /**
     * Номер предыдущего полиса
     * @var string
     */
    public $prevPolicyNumber;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['vehicleType'], 'required'],
            ['vehiclePower', 'required', 'when' => function ($model) {
                return in_array($model->vehicleType, array_keys(PreCalculationDictionary::VEHICLE_TYPES));
            }],
            ['driversAge', 'required', 'when' => function ($model) {
                return $model->multiDrive == PreCalculationDictionary::MULTI_DRIVE_YES;
            }],
            ['driversExp', 'required', 'when' => function ($model) {
                return $model->multiDrive == PreCalculationDictionary::MULTI_DRIVE_YES;
            }],
            [['driverMiddleName', 'driverLastName', 'driverFirstName', 'driverBirthDate'], 'required', 'when' => function ($model) {
                return $model->extractCalculation == PreCalculationDictionary::EXTRACT_CALC_YES && $model->multiDrive == PreCalculationDictionary::MULTI_DRIVE_NO;
            }],
            [['dateContract', 'driverBirthDate'], 'date', 'format' => 'php:Y-m-d'],
            [[
                'vehicleType',
                'vehiclePower',
                'vehicleYear',
                'multiDrive',
                'extractCalculation',
                'ownerCity',
                'useTrailer'
            ], 'integer'],

            [['usePeriod'], 'integer', 'max' => 12, 'min' => 1],
            ['driversAge', 'each', 'rule' => ['integer']],
            ['driversExp', 'each', 'rule' => ['integer']],
            [
                [
                    'driverMiddleName',
                    'driverLastName',
                    'driverFirstName',
                    'driverBirthDate',
                    'driverLicenseSerial',
                    'driverLicenseForeign',
                    'driverLicenseNumber',
                    'driverPrevLicenseSerial',
                    'driverPrevLicenseNumber',
                    'driverPrevLicenseDate',
                    'driverPrevLastName',
                    'driverPrevFirstName',
                    'driverPrevMiddleName',
                    'applyKbm'
                ],
                'safe'
            ],
            ['vehicleType', 'in', 'range' => array_keys(PreCalculationDictionary::VEHICLE_TYPES)],
            ['multiDrive', 'in', 'range' => [PreCalculationDictionary::MULTI_DRIVE_YES, PreCalculationDictionary::MULTI_DRIVE_NO]],
            ['useTrailer', 'in', 'range' => [PreCalculationDictionary::USE_TRAILER_NO, PreCalculationDictionary::USE_TRAILER_YES]],
            ['extractCalculation', 'in', 'range' => [PreCalculationDictionary::EXTRACT_CALC_YES, PreCalculationDictionary::EXTRACT_CALC_NO]],
            [
                [
                    'driverMiddleName',
                    'driverLastName',
                    'driverFirstName',
                    'driverLicenseSerial',
                    'driverLicenseNumber',
                    'driversAge',
                    'driversExp'
                ], 'validateDrivers'
            ],
            ['prevPolicySerial', 'required', 'when' => function ($model) {
                return !empty($model->prevPolicyNumber);
            }],
            ['prevPolicyNumber', 'required', 'when' => function ($model) {
                return !empty($model->prevPolicySerial);
            }],
            [['prevPolicySerial', 'prevPolicyNumber'], 'string'],
        ];
    }

    /**
     * Проверяет массив данных о водителях
     *
     * @param $attribute
     * @param $params
     */
    public function validateDrivers($attribute, $params)
    {
        if ($this->multiDrive == PreCalculationDictionary::MULTI_DRIVE_YES) {
            return true;
        }
        if (count($this->driversAge) != count($this->driversExp)) {
            $this->addError($attribute, 'Для водителя(ей) не заполнены пары возраст и стаж.');
        }
        // При точном расчете, требуется передавать ФИО водителя (водителей), даты рождения
        if ($this->extractCalculation == PreCalculationDictionary::EXTRACT_CALC_YES) {
            foreach (
                [
                    'driverMiddleName',
                    'driverLastName',
                    'driverFirstName',
                    'driverBirthDate',
                    'driverLicenseSerial',
                    'driverLicenseNumber',
                    'driversAge',
                    'driversExp'
                ] as $key) {
                if ($key == $attribute) {
                    continue;
                }
                $count = count($this->{$attribute});
                if (
                    $count != count($this->driverLastName) ||
                    $count != count($this->driverMiddleName) ||
                    $count != count($this->driverFirstName) ||
                    $count != count($this->driverBirthDate) ||
                    $count != count($this->driverLicenseSerial) ||
                    $count != count($this->driverLicenseNumber) ||
                    $count != count($this->driversAge) ||
                    $count != count($this->driversExp)
                ) {
                    $this->addError($attribute, "Не для всех водителей(я) заполнены $attribute.");
                }
            }
        }
    }
}