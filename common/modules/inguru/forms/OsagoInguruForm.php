<?php
namespace common\modules\inguru\forms;

use common\modules\inguru\dictionaries\FinalCalculationDictionary;
use common\modules\inguru\dictionaries\OsagoInguruFormDictionary;
use common\modules\inguru\dictionaries\PreCalculationDictionary;
use common\models\Model;
use frontend\modules\inguru\services\CarInfoService;
use yii\helpers\ArrayHelper;

/**
 * OSAGO Inguru form
 */
class OsagoInguruForm extends Model
{
    public $preCalculation;

    // Аттрибуты для раздела "Автомобиль"
    public $vehicleLicensePlate;
    public $vehicleType;
    public $vehiclePower;
    public $vehicleYear;
    public $vehicleVin;
    public $vehicleBodyNum;
    public $vehicleChassisNum;
    public $vehicleIdentity = OsagoInguruFormDictionary::CAR_HAVE_VIN;
    public $vehicleDocType;
    public $vehicleDocSerial;
    public $vehicleDocNumber;
    public $vehicleDocDate;
    public $vehicleBrand;
    public $vehicleModel;
    public $vehicleWeight;
    public $vehicleMaxWeight;
    public $vehicleSeats;
    public $vehicleDc;
    public $vehicleDcDate;
    public $vehicleIssueDate;

    // Аттрибуты для раздела "Водители"
    public $exactCalculation = 1;
    public $multiDrive;
    public $driversAge = [];
    public $driversExp = [];
    public $driverLastName = [];
    public $driverFirstName = [];
    public $driverMiddleName = [];
    public $driverBirthDate = [];
    public $driverLicenseSerial = [];
    public $driverLicenseNumber = [];
    public $driverLicenseForeign = [];
    public $driverExpDate = [];
    public $driverPrevAddInfo = [];
    public $driverPrevLastName = [];
    public $driverPrevFirstName = [];
    public $driverPrevMiddleName = [];
    public $driverPrevLicenseSerial = [];
    public $driverPrevLicenseNumber = [];
    public $driverPrevLicenseDate;

    // Аттрибуты для раздела "Собственник"
    public $ownerCity;
    public $ownerCityName;
    public $usePeriod;
    public $ownerPassportType;
    public $insurerPassportType;
    public $ownerPassportSerial;
    public $ownerPassportNumber;
    public $ownerPassportDate;
    public $email;
    public $purpose;
    public $ownerPhone;
    public $ownerDateOSAGOStart;
    public $ownerLastName;
    public $ownerFirstName;
    public $ownerMiddleName;
    public $ownerBirthDate;
    public $ownerDadataAddress;
    public $insurerIsOwner;
    public $insurerDadataAddress;
    public $insurerLastName;
    public $insurerFirstName;
    public $insurerMiddleName;
    public $insurerBirthDate;
    public $insurerPassportSerial;
    public $insurerPassportNumber;
    public $insurerPassportDate;
    public $useTrailer;
    public $prevPolicySerial;
    public $prevPolicyNumber;
    public $insurerCustomCarBrandModel = [];

    // Атрибуты для раздела "Полис"
    public $prevLicenseNumber;
    public $prevLicenseDate;

    // Атрибуты для оплаты
    public $eId;
    public $company;
    public $successUrl;
    public $failUrl;
    public $smsCode;
    public $paymentUrl;

    const SCENARIO_STEP_TYPE_CALC = 'type_calc';
    const SCENARIO_STEP_CAR_INFO = 'car';
    const SCENARIO_STEP_DRIVER = 'driver';
    const SCENARIO_STEP_OWNER = 'owner';
    const SCENARIO_STEP_POLICY = 'policy';
    const SCENARIO_STEP_CALCULATE = 'calc';
    const SCENARIO_STEP_PAYMENT = 'payment';
    const SCENARIO_VALIDATE_CAR_NUMBER = 'validate_car_number';

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();

        $scenarios[self::SCENARIO_VALIDATE_CAR_NUMBER] = ['vehicleLicensePlate'];
        $scenarios[self::SCENARIO_STEP_TYPE_CALC] = ['preCalculation'];
        $scenarios[self::SCENARIO_STEP_CAR_INFO] = [
            'preCalculation',
            'vehicleLicensePlate',
            'vehicleType',
            'vehiclePower',
            'vehicleYear',
            'vehicleIdentity',
            'vehicleVin',
            'vehicleBodyNum',
            'vehicleChassisNum',
            'vehicleDocType',
            'vehicleDocSerial',
            'vehicleDocNumber',
            'vehicleDocDate',
            'vehicleBrand',
            'vehicleModel',
            'vehicleWeight',
            'vehicleMaxWeight',
            'vehicleSeats',
            'vehicleDc',
            'vehicleDcDate',
            'vehicleIssueDate',
            'useTrailer',
        ];
        $scenarios[self::SCENARIO_STEP_DRIVER] = [
            'multiDrive',
            'preCalculation',
            'driversAge',
            'driversExp',
            'driverLastName',
            'driverFirstName',
            'driverMiddleName',
            'driverBirthDate',
            'driverLicenseSerial',
            'driverLicenseNumber',
            'driverLicenseForeign',
            'exactCalculation',
            'driverExpDate',
            'driverPrevAddInfo',
            'driverPrevLastName',
            'driverPrevFirstName',
            'driverPrevMiddleName',
            'driverPrevLicenseSerial',
            'driverPrevLicenseNumber',
            'driverPrevLicenseDate'
        ];
        $scenarios[self::SCENARIO_STEP_OWNER] = [
            'preCalculation',
            'ownerCity',
            'usePeriod',
            'ownerCityName',
            'ownerPassportType',
            'insurerPassportType',
            'ownerPassportSerial',
            'ownerPassportNumber',
            'ownerPassportDate',
            'email',
            'purpose',
            'ownerPhone',
            'ownerDateOSAGOStart',
            'ownerLastName',
            'ownerFirstName',
            'ownerMiddleName',
            'ownerBirthDate',
            'ownerDadataAddress',
            'insurerIsOwner',
            'insurerDadataAddress',
            'insurerLastName',
            'insurerFirstName',
            'insurerMiddleName',
            'insurerBirthDate',
            'insurerPassportSerial',
            'insurerPassportNumber',
            'insurerPassportDate'
        ];
        $scenarios[self::SCENARIO_STEP_POLICY] = ['preCalculation', 'prevPolicySerial', 'prevPolicyNumber'];
        $scenarios[self::SCENARIO_STEP_PAYMENT] = ['eId', 'successUrl', 'failUrl', 'paymentUrl', 'smsCode', 'company'];


        // Сценарий расчета - объединяет предыдущие шаги
        $scenarios[self::SCENARIO_STEP_CALCULATE] = ['insurerCustomCarBrandModel'];
        foreach (
            [
                self::SCENARIO_STEP_CAR_INFO,
                self::SCENARIO_STEP_DRIVER,
                self::SCENARIO_STEP_OWNER,
                self::SCENARIO_STEP_POLICY
            ] as $scenarioName) {
            $scenarios[self::SCENARIO_STEP_CALCULATE] = ArrayHelper::merge($scenarios[self::SCENARIO_STEP_CALCULATE], $scenarios[$scenarioName]);
        }

        return $scenarios;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        $rules = [];

        // Тип расчета - должен быть выбран для всех сценариев, от этого зависит валидация полей (валидация для полного или предварительного расчета)
        $rules[] = [['preCalculation'], 'required'];
        $rules[] = [['preCalculation'], 'integer'];
        $rules[] = [['preCalculation'], 'in', 'range' => array_keys($this->getTypeCalc())];

        // Сценарий валидации гос. номера авто
        if (in_array($this->scenario, [self::SCENARIO_VALIDATE_CAR_NUMBER])) {
            $rules[] = [['vehicleLicensePlate'], 'required'];
            $rules[] = [['vehicleLicensePlate'], 'validateCarNumber'];
        }

        // Сценарий проверки даных об авто и расчета
        if (in_array($this->scenario, [self::SCENARIO_STEP_CAR_INFO, self::SCENARIO_STEP_CALCULATE])) {
            // Поля необходимые к заполнению при предварительном расчете
            $rules[] = [['vehicleType', 'vehiclePower', 'vehicleYear'], 'required'];
            // Поля необходимые к заполнению при полном расчете
            $rules[] = [
                [
                    'vehicleIdentity',
                    'vehicleDocType',
                    'vehicleDocSerial',
                    'vehicleDocNumber',
                    'vehicleDocDate',
                    'vehicleBrand',
                    'vehicleModel'
                ], 'required', 'when' => function ($model) {
                    return $model->preCalculation == OsagoInguruFormDictionary::PRE_CALCULATION_NO;
                }];
            $rules[] = [['vehicleBrand', 'vehicleModel'], 'validateVehicleBrandModel'];
            $rules[] = [
                [
                    'vehicleVin',
                ], 'required', 'when' => function ($model) {
                    return $model->preCalculation == OsagoInguruFormDictionary::PRE_CALCULATION_NO && $model->vehicleIdentity == OsagoInguruFormDictionary::CAR_HAVE_VIN;
                }];
            $rules[] = [
                [
                    'vehicleBodyNum',
                ], 'required', 'when' => function ($model) {
                    return $model->preCalculation == OsagoInguruFormDictionary::PRE_CALCULATION_NO && $model->vehicleIdentity == OsagoInguruFormDictionary::CAR_HAVE_BODY;
                }];
            $rules[] = [
                [
                    'vehicleChassisNum'
                ], 'required', 'when' => function ($model) {
                    return $model->preCalculation == OsagoInguruFormDictionary::PRE_CALCULATION_NO && $model->vehicleIdentity == OsagoInguruFormDictionary::CAR_HAVE_CHASSIS;
                }];

            $rules[] = [['vehicleDocType'], 'in', 'range' => array_keys($this->getTypeDocCar())];
            $rules[] = [['vehicleIdentity'], 'in', 'range' => array_keys($this->getTypeCarIdentity())];

            // Гос. номер обязателен при полном расчете и если выбран тип документов на авто СТС
            $rules[] = [['vehicleLicensePlate'], 'required', 'when' => function ($model) {
                return
                    $model->preCalculation == OsagoInguruFormDictionary::PRE_CALCULATION_NO && $model->vehicleDocType == FinalCalculationDictionary::CAR_DOC_TYPE_STS;
            }];
            // Требуется только для грузовиков при полном расчете
            $rules[] = [['vehicleWeight', 'vehicleMaxWeight'], 'required', 'when' => function ($model) {
                return
                    $model->preCalculation == OsagoInguruFormDictionary::PRE_CALCULATION_NO &&
                    in_array($model->vehicleType, [PreCalculationDictionary::VEHICLE_TYPE_TRUCK, PreCalculationDictionary::VEHICLE_TYPE_TRUCK_MORE_16]);
            }];
            // Требуется только для автобусов при полном расчете
            $rules[] = [['vehicleSeats'], 'required', 'when' => function ($model) {
                return
                    $model->preCalculation == OsagoInguruFormDictionary::PRE_CALCULATION_NO &&
                    in_array($model->vehicleType, [PreCalculationDictionary::VEHICLE_TYPE_BUS, PreCalculationDictionary::VEHICLE_TYPE_BUS_MORE_16]);
            }];
            // При полном расчете требуются данные диагностической карты для легковых автомобилей и мотоциклов, если только им не 4 и менее лет выпуска
            $rules[] = [['vehicleDc', 'vehicleDcDate', 'vehicleIssueDate'], 'required', 'when' => function ($model) {
                if ($model->preCalculation == OsagoInguruFormDictionary::PRE_CALCULATION_YES) {
                    return false;
                }
                $isRequired = true;
                if (in_array($model->vehicleType, [PreCalculationDictionary::VEHICLE_TYPE_CAR, PreCalculationDictionary::VEHICLE_TYPE_MOTO])) {
                    $year = (int)date('Y', time()) - (int)$model->vehicleYear;
                    $isRequired = $year > 4;
                }
                return $isRequired;
            }];

            $rules[] = [['vehicleLicensePlate'], 'validateCarNumber'];
            $rules[] = [['vehicleDocDate', 'vehicleDcDate', 'vehicleIssueDate'], 'date', 'format' => 'php:Y-m-d'];
            $rules[] = [['vehicleDocDate'], 'validateVehicleDocDate'];
            $rules[] = [['vehicleYear'], 'integer', 'max' => (int)date('Y', time()), 'min' => 1900];
            $rules[] = [['vehiclePower'], 'integer', 'max' => 1000, 'min' => 1, 'message' => 'Мощность должна быть указана как число от 1 до 1000.'];
            $rules[] = [['vehiclePower', 'vehicleType'], 'integer'];
            $rules[] = [['vehicleType'], 'in', 'range' => array_keys($this->getTypesCar())];
            $rules[] = [['useTrailer'], 'in', 'range' => array_keys($this->getTrailer())];
            $rules[] = [['vehiclePower', 'vehicleType'], 'integer'];
            $rules[] = [['vehicleIssueDate', 'vehicleDcDate'], 'validateCarIssueAndDcDates'];
        }
        // Сценарий проверки даных о водителе (водителях) и расчета
        if (in_array($this->scenario, [self::SCENARIO_STEP_DRIVER, self::SCENARIO_STEP_CALCULATE])) {
            $rules[] = [['multiDrive'], 'required'];

            $rules[] = [
                [
                    'driversExp',
                    'driverMiddleName',
                    'driverLastName',
                    'driverFirstName',
                    'driverBirthDate',
                    'driverLicenseSerial',
                    'driverLicenseNumber',
                ],
                'required', 'when' => function ($model) {
                    return $model->multiDrive == OsagoInguruFormDictionary::IS_MULTI_DRIVE_NO;
                }
            ];
            // Требуется только при полном расчете
            $rules[] = [['driverExpDate'], 'required', 'when' => function ($model) {
                return $model->preCalculation == OsagoInguruFormDictionary::PRE_CALCULATION_NO;
            }];
            $rules[] = [['multiDrive', 'exactCalculation'], 'integer'];

            $rules[] = [[
                'driversExp',
                'driverMiddleName',
                'driverLastName',
                'driverFirstName',
                'driverBirthDate',
                'driverLicenseSerial',
                'driverLicenseNumber',
                'driverLicenseForeign',
                'driverExpDate',
                'driverPrevAddInfo',
                'driverPrevLastName',
                'driverPrevFirstName',
                'driverPrevMiddleName',
                'driverPrevLicenseSerial',
                'driverPrevLicenseNumber',
                'driverPrevLicenseDate'
            ], 'validateDrivers', 'skipOnEmpty' => false, 'skipOnError' => false];
        }
        // Сценарий проверки даных о собственнике и расчета
        if (in_array($this->scenario, [self::SCENARIO_STEP_OWNER, self::SCENARIO_STEP_CALCULATE])) {
            $rules[] = [['ownerCity', 'usePeriod'], 'required'];
            $rules[] = [['ownerCity', 'ownerCityName'], 'string'];
            $rules[] = [['usePeriod'], 'integer', 'max' => 12, 'min' => 3, 'message' => 'Период использования ТС должен быть указан в месяцах от 3 до 12'];

            // Требуется только при полном расчете
            $rules[] = [[
                'email',
                'ownerPassportSerial',
                'ownerPassportNumber',
                'ownerPassportDate',
                'purpose',
                'ownerPhone',
                'ownerDateOSAGOStart',
                'ownerLastName',
                'ownerFirstName',
                'ownerMiddleName',
                'ownerBirthDate',
                'insurerIsOwner',
                'ownerDadataAddress',
            ], 'required', 'when' => function ($model) {
                return $model->preCalculation == OsagoInguruFormDictionary::PRE_CALCULATION_NO;
            }];
            $rules[] = [[
                'insurerDadataAddress',
                'insurerLastName',
                'insurerFirstName',
                'insurerMiddleName',
                'insurerBirthDate',
                'insurerPassportSerial',
                'insurerPassportNumber',
                'insurerPassportDate'
            ], 'required', 'when' => function ($model) {
                return $model->preCalculation == OsagoInguruFormDictionary::PRE_CALCULATION_NO && $model->insurerIsOwner == FinalCalculationDictionary::ONE_FACE_NO;
            }];
            $rules[] = [['ownerLastName', 'ownerMiddleName', 'ownerFirstName', 'insurerLastName', 'insurerFirstName', 'insurerMiddleName'], 'string'];
            $rules[] = ['ownerPhone', 'match', 'pattern' => '/[0-9]/i'];
            $rules[] = [['ownerPhone'], 'string', 'min' => 11, 'max' => 11];
            $rules[] = [['ownerPhone'], 'validatePhone'];
            $rules[] = [['purpose'], 'in', 'range' => array_keys($this->getCarPurposes())];
            $rules[] = [['insurerIsOwner'], 'in', 'range' => array_keys($this->getOneFaces())];
            $rules[] = [['ownerPassportType', 'insurerPassportType'], 'in', 'range' => array_keys($this->getPassportTypes())];
            $rules[] = ['email', 'email'];
            $rules[] = [['ownerPassportDate', 'ownerDateOSAGOStart', 'ownerBirthDate', 'insurerBirthDate', 'insurerPassportDate'], 'date', 'format' => 'php:Y-m-d'];
            $rules[] = [['ownerPassportSerial', 'insurerPassportSerial'], 'string', 'min' => 4, 'max' => 4];
            $rules[] = [['ownerPassportNumber', 'insurerPassportNumber'], 'string', 'min' => 6, 'max' => 6];
            $rules[] = [['ownerPassportSerial', 'ownerPassportNumber', 'ownerLastName', 'ownerMiddleName', 'ownerFirstName'], 'trim'];
            $rules[] = [['insurerDadataAddress', 'ownerDadataAddress'], 'safe'];
            $rules[] = [['insurerDadataAddress', 'ownerDadataAddress'], 'validateDaData'];
            $rules[] = [['insurerCustomCarBrandModel'], 'safe']; // Массив с данными брен/модель для страховых компаний (бренд/модель у страховых компаний может различаться - иногда надо задавать их кастомно для конкретной страховой)
        }
        // Сценарий продления полиса и расчета
        if (in_array($this->scenario, [self::SCENARIO_STEP_POLICY, self::SCENARIO_STEP_CALCULATE])) {
            $rules[] = [['prevPolicySerial'], 'required', 'when' => function ($model) {
                return !empty($model->prevPolicyNumber);
            }, 'message' => 'Пожалуйста, укажите "{attribute}"'];
            $rules[] = [['prevPolicyNumber'], 'required', 'when' => function ($model) {
                return !empty($model->prevPolicySerial);
            }, 'message' => 'Пожалуйста, укажите "{attribute}"'];
            $rules[] = [['prevPolicySerial', 'prevPolicyNumber'], 'string'];
            $rules[] = [['prevPolicySerial', 'prevPolicyNumber'], 'trim'];
        }
        // Сценарий оплаты полиса
        if (in_array($this->scenario, [self::SCENARIO_STEP_PAYMENT])) {
            $rules[] = [['eId', 'successUrl', 'failUrl', 'paymentUrl'], 'required'];
            $rules[] = [['successUrl', 'failUrl', 'smsCode', 'eId', 'company'], 'trim'];
            $rules[] = [['successUrl', 'failUrl', 'smsCode', 'successUrl', 'failUrl', 'eId', 'company', 'paymentUrl'], 'string'];
        }

        return $rules;
    }

    /**
     * Проверяет, что дата выдачи документа на авто не больше текущей даты
     * @param $attribute
     * @param $params
     * @param $items
     */
    public function validateVehicleDocDate($attribute, $params, $items)
    {
        if (!empty($this->vehicleDocDate)) {
            $dateTime = strtotime($this->vehicleDocDate . ' 00:00:00');
            if ($dateTime > time()) {
                $this->addError($attribute, 'Дата выдачи документа на автомобиль не может быть больше текущей даты');
            }
        }
    }

    /**
     * @param $attribute
     * @param $params
     * @param $items
     */
    public function validatePhone($attribute, $params, $items)
    {
        $phone = preg_replace("/[^0-9]/", '', $this->{$attribute});
        if (!empty($phone) && substr($phone, 0, 2) != '79') {
            $this->addError($attribute, 'Неверно указан номер телефона. Номер телефона должен начинаться с +79');
        }
    }

    /**
     * @param $attribute
     * @param $params
     * @param $items
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function validateVehicleBrandModel($attribute, $params, $items)
    {
        $service = new CarInfoService();
        $errorMsg = "";
        if (!empty($this->{$attribute}) && $this->preCalculation == OsagoInguruFormDictionary::PRE_CALCULATION_NO) {
            switch ($attribute) {
                case "vehicleBrand":
                    $result = $service->searchBrandCarByName($this->{$attribute});
                    $errorMsg .= "Марка ТС " . trim($this->{$attribute}) . " не найдена.";
                    break;
                case "vehicleModel":
                    $result = $service->searchModelCarByName($this->vehicleModel, $this->vehicleBrand);
                    $errorMsg .= "Модель ТС " . trim($this->{$attribute});
                    if (!empty($this->vehicleBrand)) {
                        $errorMsg .= " для марки " . $this->vehicleBrand . " не найдена.";
                    }
                    break;
            }
            if (empty($result)) {
                $this->addError($attribute, $errorMsg);
            }
        }
    }

    /**
     * Проверяет, что дата выдачи диагностической карты не может быть больше 2х лет от текущей даты,
     * а срок действия диагностической карты не может быть меньше текущей даты
     * @param $attribute
     * @param $params
     * @param $items
     * @throws \yii\base\InvalidConfigException
     */
    public function validateCarIssueAndDcDates($attribute, $params, $items)
    {
        if (!empty($this->{$attribute}) && $this->preCalculation == OsagoInguruFormDictionary::PRE_CALCULATION_NO) {
            $issetDateErrorMsg = false;
            switch ($attribute) {
                // Проверяет, что дата выдачи диагностической карты не может быть больше 2х лет от текущей даты
                case 'vehicleIssueDate':
                    $validateDate = date("Y-m-d 23:59:59", strtotime("+2 year"));
                    $validateDate = strtotime($validateDate);
                    if (strtotime($this->{$attribute}) > $validateDate) {
                        $issetDateErrorMsg = "Дата выдачи диагностической карты не может быть больше " . \Yii::$app->formatter->asDate($validateDate, 'php:d.m.Y');
                    }
                    break;
                // Пролверяет, что срок действия диагностической карты не может быть меньше текущей даты
                case 'vehicleDcDate':
                    if (strtotime($this->{$attribute} . ' 23:59:59') < time()) {
                        $issetDateErrorMsg = "Срок действия диагностической карты не может быть меньше текущей даты";
                    }

                    break;
            }
            if ($issetDateErrorMsg) {
                $this->addError($attribute, $issetDateErrorMsg);
            }
        }
    }

    /**
     * Проверяет, есть ли введенный адрес в АПИ dadata
     *
     * @param $attribute
     * @param $params
     * @param $items
     */
    public function validateDaData($attribute, $params, $items)
    {
        $search = \Yii::$app->api->requestWithInfo('address/search/detail', 'GET', ['query' => $this->{$attribute}]);
        if (empty($search['data']['items'])) {
            $this->addError($attribute, "Адрес не найден в базе данных адресов. Пожалуйста, начните вводить адрес вручную и выберите нужный из выпадающего списка.");
        }
    }

    /**
     * Проверяет, что дата выдачи полиза больше текузей даты хотя бы на 1 день
     *
     * @param $attribute
     * @param $params
     * @param $items
     */
    public function validateOSAGOStartDate($attribute, $params, $items)
    {
        $date = $this->{$attribute};
        $dateTime = strtotime("$date 12:00:00");
        $minDate = date('Y-m-d', strtotime("+1 day"));
        $minDateTime = strtotime($minDate . " 00:00:00");
        if ($minDateTime > $dateTime) {
            $this->addError($attribute, 'Дата выдачи полиса должна быть больше текущей');
        }
    }

    /**
     * Валидация ввода гос. номера
     *
     * @param $attribute
     * @param $params
     * @param $items
     */
    public function validateCarNumber($attribute, $params, $items)
    {
        if (!preg_match('/^[АВЕКМНОРСТУХ]\d{3}(?<!000)[АВЕКМНОРСТУХ]{2}\d{2,3}$/ui', $this->{$attribute})) {
            $this->addError($attribute, 'Ошибка ввода гос. номера. Введите номер в формате а123яя45');
        }
    }

    /**
     * Валидация данных о водителях
     *
     * @param $attribute
     * @param $params
     * @param $items
     */
    public function validateDrivers($attribute, $params, $items)
    {
        // Если мультидрайв - проверять не надо заполненность массивов данных о водителях - эти данные вообще не надо писать
        if ($this->multiDrive == OsagoInguruFormDictionary::IS_MULTI_DRIVE_YES) {
            return true;
        }
        $countDrivers = 0;
        $labels = $this->attributeLabels();

        // Проверяю, что все массивы в drivers заполненны
        if (is_array($this->{$attribute})) {
            foreach ($this->{$attribute} as $key => $value) {
                ++$countDrivers;
                if ($countDrivers > OsagoInguruFormDictionary::MAX_DRIVERS_COUNT) {
                    $this->addError($attribute . '_' . $key,
                        'Превышено максимально возможное количество добавляемых водителей. Мкасимально возможное количество водителей: ' . OsagoInguruFormDictionary::MAX_DRIVERS_COUNT);
                    continue;
                }
                if (is_null($this->{$attribute}[$key]) || $this->{$attribute}[$key] == "") {
                    // Если это предварительный расчет и пришел пустой массив для полного расчета - пропускю без ошибки
                    if ($this->preCalculation == OsagoInguruFormDictionary::PRE_CALCULATION_YES
                        &&
                        in_array($attribute, ['driverExpDate']))
                    {
                        continue;
                    }
                    // Если это данные о предыдущем ВУ - проверять, только если driverPrevAddInfo == 1
                    if (strripos($attribute, 'driverPrev') !== false
                        &&
                        $this->driverPrevAddInfo[$key] != OsagoInguruFormDictionary::ADD_PREV_DRIVER_INFO_YES)
                    {
                        continue;
                    }

                    $this->addError($attribute . '_' . $key, 'Пожалуйста, заполните поле "' . $labels[$attribute] . '".');
                    continue;
                }
                // Валидация дат
                if (
                    in_array($attribute, ['driverPrevLicenseDate', 'driverExpDate', 'driverBirthDate']) &&
                    !empty($value) &&
                    !is_numeric(strtotime($value))
                ) {
                    $this->addError($attribute . '_' . $key, 'Неверно указана дата');
                }

                // Кастомная валидация для некоторых полей водителя
                switch ($attribute) {
                    case "driverLicenseForeign":
                        if (!in_array((int)$value, array_keys($this->getDriverLicenseTypes()))) {
                            $this->addError($attribute . '_' . $key, 'Указан неверный тип водительсккого удостоверения');
                        }
                        break;
                    case "driverBirthDate":
                    case "driversAge":
                        // Получаю возраст водителя
                        if ($attribute == 'driverBirthDate') {
                            $value = \Yii::$app->formatter->getAgeByDate($value);
                        }

                        if ((int)$value < 18 || (int)$value > 90) {
                            $this->addError($attribute . '_' . $key, 'Возраст водителя должен быть от 18 до 90');
                        }
                        break;
                    case "driversExp":
                        if (!is_numeric($value) || is_null($value)) {
                            $this->addError($attribute . '_' . $key, 'Вы не указали стаж');
                        }
                        break;
                    case "driverPrevLicenseSerial":
                    case "driverPrevLicenseNumber":
                    case "driverPrevLicenseDate":
                        // Если у водителя указали номер предыдущего водительского удостовернеия, серию или дату - нужно проверять, что заполненно и то и то
                        if (empty($value) && (
                                !empty($this->driverPrevLicenseNumber[$key]) ||
                                !empty($this->driverPrevLicenseSerial[$key]) ||
                                !empty($this->driverPrevLicenseDate[$key]))
                        ) {
                            $this->addError($attribute . '_' . $key, 'Пожалуйста, заполните поле "' . $labels[$attribute] . '".');
                        }
                        break;
                }
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'preCalculation' => 'Тип расчета',

            'vehicleType' => 'Тип',
            'vehiclePower' => 'Мощность',
            'vehicleYear' => 'Год выпуска',
            'vehicleLicensePlate' => 'Гос. номер',

            'vehicleIdentity' => 'Идентифицировать ТС по',
            'vehicleVin' => 'VIN',
            'vehicleBodyNum' => 'Номер кузова',
            'vehicleChassisNum' => 'Номер шасси',
            'vehicleDocType' => 'Документ на автомобиль',
            'vehicleDocSerial' => 'Серия документа на автомобиль',
            'vehicleDocNumber' => 'Номер документа на автомобиль',
            'vehicleDocDate' => 'Дата выдачи документа на автомобиль',
            'vehicleBrand' => 'Марка',
            'vehicleModel' => 'Модель',
            'vehicleWeight' => 'Масса без нагрузки (кг)',
            'vehicleMaxWeight' => 'Разрешенная максимальная масса (кг)',
            'vehicleSeats' => 'Число пассажирских мест',
            'vehicleDc' => 'Номер диагностической карты',
            'vehicleDcDate' => 'Срок действия диагностической карты',
            'vehicleIssueDate' => 'Дата выдачи диагностической карты',
            'useTrailer' => 'Транспортное средство используется вместе с прицепом',

            'driverLastName' => 'Фамилия',
            'driverMiddleName' => 'Отчество',
            'driverFirstName' => 'Имя',
            'driverBirthDate' => 'Дата рожения',
            'driverLicenseSerial' => 'Серия водительского удостоверения',
            'driverLicenseNumber' => 'Номер водительского удостоверения',
            'driversAge' => 'Возраст водителя (в годах)',
            'driversExp' => 'Стаж водителя (в годах)',
            'driverExpDate' => 'Дата выдачи водительского удостоверения',
            'driverPrevLastName' => 'Фамилия',
            'driverPrevFirstName' => 'Имя',
            'driverPrevMiddleName' => 'Отчество',
            'driverPrevLicenseSerial' => 'Серия водительского удостоверения',
            'driverPrevLicenseNumber' => 'Номер водительского удостоверения',
            'driverPrevLicenseDate' => 'Дата выдачи водительского удостоверения',
            'driverPrevAddInfo' => 'Указать предыдущее ВУ для восстановления КБМ',
            'driverLicenseForeign' => 'Тип водительского удостовереия',

            'ownerCityName' => 'Город регистрации собственника',
            'ownerCity' => 'Код города регистрации собственники в системе КЛАДР',
            'usePeriod' => 'Период использования',
            'ownerPassportDate' => 'Дата выдачи паспорта',
            'ownerPassportSerial' => 'Серия паспорта',
            'ownerPassportNumber' => 'Номер паспорта',
            'email' => 'Адрес электронной почты страхователя, на который будет выслан полис',
            'ownerPhone' => 'Телефон страхователя',
            'ownerDateOSAGOStart' => 'Дата начала действия полиса',
            'ownerLastName' => 'Фамилия',
            'ownerFirstName' => 'Имя',
            'ownerMiddleName' => 'Отчество',
            'ownerBirthDate' => 'Дата рождения',
            'purpose' => 'Цель использования автомобиля',
            'insurerIsOwner' => 'Страхователь и владелец автомобиля одно лицо',
            'ownerDadataAddress' => 'Адрес регистрации владельца автомобиля',
            'insurerDadataAddress' => 'Адрес регистрации страхователя',
            'ownerPassportType' => 'Тип паспота',
            'insurerPassportType' => 'Тип паспота',

            'insurerLastName' => 'Фамилия',
            'insurerFirstName' => 'Имя',
            'insurerMiddleName' => 'Отчество',
            'insurerBirthDate' => 'Дата рождения',
            'insurerPassportSerial' => 'Серия паспорта',
            'insurerPassportNumber' => 'Номер паспорта',
            'insurerPassportDate' => 'Дата выдачи паспорта',

            'prevPolicySerial' => 'Серия предыдущего полиса',
            'prevPolicyNumber' => 'Номер предыдущего полиса',

            'eId' => 'Идентификатор оплаты в ИГНУРУ',
            'company' => 'Код выбраной компании для оплаты',
            'successUrl' => 'URL перехода при удачной оплате',
            'failUrl' => 'URL перехода при не удачной оплате',
            'paymentUrl' => 'URL перехода на оплату',
            'smsCode' => 'СМС код подтверждения оплаты',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        $this->multiDrive = (int)$this->multiDrive;

        // Оставляю в гос.номере авто только цифры и русские буквы
        $this->vehicleLicensePlate = !empty($this->vehicleLicensePlate)
            ? preg_replace('/[^а-яё\d]/ui', '', $this->vehicleLicensePlate)
            : $this->vehicleLicensePlate;

        // Оставляю в телефонном номере только цифры
        $this->ownerPhone = !empty($this->ownerPhone)
            ? preg_replace("/[^0-9]/", '', $this->ownerPhone)
            : $this->ownerPhone;

        // Преобразование дат в формат Y-m-d
        foreach (
            [
                'vehicleDocDate',
                'vehicleDcDate',
                'vehicleIssueDate',
                'ownerPassportDate',
                'ownerDateOSAGOStart',
                'ownerBirthDate',
                'insurerPassportDate',
                'insurerBirthDate'
            ] as $field) {
            if (empty($this->{$field})) {
                continue;
            }
            $this->{$field} = \Yii::$app->formatter->asDate($this->{$field}, 'php:Y-m-d');
        }

        return parent::beforeValidate();
    }

    /**
     * Вычисляет возраст водителя (в годах) по дате рождения
     */
    public function setDriversAge()
    {
        if (!empty($this->driverBirthDate)) {
            foreach ($this->driverBirthDate as $key => $value) {
                if (empty($value)) {
                    continue;
                }
                $this->driversAge[$key] = \Yii::$app->formatter->getAgeByDate($value);
            }
        }
    }

    /**
     * Вычисляет стаж водителя (в годах) по дате выдачи первого водительского удостоверения
     */
    public function setDriversExp()
    {
        if (!empty($this->driverExpDate)) {
            foreach ($this->driverExpDate as $key => $value) {
                if (empty($value)) {
                    continue;
                }
                $this->driversExp[$key] = \Yii::$app->formatter->getAgeByDate($value);
            }
        }
    }

    /**
     * Период использования (кол-во месяцев)
     *
     * @return array
     */
    public function getUsePeriodItems()
    {
        return PreCalculationDictionary::MONTHS;
    }

    /**
     * Возвращает список типов ТС
     *
     * @return string[]
     */
    public function getTypesCar()
    {
        return PreCalculationDictionary::VEHICLE_TYPES;
    }

    /**
     * Тип расчета
     * @return string[]
     */
    public function getTypeCalc()
    {
        return OsagoInguruFormDictionary::TYPE_CALC;
    }

    /**
     * Типы документов на авто
     * @return string[]
     */
    public function getTypeDocCar()
    {
        return FinalCalculationDictionary::CAR_DOCS;
    }

    /**
     * Тип идентификации ТС (по VIN, кузову, шасси)
     * @return string[]
     */
    public function getTypeCarIdentity()
    {
        return OsagoInguruFormDictionary::CAR_HAVE_LIST;
    }

    /**
     * Цели использования автомобиля
     * @return string[]
     */
    public function getCarPurposes()
    {
        return FinalCalculationDictionary::CAR_PURPOSES;
    }

    /**
     * Страхователь и владелец автомобиля одно лицо
     * @return string[]
     */
    public function getOneFaces()
    {
        return FinalCalculationDictionary::ONE_FACES;
    }

    /**
     * Список страховых компаний используемых в сервисе
     * @return string[]
     */
    public static function insuranceCompaniesList()
    {
        return FinalCalculationDictionary::INSURANCE_COMPANIES;
    }

    /**
     * Признак управления транспортным средством с прицепом
     *
     * @return string[]
     */
    public function getTrailer()
    {
        return FinalCalculationDictionary::USE_TRAILER;
    }

    /**
     * Возвращает типы водительского удостоверения
     *
     * @return string[]
     */
    public function getDriverLicenseTypes()
    {
        return PreCalculationDictionary::DRIVER_LICENSE_TYPES;
    }

    /**
     * Возвращает типы паспорта (РФ или иностранный)
     *
     * @return string[]
     */
    public function getPassportTypes()
    {
        return FinalCalculationDictionary::PASSPORT_TYPES;
    }
}
