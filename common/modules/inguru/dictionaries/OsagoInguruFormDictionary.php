<?php

namespace common\modules\inguru\dictionaries;

/**
 * Class OsagoInguruFormDictionary
 *
 * @package common\modules\inguru\dictionaries
 */
class OsagoInguruFormDictionary
{
    /**
     * Количество максимально возможных водителей
     */
    public const MAX_DRIVERS_COUNT = 5;

    /**
     * Ключ названия сессии для записи предложений от страховой компании
     */
    public const SESSION_INSURE_DATA_NAME = 'inguru-insure-offer';

    /**
     * Тип расчета
     */
    public const PRE_CALCULATION_YES = 1; // Предварительный расчет
    public const PRE_CALCULATION_NO = 0; // Полный расчет
    const TYPE_CALC = [
        self::PRE_CALCULATION_YES => "Предварительный расчет (минимум данных)",
        self::PRE_CALCULATION_NO => "Полный расчет (с возможностью оформления полиса)",
    ];

    /**
     * Указать предыдущее ВУ
     */
    public const ADD_PREV_DRIVER_INFO_YES = 1;
    public const ADD_PREV_DRIVER_INFO_NO = 0;

    /**
     * Мультидрайв (ввод водителей)
     */
    public const IS_MULTI_DRIVE_YES = 1;
    public const IS_MULTI_DRIVE_NO = 0;

    /**
     * Идентификация ТС по VIN, или кузову, или шасси
     */
    public const CAR_HAVE_VIN = 0;
    public const CAR_HAVE_BODY = 1;
    public const CAR_HAVE_CHASSIS = 2;
    const CAR_HAVE_LIST = [
        self::CAR_HAVE_VIN => "VIN номер ТС",
        self::CAR_HAVE_BODY => "Номер кузова",
        self::CAR_HAVE_CHASSIS => "Номер шасси",
    ];
}