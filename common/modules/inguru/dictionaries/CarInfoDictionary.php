<?php

namespace common\modules\inguru\dictionaries;

/**
 * Class CarInfoDictionary
 *
 * @package common\modules\inguru\dictionaries
 */
class CarInfoDictionary
{
    /**
     * Константа для передачи в Ингуру
     */
    const VEHICLE_CONS = 'vehicle';
}