<?php

namespace common\modules\inguru\dictionaries;

/**
 * Class OsagoDocumentsDictionary
 *
 * @package common\modules\inguru\dictionaries
 */
class OsagoDocumentsDictionary
{
    /**
     * Константа для передачи в Ингуру
     */
    const API_CONS = 'document';
}