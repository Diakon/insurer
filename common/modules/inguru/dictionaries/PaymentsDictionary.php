<?php

namespace common\modules\inguru\dictionaries;

/**
 * Class PreparePaymentsDictionary
 *
 * @package common\modules\inguru\dictionaries
 */
class PaymentsDictionary
{
    /**
     * Константа для передачи в Ингуру
     */
    const INGURU_CONST = 'payurl';
}