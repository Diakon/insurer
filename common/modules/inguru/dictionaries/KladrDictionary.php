<?php

namespace common\modules\inguru\dictionaries;

/**
 * Class KladrDictionary
 *
 * @package common\modules\inguru\dictionaries
 */
class KladrDictionary
{
    /**
     * Константа для передачи в Ингуру
     */
    const API_CONS = 'kladr';
}