<?php

namespace common\modules\inguru\dictionaries;

/**
 * Class PreCalculationDictionary
 *
 * @package common\modules\inguru\dictionaries
 */
class PreCalculationDictionary
{
    /**
     * Мультидрайв
     */
    public const MULTI_DRIVE_YES = 1;
    public const MULTI_DRIVE_NO = 0;

    /**
     * Признак точного расчета с запросом в РСА
     */
    public const EXTRACT_CALC_YES = 1;
    public const EXTRACT_CALC_NO = 0;

    /**
     * Транспортное средство с прицепом
     */
    public const USE_TRAILER_YES = 1;
    public const USE_TRAILER_NO = 0;

    /**
     * Типы ТС
     */
    public const VEHICLE_TYPE_CAR = 1;
    public const VEHICLE_TYPE_TAXI = 2;
    public const VEHICLE_TYPE_BUS = 3;
    public const VEHICLE_TYPE_BUS_MORE_16 = 4;
    public const VEHICLE_TYPE_TRUCK = 5;
    public const VEHICLE_TYPE_TRUCK_MORE_16 = 6;
    public const VEHICLE_TYPE_MOTO = 7;
    public const VEHICLE_TYPE_TROLLEYBUS = 8;
    public const VEHICLE_TYPE_TRAMS = 9;
    public const VEHICLE_TYPE_SPEC = 10;
    public const VEHICLE_TYPE_TAXI_BUS = 11;
    const VEHICLE_TYPES = [
        self::VEHICLE_TYPE_CAR => 'Легковой автомобиль',
        self::VEHICLE_TYPE_TAXI => 'Такси',
        self::VEHICLE_TYPE_BUS => 'Автобусы (16 и менее мест)',
        self::VEHICLE_TYPE_BUS_MORE_16 => 'Автобусы (более 16 мест)',
        self::VEHICLE_TYPE_TRUCK => 'Грузовики (16 и менее тонн)',
        self::VEHICLE_TYPE_TRUCK_MORE_16 => 'Грузовики (более 16 тонн)',
        self::VEHICLE_TYPE_MOTO => 'Мотоциклы',
        self::VEHICLE_TYPE_TROLLEYBUS => 'Троллейбусы',
        self::VEHICLE_TYPE_TRAMS => 'Трамваи',
        self::VEHICLE_TYPE_SPEC => 'Спец. техника',
        self::VEHICLE_TYPE_TAXI_BUS => 'Маршрутные автобусы',
    ];

    /**
     * Период использования (кол-во месяцев) ТС
     */
    public const MONTH_3 = 3;
    public const MONTH_4 = 4;
    public const MONTH_5 = 5;
    public const MONTH_6 = 6;
    public const MONTH_7 = 7;
    public const MONTH_8 = 8;
    public const MONTH_9 = 9;
    public const MONTH_12 = 12;
    const MONTHS = [
        self::MONTH_3 => '3 месяца',
        self::MONTH_4 => '4 месяца',
        self::MONTH_5 => '5 месяцев',
        self::MONTH_6 => '6 месяцев',
        self::MONTH_7 => '7 месяцев',
        self::MONTH_8 => '8 месяцев',
        self::MONTH_9 => '9 месяцев',
        self::MONTH_12 => '12 месяцев',
    ];

    /**
     * Тип водительского удостоверения
     */
    public const DRIVER_LICENSE_TYPE_RU = 0;
    public const DRIVER_LICENSE_TYPE_FOREIGN = 1;
    const DRIVER_LICENSE_TYPES = [
        self::DRIVER_LICENSE_TYPE_RU => "РФ",
        self::DRIVER_LICENSE_TYPE_FOREIGN => "Иностранное"
    ];
}