<?php

namespace common\modules\inguru\dictionaries;

/**
 * Class FinalCalculationDictionary
 *
 * @package common\modules\inguru\dictionaries
 */
class FinalCalculationDictionary extends PreCalculationDictionary
{
    /**
     * Типы документов автомобиля
     */
    public const CAR_DOC_TYPE_PTS = 0;
    public const CAR_DOC_TYPE_STS = 1;
    public const CAR_DOC_TYPE_EPTS = 2;
    const CAR_DOCS = [
        self::CAR_DOC_TYPE_PTS => 'ПТС',
        self::CAR_DOC_TYPE_STS => 'СТС',
        self::CAR_DOC_TYPE_EPTS => 'ЭПТС',
    ];

    /**
     * Страховые компании
     */
    public const INSURANCE_COMPANY_ALPHA = 1;
    //public const INSURANCE_COMPANY_INGOS = 3;
    public const INSURANCE_COMPANY_ROSGOS = 5;
    public const INSURANCE_COMPANY_SOGLASIE = 7;
    public const INSURANCE_COMPANY_ZETTA = 27;
    public const INSURANCE_COMPANY_RESO = 32;
    public const INSURANCE_COMPANY_VSK = 33;
    public const INSURANCE_COMPANY_RENESSANS = 36;
    public const INSURANCE_COMPANY_TINKOFF = 107;
    public const INSURANCE_COMPANY_MAFIN = 142;
    public const INSURANCE_COMPANY_MAX = 10;
    public const INSURANCE_COMPANY_ABSOLUT = 75;
    public const INSURANCE_COMPANY_EVRO = 127;
    public const INSURANCE_COMPANY_GAIDE = 47;
    public const INSURANCE_COMPANY_ASTRO = 98;
    public const INSURANCE_COMPANY_VERNA = 125;
    public const INSURANCE_COMPANY_SOGAZ = 46;
    public const INSURANCE_COMPANY_UGORIA = 25;
    public const INSURANCE_COMPANY_OSK = 61;
    public const INSURANCE_COMPANY_INTACH = 39;
    const INSURANCE_COMPANIES = [
        self::INSURANCE_COMPANY_ALPHA => 'АльфаСтрахование',
       // self::INSURANCE_COMPANY_INGOS => 'Ингосстрах',
        self::INSURANCE_COMPANY_ROSGOS => 'Росгосстрах',
        self::INSURANCE_COMPANY_SOGLASIE => 'Согласие',
        self::INSURANCE_COMPANY_ZETTA => 'Зетта Страхование',
        self::INSURANCE_COMPANY_RESO => 'РЕСО',
        self::INSURANCE_COMPANY_VSK => 'ВСК',
        self::INSURANCE_COMPANY_RENESSANS => 'Ренессанс Страхование',
        self::INSURANCE_COMPANY_TINKOFF => 'Тинькофф Страхование',
        self::INSURANCE_COMPANY_MAFIN => 'Mafin',
        self::INSURANCE_COMPANY_MAX => 'МАКС',
        self::INSURANCE_COMPANY_ABSOLUT => 'Абсолют Страхование',
        self::INSURANCE_COMPANY_EVRO => 'ЕВРОИНС',
        self::INSURANCE_COMPANY_OSK => 'ОСК',
        self::INSURANCE_COMPANY_GAIDE => 'Гайде',
        self::INSURANCE_COMPANY_ASTRO => 'Астро-Волга',
        self::INSURANCE_COMPANY_VERNA => 'ВЕРНА',
        self::INSURANCE_COMPANY_SOGAZ => 'СОГАЗ',
        self::INSURANCE_COMPANY_UGORIA => 'Югория',
        self::INSURANCE_COMPANY_INTACH => 'Интач',
    ];

    /**
     * Способ использования автомобиля
     */
    public const CAR_PURPOSE_PERSONAL = 'personal';
    public const CAR_PURPOSE_TAXI = 'taxi';
    public const CAR_PURPOSE_TRAINING = 'training';
    public const CAR_PURPOSE_DANGER = 'dangerous';
    public const CAR_PURPOSE_RENTAL = 'rental';
    public const CAR_PURPOSE_PASSENGER = 'passenger';
    public const CAR_PURPOSE_SPECIAL = 'special';
    public const CAR_PURPOSE_SERVICES = 'services';
    public const CAR_PURPOSE_OTHER = 'other';
    const CAR_PURPOSES = [
        self::CAR_PURPOSE_PERSONAL => 'Личная',
        self::CAR_PURPOSE_TAXI => 'Такси',
        self::CAR_PURPOSE_TRAINING => 'Учебная езда',
        self::CAR_PURPOSE_DANGER => 'Перевозка опасных и легковоспламеняющихся грузов',
        self::CAR_PURPOSE_RENTAL => 'Прокат/краткосрочная аренда',
        self::CAR_PURPOSE_PASSENGER => 'Регулярные пассажирские перевозки/перевозки пассажиров по заказам',
        self::CAR_PURPOSE_SPECIAL => 'Дорожные и специальные транспортные средства',
        self::CAR_PURPOSE_SERVICES => 'Экстренные и коммунальные службы',
        self::CAR_PURPOSE_OTHER => 'Прочее',
    ];

    /**
     * Страхователь и владелец автомобиля одно лицо
     */
    public const ONE_FACE_YES = 1;
    public const ONE_FACE_NO = 0;
    const ONE_FACES = [
        self::ONE_FACE_YES => 'Да',
        self::ONE_FACE_NO => 'Нет',
    ];

    /**
     * Признак управления транспортным средством с прицепом
     */
    public const USE_TRAILER_YES = 1;
    public const USE_TRAILER_NO = 0;
    const USE_TRAILER = [
        self::USE_TRAILER_NO => 'Нет',
        self::USE_TRAILER_YES => 'Да',
    ];

    /**
     * Тип паспорта
     */
    public const PASSPORT_TYPE_RU = 0;
    public const PASSPORT_TYPE_FOREIGN = 1;
    const PASSPORT_TYPES = [
        self::PASSPORT_TYPE_RU => 'Паспорт РФ',
        self::PASSPORT_TYPE_FOREIGN => 'Иностранное гражданство',
    ];
}