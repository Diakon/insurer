<?php

namespace common\modules\inguru\interfaces;

/**
 * Interface ApiInterface
 * @package common\modules\inguru\interfaces
 */
interface ApiInterface
{
    /**
     * Возвращает URL ИНГУРУ для отправки запроса
     *
     * @return mixed
     */
    public function getApiUrl();

    /**
     * Метод возвращающий данные для отправки в АПИ ИНГУРУ
     * @param $model
     * @return mixed
     */
    public function getApiDataByModel($model);

    /**
     * Метод возвращающий формат curl запроса для отправки в АПИ ИНГУРУ
     *
     * @return mixed
     */
    public function getApiCurlFormat();

    /**
     * Метод возвращает curl формат запрос в АПИ ИНГУРУ
     *
     * @return mixed
     */
    public function getApiCurlMethod();
}