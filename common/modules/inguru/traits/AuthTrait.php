<?php

namespace common\modules\inguru\traits;

trait AuthTrait
{
    /**
     * Возвращает токен для авторизации в inguru по логину и паролю
     *
     * @param string $login
     * @param string $password
     * @return string
     */
    public function getToken($login, $password)
    {
        $time = time();
        $age = 60 * 60 * 24;
        $hash = md5($login . ':' . $time . ':' . $age . ':' . hash('sha256', md5($password)));

        return "INGURU " . (base64_encode(implode(':', [hash('sha256', $login), $time, $age, $hash])));
    }


}