<?php

namespace common\modules\inguru\traits;

use yii\httpclient\Client;

trait RequestApiTrait
{
    /**
     * Отправляет curl запрос
     * @param string $url
     * @param string $token
     * @param array $data
     * @param int $format
     * @param string $method
     * @return \yii\httpclient\Response
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function request(string $url, string $token, array $data, $format, string $method)
    {
        $client = new Client();
        return $client->createRequest()
            ->setFormat($format)
            ->setMethod($method)
            ->setUrl($url)
            ->addHeaders(['Authorization' => $token, 'Content-Type' => 'application/x-www-form-urlencoded'])
            ->setData($data)
            ->send();
    }
}