<?php
namespace common\models;

use yii\base\Component;

/**
 * Class Service
 *
 * @package common\components
 */
class Service extends Component
{
    /**
     * Проверяет, является ли строка json
     * @param $string
     * @return bool
     */
    protected function isJson($string) {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}