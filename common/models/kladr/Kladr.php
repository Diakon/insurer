<?php
namespace common\models\kladr;
use common\models\ActiveRecordModel;

/**
 * Class Kladr
 * @package common\models\kladr
 */
class Kladr extends ActiveRecordModel
{
    /**
     * Статусы удаления
     */
    const STATUS_DELETED_YES = 0; // Запись удалена
    const STATUS_DELETED_NO = 1;  // Запись активна

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kladr';
    }

    /**
     * Define rules for validation
     */
    public function rules()
    {
        return [
            [['name', 'socr', 'code', 'gninmb', 'uno', 'ocatd', 'status', 'status_deleted'], 'required'],
            [['index'], 'string', 'max' => 6],
            [['uno'], 'string', 'max' => 4],
            [['id', 'status', 'status_deleted'], 'integer'],
        ];
    }
}