<?php
namespace common\models\kladr;

/**
 * Города из КЛАДР
 *
 * Class City
 * @package common\models\kladr
 */
class City extends \api\modules\kladr\models\Kladr
{
    /**
     * @inheritdoc
     */
    public static function find()
    {
        $query = Kladr::find();
        $query->andWhere(['socr' => 'г']);
        $query->andWhere(['status_deleted' => self::STATUS_DELETED_NO]);

        return $query;
    }

}