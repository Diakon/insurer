<?php

namespace console\controllers;

use common\modules\bonuses\dictionaries\BonusDictionary;
use common\modules\bonuses\models\Bonus;
use common\modules\bonuses\services\BonusService;
use common\modules\inguru\dictionaries\StatusOrderDictionary;
use common\modules\inguru\models\StatusOrder;
use common\modules\inguru\services\StatusOrderService;
use common\modules\orders\dictionaries\OrderDictionary;
use common\modules\orders\models\Order;
use common\modules\orders\services\OrderPropertyService;
use common\modules\sravniru\services\ReportOrdersService;
use Yii;
use yii\base\BaseObject;
use yii\console\Controller;
use yii\helpers\ArrayHelper;
use common\modules\inguru\traits\AuthTrait;
use common\modules\inguru\traits\RequestApiTrait;
use yii\helpers\Json;

/**
 * Class CronController
 * @package console\controllers
 */
class CronController extends Controller
{
    use AuthTrait;
    use RequestApiTrait;

    /**
     * Делает запрос в страховую компанию и получает статус оплаты заказа. Если оплачен - начисляет бонусы
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function actionCheckBonuses()
    {
        // Получаю id заказов, которые успешно обработаны и которым были начислены бонусы
        $ordersIds = ArrayHelper::map(
            Bonus::find()
                ->select('order_id')
                ->where(['status_send_store' => BonusDictionary::STATUS_SEND_STORE_SUCCESS])
                ->asArray()
                ->all(), 'order_id', 'order_id');

        // Проходим по всем заказам которые в статусе "оплачен" или "ожидает оплаты" и по ним еще не ыбло начислений бонусов (нет в $ordersIds)
        $orders = Order::find()->where(['in', 'status', [OrderDictionary::STATUS_COMPLETE, OrderDictionary::STATUS_AWAITING_PAYMENT]]);
        if (!empty($ordersIds)) {
            $orders->andWhere(['not in', 'id', $ordersIds]);
        }
       // $orders->andWhere(['type' => OrderDictionary::TYPE_SRAVNIRU]);

        foreach ($orders->each(100) as $order) {
            if (empty($order->commission) || empty($order->price)) {
                \Yii::error('Для заказа ' . $order->id . ' не указан размер комиссии и/или стоимость полиса', 'statusPaymentError');
                continue;
            }
            $needAddBonus = false;  // Флаг, если true - перехожу к начислению бонуса

            // В соответствии с типом страховой службы получаю флаг того надо ли начислять бонусы, а сам заказ переводить в статус "Завершен"
            switch ($order->type) {
                // Заказ в ингуру
                case OrderDictionary::TYPE_INGURU:
                    // Получаю статус заказа
                    $serviceOrderProperty = new OrderPropertyService();
                    $eId = $serviceOrderProperty->getOrderPropertyValue($order->properties, 'eId');
                    if (empty($eId)) {
                        \Yii::error('Для заказа ' . $order->id . ' не возможно получить статус оплаты - не указан eId', 'statusPaymentError');
                        break;
                    }
                    $service = new StatusOrderService();
                    $model = new StatusOrder();
                    $model->eId = $eId;
                    $response = $this->request(
                        $service->getApiUrl(),
                        $this->getToken(),
                        $service->getApiDataByModel($model),
                        $service->getApiCurlFormat(),
                        $service->getApiCurlMethod()
                    );

                    if ($response->isOk) {
                        $response = $response->data;
                        if (!empty($response['errors'])) {
                            $error = Json::encode($response['errors']);
                            \Yii::error('Для заказа ' . $order->id . ', при попытке получить статус оплаты, ИНГУРУ вернул ошибку:' . $error, 'statusPaymentError');
                            break;
                        }

                        // Если заказ был оплачен - сохраняю статус заказа как  OrderDictionary::STATUS_COMPLETE и в bonuses
                        if (
                            !empty($response['results']) &&
                            in_array((int)$response['results']['state'], [
                                StatusOrderDictionary::INGURU_STATUS_PAYMENT_SUCCESS,
                               // StatusOrderDictionary::INGURU_STATUS_PAYMENT_OTHER_SUCCESS
                            ])
                        ) {
                            $needAddBonus = true;
                        }
                    }

                break;
                // Заказ в сравни.ру
                case OrderDictionary::TYPE_SRAVNIRU:
                    $dateFrom = Yii::$app->formatter->asDate(strtotime('-15 days'), 'php:Y-m-d');
                    $dateTo = Yii::$app->formatter->asDate(strtotime('+1 day'), 'php:Y-m-d');
                    $response = ReportOrdersService::getOrderStatusInfo($order, $dateFrom, $dateTo);
                    if (!empty($response['status'])) {
                        $needAddBonus = true;
                    }

                    break;
            }

            // Если все верно - начислюя бонусы
            if ($needAddBonus) {
                $transaction = Yii::$app->db->beginTransaction();

                $order->status = OrderDictionary::STATUS_COMPLETE;
                if($order->save()) {
                    $bonusService = new BonusService();
                    if ($bonusService->save($order)) {
                        BonusService::sendStoreBonus($bonusService->bonus);
                        $transaction->commit();
                        continue;
                    }
                }

                $transaction->rollBack();
            }
        }

    }

}