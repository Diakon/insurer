<?php

use yii\db\Migration;

/**
 * Class m210318_163925_cars
 */
class m210318_163925_cars extends Migration
{
    const TABLE_NAME = 'cars';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'number' => $this->string(20)->notNull(),
            'vin' => $this->string(100),
            'body_num' => $this->string(100),
            'chassis_num' => $this->string(100),
            'year' => $this->integer(),
            'power' => $this->integer(),
            'category' => $this->tinyInteger(3),
            'type' => $this->tinyInteger(3),
            'model' => $this->string(100),
            'brand' => $this->string(100),
            'model_brand_doc' => $this->string(100),
            'diag_card_number' => $this->string(100),
            'diag_card_date' => $this->date(),
            'diag_card_issue_date' => $this->date(),
            'doc_type' => $this->tinyInteger(3),
            'doc_serial' => $this->string(100),
            'doc_number' => $this->string(100),
            'doc_date' => $this->date(),
            'updated_at' => $this->integer(),
            'created_at' => $this->integer()
        ]);

        $this->createIndex('IND_' . self::TABLE_NAME . '_number', self::TABLE_NAME, 'number', true);
        $this->createIndex('IND_' . self::TABLE_NAME . '_vin', self::TABLE_NAME, 'vin', true);

        Yii::$app->db->getSchema()->refresh();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
        Yii::$app->db->getSchema()->refresh();
    }
}
