<?php

use yii\db\Migration;

/**
 * Class m210309_082303_create_table_kladr
 */
class m210309_082303_create_table_kladr extends Migration
{
    const TABLE_NAME = 'kladr';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $geo = file_get_contents(Yii::getAlias('@console') . '/migrations/dump/kladr.sql');
        if ($geo) {
            $tableOptions = null;
            if ($this->db->driverName === 'mysql') {
                $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB AUTO_INCREMENT=1';
            }

            $this->createTable(self::TABLE_NAME, [
                "id" => "INT(11) NOT NULL AUTO_INCREMENT",
                'name' => "varchar(40) NOT NULL",
                'socr' => "varchar(10) NOT NULL",
                'code' => "varchar(13) NOT NULL",
                'index' => "varchar(6) NULL DEFAULT NULL",
                'gninmb' => "varchar(4) NOT NULL",
                'uno' => "varchar(4) NULL DEFAULT NULL",
                'ocatd' => "varchar(11) NOT NULL",
                'status' => "tinyint(1) NOT NULL",
                'status_deleted' => "tinyint(1) NOT NULL",
                'PRIMARY KEY (id)',
            ], $tableOptions);

            $this->execute($geo);
            $this->createIndex('IND_' . self::TABLE_NAME . '_name', self::TABLE_NAME, 'name');
            $this->createIndex('IND_' . self::TABLE_NAME . '_code', self::TABLE_NAME, 'code');
            $this->createIndex('IND_' . self::TABLE_NAME . '_socr', self::TABLE_NAME, 'socr');
            $this->createIndex('IND_' . self::TABLE_NAME . '_status', self::TABLE_NAME, 'status');
            $this->createIndex('IND_' . self::TABLE_NAME . '_status_deleted', self::TABLE_NAME, 'status_deleted');

            Yii::$app->db->getSchema()->refresh();
        } else {
            echo 'File not found';
            Yii::$app->end();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
        Yii::$app->db->getSchema()->refresh();
    }
}
