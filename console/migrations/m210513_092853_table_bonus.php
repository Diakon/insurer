<?php

use yii\db\Migration;

/**
 * Class m210513_092853_table_bonus
 */
class m210513_092853_table_bonus extends Migration
{
    const TABLE_ORDERS = 'orders';
    const TABLE_NAME = 'bonuses';
    const TABLE_LOG = 'bonuses_logs';
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(self::TABLE_ORDERS, 'price', $this->float());
        $this->addColumn(self::TABLE_ORDERS, 'commission', $this->float());

        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'order_id' => $this->integer()->notNull(),
            'bonus' => $this->float()->notNull(),
            'status_send_store' => $this->boolean()->defaultValue(false),
            'updated_at' => $this->integer(),
            'created_at' => $this->integer()
        ]);
        $this->createIndex('IND_' . self::TABLE_NAME . '_status_send_store', self::TABLE_NAME, 'status_send_store');
        $this->createIndex('IND_' . self::TABLE_NAME . '_user_id', self::TABLE_NAME, 'user_id');
        $this->addForeignKey('FK_' . self::TABLE_NAME . '_order_id', self::TABLE_NAME, 'order_id', self::TABLE_ORDERS, 'id', 'CASCADE');

        $this->createTable(self::TABLE_LOG, [
            'id' => $this->primaryKey(),
            'bonus_id' => $this->integer()->notNull(),
            'status_send' => $this->boolean()->defaultValue(false),
            'request' => $this->text(),
            'answer' => $this->text(),
            'updated_at' => $this->integer(),
            'created_at' => $this->integer()
        ]);
        $this->addForeignKey('FK_' . self::TABLE_LOG . '_bonus_id', self::TABLE_LOG, 'bonus_id', self::TABLE_NAME, 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('FK_' . self::TABLE_NAME . '_order_id', self::TABLE_NAME);
        $this->dropForeignKey('FK_' . self::TABLE_LOG . '_bonus_id', self::TABLE_LOG);

        $this->dropColumn(self::TABLE_ORDERS, 'price');
        $this->dropColumn(self::TABLE_ORDERS, 'commission');

        $this->dropTable(self::TABLE_LOG);
        $this->dropTable(self::TABLE_NAME);

        return false;
    }
}
