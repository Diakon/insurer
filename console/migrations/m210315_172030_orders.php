<?php

use yii\db\Migration;

/**
 * Class m210315_172030_orders
 */
class m210315_172030_orders extends Migration
{
    const TABLE_ORDERS = 'orders';
    const TABLE_PROPERTY = 'orders_properties';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(self::TABLE_ORDERS, [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'status' => $this->tinyInteger(1)->notNull(),
            'step' => $this->string(100)->notNull(),
            'type' => $this->tinyInteger(2)->notNull(),
            'updated_at' => $this->integer(),
            'created_at' => $this->integer()
        ]);
        $this->addForeignKey("FK_" . self::TABLE_ORDERS . "_user_id", self::TABLE_ORDERS, "user_id", \common\models\User::tableName(), 'id', 'CASCADE');
        $this->createIndex("IND_" . self::TABLE_ORDERS . "_status", self::TABLE_ORDERS, "status");
        $this->createIndex("IND_" . self::TABLE_ORDERS . "_user_step", self::TABLE_ORDERS, ["user_id", "step"]);
        $this->createIndex("IND_" . self::TABLE_ORDERS . "_type", self::TABLE_ORDERS, ["type"]);

        $this->createTable(self::TABLE_PROPERTY, [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->notNull(),
            'type' => $this->tinyInteger(2)->notNull(),
            'key' => $this->string(256)->notNull(),
            'value' => $this->text(),
            'updated_at' => $this->integer(),
            'created_at' => $this->integer()
        ]);

        $this->addForeignKey("FK_" . self::TABLE_PROPERTY . "_order_id", self::TABLE_PROPERTY, "order_id", self::TABLE_ORDERS, 'id', 'CASCADE');
        $this->createIndex("IND_" . self::TABLE_PROPERTY . "_order_id_type", self::TABLE_PROPERTY, ["order_id", "type", "key"]);
        $this->createIndex("IND_" . self::TABLE_PROPERTY . "_type", self::TABLE_PROPERTY, ["type"]);

        Yii::$app->db->getSchema()->refresh();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey("FK_" . self::TABLE_ORDERS . "_user_id", self::TABLE_ORDERS);
        $this->dropForeignKey("FK_" . self::TABLE_PROPERTY . "_order_id", self::TABLE_PROPERTY);
        $this->dropTable(self::TABLE_PROPERTY);
        $this->dropTable(self::TABLE_ORDERS);

        Yii::$app->db->getSchema()->refresh();
    }
}
