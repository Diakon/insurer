<?php

use yii\db\Migration;

/**
 * Class m210618_064046_car_add_fields
 */
class m210618_064046_car_add_fields extends Migration
{
    const TABLE_NAME = 'cars';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, 'model_id', $this->integer());
        $this->addColumn(self::TABLE_NAME, 'brand_id', $this->integer());
        $this->addColumn(self::TABLE_NAME, 'source_id', $this->tinyInteger(2)->notNull());
        $this->createIndex('IND_' . self::TABLE_NAME . '_source_id', self::TABLE_NAME, 'source_id');
        \common\modules\cars\models\Car::updateAll(['source_id' => \common\modules\cars\dictionaries\CarDictionary::SOURCE_INGURU]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'model_id');
        $this->dropColumn(self::TABLE_NAME, 'brand_id');
        $this->dropColumn(self::TABLE_NAME, 'source_id');
    }
}
