<?php

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-api',
    'basePath' => dirname(__DIR__),    
    'bootstrap' => ['log'],
    'modules' => [
        'inguru' => [
            'basePath' => '@app/modules/inguru',
            'class' => 'api\modules\inguru\Module'
        ],
        'kladr' => [
            'basePath' => '@app/modules/kladr',
            'class' => 'api\modules\kladr\Module'
        ],
        'users' => [
            'basePath' => '@app/modules/users',
            'class' => 'api\modules\users\Module'
        ]
    ],
	'aliases' => [
        '@api' => dirname(dirname(__DIR__)) . '/api',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'gZKNvxJGfNasg0S06LOGooBhNRlR0G4O',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                [
                    'class' => \yii\rest\UrlRule::class,
                    'controller' => ['users/auth'],
                    'pluralize' => false,
                    'prefix' => 'api',
                    'extraPatterns' => [
                        'POST /' => 'login',
                    ]
                ],
                [
                    'class' => \yii\rest\UrlRule::class,
                    'controller' => ['inguru/pre-calculation'],
                    'pluralize' => false,
                    'prefix' => 'api',
                    'extraPatterns' => [
                        'POST /' => 'calculation',
                    ]
                ],
                [
                    'class' => \yii\rest\UrlRule::class,
                    'controller' => ['inguru/final-calculation'],
                    'pluralize' => false,
                    'prefix' => 'api',
                    'extraPatterns' => [
                        'POST /' => 'calculation',
                    ]
                ],
                [
                    'class' => \yii\rest\UrlRule::class,
                    'controller' => ['inguru/car-info'],
                    'pluralize' => false,
                    'prefix' => 'api',
                    'extraPatterns' => [
                        'POST /' => 'number',
                    ]
                ],
                [
                    'class' => \yii\rest\UrlRule::class,
                    'controller' => ['inguru/model-brand-info'],
                    'pluralize' => false,
                    'prefix' => 'api',
                    'extraPatterns' => [
                        'POST /' => 'number',
                    ]
                ],
                [
                    'class' => \yii\rest\UrlRule::class,
                    'controller' => ['kladr/city'],
                    'prefix' => 'api',
                    'pluralize' => false,
                ],
            ],        
        ]
    ],
    'params' => $params,
];



