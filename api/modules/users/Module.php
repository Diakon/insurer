<?php
namespace api\modules\users;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'api\modules\users\controllers';

    public function init()
    {
        parent::init();
    }
}
