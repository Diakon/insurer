<?php

namespace api\modules\users\controllers;

use Yii;
use yii\helpers\Json;
use yii\rest\ActiveController;
use yii\web\HttpException;
use yii\web\Response;

/**
 * Class ApiController
 * @package api\modules\eca\controllers
 */
class ApiController extends ActiveController
{
    /**
     * Возвращает результат запроса АПИ
     *
     * @param $data
     * @param int $code
     * @return string
     * @throws HttpException
     */
    public function returnResult($data, $code = 200)
    {
        if ($code == 200) {
            return $data;
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        throw new HttpException($code, is_array($data) ? Json::encode($data) : $data);
    }
}


