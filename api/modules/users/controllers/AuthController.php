<?php

namespace api\modules\users\controllers;

use api\modules\users\models\forms\LoginForm;
use Yii;
use yii\helpers\Json;

/**
 * Class AuthController
 * @package api\modules\users\controllers
 */
class AuthController extends ApiController
{
    public $modelClass = 'api\modules\eca\models\forms\LoginForm';

    /**
     * @return string
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\web\HttpException
     */
    public function actionLogin()
    {
        $requestParams = Yii::$app->getRequest()->getBodyParams();
        $model = new LoginForm();
        if ($model->load($requestParams, '') && $model->validate()) {
            $user = $model->getUserData();
            return $this->returnResult(['id' => $user->id, 'login' => $user->login, 'authKey' => $user->authKey]);
        } else {
            return $this->returnResult(Json::encode($model->errors), 400);
        }
    }
}


