<?php

namespace api\modules\inguru\controllers;

use Yii;
use yii\helpers\Json;
use yii\rest\ActiveController;
use yii\web\HttpException;
use yii\web\Response;
use common\modules\inguru\traits\RequestApiTrait;

/**
 * Class ApiController
 * @package api\modules\inguru\controllers
 */
class ApiController extends ActiveController
{
    use RequestApiTrait;

    /**
     * Отправляет curl запрос
     *
     * @param $url
     * @param $token
     * @param $data
     * @param $format
     * @param $method
     * @return mixed
     * @throws HttpException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function sendRequest($url, $token, $data, $format, $method)
    {
        $response = $this->request($url, $token, $data, $format, $method);

        if ($response->isOk) {
            $response = $response->data;
            if (!empty($response['errors'])) {
                $this->returnResult($response['errors'], 400);
            } else {
                return $response;
            }
        }

        $this->returnResult(["API call error"], 400);
    }

    /**
     * Возвращает результат запроса АПИ
     *
     * @param $data
     * @param int $code
     * @return string
     * @throws HttpException
     */
    public function returnResult($data, $code = 200)
    {
        if ($code == 200) {
            return $data;
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        throw new HttpException($code, is_array($data) ? Json::encode($data) : $data);
    }
}


