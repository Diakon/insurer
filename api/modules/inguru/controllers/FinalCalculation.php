<?php

namespace api\modules\inguru\controllers;

use api\modules\inguru\models\PreCalculation;
use common\modules\inguru\services\FinalCalculationService;
use common\modules\inguru\services\PreCalculationService;
use Yii;
use common\modules\inguru\traits\AuthTrait;
use yii\helpers\Json;
use yii\web\HttpException;

/**
 * Выполняет предварительный расчет стоимости ОСАГО
 *
 * Class FinalCalculation
 * @package api\modules\inguru\controllers
 */
class FinalCalculation extends ApiController
{
    use AuthTrait;

    public $modelClass = 'api\modules\inguru\models\FinalCalculation';

    private $service;

    /**
     * FinalCalculation constructor.
     * @param $id
     * @param $module
     * @param PreCalculationService $service
     * @param array $config
     */
    public function __construct($id, $module, FinalCalculationService $service, $config = [])
    {
        $this->service = $service;
        parent::__construct($id, $module, $config);
    }

    /**
     * @return string
     * @throws HttpException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function actionCalculation()
    {
        $requestParams = Yii::$app->getRequest()->getBodyParams();

        $model = new PreCalculation();
        if ($model->load($requestParams, '') && $model->validate()) {
            $token = $this->getToken();
            $result = $this->sendRequest(
                $this->service->getApiUrl(),
                $token,
                $this->service->getApiDataByModel($model),
                $this->service->getApiCurlFormat(),
                $this->service->getApiCurlMethod()
            );
            return $this->returnResult($result['results'] ?? $result);
        } else {
            return $this->returnResult(Json::encode($model->errors), 400);
        }
    }
}


