<?php

namespace api\modules\inguru\controllers;

use api\modules\inguru\models\PreCalculation;
use common\modules\inguru\services\PaymentsService;
use Yii;
use common\modules\inguru\traits\AuthTrait;
use yii\helpers\Json;
use yii\web\HttpException;

/**
 * Возвращает ссылку на оплату полиса ОСАГО
 *
 * Class PaymentsController
 * @package api\modules\inguru\controllers
 */
class PaymentsController extends ApiController
{
    use AuthTrait;

    public $modelClass = 'api\modules\inguru\models\FinalCalculation';

    private $service;

    /**
     * PaymentsController constructor.
     * @param $id
     * @param $module
     * @param PaymentsService $service
     * @param array $config
     */
    public function __construct($id, $module, PaymentsService $service, $config = [])
    {
        $this->service = $service;
        parent::__construct($id, $module, $config);
    }

    /**
     * @return string
     * @throws HttpException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function actionCalculation()
    {
        $requestParams = Yii::$app->getRequest()->getBodyParams();

        $model = new PreCalculation();
        if ($model->load($requestParams, '') && $model->validate()) {
            $token = $this->getToken();
            $result = $this->sendRequest(
                $this->service->getApiUrl(),
                $token,
                $this->service->getApiDataByModel($model),
                $this->service->getApiCurlFormat(),
                $this->service->getApiCurlMethod()
            );
            return $this->returnResult($result['results'] ?? $result);
        } else {
            return $this->returnResult(Json::encode($model->errors), 400);
        }
    }
}


