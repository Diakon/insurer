<?php

namespace api\modules\inguru\controllers;

use api\modules\inguru\models\ModelBrandInfo;
use common\modules\inguru\services\ModelBrandInfoService;
use Yii;
use common\modules\inguru\traits\AuthTrait;
use yii\helpers\Json;
use yii\web\HttpException;

/**
 * Получение даннных об модели / макри авто
 * Class ModelBrandInfoController
 * @package api\modules\inguru\controllers
 */
class ModelBrandInfoController extends ApiController
{
    use AuthTrait;

    public $modelClass = 'api\modules\inguru\models\ModelBrandInfo';

    private $service;

    /**
     * ModelBrandInfoController constructor.
     * @param $id
     * @param $module
     * @param ModelBrandInfoService $service
     * @param array $config
     */
    public function __construct($id, $module, ModelBrandInfoService $service, $config = [])
    {
        $this->service = $service;
        parent::__construct($id, $module, $config);
    }

    /**
     * Возвращает данные об марке и модели автомобиля
     * @return string
     * @throws HttpException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function actionNumber()
    {
        $requestParams = Yii::$app->getRequest()->getBodyParams();

        $model = new ModelBrandInfo();
        if ($model->load($requestParams, '') && $model->validate()) {
            if (!$model->validate()) {
                print_r($model->errors);die();
            }
            $token = $this->getToken();
            $result = $this->sendRequest(
                $this->service->getApiUrl(),
                $token,
                $this->service->getApiDataByModel($model),
                $this->service->getApiCurlFormat(),
                $this->service->getApiCurlMethod()
            );

            return $this->returnResult($result['results'] ?? $result);
        } else {
            return $this->returnResult(Json::encode($model->errors), 400);
        }
    }
}


