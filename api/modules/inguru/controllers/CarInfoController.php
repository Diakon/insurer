<?php

namespace api\modules\inguru\controllers;

use api\modules\inguru\models\CarInfo;
use common\modules\inguru\services\CarInfoService;
use Yii;
use common\modules\inguru\traits\AuthTrait;
use yii\helpers\Json;
use yii\web\HttpException;

/**
 * Получение даннных об автомобиле по гос. номеру
 *
 * Class CarInfoController
 * @package api\modules\inguru\controllers
 */
class CarInfoController extends ApiController
{
    use AuthTrait;

    public $modelClass = 'api\modules\inguru\models\CarInfo';

    private $service;

    /**
     * CarInfoController constructor.
     * @param $id
     * @param $module
     * @param CarInfoService $service
     * @param array $config
     */
    public function __construct($id, $module, CarInfoService $service, $config = [])
    {
        $this->service = $service;
        parent::__construct($id, $module, $config);
    }

    /**
     * Возвращает данные об авто по его номеру
     * @return string
     * @throws HttpException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function actionNumber()
    {
        $requestParams = Yii::$app->getRequest()->getBodyParams();

        $model = new CarInfo();
        if ($model->load($requestParams, '') && $model->validate()) {
            $token = $this->getToken();
            $result = $this->sendRequest(
                $this->service->getApiUrl(),
                $token,
                $this->service->getApiDataByModel($model),
                $this->service->getApiCurlFormat(),
                $this->service->getApiCurlMethod()
            );

            return $this->returnResult($result['results'] ?? $result);
        } else {
            return $this->returnResult(Json::encode($model->errors), 400);
        }
    }
}


