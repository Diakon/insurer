<?php
namespace api\modules\inguru;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'api\modules\inguru\controllers';

    public function init()
    {
        parent::init();
    }
}
