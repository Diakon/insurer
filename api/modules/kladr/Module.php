<?php
namespace api\modules\kladr;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'api\modules\kladr\controllers';

    public function init()
    {
        parent::init();
    }
}
