<?php
namespace api\modules\kladr\models;

/**
 * Class Kladr
 * @package api\modules\kladr\models
 */
class Kladr extends \common\models\kladr\Kladr
{
    /**
     * @inheritdoc
     */
    public static function find()
    {
        $query = parent::find();

        $params = \Yii::$app->request->get();
        if (!empty($params)) {
            foreach ($params as $keyParam => $valParam) {
                $valParam = trim($valParam);
                switch ($keyParam) {
                    case "name":
                        $query->andWhere(['like', $keyParam, "$valParam%", false]);
                        break;
                    default:
                        $query->andWhere([$keyParam => $valParam]);
                }
            }
        }

        return $query;
    }
}