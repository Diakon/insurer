<?php

namespace api\modules\kladr\controllers;

use yii\rest\ActiveController;

/**
 * Class KladrController
 * @package api\modules\kladr\controllers
 */
class CityController extends ActiveController
{
    public $modelClass = 'api\modules\kladr\models\City';
}


