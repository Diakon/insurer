<?php
namespace frontend\controllers;

use common\modules\inguru\forms\OsagoInguruForm;
use common\modules\orders\dictionaries\OrderDictionary;
use common\modules\orders\models\Order;
use common\modules\orders\services\OrderService;
use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\HttpException;
use yii\web\Response;

/**
 * Site controller
 */
class SiteController extends Controller
{
    const CODE_SUCCESS = 200;
    const CODE_ERROR = 400;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'success', 'fail'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'calculation', 'faq'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Главная страница
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Страница расчета
     *
     * @return mixed
     */
    public function actionCalculation()
    {
        $orderId = Yii::$app->request->get('order_id', 0);
        $companyId = Yii::$app->request->get('company_id');
        if (!empty($orderId)) {
            // Проверяю, что это заказ авторизованного пользователя
            $service = new OrderService();
            /**
             * @var $order Order
             */
            $order = $service->getOrdersByParams(
                ['id' => $orderId, 'user_id' => Yii::$app->user->id],
                'id',
                SORT_ASC,
                true
            );
            // Если заказ не новый и не в статусе отложен для оплаты - расчитывать нечего
            if (empty($order) || !in_array($order->status, [OrderDictionary::STATUS_NEW, OrderDictionary::STATUS_AWAITING_PAYMENT])) {
                throw new HttpException(404 ,'Страница не найдена');
            }
            // Для не завершенных расчетов (заказы из раздела Мои расчеты) в ИНГУРУ и сравни.ру на этапе Расчет всегда открывать с этапа Полис (предыдущий перед Расчет этап)
            if (
                in_array($order->type, [OrderDictionary::TYPE_INGURU, OrderDictionary::TYPE_SRAVNIRU]) &&
                $order->status == OrderDictionary::STATUS_NEW &&
                $order->step == OsagoInguruForm::SCENARIO_STEP_CALCULATE
            ) {
                $order->step = OsagoInguruForm::SCENARIO_STEP_POLICY;
                $service->save($order, false);
            }
        }

        // Определяю вьюшку на основе типа заказа (каким сервисом пользовались при заказе)
        switch (!empty($order) ? $order->type : null) {
            case OrderDictionary::TYPE_SRAVNIRU:
                $viewName = '_sravniru';
                break;
            case OrderDictionary::TYPE_INGURU:
                $viewName = '_inguru';
                break;
            default:
                $viewName = '_sravniru';
        }

        return $this->render('calculation', ['viewName' => $viewName, 'order' => !empty($order) ? $order : null, 'companyId' => $companyId]);
    }

    /**
     * Возвращает JSON ответ
     *
     * @param $code
     * @param null $msg
     * @return array
     */
    public function returnAjax($code, $msg = null)
    {
        $returnData = [];
        Yii::$app->response->format = Response::FORMAT_JSON;
        Yii::$app->response->statusCode = $code;
        $returnData['status'] = $code;
        if (!is_null($msg)) {
            $returnData['msg'] = $msg;
        }

        return $returnData;
    }
}
