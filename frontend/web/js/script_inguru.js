/**
 * Скрипт JS для модуля ИНГУРУ
 * @type {{setErrorMsgOnForm: PROJECT.setErrorMsgOnForm, init: PROJECT.init, stepSwitch: PROJECT.stepSwitch, initPage: PROJECT.initPage, installDatePicker: PROJECT.installDatePicker, addRemoveDriver: PROJECT.addRemoveDriver, installMaskInput: PROJECT.installMaskInput, clearFields: PROJECT.clearFields, payment: PROJECT.payment, clearErrorMsgOnForm: PROJECT.clearErrorMsgOnForm, setCarInfoByNumber: PROJECT.setCarInfoByNumber}}
 */
var PROJECT = {
    init: function (settings) {
        //конфиги
        PROJECT.config = {
            urlInguruOrdersList: '/inguru/orders-list/',
            urlInguruTypeCalc: '/inguru/type-calc/',
            urlInguruCarInfo: '/inguru/car-info/',
            urlInguruDriverInfo: '/inguru/driver-info/',
            urlOwnerDriverInfo: '/inguru/owner-info/',
            urlOwnerDriverPolicy: '/inguru/policy-info/',
            urlOwnerDriverCalc: '/inguru/calculation/',
            urlPayment: '/inguru/payments/',
        };

        $.extend(PROJECT.config, settings);

        PROJECT.stepSwitch();
        PROJECT.clearFields();
        PROJECT.initPage();
        PROJECT.addRemoveDriver();
        PROJECT.setCarInfoByNumber();
        PROJECT.setCarIdentityType();
        PROJECT.setErrorMsgOnForm(responseServer = null, stepName = null);
        PROJECT.clearErrorMsgOnForm();
        PROJECT.payment();
    },

    /**
     * Возвращает ID заказа
     * @returns {number}
     */
    getOrderId:function () {
      return parseInt($('.js-order-id').val());
    },

    /**
     * Переход по шагам оформления ОСАГО
     */
    stepSwitch:function () {
        /**
         * Кнопка возврата на предыдущий шаг
         */
        $(document).on('click', '.js-prev-step-btn', function () {
            let currentStepName = $('.js-input-step').val(); // Текеущий шаг
            // Получаю шаг, который надо обработать
            let prevStepName = "";
            $(".js-step").each(function() {
                let stepName = $(this).data("step");
                // Дошли до текущего шага - надо вернуться к предыдущему
                if (stepName == currentStepName) {
                    return false;
                }
                prevStepName = stepName;
            });

            if (prevStepName == "typecalc") {
                $(this).hide(); // Прячу кнопку т.к. на 1ом шаге (car) ее не надо показывать
            }
            $('.js-input-step').val(prevStepName);
            prevStep(prevStepName);

            return false;
        });

        /**
         * Возврат на предыдущий шаг
         * @param stepName
         */
        function prevStep(stepName)
        {
            $('.js-ajax-load-image').show();
            $('.js-step').hide(); // Скрфваю все формы - showForm покажет нужную, когда подтянет данные
            // В меню ставлю активным (выделенным) новый шаг
            $('.js-menu-item').removeClass('js-menu-item-active')
            $('.js-menu-item-' + stepName).addClass('js-menu-item-active');
            // Для водителей надо очистить от старых страниц загруженных по ajax
            if (stepName == 'driver') {
                $('.js-driver-block').remove();
            }
            showForm(stepName);
            $('.js-ajax-load-image').hide();
            setTimeout(function() {PROJECT.installMaskInput();}, 2000);
        }

        // ----------------------------------

        /**
         * Кнопка перехода на следующий шаг
         */
        $(document).on('click', '.js-next-step-btn', function () {
            // Получаю шаг, который надо обработать
            let stepName = $('.js-input-step').val();
            nextStep(stepName);

            return false;
        });

        /**
         * Переход на следующий шаг
         * @param stepName
         */
        function nextStep(stepName) {
            $('.js-ajax-load-image').show();
            let url = PROJECT.getStepUrl(stepName);
            // Проверяю введенные данные
            let form = $('#js-osago-form').serialize();
            PROJECT.clearErrorMsgOnForm()

            $.ajax({
                type: 'POST',
                url: url + '?action=validate&order_id=' + PROJECT.getOrderId(),
                data: form,
                success: function(data) {
                    $('.js-ajax-load-image').hide();
                    let step = data.msg.step;

                    if (data.msg.orderId) {
                        $('.js-order-id').val(parseInt(data.msg.orderId))
                    }
                    $('.js-step').hide(); // Скрываю все формы - showForm покажет нужную, когда подтянет данные
                    // В меню ставлю активным (выделенным) новый шаг
                    $('.js-menu-item').removeClass('js-menu-item-active')
                    $('.js-menu-item-' + step).addClass('js-menu-item-active');

                    // Для водителей надо очистить от старых страниц загруженных по ajax
                    if (step == 'driver') {
                        $('.js-driver-block').remove();
                    }

                    // Показываю - скрываю кнопку возврата назад в зависимости от шага
                    if (step != "typecalc") {
                        $('.js-prev-step-btn').show();
                    } else {
                        $('.js-prev-step-btn').hide();
                    }

                    // Если последний шаг - расчет (calc), то произвожу отправку всех введеных на прошлом шаге данных и вывод результата
                    if (step == "calc") {
                        PROJECT.calculate(step);
                    } else {
                        showForm(step);
                    }
                },
                error:  function(data){
                    $('.js-ajax-load-image').hide();
                    PROJECT.setErrorMsgOnForm(data, stepName);
                }
            });
        }

        /**
         * Переход на следующий шаг
         *
         * @param step
         * @returns {boolean}
         */
        function showForm(step) {
            $('.js-action-reload-page-block').hide();
            $('.js-next-step-btn').show();
            $('.js-driver-btn').hide();
            $('.js-input-step').val(step); // Скрытое поле текущего шага
            let html = $('.js-step-' + step).html();
            let url = PROJECT.getStepUrl(step);

            // Если шаг добавления водителей - показываю кнопки +/- для добавления
            if (step == "driver") {
                $('.js-driver-btn').show();
            }

            // Форма не тянулась - тяну
            let form = $('#js-osago-form').serialize();
            $.ajax({
                url: url + '?action=formHtml&order_id=' + PROJECT.getOrderId(),
                type: 'post',
                data: form,
                success: function (data) {
                    $('.js-step-' + step).empty().append(data).show();

                    // Инсталирую календарь и/или автокомплит, для шага "Автомобиль", "Водитель", "Собственник" или "Полис"
                    switch (step) {
                        case "owner":
                            PROJECT.installDatePicker(jQuery('#osagoinguruform-ownerpassportdate'), true);
                            PROJECT.installDatePicker(jQuery('#osagoinguruform-insurerpassportdate'), true);
                            PROJECT.installDatePicker(jQuery('#osagoinguruform-ownerdateosagostart'), false);
                            PROJECT.installDatePicker(jQuery('#osagoinguruform-ownerbirthdate'), true);
                            PROJECT.installDatePicker(jQuery('#osagoinguruform-insurerbirthdate'), true);
                            jQuery('#js-city-owner').autocomplete({"source":"\/inguru\/owner-info\/?action=searchCity","minLength":"3","select":function( event, ui ) {
                                    $('#osagoinguruform-ownercity').val(ui.item.id);
                                }});
                            jQuery('#osagoinguruform-ownerdadataaddress').autocomplete({"source":"\/inguru\/owner-info\/?action=searchAddress","minLength":"3","select":function( event, ui ) {
                                    $(this).val(ui.item.value);
                                }});
                            jQuery('#osagoinguruform-insurerdadataaddress').autocomplete({"source":"\/inguru\/owner-info\/?action=searchAddress","minLength":"3","select":function( event, ui ) {
                                    $(this).val(ui.item.value);
                                }});
                            break;
                        case "policy":
                            PROJECT.installDatePicker(jQuery('#osagoinguruform-prevlicensedate'), false);
                            break;
                        case "driver":
                            let isMultiDrive = parseInt($('.js-is-multi-drive-input').val());
                            if (isMultiDrive == 1) {
                                $('.js-add-remove-driver-btn').hide(); // Скрываю кнопку "Добавить водителя", если мультидрайв
                            }
                            let driverCount = parseInt($('.js-input-driver-count').val());
                            PROJECT.installDatePicker(jQuery('#osagoinguruform-driverbirthdate_' + driverCount), true);
                            PROJECT.installDatePicker(jQuery('#osagoinguruform-driverexpdate_' + driverCount), true);
                            PROJECT.installDatePicker(jQuery('#osagoinguruform-driverprevlicensedate_' + driverCount), true);
                            break;
                        case "car":
                            PROJECT.installDatePicker(jQuery('#osagoinguruform-vehicledocdate'), false);
                            PROJECT.installDatePicker(jQuery('#osagoinguruform-vehicledcdate'), false, true);

                            // Дата выдачи диагностической карты не может быть больше 2х лет от текущей даты
                            let issueDate = new Date();
                            let yearIssueDate = issueDate.getFullYear();
                            let monthIssueDate = issueDate.getMonth();
                            let dayIssueDate = issueDate.getDate();
                            issueDate = new Date(yearIssueDate + 2, monthIssueDate, dayIssueDate);
                            PROJECT.installDatePicker(jQuery('#osagoinguruform-vehicleissuedate'),
                                false,
                                false,
                                issueDate
                            );

                            // Автокомплит на ввод марки авто
                            jQuery('#js-vehicle-brand-name').autocomplete({"source":"\/inguru\/car-info\/?action=searchBrand","minLength":"3","select":function( event, ui ) {
                                    $('#js-vehicle-brand-name').val(ui.item.id);
                                }});
                            // Автокомплит на ввод модели авто
                            $("#js-vehicle-model-name").autocomplete({
                                source: function(request, response) {
                                    $.getJSON("/inguru/car-info/", {action: 'searchModel', brandName: $('#js-vehicle-brand-name').val(), term: $('#js-vehicle-model-name').val()},
                                        response);
                                },
                                minLength: 3,
                                select: function(event, ui){
                                    $('#js-vehicle-model-name').val(ui.item.id);
                                }
                            });

                            break;
                    }
                    window.moveTo(0,0); // Перемещаю к верху страницы экран
                    PROJECT.installMaskInput();
                }
            });

            return true;
        }
    },

    /**
     * Выполняет расчет стоимости полиса
     * @param step
     */
    calculate:function (step) {
        $('.js-ajax-load-image').show();
        $('.js-next-step-btn').hide();
        $('.js-step-' + step).empty();
        $('.js-input-step').val(step);
        let url = PROJECT.getStepUrl(step);
        let form = $('#js-osago-form').serialize();
        var selectedCompany = parseInt($('.js-input-selected-company').val());
        // Получаю тип расчета
        $.when(
            $.ajax({
                type: 'GET',
                url: PROJECT.config.urlInguruTypeCalc + "?action=typeCalc&order_id=" + PROJECT.getOrderId(),
            })).then(function(data, textStatus, jqXHR) {
                let isPreCalculate = parseInt(data.msg);
                isPreCalculate = isPreCalculate == 1;
                // Если предварительный расчет - отправляю все данные и жду ответ (быстрый расчет)
                if (isPreCalculate) {
                    $('.js-switch-to-final-calculation-btn').hide();
                    $.ajax({
                        type: 'POST',
                        url: url + "?order_id=" + PROJECT.getOrderId(),
                        data: form,
                        success: function (data) {
                            $('.js-ajax-load-image').hide();
                            $('.js-step-' + step).empty().append(data).show();
                            $('.js-switch-to-final-calculation-btn').show(); // Показываю кнопку "Перейти к полному расчету"
                        },
                        error:  function(data){
                            $('.js-ajax-load-image').hide();
                            alert("Ошибка запроса в АПИ Ингуру.")
                        }
                    });
                }
                else {
                    // Полный расчет - надо получать ответы от страховых компаний последовательно. Запрос в страховой А - получили ответ, вывели, отправляем запрос в страховую компанию Б и тд
                    // Получаю список страховых компаний доступных для работы в сервисе
                    var countCompany = 0;
                    var processedCompany = 0;
                    $('.js-step-' + step).empty();
                    $('.js-action-reload-page-block').hide();
                    $(".js-insurance-companies-list-id").each(function() {
                        let id = parseInt($(this).data('id'));
                        countCompany = countCompany + 1;
                        $.when(
                            $.ajax({
                                type: 'POST',
                                url: url + "?insuranceCompanyId=" + id + "&order_id=" + PROJECT.getOrderId() + "&row=" + countCompany,
                                data: form
                            })).then(function(data, textStatus, jqXHR) {
                                ++processedCompany;
                                // Если все еще на текущем шаге - ждут ответы от страховых, то вывожу полученный ответ
                                if ($('.js-input-step').val() == step) {
                                    $('.js-step-' + step).append(data).show();
                                    // Вывожу заголовок
                                    $('.js-title-input-info-block').empty().append('<h3>Пожалуйста, дождитесь ответа от всех страховых компаний. После этого Вы сможете выбрать наиболее подходящее Вам предложение и, если захотите, перейти к оплате.</h3>');

                                    // Если это запрос по выбранной ранее компании - скрываю прелоудер, показываю кнопки
                                    if (processedCompany >= countCompany || selectedCompany == 1) {
                                        $('.js-ajax-load-image').hide();
                                        $('.js-payment-btn-block').show(); // Показыть кнопки перехода к оплате
                                        if (selectedCompany == 1) {
                                            $('.js-save-to-awaiting-payment').remove(); // Убираю кнопку перенести в ожидают оплаты - это уде было сделано
                                        }
                                    }

                                    // Селектор для кастомного выбора модели ТС (если выбрали марку)
                                    setTimeout(
                                        function ()
                                        {
                                            if (processedCompany >= countCompany) {
                                                $('.js-action-reload-page-block').show();
                                                $('.js-vehicle-brand-model-manual-input').show();
                                            }
                                        }, 100);

                                } else {
                                    // Ушли со шага - убираю ответ
                                    $('.js-step-' + step).empty();
                                }
                            }
                        ).fail(function() {
                            processedCompany = processedCompany + 1;
                        });
                    });

                    setTimeout(
                        function ()
                        {
                            $('.js-ajax-load-image').hide();
                            $('.js-payment-btn-block').show(); // Показыть кнопки перехода к оплате
                            $('.js-vehicle-brand-model-manual-input').show();
                            $('.js-action-reload-page-block').show();
                        }, 360000); // Через 360 сек. прелоудер скрыть в любом случе (таймаут страницы пройдет)
                }

                return true;
            }
        );
        // Пользователь задал кастомную бренд - модель для ТС и нажал кнопку сохранить
        $(document).on('click', '.js-set-car-brand-model-for-company', function () {
            let companyId = parseInt($(this).data('company'));
            let brand = $('#js-vehicle-brand-name-company-id-' + companyId).val();
            let model = $('#js-vehicle-model-name-company-id-' + companyId).val();
            $.ajax({
                type: 'POST',
                url: PROJECT.config.urlOwnerDriverCalc + '?order_id=' + PROJECT.getOrderId() + '&action=setCustomBrandModelCar',
                data: {'companyId': companyId, 'brand': brand, 'model':model},
                success: function (data) {
                    location.reload();
                },
                error:  function(data){
                    location.reload();
                }
            });

            return false;
        });
    },

    /**
     * Оперции с оплатой полиса
     */
    payment:function () {
        // Нажали на кнопку оплаты или сохранить в "Ожидают оплаты"
        $(document).on('click', '.js-prepare-payment-service, .js-save-to-awaiting-payment', function () {
            let awaiting = $(this).hasClass('js-save-to-awaiting-payment') ? 1 : 0;
            let eId = parseInt($(this).data('id'));
            let company = $(this).data('company');
            $(this).attr('disabled', true);
            $(this).append('<br>Пожалуйста, подождите - Ваш запрос обрабатывается.');
            $.ajax({
                type: 'POST',
                url: PROJECT.config.urlPayment + '?order_id=' + PROJECT.getOrderId(),
                data: {'eId': eId, 'company': company, 'awaiting': awaiting},
                success: function (data) {
                   document.location.href = data.msg['url'];
                },
                error:  function(data){
                    let msg = 'Возникла ошибка.\n';
                    if (data.msg) {
                        msg += "Страховая компания вернула следующий ответ:"
                        msg += data.msg;
                    }
                    msg += "\nПожалуйста, попробуйте снова или обратитесь к администратору.";
                    alert(msg)
                }
            });

            return false;
        });

    },

    /**
     * Возвращает url для ajax запроса
     * @param step
     * @returns {string}
     */
    getStepUrl:function (step) {
        let url = "";
        switch (step) {
            case "typecalc":
                url = PROJECT.config.urlInguruTypeCalc;
                break;
            case "car":
                url = PROJECT.config.urlInguruCarInfo;
                break;
            case "driver":
                url = PROJECT.config.urlInguruDriverInfo;
                break;
            case "owner":
                url = PROJECT.config.urlOwnerDriverInfo;
                break;
            case "policy":
                url = PROJECT.config.urlOwnerDriverPolicy;
                break;
            case "calc":
                url = PROJECT.config.urlOwnerDriverCalc;
                break;
        }

        return url;
    },

    /**
     * Выводит ошибки на форме
     * @param responseServer Ответ сервера
     * @param stepName Текущий шаг
     */
    setErrorMsgOnForm: function (responseServer, stepName) {
        if (responseServer != null) {
            if (responseServer.responseJSON.msg) {
                Object.keys(responseServer.responseJSON.msg).forEach(prop => {
                    // Водители - массив и обрабатывать вывод ошибок надо по другому
                    $('.js-error-' + prop).empty();
                    if (stepName == "driver") {
                        Object.keys(responseServer.responseJSON.msg[prop]).forEach(keyDriver => {
                            let error = responseServer.responseJSON.msg[prop][keyDriver];
                            $('#' + prop).addClass('error-input');
                            $('.js-error-' + prop).append(error);
                        });
                    } else {
                        let error = responseServer.responseJSON.msg[prop][0];
                        $('#' + prop).addClass('error-input');
                        $('.js-error-' + prop).append(error);
                    }
                });
            }
        }
    },

    /**
     * Инициализирует страницу при открыттии - выводит данные формы, подключает автокомплиты и тд.
     */
    initPage: function () {
        // Тянет на страницу данные при открытии
        if ($('input').hasClass('js-input-step')) {
            let step = $('.js-input-step').val();
            let url = PROJECT.getStepUrl(step);
            $('.js-menu-item-active').remove();
            $('.js-menu-item-' + step).addClass('js-menu-item-active');

            if (step != 'typecalc') {
                $('.js-prev-step-btn').show();
            }
            if (step == 'calc') {
                PROJECT.calculate(step);
            } else {
                $.ajax({
                    url: url + "?order_id=" + PROJECT.getOrderId(),
                    type: 'get',
                    success: function (data) {
                        $('.js-step-typecalc').empty().append(data);
                    }
                });
            }
            setTimeout(function() {PROJECT.installMaskInput();}, 2000);
        }

        // Устанаваю действия на определенные кнопки:
        // Показывать - скрывать доп. поля при смене типа авто
        $(document).on('change', '.js-car-info-type-car-selector', function () {
            let type = parseInt($(this).val());
            $('.js-car-info-type').hide();
            $('.js-car-info-type-id-'+ type).show();
        });
        // Показывать данные о страхователе, если это не одно лицо с владельцем авто
        $(document).on('change', '#osagoinguruform-insurerisowner', function () {
            let status = parseInt($(this).val());
            $('.js-insurer-is-owner-yes').hide();
            if (status == 0) {
                $('.js-insurer-is-owner-yes').show();
            }
        });
        // В поля ввода ФИО можно вводить только русские буквы и символ пробела
        $(document).on('keyup', '.js-fio-input', function () {
            let that = this;
            let res = /[^А-Яа-я-]/g.exec(that.value);
            that.value = that.value.replace(res, '');
        });

        // Если нажали на кнопку "Перейти к полному расчету" - отправляю запрос на сервер и делаю обновление страницы (кнопка появляется на экране предварительного расчета)
        $(document).on('click', '.js-switch-to-final-calculation-btn', function () {
            $.ajax({
                url: PROJECT.config.urlInguruTypeCalc + "?order_id=" + PROJECT.getOrderId() + "&action=changeToFinalCalculation",
                type: 'get',
                success: function (data) {
                    location.href = '/calculation/?order_id=' + PROJECT.getOrderId();
                },
                error:  function(data){
                    let msg = 'Заказ не найден';
                    alert(msg)
                }
            });
        });

        // Получение и вывод селектора с моделями ТС в конкретной страховой компании, если поменяли марку (в блоке Расчет)
        $(document).on('change', '.js-vehicle-brand-name-company', function () {
            let companyId = parseInt($(this).data('company'));
            let brandName = $(this).val();
            $.ajax({
                url: PROJECT.config.urlInguruCarInfo + "?action=searchModel&companyId=" + companyId + '&brandName=' + brandName + "&term=1",
                type: 'get',
                success: function (data) {
                    let pattern = '<option value="">Выберите модель ТС из списка</option>';
                    $(JSON.parse(data)).each(function(index, value) {
                        pattern += '<option value="' + value['id'] + '">' + value['label'] + '</option>';
                    });
                    $('#js-vehicle-model-name-company-id-' + companyId).empty().append(pattern);
                },
                error:  function(data){
                    let msg = 'Ошибка получения списка моделей ТС';
                    alert(msg)
                }
            });
        });

    },

    /**
     * Добавляет / убирает водителя
     */
    addRemoveDriver:function () {
        // Кнопки переключения Мульти драйв / указать данные о водителях
        $(document).on('click', '.js-is-multi-drive-href', function () {
            let isMultiDrive = parseInt($(this).data('type'));
            $('.js-is-multi-drive-input').val(isMultiDrive);
            setTimeout(function () {
                if (isMultiDrive == 0) {
                    $('.js-add-remove-driver-btn').show();
                    $('.js-drivers-main-block').show();
                } else {
                    $('.js-add-remove-driver-btn').hide();
                    $('.js-next-step-btn').click();
                }
            }, 300);

            return false;
        });

        $(document).on('click', '.js-add-remove-driver-btn', function () {
            let block = $('.js-input-driver-count');
            let currentCount = parseInt(block.val());
            let type = $(this).data('type');
            let count = type == 'add' ? currentCount + 1 : currentCount - 1;
            count = count < 1 ? 0 : count;
            block.val(count);
            if (type == 'add') {
                $.ajax({
                    url: PROJECT.config.urlInguruDriverInfo + '?row=' + count + "&order_id=" + PROJECT.getOrderId() + "&add=1",
                    type: 'get',
                    success: function (data) {
                        $('.js-step-driver').append(data);
                        PROJECT.installDatePicker(jQuery('#osagoinguruform-driverbirthdate_' + count), true);
                        PROJECT.installDatePicker(jQuery('#osagoinguruform-driverexpdate_' + count), true);
                        PROJECT.installDatePicker(jQuery('#osagoinguruform-driverprevlicensedate_' + count), true);
                        PROJECT.installMaskInput();
                    }
                });
            }
            else {
                let rowId = $(this).data('row');
                $('.js-driver-block-id-' + rowId).remove();
            }

            return false;
        });
    },

    /**
     * Инсталирует js календаря
     * @param block  блок для которого установить календарь
     * @param changeYear  Флаг запрета смены года
     * @param minDate Минимальная дата - текущая дата
     * @param maxDate Максимальная дата (в формету new Date())
     */
    installDatePicker:function(block, changeYear = false, minDate = false, maxDate = false)
    {
        let params = {
            "dateFormat":"dd.mm.yy",
            "monthNames":['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
            "dayNamesMin":['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
        }
        if (changeYear) {
            params['changeYear'] = true;
        }
        if (minDate) {
            params['minDate'] = 0;
        }
        if (maxDate) {
            params['maxDate'] = maxDate;
        }

        block.datepicker($.extend({}, $.datepicker.regional['ru'], params));
    },

    /**
     * Инсталирует масски воода
     */
    installMaskInput:function()
    {
        setTimeout(function (){
            let inputMaskDriverSerial = new Inputmask("99**"); // Серия водительского удостоверения
            let inputMaskDriverNumber = new Inputmask("999999"); // Номер водительского удостоверения
            let inputMaskDate = new Inputmask("99.99.9999"); // Дата
            let inputMaskYear = new Inputmask("9999"); // Год
            let inputMaskPassportSerial = new Inputmask("9999"); // Серия паспорта
            let inputMaskPassportNumber = new Inputmask("999999"); // Номер паспорта
            let phoneNumber = new Inputmask("+7 (\\999) 999-99-99"); // Номер телефона

            // Маски в разделе "Водители"
            for (let i=0; i < 10; ++i){
                inputMaskDate.mask($('#osagoinguruform-driverbirthdate_' + i));
                inputMaskDate.mask($('#osagoinguruform-driverexpdate_' + i));
                inputMaskDate.mask($('#osagoinguruform-driverprevlicensedate_' + i));
                inputMaskPassportSerial.mask($('#osagoinguruform-driverlicenseserial_' + i));
                inputMaskPassportNumber.mask($('#osagoinguruform-driverlicensenumber_' + i));
            }
            inputMaskYear.mask($('#osagoinguruform-vehicleyear')); // Год выпуска ТС
            inputMaskDate.mask($('#osagoinguruform-vehicledocdate')); // Дата выдачи документа на авто
            inputMaskDate.mask($('#osagoinguruform-prevlicensedate')); // Дата выдачи предыдущего водительского удостоверения
            inputMaskDate.mask($('#osagoinguruform-vehicledcdate')); // Дата выдачи диагностической карты
            inputMaskDate.mask($('#osagoinguruform-vehicleissuedate')); // Дата действия диагностической карты
            inputMaskPassportSerial.mask($('#osagoinguruform-ownerpassportserial')); // Серия паспорта владельца автомобиля
            inputMaskPassportNumber.mask($('#osagoinguruform-ownerpassportnumber')); // Номер паспорта владельца автомобиля
            inputMaskPassportSerial.mask($('#osagoinguruform-insurerpassportserial')); // Серия паспорта страхователя
            inputMaskPassportNumber.mask($('#osagoinguruform-insurerpassportnumber')); // Номер паспорта страхователя
            inputMaskDate.mask($('#osagoinguruform-ownerpassportdate')); // Дата выдачи паспорта
            inputMaskDate.mask($('#osagoinguruform-insurerpassportdate')); // Дата выдачи паспорта
            inputMaskDate.mask($('#osagoinguruform-ownerdateosagostart')); // Желаемая дата выдачи полиса ОСАГО
            inputMaskDate.mask($('#osagoinguruform-ownerbirthdate')); // Дата рождения владельца автомобиля
            inputMaskDate.mask($('#osagoinguruform-insurerbirthdate')); // Дата рождения страхователя
            inputMaskDriverSerial.mask($('.js-driver-license-serial-mask')); // Серия водительского удостоверения
            inputMaskDriverNumber.mask($('.js-driver-license-number-mask')); // Номер водительского удостоверения
            phoneNumber.mask($('#osagoinguruform-ownerphone')); // Номер телефона

            $('#osagoinguruform-vehiclelicenseplate').on('keypress', function() {
                var that = this;
                setTimeout(function() {
                    var res = /[^а-я0-9]/g.exec(that.value);
                    that.value = that.value.replace(res, '');
                }, 0);
            });

            }, 100);
    },

    /***
     * Возвращает данные об авто по гос. номеру
     */
    setCarInfoByNumber:function () {
        $(document).on('click', '.js-set-car-info-by-number', function () {
            let stepName = $('.js-input-step').val();
            $.ajax({
                type: 'POST',
                url: PROJECT.config.urlInguruCarInfo + '?action=carInfoByNumber&order_id=' + PROJECT.getOrderId(),
                data: $('#js-osago-form').serialize(),
                success: function (data) {
                    // Проставляю данные по авто
                    console.log(data.msg.car);
                    PROJECT.clearErrorMsgOnForm();
                    $('#osagoinguruform-vehicletype').val(data.msg.car.type);
                    $('#osagoinguruform-vehiclepower').val(data.msg.car.power);
                    $('#osagoinguruform-vehicleyear').val(data.msg.car.year);
                    $('#osagoinguruform-vehiclevin').val(data.msg.car.vin);
                    $('#osagoinguruform-vehiclemodel').val(data.msg.car.model);
                    $('#js-vehicle-model-name').val(data.msg.car.model);
                    $('#osagoinguruform-vehiclebrand').val(data.msg.car.brand);
                    $('#js-vehicle-brand-name').val(data.msg.car.brand);
                    $('#osagoinguruform-vehicledoctype').val(data.msg.car.doc_type);
                    $('#osagoinguruform-vehicledocserial').val(data.msg.car.doc_serial);
                    $('#osagoinguruform-vehicledocnumber').val(data.msg.car.doc_number);
                   // $('#osagoinguruform-vehicledocdate').val(data.msg.car.doc_date).val( (new Date()).toString('dd.mm.yyyy') );
                    let dateString = data.msg.car.doc_date;
                    let dateParts = dateString.split("-");
                    $('#osagoinguruform-vehicledocdate').val(dateParts[2] + '.' + dateParts[1] + '.' + dateParts[0]);

                    document.body.innerHTML = dateObject.toString();

                },
                error:  function(data){
                    PROJECT.setErrorMsgOnForm(data, stepName);
                }
            });

            return false;
        });
    },

    /**
     * Выполняет переключение между типами идентификацияя авто (по VIN, кузову, шасси)
     */
    setCarIdentityType:function () {
        $(document).on('change', '.js-vehicle-type-identity-select', function () {
            let type = parseInt($(this).val());
            $('.js-vehicle-type-identity').hide();
            $('.js-vehicle-type-identity-id-' + type).show();
        });
    },

    /**
     * Убирает ошибки с формы (стилизация выделения ошибок)
     */
    clearErrorMsgOnForm:function () {
        $('input').removeClass('error-input');
        $('.js-error-msg').empty();
    },

    /**
     * Очищает формы
     */
    clearFields:function () {
    },
};
PROJECT.init();