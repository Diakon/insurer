/**
 * Скрипт JS для модуля Сравни.ру
*/
var PROJECT = {
    init: function (settings) {
        //конфиги
        PROJECT.config = {
            urlCarInfo: '/sravniru/car-info/',
            urlTypeCalc: '/sravniru/type-calc/',
            urlDriverInfo: '/sravniru/driver-info/',
            urlOwnerInfo: '/sravniru/owner-info/',
            urlPolicy: '/sravniru/policy-info/',
            urlCalc: '/sravniru/calculation/',
            urlPayment: '/sravniru/payments/',
        };

        $.extend(PROJECT.config, settings);

        PROJECT.installMaskInput();
        PROJECT.addRemoveDriver();
        PROJECT.initPage();
        PROJECT.stepSwitch();
        PROJECT.clearErrorMsgOnForm();
        PROJECT.setErrorMsgOnForm(responseServer = null, stepName = null);
        PROJECT.setCarInfoByNumber();
        PROJECT.setCarIdentityType();
        PROJECT.payment();
    },

    /**
     * Возвращает ID заказа
     * @returns {number}
     */
    getOrderId:function () {
        return parseInt($('.js-order-id').val());
    },

    /**
     * Возвращает url для ajax запроса
     * @param step
     * @returns {string}
     */
    getStepUrl:function (step) {
        let url = "";
        switch (step) {
            case "typecalc":
                url = PROJECT.config.urlTypeCalc;
                break;
            case "car":
                url = PROJECT.config.urlCarInfo;
                break;
            case "driver":
                url = PROJECT.config.urlDriverInfo;
                break;
            case "owner":
                url = PROJECT.config.urlOwnerInfo;
                break;
            case "policy":
                url = PROJECT.config.urlPolicy;
                break;
            case "calc":
                url = PROJECT.config.urlCalc;
        }

        return url;
    },

    /**
     * Переход по шагам оформления ОСАГО
     */
    stepSwitch:function () {
        /**
         * Кнопка возврата на предыдущий шаг
         */
        $(document).on('click', '.js-prev-step-btn', function () {
            let currentStepName = $('.js-input-step').val(); // Текеущий шаг
            // Получаю шаг, который надо обработать
            let prevStepName = "";
            $(".js-step").each(function() {
                let stepName = $(this).data("step");
                // Дошли до текущего шага - надо вернуться к предыдущему
                if (stepName == currentStepName) {
                    return false;
                }
                prevStepName = stepName;
            });

            if (prevStepName == "typecalc") {
                $(this).hide(); // Прячу кнопку т.к. на 1ом шаге (car) ее не надо показывать
            }
            $('.js-input-step').val(prevStepName);
            prevStep(prevStepName);

            return false;
        });

        /**
         * Возврат на предыдущий шаг
         * @param stepName
         */
        function prevStep(stepName)
        {
            $('.js-ajax-load-image').show();
            $('.js-step').hide(); // Скрфваю все формы - showForm покажет нужную, когда подтянет данные
            // В меню ставлю активным (выделенным) новый шаг
            $('.js-menu-item').removeClass('js-menu-item-active')
            $('.js-menu-item-' + stepName).addClass('js-menu-item-active');
            // Для водителей надо очистить от старых страниц загруженных по ajax
            if (stepName == 'driver') {
                $('.js-driver-block').remove();
            }
            showForm(stepName);
            $('.js-ajax-load-image').hide();
            setTimeout(function() {PROJECT.installMaskInput();}, 2000);
        }

        // ----------------------------------

        /**
         * Кнопка перехода на следующий шаг
         */
        $(document).on('click', '.js-next-step-btn', function () {
            // Получаю шаг, который надо обработать
            let stepName = $('.js-input-step').val();
            nextStep(stepName);

            return false;
        });

        /**
         * Переход на следующий шаг
         * @param stepName
         */
        function nextStep(stepName) {
            $('.js-ajax-load-image').show();
            let url = PROJECT.getStepUrl(stepName);
            // Проверяю введенные данные
            let form = $('#js-osago-form').serialize();
            PROJECT.clearErrorMsgOnForm()

            $.ajax({
                type: 'POST',
                url: url + '?action=validate&order_id=' + PROJECT.getOrderId(),
                data: form,
                success: function(data) {
                    $('.js-ajax-load-image').hide();
                    let step = data.msg.step;

                    if (data.msg.orderId) {
                        $('.js-order-id').val(parseInt(data.msg.orderId))
                    }
                    $('.js-step').hide(); // Скрываю все формы - showForm покажет нужную, когда подтянет данные
                    // В меню ставлю активным (выделенным) новый шаг
                    $('.js-menu-item').removeClass('js-menu-item-active')
                    $('.js-menu-item-' + step).addClass('js-menu-item-active');

                    // Для водителей надо очистить от старых страниц загруженных по ajax
                    if (step == 'driver') {
                        $('.js-driver-block').remove();
                    }

                    // Показываю - скрываю кнопку возврата назад в зависимости от шага
                    if (step != "typecalc") {
                        $('.js-prev-step-btn').show();
                    } else {
                        $('.js-prev-step-btn').hide();
                    }

                    // Если последний шаг - расчет (calc), то произвожу отправку всех введеных на прошлом шаге данных и вывод результата
                    if (step == "calc") {
                        PROJECT.calculate(step);
                    } else {
                        showForm(step);
                    }
                },
                error:  function(data){
                    $('.js-ajax-load-image').hide();
                    PROJECT.setErrorMsgOnForm(data, stepName);
                }
            });
        }

        /**
         * Переход на следующий шаг
         *
         * @param step
         * @returns {boolean}
         */
        function showForm(step) {
            $('.js-action-reload-page-block').hide();
            $('.js-next-step-btn').show();
            $('.js-driver-btn').hide();
            $('.js-input-step').val(step); // Скрытое поле текущего шага
            let html = $('.js-step-' + step).html();
            let url = PROJECT.getStepUrl(step);

            // Если шаг добавления водителей - показываю кнопки +/- для добавления
            if (step == "driver") {
                $('.js-driver-btn').show();
            }

            // Форма не тянулась - тяну
            let form = $('#js-osago-form').serialize();
            $.ajax({
                url: url + '?action=formHtml&order_id=' + PROJECT.getOrderId(),
                type: 'post',
                data: form,
                success: function (data) {
                    $('.js-step-' + step).empty().append(data).show();

                    PROJECT.installDatePicker(jQuery('.js-date-picker-change-year-yes'), true);
                    PROJECT.installDatePicker(jQuery('.js-date-picker-change-year-no'), false);
                    PROJECT.installDatePicker(jQuery('.js-date-picker-change-year-yes-mindate'), false, true);
                    PROJECT.installDatePicker(jQuery('.js-date-picker-change-year-no-mindate'), false, true);
                    PROJECT.installDatePicker(jQuery('.js-date-picker-change-year-no-mindate-4day'), false, true, false, 4);
                    // Инсталирую автокомплит, для шага "Автомобиль", "Водитель", "Собственник" или "Полис"
                    switch (step) {
                        case "owner":
                            jQuery('#osagosravniruform-ownercity').autocomplete({"source":"\/sravniru\/owner-info\/?action=searchCity","minLength":"3","select":function( event, ui ) {
                                    $('#osagosravniruform-ownercity').val(ui.item.id);
                                }});
                            jQuery('#osagosravniruform-ownerdadataaddress').autocomplete({"source":"\/sravniru\/owner-info\/?action=searchAddress","minLength":"3","select":function( event, ui ) {
                                    $(this).val(ui.item.value);
                                }});
                            jQuery('#osagosravniruform-insurerdadataaddress').autocomplete({"source":"\/sravniru\/owner-info\/?action=searchAddress","minLength":"3","select":function( event, ui ) {
                                    $(this).val(ui.item.value);
                                }});
                            break;
                        case "driver":
                            let isMultiDrive = parseInt($('.js-is-multi-drive-input').val());
                            if (isMultiDrive == 1) {
                                $('.js-add-remove-driver-btn').hide(); // Скрываю кнопку "Добавить водителя", если мультидрайв
                            }
                            break;
                        case "car":
                            // Дата выдачи диагностической карты не может быть больше 2х лет от текущей даты
                            let issueDate = new Date();
                            let yearIssueDate = issueDate.getFullYear();
                            let monthIssueDate = issueDate.getMonth();
                            let dayIssueDate = issueDate.getDate();
                            issueDate = new Date(yearIssueDate + 2, monthIssueDate, dayIssueDate);
                            PROJECT.installDatePicker(jQuery('#osagosravniruform-vehicleissuedate'),
                                false,
                                false,
                                issueDate
                            );
                            break;
                    }
                    window.moveTo(0,0); // Перемещаю к верху страницы экран
                    PROJECT.installMaskInput();
                }
            });

            return true;
        }
    },

    /**
     * Инсталирует масски воода
     */
    installMaskInput:function()
    {
        setTimeout(function (){
            let inputMaskDriverSerial = new Inputmask("99**"); // Серия водительского удостоверения
            let inputMaskDriverNumber = new Inputmask("999999"); // Номер водительского удостоверения
            let inputMaskDate = new Inputmask("99.99.9999"); // Дата
            let inputMaskYear = new Inputmask("9999"); // Год
            let inputMaskPassportSerial = new Inputmask("9999"); // Серия паспорта
            let inputMaskPassportNumber = new Inputmask("999999"); // Номер паспорта
            let phoneNumber = new Inputmask("+7 (\\999) 999-99-99"); // Номер телефона

            inputMaskYear.mask($('.js-mask-year')); // Год
            inputMaskDate.mask($('.js-mask-date')); // Дата
            inputMaskPassportSerial.mask($('.js-mask-pass-serial')); // Серия паспорта
            inputMaskPassportNumber.mask($('.js-mask-pass-number')); // Номер паспорта
            inputMaskDriverSerial.mask($('.js-mask-driver-serial')); // Серия водительского удостоверения
            inputMaskDriverNumber.mask($('.js-mask-driver-number')); // Номер водительского удостоверения
            phoneNumber.mask($('.js-mask-phone')); // Номер телефона

            $('#osagosravniruform-vehiclelicenseplate').on('keypress', function() {
                var that = this;
                setTimeout(function() {
                    var res = /[^а-я0-9]/g.exec(that.value);
                    that.value = that.value.replace(res, '');
                }, 0);
            });

        }, 100);
    },

    /**
     * Добавляет / убирает водителя
     */
    addRemoveDriver:function () {
        // Кнопки переключения Мульти драйв / указать данные о водителях
        $(document).on('click', '.js-is-multi-drive-href', function () {
            let isMultiDrive = parseInt($(this).data('type'));
            $('.js-is-multi-drive-input').val(isMultiDrive);
            setTimeout(function () {
                if (isMultiDrive == 0) {
                    $('.js-add-remove-driver-btn').show();
                    $('.js-drivers-main-block').show();
                } else {
                    $('.js-add-remove-driver-btn').hide();
                    $('.js-next-step-btn').click();
                }
            }, 300);

            return false;
        });

        $(document).on('click', '.js-add-remove-driver-btn', function () {
            let block = $('.js-input-driver-count');
            let currentCount = parseInt(block.val());
            let type = $(this).data('type');
            let count = type == 'add' ? currentCount + 1 : currentCount - 1;
            count = count < 1 ? 0 : count;
            block.val(count);
            if (type == 'add') {
                $.ajax({
                    url: PROJECT.config.urlDriverInfo + '?row=' + count + "&order_id=" + PROJECT.getOrderId() + "&add=1",
                    type: 'get',
                    success: function (data) {
                        $('.js-step-driver').append(data);
                        PROJECT.installDatePicker(jQuery('.js-date-picker-change-year-yes'), true);
                        PROJECT.installDatePicker(jQuery('.js-date-picker-change-year-no'), false);
                        PROJECT.installDatePicker(jQuery('.js-date-picker-change-year-yes-mindate'), false, true);
                        PROJECT.installDatePicker(jQuery('.js-date-picker-change-year-no-mindate'), false, true);

                        PROJECT.installMaskInput();
                    }
                });
            }
            else {
                let rowId = $(this).data('row');
                $('.js-driver-block-id-' + rowId).remove();
            }

            return false;
        });
    },

    /**
     * Инициализирует страницу при открыттии - выводит данные формы, подключает автокомплиты и тд.
     */
    initPage: function () {
        // Тянет на страницу данные при открытии
        if ($('input').hasClass('js-input-step')) {
            let step = $('.js-input-step').val();
            let url = PROJECT.getStepUrl(step);
            $('.js-menu-item-active').remove();
            $('.js-menu-item-' + step).addClass('js-menu-item-active');

            if (step != 'typecalc') {
                $('.js-prev-step-btn').show();
            }
            if (step == 'calc') {
                PROJECT.calculate(step);
            } else {
                $.ajax({
                    url: url + "?order_id=" + PROJECT.getOrderId(),
                    type: 'get',
                    success: function (data) {
                        $('.js-step-typecalc').empty().append(data);
                    }
                });
            }
            setTimeout(function() {
                PROJECT.installMaskInput();
                PROJECT.installDatePicker(jQuery('.js-date-picker-change-year-yes'), true);
                PROJECT.installDatePicker(jQuery('.js-date-picker-change-year-no'), false);
                PROJECT.installDatePicker(jQuery('.js-date-picker-change-year-yes-mindate'), false, true);
                PROJECT.installDatePicker(jQuery('.js-date-picker-change-year-no-mindate'), false, true);
                PROJECT.installDatePicker(jQuery('.js-date-picker-change-year-no-mindate-4day'), false, true, false, 4);
                }, 1000);
        }

        // Устанаваю действия на определенные кнопки:
        // Показывать - скрывать доп. поля при смене типа авто
        $(document).on('change', '.js-car-info-type-car-selector', function () {
            let type = parseInt($(this).val());
            $('.js-car-info-type').hide();
            $('.js-car-info-type-id-'+ type).show();
        });
        // Показывать данные о страхователе, если это не одно лицо с владельцем авто
        $(document).on('change', '#osagosravniruform-insurerisowner', function () {
            let status = parseInt($(this).val());
            $('.js-insurer-is-owner-yes').hide();
            if (status == 0) {
                $('.js-insurer-is-owner-yes').show();
            }
        });
        // В поля ввода ФИО можно вводить только русские буквы и символ пробела
        $(document).on('keyup', '.js-fio-input', function () {
            let that = this;
            let res = /[^А-Яа-я-]/g.exec(that.value);
            that.value = that.value.replace(res, '');
        });

        // Если нажали на кнопку "Перейти к полному расчету" - отправляю запрос на сервер и делаю обновление страницы (кнопка появляется на экране предварительного расчета)
        $(document).on('click', '.js-switch-to-final-calculation-btn', function () {
            $.ajax({
                url: PROJECT.config.urlTypeCalc + "?order_id=" + PROJECT.getOrderId() + "&action=changeToFinalCalculation",
                type: 'get',
                success: function (data) {
                    location.href = '/calculation/?order_id=' + PROJECT.getOrderId();
                },
                error:  function(data){
                    let msg = 'Заказ не найден';
                    alert(msg)
                }
            });
        });

        // При смене бренда - обновлять список моделей
        $(document).on('change', '.js-vehicle-brand-id', function () {
            PROJECT.updateBrandList();
        });

        // Показывать данные о страхователе, если это не одно лицо с владельцем авто
        $(document).on('change', '#js-insurer-is-owner-switch', function () {
            let isset = $(this).is(':checked');
            let status = isset ? 1 : 0;
            $('#osagosravniruform-insurerisowner').val(status);
            $('.js-insurer-is-owner-yes').hide();
            if (status == 0) {
                $('.js-insurer-is-owner-yes').show();
            }
        });

        // Переключатель "Есть предыдущий полис"
        $(document).on('change', '#js-prev-policy-isset-switch', function () {
            let isset = $(this).is(':checked');
            $('.js-prev-policy-isset-input').val(isset ? 1 : 0);
            $('.js-isset-prev-policy-block').hide();
            if (isset) {
                $('.js-isset-prev-policy-block').show();
            }
        });

        // Смена даты в поле "Окончание действия полиса" при вводе даты начала действия полиса (прибавляем 1 год)
        $(document).on('change', '.js-osago-start-date-input', function () {
            let date = $(this).val();
            if (date && date != "") {
                let startDate = moment(date, 'DD.MM.YYYY');
                let endDate = startDate.clone();
                endDate.add(1, 'years').subtract('1', 'days');
                endDate = endDate.format('DD.MM.YYYY');
                $('.js-osago-end-date-input').empty().val(endDate == "Invalid date" ? "" : endDate);
            }
        });
    },

    /**
     * Обновляет список марок авто (выпадающий список)
     * @param carModelId
     * @param carBrandId
     */
    updateBrandList:function (carModelId = false, carBrandId = false) {
        let brandId = carBrandId ? carBrandId : $('.js-vehicle-brand-id').val();
        $.ajax({
            url: PROJECT.config.urlCarInfo + "?order_id=" + PROJECT.getOrderId() + "&action=searchModel",
            type: 'post',
            data: {brandId:brandId},
            success: function (data) {
                data = JSON.parse(data);
                let pattern = '<option value="">Выберите модкль ТС</option>';
                $.each(data, function(index, value) {
                    pattern += '<option value="' + index + '">' + value + '</option>'
                });
                let block = $(".js-vehicle-model-id");
                block.empty().append(pattern);
                if (carModelId) {
                    setTimeout(function (){block.val(carModelId)}, 500);
                }
                return true;
            },
            error:  function(data){
                let msg = 'Ошибка получения списка моделей';
                alert(msg);
                return false;
            }
        });
    },

    /**
     * Выполняет расчет стоимости полиса
     * @param step
     */
    calculate:function (step) {
        $('.js-ajax-load-image').show();
        $('.js-next-step-btn').hide();
        $('.js-step-' + step).empty();
        $('.js-input-step').val(step);
        let url = PROJECT.getStepUrl(step);
        let form = $('#js-osago-form').serialize();
        var selectedCompany = parseInt($('.js-input-selected-company').val());
        // Получаю тип расчета
        $.when(
            $.ajax({
                type: 'GET',
                url: PROJECT.config.urlTypeCalc + "?action=typeCalc&order_id=" + PROJECT.getOrderId(),
            })).then(function(data, textStatus, jqXHR) {
                let isPreCalculate = parseInt(data.msg);
                isPreCalculate = isPreCalculate == 1;
                // Если предварительный расчет - отправляю все данные и жду ответ (быстрый расчет)
                if (isPreCalculate) {
                    $('.js-switch-to-final-calculation-btn').hide();
                    $.ajax({
                        type: 'POST',
                        url: url + "?order_id=" + PROJECT.getOrderId(),
                        data: form,
                        success: function (data) {
                            $('.js-ajax-load-image').hide();
                            $('.js-step-' + step).empty().append(data).show();
                            $('.js-switch-to-final-calculation-btn').show(); // Показываю кнопку "Перейти к полному расчету"
                        },
                        error:  function(data){
                            $('.js-ajax-load-image').hide();
                            alert("Ошибка запроса")
                        }
                    });
                }
                else {
                    // Полный расчет
                    $.ajax({
                        type: 'POST',
                        url: url + "?order_id=" + PROJECT.getOrderId(),
                        data: form,
                        success: function (data) {
                            $('.js-step-' + step).append(data).show();
                            $('.js-ajax-load-image').hide();
                        },
                        error:  function(data){
                            $('.js-ajax-load-image').hide();
                            alert("Ошибка запроса");
                        }
                    });
                }

                return true;
            }
        );
    },

    /**
     * Выполняет переключение между типами идентификацияя авто (по VIN, кузову, шасси)
     */
    setCarIdentityType:function () {
        $(document).on('change', '.js-vehicle-type-identity-select', function () {
            let type = parseInt($(this).val());
            $('.js-vehicle-type-identity').hide();
            $('.js-vehicle-type-identity-id-' + type).show();
        });
    },

    /**
     * Убирает ошибки с формы (стилизация выделения ошибок)
     */
    clearErrorMsgOnForm:function () {
        $('input, select').removeClass('error-input');
        $('.js-error-msg').empty();
    },

    /**
     * Выводит ошибки на форме
     * @param responseServer Ответ сервера
     * @param stepName Текущий шаг
     */
    setErrorMsgOnForm: function (responseServer, stepName) {
        if (responseServer != null) {
            if (responseServer.responseJSON.msg) {
                Object.keys(responseServer.responseJSON.msg).forEach(prop => {
                    // Водители - массив и обрабатывать вывод ошибок надо по другому
                    $('.js-error-' + prop).empty();
                    if (stepName == "driver") {
                        Object.keys(responseServer.responseJSON.msg[prop]).forEach(keyDriver => {
                            let error = responseServer.responseJSON.msg[prop][keyDriver];
                            $('#' + prop).addClass('error-input');
                            $('.js-error-' + prop).append(error);
                        });
                    } else {
                        let error = responseServer.responseJSON.msg[prop][0];
                        $('#' + prop).addClass('error-input');
                        $('.js-error-' + prop).append(error);
                    }
                });
            }
        }
    },

    /**
     * Инсталирует js календаря
     * @param block  блок для которого установить календарь
     * @param changeYear  Флаг запрета смены года
     * @param minDate Минимальная дата - текущая дата
     * @param maxDate Максимальная дата (в формету new Date())
     * @param minDay
     */
    installDatePicker:function(block, changeYear = false, minDate = false, maxDate = false, minDay = false)
    {
        let params = {
            "dateFormat":"dd.mm.yy",
            "monthNames":['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
            "dayNamesMin":['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
        }
        if (changeYear) {
            params['changeYear'] = true;
        }
        if (minDate) {
            params['minDate'] = minDay ? minDay : 0;
        }
        if (maxDate) {
            params['maxDate'] = maxDate;
        }

        block.datepicker($.extend({}, $.datepicker.regional['ru'], params));
    },

    /***
     * Возвращает данные об авто по гос. номеру
     */
    setCarInfoByNumber:function () {
        $(document).on('click', '.js-set-car-info-by-number', function () {
            let stepName = $('.js-input-step').val();
            $.ajax({
                type: 'POST',
                url: PROJECT.config.urlCarInfo + '?action=carInfoByNumber&order_id=' + PROJECT.getOrderId(),
                data: $('#js-osago-form').serialize(),
                success: function (data) {
                    // Проставляю данные по авто
                    console.log(data.msg.car);
                    PROJECT.clearErrorMsgOnForm();
                    $('#osagosravniruform-vehicletype').val(data.msg.car.type);
                    $('#osagosravniruform-vehiclepower').val(data.msg.car.power);
                    $('#osagosravniruform-vehicleyear').val(data.msg.car.year);
                    $('#osagosravniruform-vehiclevin').val(data.msg.car.vin);
                    $('#osagosravniruform-vehiclebrand').val(data.msg.car.brand_id);
                    $('#osagosravniruform-vehicledoctype').val(data.msg.car.doc_type);
                    $('#osagosravniruform-vehicledocserial').val(data.msg.car.doc_serial);
                    $('#osagosravniruform-vehicledocnumber').val(data.msg.car.doc_number);

                    PROJECT.updateBrandList(data.msg.car.model_id, data.msg.car.brand_id);
                    let dateString = data.msg.car.doc_date;
                    if (dateString != null) {
                        let dateParts = dateString.split("-");
                        console.log(dateParts);
                        $('#osagosravniruform-vehicledocdate').val(dateParts[2] + '.' + dateParts[1] + '.' + dateParts[0]);
                    }

                },
                error:  function(data){
                    PROJECT.setErrorMsgOnForm(data, stepName);
                }
            });

            return false;
        });
    },

    /**
     * Оперции с оплатой полиса
     */
    payment:function () {
        // Нажали на сохранить в "Ожидают оплаты"
        $(document).on('click', '.js-save-to-awaiting-payment', function () {
            let awaiting = $(this).hasClass('js-save-to-awaiting-payment') ? 1 : 0;
            let id = $(this).data('id');
            let urlPayment = $(this).data('url');
            let hash = $(this).data('hash');
            let companyId = $(this).data('company-id');
            let companyName = $(this).data('company-name');
            $(this).attr('disabled', true);
            $(this).append('<br>Пожалуйста, подождите - Ваш запрос обрабатывается.');
            $.ajax({
                type: 'POST',
                url: PROJECT.config.urlPayment + '?order_id=' + PROJECT.getOrderId(),
                data: {'id': id, 'companyId': companyId, 'companyName': companyName, 'url': urlPayment, 'hash': hash, 'awaiting': awaiting},
                success: function (data) {
                    document.location.href = data.msg['url'];
                },
                error:  function(data){
                    let msg = 'Возникла ошибка.\n';
                    if (data.msg) {
                        msg += "Страховая компания вернула следующий ответ:"
                        msg += data.msg;
                    }
                    msg += "\nПожалуйста, попробуйте снова или обратитесь к администратору.";
                    alert(msg)
                }
            });

            return false;
        });

    },
};
PROJECT.init();