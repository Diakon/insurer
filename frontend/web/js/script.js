/**
 * Основной скрипт подключаемый всегда на всех страницах
 * @type {{init: BASE_SCRIPT.init, supportChat: ((function(): number)|*)}}
 */
var BASE_SCRIPT = {
    init: function (settings) {
        //конфиги
        BASE_SCRIPT.config = {

        };
        $.extend(BASE_SCRIPT.config, settings);
        BASE_SCRIPT.chatSupport();
        BASE_SCRIPT.orders();
        BASE_SCRIPT.faq();
    },

    /**
     * Страница FAQ
     */
    faq:function () {
      $(document).on('click', '.js-faq-block', function () {
          $(this).find('.js-faq-answer').slideToggle();
          return false;
      });
    },

    /**
     * Страница заказов
     */
    orders:function () {
        // Кнопка "Поделиться ссылкой на оплату" - копирование в буфер обмена
        $(document).on('click', '.js-share-payment-link', function () {
            let paymentUrl = document.getElementById("js-payment-url-link-input");
            paymentUrl.select();
            document.execCommand("copy");
            alert("Ссылка на оплату скопирована в буфер обмена.");
            return false;
        });
        // Если это страница перехода на экран оплаты - делаю ajax запрос получения ссылки
        $(document).on('click', '.js-redirect-to-payment-order-accept-btn', function () {
            $('.js-ajax-load-image').show();
            $('.js-payment-link-page-error').empty();
            $(this).hide();
            $.ajax({
                type: 'POST',
                url: location.href,
                success: function (data) {
                    document.location.href = data.msg['url'];
                },
                error:  function(data){
                    $('.js-ajax-load-image').hide();
                    $('.js-redirect-to-payment-order-accept-btn').show();
                    let msg = 'Ссылка на оплату устарела. Пожалуйста, попросите сгенерировать новую ссылку.';
                    $('.js-payment-link-page-error').empty().append(msg);
                }
            });
        });
        // Кнопка получения ссылок документов на оплату
        $(document).on('click', '.js-payment-document-link', function () {
            let url = $(this).data('url');
            let block = $(this).parent().find('.js-payment-document-block');
            $.ajax({
                type: 'POST',
                url: url,
                success: function (data) {
                    if (data.msg.documents) {
                        let links = '';
                        $(data.msg.documents).each(function(key, value) {
                            if (value.name != 'Экземпляр полиса ОСАГО для страховщика.pdf') {
                                links += '<a class="btn btn-sm btn-primary" style="margin-bottom: 5px;" href="' + value.url + '" target="_blank">' + value.name + '</a><br>';
                            }
                        });
                        block.empty().append(links);
                        block.show();
                    }
                },
                error:  function(data){
                    let msg = '<p style="color: #FF6B6B">Ошибка при получении ссылок об оплаченом заказе. Пожалуйста, попробуйте снова или обратитесь в поддержку.</p>';
                    block.empty().append(msg);
                    block.show();
                }
            });

            return false;
        });
    },

    /**
     * Чат поддержки
     */
    chatSupport:function () {
        // цвет кнопки чата
        window.ChatraSetup = {
            buttonSize: 1,
            buttonPosition: window.innerWidth < 1024 ? // порог ширины
                'bl' : // положение кнопки чата на маленьких экранах
                'br',  // положение кнопки чата на больших экранах
        };
    }
};
BASE_SCRIPT.init();