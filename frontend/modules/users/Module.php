<?php
namespace frontend\modules\users;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\users\controllers';

    public function init()
    {
        parent::init();
    }
}
