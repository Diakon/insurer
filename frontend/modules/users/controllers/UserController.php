<?php
namespace frontend\modules\users\controllers;

use common\modules\users\services\UserService;
use frontend\modules\users\models\forms\LoginForm;
use Yii;
use frontend\controllers\SiteController;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * Class UserController
 * @package frontend\modules\users\controllers
 */
class UserController extends SiteController {

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup', 'profile'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'profile'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            /*
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
            */
        ]);
    }

    /**
     * Авторизация
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Выход
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Профиль пользователя
     *
     * @return mixed
     */
    public function actionProfile()
    {
        // Получаю данные о пользователе
        $service = new UserService();
        $profile = $service->getUserProfileById(Yii::$app->user->id);

        return $this->render('profile', ['profile' => $profile]);
    }
}