<?php
/**
 * @var $profile array
 */

$this->title = 'Страхование: Профиль';
?>
<div class="col-md-3"></div>
<div class="col-md-6">
    <div class="row">
        <img src="<?= !empty($profile['profile']['avatar']) ? $profile['profile']['avatar'] : '/images/no-avatar.png' ?>" width="70px">
        <span class="text-label">Логин:</span> <b><?= $profile['login'] ?></b>
    </div>
    <div class="row">
        <h3>
            <?php
            $fio = '';
            if (!empty($profile['personalData']['last_name'])) {
                $fio .= $profile['personalData']['last_name'] . ' ';
            }
            if (!empty($profile['personalData']['first_name'])) {
                $fio .= $profile['personalData']['first_name'] . ' ';
            }
            if (!empty($profile['personalData']['second_name'])) {
                $fio .= $profile['personalData']['second_name'] . ' ';
            }
            ?>
            <?= trim($fio) ?>
        </h3>
    </div>
    <div class="row" style="margin-top: 20px;">
        <span class="text-label">Телефон:</span> <b><?= $profile['personalData']['phone'] ?? "" ?></b>
    </div>
    <div class="row" style="margin-top: 20px;">
        <span class="text-label">Почта:</span> <b><?= $profile['profile']['email'] ?? "" ?></b>
    </div>
</div>
<div class="col-md-3"></div>
<div class="col-md-12">
    <div class="row">
        <div align="center" style="margin-top: 100px;">
            <a class="btn btn_big btn_wide btn_purple btn_mt" href="<?= \yii\helpers\Url::to(['/logout']) ?>">
                <div style="clear: both; font-size: 14px;font-weight: 500;">
                    Выход
                </div>
            </a>
        </div>
    </div>
</div>