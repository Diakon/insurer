<?php
/**
 * Поля для расширенного расчета
 * @var $model \common\modules\inguru\forms\OsagoInguruForm
 */

use common\modules\inguru\dictionaries\OsagoInguruFormDictionary;
use common\modules\inguru\dictionaries\PreCalculationDictionary;
use yii\helpers\Html;

$labels = $model->attributeLabels();
?>

<div id="js-car-extended-info-block" style="<?= $model->preCalculation == OsagoInguruFormDictionary::PRE_CALCULATION_NO ? '' : 'display:none' ?>">
    <div class="row">
        <?= Html::activeLabel($model, 'vehicleIdentity') ?><br>
        <?= Html::activeDropDownList($model, 'vehicleIdentity', $model->getTypeCarIdentity(),
            ['class' => 'form-control js-vehicle-type-identity-select', 'prompt' => $labels['vehicleIdentity']]); ?>
        <div class="js-error-msg js-error-osagoinguruform-vehicleidentity"></div>
    </div>
    <div class="row js-vehicle-type-identity js-vehicle-type-identity-id-<?= OsagoInguruFormDictionary::CAR_HAVE_VIN ?>"
         style="<?= empty($model->vehicleIdentity) ? "" : "display:none" ?>">
        <?= Html::activeLabel($model, 'vehicleVin') ?><br>
        <?= Html::activeTextInput($model, 'vehicleVin', ['class' => 'form-control', 'placeholder' => $labels['vehicleVin']]); ?>
        <div class="js-error-msg js-error-osagoinguruform-vehiclevin"></div>
    </div>
    <div class="row js-vehicle-type-identity js-vehicle-type-identity-id-<?= OsagoInguruFormDictionary::CAR_HAVE_BODY ?>"
         style="<?= $model->vehicleIdentity == OsagoInguruFormDictionary::CAR_HAVE_BODY ? "" : "display:none" ?>">
        <?= Html::activeLabel($model, 'vehicleBodyNum') ?><br>
        <?= Html::activeTextInput($model, 'vehicleBodyNum', ['class' => 'form-control', 'placeholder' => $labels['vehicleBodyNum']]); ?>
        <div class="js-error-msg js-error-osagoinguruform-vehiclebodynum"></div>
    </div>
    <div class="row js-vehicle-type-identity js-vehicle-type-identity-id-<?= OsagoInguruFormDictionary::CAR_HAVE_CHASSIS ?>"
         style="<?= $model->vehicleIdentity == OsagoInguruFormDictionary::CAR_HAVE_CHASSIS ? "" : "display:none" ?>">
        <?= Html::activeLabel($model, 'vehicleChassisNum') ?><br>
        <?= Html::activeTextInput($model, 'vehicleChassisNum', ['class' => 'form-control', 'placeholder' => $labels['vehicleChassisNum']]); ?>
        <div class="js-error-msg js-error-osagoinguruform-vehiclechassisnum"></div>
    </div>

    <div class="row">
        <?= Html::activeLabel($model, 'vehicleBrand') ?><br>
        <?= \yii\jui\AutoComplete::widget([
            'model' => $model,
            'attribute' => 'vehicleBrand',
            'clientOptions' => [
                'source' => \yii\helpers\Url::to(['/inguru/car-info/', 'action' => 'searchBrand']),
                'minLength'=>'3',
                'select' => new \yii\web\JsExpression("function( event, ui ) {
                $(this).val(ui.item.id);
                }")
            ],
            'options'=>[
                'id' => 'js-vehicle-brand-name',
                'class' => 'form-control',
                'placeholder' => $labels['vehicleBrand']
            ]
        ]);
        ?>
        <div class="js-error-msg js-error-osagoinguruform-vehiclebrand"></div>
    </div>
    <div class="row">
        <?= Html::activeLabel($model, 'vehicleModel') ?><br>
        <?= \yii\jui\AutoComplete::widget([
            'model' => $model,
            'attribute' => 'vehicleModel',
            'clientOptions' => [
                'source' => \yii\helpers\Url::to(['/inguru/car-info/', 'action' => 'searchModel', 'brandName' => $model->vehicleBrand]),
                'minLength'=>'3',
                'select' => new \yii\web\JsExpression("function( event, ui ) {
                $(this).val(ui.item.id);
                }")
            ],
            'options'=>[
                'id' => 'js-vehicle-model-name',
                'class' => 'form-control',
                'placeholder' => $labels['vehicleModel']
            ]
        ]);
        ?>
        <div class="js-error-msg js-error-osagoinguruform-vehiclemodel"></div>
    </div>

    <div class="row">
        <?= Html::activeLabel($model, 'vehicleDocType') ?><br>
        <?= Html::activeDropDownList($model, 'vehicleDocType', $model->getTypeDocCar(),  ['class' => 'form-control', 'prompt' => $labels['vehicleDocType']]); ?>
        <div class="js-error-msg js-error-osagoinguruform-vehicledoctype"></div>
    </div>
    <div class="row">
        <?= Html::activeLabel($model, 'vehicleDocSerial') ?><br>
        <?= Html::activeTextInput($model, 'vehicleDocSerial', ['class' => 'form-control js-driver-license-serial-mask', 'placeholder' => $labels['vehicleDocSerial']]); ?>
        <div class="js-error-msg js-error-osagoinguruform-vehicledocserial"></div>
    </div>
    <div class="row">
        <?= Html::activeLabel($model, 'vehicleDocNumber') ?><br>
        <?= Html::activeTextInput($model, 'vehicleDocNumber', ['class' => 'form-control js-driver-license-number-mask', 'placeholder' => $labels['vehicleDocNumber']]); ?>
        <div class="js-error-msg js-error-osagoinguruform-vehicledocnumber"></div>
    </div>
    <div class="row">
        <?= Html::activeLabel($model, 'vehicleDocDate') ?><br>
        <?= Html::activeTextInput($model, 'vehicleDocDate', ['value' => Yii::$app->formatter->asDate($model->vehicleDocDate, 'php:d.m.Y'), 'class' => 'form-control', 'placeholder' => $labels['vehicleDocDate']]); ?>
        <div class="js-error-msg js-error-osagoinguruform-vehicledocdate"></div>
    </div>


    <!-- Если тип ТС грузовик, то надо ввести данные по массе  -->
    <div class="js-car-info-type js-car-info-type-id-<?= PreCalculationDictionary::VEHICLE_TYPE_TRUCK ?> js-car-info-type-id-<?= PreCalculationDictionary::VEHICLE_TYPE_TRUCK_MORE_16 ?>"
         style="<?= !in_array($model->vehicleType,
             [PreCalculationDictionary::VEHICLE_TYPE_TRUCK, PreCalculationDictionary::VEHICLE_TYPE_TRUCK_MORE_16]) ? "display:none" : "" ?>">
        <div class="row">
            <?= Html::activeLabel($model, 'vehicleWeight') ?><br>
            <?= Html::activeTextInput($model, 'vehicleWeight', ['class' => 'form-control', 'placeholder' => $labels['vehicleSeats']]); ?>
            <div class="js-error-msg js-error-osagoinguruform-vehicleweight"></div>
        </div>
        <div class="row">
            <?= Html::activeLabel($model, 'vehicleMaxWeight') ?><br>
            <?= Html::activeTextInput($model, 'vehicleMaxWeight', ['class' => 'form-control', 'placeholder' => $labels['vehicleMaxWeight']]); ?>
            <div class="js-error-msg js-error-osagoinguruform-vehiclemaxweight"></div>
        </div>
    </div>

    <!-- Если тип ТС автобус, то надо ввести данные по местам  -->
    <div class="js-car-info-type js-car-info-type-id-<?= PreCalculationDictionary::VEHICLE_TYPE_BUS ?> js-car-info-type-id-<?= PreCalculationDictionary::VEHICLE_TYPE_BUS_MORE_16 ?>"
         style="<?= !in_array($model->vehicleType,
             [PreCalculationDictionary::VEHICLE_TYPE_BUS, PreCalculationDictionary::VEHICLE_TYPE_BUS_MORE_16]) ? "display:none" : "" ?>">
        <div class="row">
            <?= Html::activeLabel($model, 'vehicleSeats') ?><br>
            <?= Html::activeTextInput($model, 'vehicleSeats', ['class' => 'form-control', 'placeholder' => $labels['vehicleSeats']]); ?>
            <div class="js-error-msg js-error-osagoinguruform-vehicleseats"></div>
        </div>
    </div>

    <!-- Если легковое авто или мотоцикл старше 4 лет выпуск - ввести данные диагностической карты -->
    <div class="js-car-info-type js-car-info-type-id-<?= PreCalculationDictionary::VEHICLE_TYPE_CAR ?> js-car-info-type-id-<?= PreCalculationDictionary::VEHICLE_TYPE_MOTO ?>"
         style="<?=
         !in_array($model->vehicleType, [PreCalculationDictionary::VEHICLE_TYPE_CAR, PreCalculationDictionary::VEHICLE_TYPE_MOTO])
         &&
         (int)date('Y', time()) - (int)$model->vehicleYear <= 4  ? "display:none" : "" ?>">
        <div class="row">
            <?= Html::activeLabel($model, 'vehicleDc') ?><br>
            <?= Html::activeTextInput($model, 'vehicleDc', ['class' => 'form-control', 'placeholder' => $labels['vehicleDc']]); ?>
            <div class="js-error-msg js-error-osagoinguruform-vehicledc"></div>
        </div>
        <div class="row">
            <?= Html::activeLabel($model, 'vehicleDcDate') ?><br>
            <?= Html::activeTextInput($model, 'vehicleDcDate', ['value' => Yii::$app->formatter->asDate($model->vehicleDcDate, 'php:d.m.Y'), 'class' => 'form-control', 'placeholder' => $labels['vehicleDcDate']]); ?>
            <div class="js-error-msg js-error-osagoinguruform-vehicledcdate"></div>
        </div>
        <div class="row">
            <?= Html::activeLabel($model, 'vehicleIssueDate') ?><br>
            <?= Html::activeTextInput($model, 'vehicleIssueDate', ['value' => Yii::$app->formatter->asDate($model->vehicleIssueDate, 'php:d.m.Y'), 'class' => 'form-control', 'placeholder' => $labels['vehicleIssueDate']]); ?>
            <div class="js-error-msg js-error-osagoinguruform-vehicleissuedate"></div>
        </div>
    </div>
</div>
