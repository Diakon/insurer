<?php
/**
 * @var $model \common\modules\inguru\forms\OsagoInguruForm
 */

use common\modules\inguru\dictionaries\OsagoInguruFormDictionary;
use yii\helpers\Html;

?>
<p>
    Вы можете заполнить данные по гос. номеру или указать вручную.
</p>
<div class="col-lg-4">
    <div class="row">
        <?= Html::activeLabel($model, 'vehicleLicensePlate') ?><br>
        <div class="col-lg-6">
            <div class="row">
                <?= Html::activeTextInput($model, 'vehicleLicensePlate', ['class' => 'form-control', 'placeholder' => 'Гос. номер']); ?>
            </div>
        </div>
        <div class="col-lg-2">
            <a href="#" class="btn btn-primary js-set-car-info-by-number">Заполнить</a>
        </div>
        <div style="clear: both;" class="js-error-msg js-error-osagoinguruform-vehiclelicenseplate"></div>
    </div>
    <br>
    <div id="js-car-info-block">
        <div class="row">
            <?= Html::activeLabel($model, 'vehicleType') ?><br>
            <?= Html::activeDropDownList($model, 'vehicleType', $model->getTypesCar(),  ['class' => 'form-control js-car-info-type-car-selector', 'prompt' => 'Выберите тип ТС']); ?>
            <div class="js-error-msg js-error-osagoinguruform-vehicletype"></div>
        </div>
        <div class="row">
            <?= Html::activeLabel($model, 'vehiclePower') ?><br>
            <?= Html::activeTextInput($model, 'vehiclePower', ['class' => 'form-control', 'placeholder' => 'Мощность']); ?>
            <div class="js-error-msg js-error-osagoinguruform-vehiclepower"></div>
        </div>
        <div class="row">
            <?= Html::activeLabel($model, 'vehicleYear') ?><br>
            <?= Html::activeTextInput($model, 'vehicleYear', ['class' => 'form-control js-mask-input-year', 'placeholder' => 'Год выпуска']); ?>
            <div class="js-error-msg js-error-osagoinguruform-vehicleyear"></div>
        </div>
        <?= Yii::$app->controller->renderPartial('_extended_form', ['model' => $model]) ?>
        <div class="row">
            <?= Html::activeLabel($model, 'useTrailer') ?><br>
            <?= Html::activeDropDownList($model, 'useTrailer', $model->getTrailer(),  ['class' => 'form-control']); ?>
            <div class="js-error-msg js-error-osagoinguruform-usetrailer"></div>
        </div>
    </div>
</div>
