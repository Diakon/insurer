<?php
/**
 * @var $model \common\modules\inguru\forms\OsagoInguruForm
 */
use yii\helpers\Html;

$label = $model->attributeLabels();
?>
<p>
    Указать предыдущий полис на указанный автомобиль?
</p>
<i>Необходим для “зеленого коридора” от Альфастрахования и помогает в оформлении ВСК</i>
<br>

<div class="col-lg-12">
    <div class="col-lg-4 js-policy-block" style="<?= !empty($model->prevPolicySerial) ? "" : "display: none" ?>">
        <div class="row">
            <div class="row">
                <?= Html::activeLabel($model, 'prevPolicySerial') ?><br>
                <?= Html::activeTextInput($model, 'prevPolicySerial', ['class' => 'form-control', 'placeholder' => $label['prevPolicySerial']]); ?>
                <div class="js-error-msg js-error-osagoinguruform-prevpolicyserial"></div>
            </div>
            <br>
        </div>

        <div class="row">
            <div class="row">
                <?= Html::activeLabel($model, 'prevPolicyNumber') ?><br>
                <?= Html::activeTextInput($model, 'prevPolicyNumber', ['class' => 'form-control', 'placeholder' => $label['prevPolicyNumber']]); ?>
                <div class="js-error-msg js-error-osagoinguruform-prevpolicynumber"></div>
            </div>
            <br>
        </div>
    </div>
</div>

<div class="col-lg-12">
    <div class="col-lg-4">
        <div class="row">
            <a href="#" class="btn bg-info" onclick="$('.js-policy-block').show(); return false;">Да</a>
            <a href="#" class="btn bg-info" onclick="
            $('.js-policy-block').hide();
            $('#osagoinguruform-prevlicensenumber').val('');
            $('#osagoinguruform-prevlicensedate').val('');
            $('.js-next-step-btn').click();
            return false;">Нет</a>
        </div>
    </div>
</div>

