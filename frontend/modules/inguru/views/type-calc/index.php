<?php
/**
 * @var $model \common\modules\inguru\forms\OsagoInguruForm
 */
use yii\helpers\Html;

?>
<p>
    Пожалуйста, укажите тип расчета.<br>
    При предварительном расчете требуется минимум данных.<br>
    Для полного расчета потребуются больше данных об авто, а так же ваши персональные данные.
    При полном расчете можно перейти к оформлению полиса ОСАГО.
</p>
<div class="col-lg-4">
    <div class="row">
        <?= Html::activeLabel($model, 'preCalculation') ?><br>
        <?= Html::activeDropDownList($model, 'preCalculation', $model->getTypeCalc(), ['class' => 'form-control', 'prompt' => 'Выберите тип расчета']); ?>
        <div class="js-error-msg js-error-osagoinguruform-precalculation"></div>
    </div>
</div>
