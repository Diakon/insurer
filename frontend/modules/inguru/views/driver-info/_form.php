<?php
/**
 * @var $model \common\modules\inguru\forms\OsagoInguruForm
 * @var $row integer
 */

use yii\helpers\Html;
use common\modules\inguru\dictionaries\OsagoInguruFormDictionary;

$labels = $model->attributeLabels();
?>
<div class="col-lg-4" style="clear: both;">
    <div class="js-driver-block js-driver-block-id-<?= $row ?>">
        <?php if ($row > 0) { ?>
            <hr>
        <?php } ?>
        <div class="row">
            <div class="row">
                <?= Html::activeLabel($model, 'driverLastName') ?><br>
                <?= Html::activeTextInput($model, 'driverLastName[' . $row . ']',
                    [
                        'id' => 'osagoinguruform-driverlastname_' . $row,
                        'class' => 'form-control js-fio-input',
                        'placeholder' => $labels['driverLastName']
                    ]); ?>
                <div class="js-error-msg js-error-osagoinguruform-driverlastname_<?= $row ?>"></div>
            </div>
        </div>
        <div class="row">
            <div class="row">
                <?= Html::activeLabel($model, 'driverFirstName') ?><br>
                <?= Html::activeTextInput($model, 'driverFirstName[' . $row . ']',
                    [
                        'id' => 'osagoinguruform-driverfirstname_' . $row,
                        'class' => 'form-control js-fio-input',
                        'placeholder' => $labels['driverFirstName']
                    ]); ?>
                <div class="js-error-msg js-error-osagoinguruform-driverfirstname_<?= $row ?>"></div>
            </div>
        </div>
        <div class="row">
            <div class="row">
                <?= Html::activeLabel($model, 'driverMiddleName') ?><br>
                <?= Html::activeTextInput($model, 'driverMiddleName[' . $row . ']',
                    [
                        'id' => 'osagoinguruform-drivermiddlename_' . $row,
                        'class' => 'form-control js-fio-input',
                        'placeholder' => $labels['driverMiddleName']
                    ]); ?>
                <div class="js-error-msg js-error-osagoinguruform-drivermiddlename_<?= $row ?>"></div>
            </div>
        </div>
        <div class="row">
            <div class="row">
                <?= Html::activeLabel($model, 'driverBirthDate') ?><br>
                <?php
                echo \yii\jui\DatePicker::widget([
                    'model' => $model,
                    'attribute' => 'driverBirthDate[' . $row . ']',
                    'language' => 'ru',
                    'dateFormat' => 'dd.MM.yyyy',
                    'options' => [
                        'id' => 'osagoinguruform-driverbirthdate_' . $row,
                        'placeholder' => $labels['driverBirthDate'],
                        'class' => 'form-control',
                    ],
                ]);
                ?>
                <div class="js-error-msg js-error-osagoinguruform-driverbirthdate_<?= $row ?>"></div>
            </div>
        </div>
        <div class="row">
            <div class="row">
                <?= Html::activeLabel($model, 'driverLicenseForeign') ?><br>
                <?= Html::activeDropDownList($model, 'driverLicenseForeign[' . $row . ']', $model->getDriverLicenseTypes(),
                    ['class' => 'form-control']); ?>
                <div class="js-error-msg js-error-osagoinguruform-driverlicenseforeign_<?= $row ?>"></div>
            </div>
        </div>
        <div class="row">
            <div class="row">
                <?= Html::activeLabel($model, 'driverLicenseSerial') ?><br>
                <?= Html::activeTextInput($model, 'driverLicenseSerial[' . $row . ']',
                    [
                        'id' => 'osagoinguruform-driverlicenseserial_' . $row,
                        'class' => 'form-control js-driver-license-serial-mask',
                        'placeholder' => $labels['driverLicenseSerial']
                    ]); ?>
                <div class="js-error-msg js-error-osagoinguruform-driverlicenseserial_<?= $row ?>"></div>
            </div>
        </div>
        <div class="row">
            <div class="row">
                <?= Html::activeLabel($model, 'driverLicenseNumber') ?><br>
                <?= Html::activeTextInput($model, 'driverLicenseNumber[' . $row . ']',
                    [
                        'id' => 'osagoinguruform-driverlicensenumber_' . $row,
                        'class' => 'form-control js-driver-license-number-mask',
                        'placeholder' => $labels['driverLicenseNumber']
                    ]); ?>
                <div class="js-error-msg js-error-osagoinguruform-driverlicensenumber_<?= $row ?>"></div>
            </div>
        </div>
        <div class="row">
            <div class="row">
                <?= Html::activeLabel($model, 'driverExpDate') ?><br>
                <?php
                echo \yii\jui\DatePicker::widget([
                    'model' => $model,
                    'attribute' => 'driverExpDate[' . $row . ']',
                    'language' => 'ru',
                    'dateFormat' => 'dd.MM.yyyy',
                    'options' => [
                        'id' => 'osagoinguruform-driverexpdate_' . $row,
                        'placeholder' => $labels['driverExpDate'],
                        'class' => 'form-control',
                    ],
                ]);
                ?>
                <div class="js-error-msg js-error-osagoinguruform-driverexpdate_<?= $row ?>"></div>
            </div>
        </div>


        <div class="row">
            <div class="row" style="padding-bottom: 10px;">
                <?= Html::activeHiddenInput($model, 'driverPrevAddInfo[' . $row . ']',
                    [
                        'id' => 'osagoinguruform-driverprevaddinfo_' . $row,
                        'class' => 'js-driver-prev-add-info-input'
                    ]) ?>
                <a href="#" class="btn bg-info" onclick="
                        let parentBlock = $(this).closest('.js-driver-block');
                        let input = parentBlock.find('.js-driver-prev-add-info-input');
                        let block = parentBlock.find('.js-driver-info-prev-info-form');
                        let addInfoVal = block.val();
                        input.val(parseInt(input.val()) == <?= OsagoInguruFormDictionary::PRE_CALCULATION_YES ?> ? <?= OsagoInguruFormDictionary::PRE_CALCULATION_NO ?> : <?= OsagoInguruFormDictionary::PRE_CALCULATION_YES ?>);
                        block.slideToggle();
                        return false;
                        ">
                    Указать предыдущее ВУ для восстановления КБМ
                </a>
            </div>
        </div>
        <div
                class="js-driver-info-prev-info-form"
                style="<?=
                !empty($model->driverPrevLastName[$row]) ||
                !empty($model->driverPrevFirstName[$row]) ||
                !empty($model->driverPrevMiddleName[$row]) ||
                !empty($model->driverPrevLicenseSerial[$row]) ||
                !empty($model->driverPrevLicenseNumber[$row]) ||
                !empty($model->driverPrevLicenseDate[$row])
                    ? ""
                    : "display: none"
                ?>"
        >
            <?= Yii::$app->controller->renderPartial('_prev_driver_info', ['model' => $model, 'row' => $row]) ?>
        </div>

        <?php if ($row > 0) { ?>
        <div class="row">
            <div class="row" style="padding-bottom: 10px;">
                <a href="#" class="btn btn-danger js-add-remove-driver-btn" data-type="remove" data-row="<?= $row ?>">Удалить
                    водителя</a>
            </div>
        </div>
        <?php } ?>
    </div>
</div>