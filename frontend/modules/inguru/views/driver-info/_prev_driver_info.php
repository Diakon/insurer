<?php
/**
 * @var $model \common\modules\inguru\forms\OsagoInguruForm
 * @var $row integer
 */

use yii\helpers\Html;

$labels = $model->attributeLabels();
?>
<div class="row">
    <div class="row">
        <?= Html::activeLabel($model, 'driverPrevLastName') ?><br>
        <?= Html::activeTextInput($model, 'driverPrevLastName[' . $row . ']',
            [
                'id' => 'osagoinguruform-driverprevlastname_' . $row,
                'class' => 'form-control js-fio-input',
                'placeholder' => $labels['driverPrevLastName']
            ]); ?>
        <div class="js-error-msg js-error-osagoinguruform-driverprevlastname_<?= $row ?>"></div>
    </div>
</div>
<div class="row">
    <div class="row">
        <?= Html::activeLabel($model, 'driverPrevFirstName') ?><br>
        <?= Html::activeTextInput($model, 'driverPrevFirstName[' . $row . ']',
            [
                'id' => 'osagoinguruform-driverprevfirstname_' . $row,
                'class' => 'form-control js-fio-input',
                'placeholder' => $labels['driverPrevFirstName']
            ]); ?>
        <div class="js-error-msg js-error-osagoinguruform-driverprevfirstname_<?= $row ?>"></div>
    </div>
</div>
<div class="row">
    <div class="row">
        <?= Html::activeLabel($model, 'driverPrevMiddleName') ?><br>
        <?= Html::activeTextInput($model, 'driverPrevMiddleName[' . $row . ']',
            [
                'id' => 'osagoinguruform-driverprevmiddlename_' . $row,
                'class' => 'form-control js-fio-input',
                'placeholder' => $labels['driverPrevMiddleName']
            ]); ?>
        <div class="js-error-msg js-error-osagoinguruform-driverprevmiddlename_<?= $row ?>"></div>
    </div>
</div>
<div class="row">
    <div class="row">
        <?= Html::activeLabel($model, 'driverPrevLicenseSerial') ?><br>
        <?= Html::activeTextInput($model, 'driverPrevLicenseSerial[' . $row . ']',
            [
                'id' => 'osagoinguruform-driverprevlicenseserial_' . $row,
                'class' => 'form-control js-driver-license-serial-mask',
                'placeholder' => $labels['driverPrevLicenseSerial']
            ]); ?>
        <div class="js-error-msg js-error-osagoinguruform-driverprevlicenseserial_<?= $row ?>"></div>
    </div>
</div>
<div class="row">
    <div class="row">
        <?= Html::activeLabel($model, 'driverPrevLicenseNumber') ?><br>
        <?= Html::activeTextInput($model, 'driverPrevLicenseNumber[' . $row . ']',
            [
                'id' => 'osagoinguruform-driverlicensenumber_' . $row,
                'class' => 'form-control js-driver-license-number-mask',
                'placeholder' => $labels['driverPrevLicenseNumber']
            ]); ?>
        <div class="js-error-msg js-error-osagoinguruform-driverprevlicensenumber_<?= $row ?>"></div>
    </div>
</div>
<div class="row">
    <div class="row">
        <?= Html::activeLabel($model, 'driverPrevLicenseDate') ?><br>
        <?php
        echo \yii\jui\DatePicker::widget([
            'model' => $model,
            'attribute' => 'driverPrevLicenseDate[' . $row . ']',
            'language' => 'ru',
            'dateFormat' => 'dd.MM.yyyy',
            'options' => [
                'id' => 'osagoinguruform-driverprevlicensedate_' . $row,
                'placeholder' => $labels['driverPrevLicenseDate'],
                'class' => 'form-control',
            ],
        ]);
        ?>
        <div class="js-error-msg js-error-osagoinguruform-driverprevlicensedate_<?= $row ?>"></div>
    </div>
</div>