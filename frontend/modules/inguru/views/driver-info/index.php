<?php
use common\modules\inguru\dictionaries\OsagoInguruFormDictionary;

/**
 * @var $model \common\modules\inguru\forms\OsagoInguruForm
 * @var $row integer
 */
$labels = $model->attributeLabels();
?>
<?php if ($row == 0) { ?>
    <?php echo \yii\helpers\Html::activeHiddenInput($model, 'multiDrive', ['class' => 'js-is-multi-drive-input']) ?>
    <a href="#" data-type="<?= OsagoInguruFormDictionary::IS_MULTI_DRIVE_NO ?>" class="js-is-multi-drive-href btn btn-info">Огрниченное число водителей</a>
    <a href="#" data-type="<?= OsagoInguruFormDictionary::IS_MULTI_DRIVE_YES ?>" class="js-is-multi-drive-href btn btn-info">Мультидрайв</a>
<?php } ?>

<div class="col-lg-12 js-drivers-main-block" style="<?= $model->multiDrive == OsagoInguruFormDictionary::IS_MULTI_DRIVE_NO ? '' : 'display:none' ?>">
    <?php if (!empty($model->driversAge)) { ?>
        <?php foreach ($model->driversAge as $row => $data) { ?>
            <?= Yii::$app->controller->renderPartial('_form', ['model' => $model, 'row' => $row]) ?>
        <?php } ?>
    <?php } else { ?>
        <?= Yii::$app->controller->renderPartial('_form', ['model' => $model, 'row' => $row]) ?>
    <?php } ?>
</div>
