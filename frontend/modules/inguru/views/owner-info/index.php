<?php
/**
 * @var $model \common\modules\inguru\forms\OsagoInguruForm
 */
use yii\helpers\Html;
$label = $model->attributeLabels();
?>
<div class="col-lg-4">
    <div class="row">
        <h4>Информация о собственнике автомобиля</h4>
        <hr>
    </div>
    <div class="row">
        <?= Html::activeLabel($model, 'ownerCityName') ?><br>
        <?= \yii\jui\AutoComplete::widget([
            'model' => $model,
            'attribute' => 'ownerCityName',
            'clientOptions' => [
                'source' => \yii\helpers\Url::to(['/inguru/owner-info/', 'action' => 'searchCity']),
                'minLength'=>'3',
                'select' => new \yii\web\JsExpression("function( event, ui ) {
                console.log(ui);
                $('#osagoinguruform-ownercity').val(ui.item.id);
                }")
            ],
            'options'=>[
                'id' => 'js-city-owner',
                'class' => 'form-control',
                'placeholder' => $label['ownerCity']
            ]
        ]);
        ?>
        <?= Html::activeHiddenInput($model, 'ownerCity') ?>
        <div class="js-error-msg js-error-osagoinguruform-ownercity"></div>
    </div>
    <br>
    <div class="row">
        <?= Html::activeLabel($model, 'usePeriod') ?><br>
        <?= Html::activeDropDownList($model, 'usePeriod', $model->getUsePeriodItems(), ['class' => 'form-control']) ?>
        <div class="js-error-msg js-error-osagoinguruform-useperiod"></div>
    </div>
    <?= Yii::$app->controller->renderPartial('_extended_form', ['model' => $model]) ?>
</div>
