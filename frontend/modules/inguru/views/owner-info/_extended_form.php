<?php
/**
 * Поля для расширенного расчета
 * @var $model \common\modules\inguru\forms\OsagoInguruForm
 */
use common\modules\inguru\dictionaries\OsagoInguruFormDictionary;
use yii\helpers\Html;
use common\modules\inguru\dictionaries\FinalCalculationDictionary;

$labels = $model->attributeLabels();
?>

<div id="js-car-extended-info-block" style="<?= $model->preCalculation == OsagoInguruFormDictionary::PRE_CALCULATION_NO ? '' : 'display:none' ?>">
    <div class="row">
        <?= Html::activeLabel($model, 'ownerLastName') ?><br>
        <?= Html::activeTextInput($model, 'ownerLastName', ['class' => 'form-control', 'placeholder' => $labels['ownerLastName']]); ?>
        <div class="js-error-msg js-error-osagoinguruform-ownerlastname"></div>
    </div>
    <div class="row">
        <?= Html::activeLabel($model, 'ownerFirstName') ?><br>
        <?= Html::activeTextInput($model, 'ownerFirstName', ['class' => 'form-control', 'placeholder' => $labels['ownerFirstName']]); ?>
        <div class="js-error-msg js-error-osagoinguruform-ownerfirstname"></div>
    </div>
    <div class="row">
        <?= Html::activeLabel($model, 'ownerMiddleName') ?><br>
        <?= Html::activeTextInput($model, 'ownerMiddleName', ['class' => 'form-control', 'placeholder' => $labels['ownerMiddleName']]); ?>
        <div class="js-error-msg js-error-osagoinguruform-ownermiddlename"></div>
    </div>
    <div class="row">
        <?= Html::activeLabel($model, 'ownerPhone') ?><br>
        <?= Html::activeTextInput($model, 'ownerPhone', ['class' => 'form-control', 'placeholder' => $labels['ownerPhone']]); ?>
        <div class="js-error-msg js-error-osagoinguruform-ownerphone"></div>
    </div>

    <div class="row">
        <?= Html::activeLabel($model, 'email') ?><br>
        <?= Html::activeTextInput($model, 'email', ['class' => 'form-control', 'placeholder' => $labels['email']]); ?>
        <div class="js-error-msg js-error-osagoinguruform-email"></div>
    </div>
    <div class="row">
        <?= Html::activeLabel($model, 'ownerBirthDate') ?><br>
        <?php
        echo \yii\jui\DatePicker::widget([
            'model' => $model,
            'attribute' => 'ownerBirthDate',
            'language' => 'ru',
            'dateFormat' => 'dd.MM.yyyy',
            'options' => [
                'id' => 'osagoinguruform-ownerbirthdate',
                'placeholder' => $labels['ownerBirthDate'],
                'class' => 'form-control',
            ],
        ]);
        ?>
        <div class="js-error-msg js-error-osagoinguruform-ownerbirthdate"></div>
    </div>
    <div class="row">
        <?= Html::activeLabel($model, 'ownerPassportType') ?><br>
        <?= Html::activeDropDownList($model, 'ownerPassportType', $model->getPassportTypes(), ['class' => 'form-control']); ?>
        <div class="js-error-msg js-error-osagoinguruform-ownerpassporttype"></div>
    </div>
    <div class="row">
        <?= Html::activeLabel($model, 'ownerPassportSerial') ?><br>
        <?= Html::activeTextInput($model, 'ownerPassportSerial', ['class' => 'form-control', 'placeholder' => $labels['ownerPassportSerial']]); ?>
        <div class="js-error-msg js-error-osagoinguruform-ownerpassportserial"></div>
    </div>
    <div class="row">
        <?= Html::activeLabel($model, 'ownerPassportNumber') ?><br>
        <?= Html::activeTextInput($model, 'ownerPassportNumber', ['class' => 'form-control', 'placeholder' => $labels['ownerPassportNumber']]); ?>
        <div class="js-error-msg js-error-osagoinguruform-ownerpassportnumber"></div>
    </div>
    <div class="row">
        <?= Html::activeLabel($model, 'ownerPassportDate') ?><br>
        <?php
        echo \yii\jui\DatePicker::widget([
            'model' => $model,
            'attribute' => 'ownerPassportDate',
            'language' => 'ru',
            'dateFormat' => 'dd.MM.yyyy',
            'options' => [
                'id' => 'osagoinguruform-ownerpassportdate',
                'placeholder' => $labels['ownerPassportDate'],
                'class' => 'form-control',
            ],
        ]);
        ?>
        <div class="js-error-msg js-error-osagoinguruform-ownerpassportdate"></div>
    </div>

    <div class="row">
        <?= Html::activeLabel($model, 'ownerDadataAddress') ?><br>
        <?= \yii\jui\AutoComplete::widget([
            'model' => $model,
            'attribute' => 'ownerDadataAddress',
            'clientOptions' => [
                'source' => \yii\helpers\Url::to(['/inguru/owner-info/', 'action' => 'searchAddress']),
                'minLength'=>'3',
                'select' => new \yii\web\JsExpression("function( event, ui ) {
                $(this).val(ui.item.value);
                }")
            ],
            'options'=>[
                'class' => 'form-control',
                'placeholder' => $labels['ownerDadataAddress']
            ]
        ]);
        ?>
        <div class="js-error-msg js-error-osagoinguruform-ownerdadataaddress"></div>
    </div>

    <div class="row">
        <?= Html::activeLabel($model, 'ownerDateOSAGOStart') ?><br>
        <?php
        echo \yii\jui\DatePicker::widget([
            'model' => $model,
            'attribute' => 'ownerDateOSAGOStart',
            'language' => 'ru',
            'dateFormat' => 'dd.MM.yyyy',
            'options' => [
                'id' => 'osagoinguruform-ownerdateosagostart',
                'placeholder' => $labels['ownerDateOSAGOStart'],
                'class' => 'form-control',
            ],
        ]);
        ?>
        <div class="js-error-msg js-error-osagoinguruform-ownerdateosagostart"></div>
    </div>

    <div class="row">
        <?= Html::activeLabel($model, 'purpose') ?><br>
        <?= Html::activeDropDownList($model, 'purpose', $model->getCarPurposes(),  ['class' => 'form-control', 'prompt' => $labels['purpose']]); ?>
        <div class="js-error-msg js-error-osagoinguruform-purpose"></div>
    </div>

    <div class="row">
        <?= Html::activeLabel($model, 'insurerIsOwner') ?><br>
        <?= Html::activeDropDownList($model, 'insurerIsOwner', $model->getOneFaces(),  ['class' => 'form-control', 'prompt' => $labels['insurerIsOwner']]); ?>
        <div class="js-error-msg js-error-osagoinguruform-insurerisowner"></div>
    </div>

    <div class="js-insurer-is-owner-yes" style="<?= $model->insurerIsOwner == FinalCalculationDictionary::ONE_FACE_NO ? "" : "display:none" ?>">
        <div class="row">
            <h4>Информация о страхователе</h4>
            <hr>
        </div>
        <div class="row">
            <?= Html::activeLabel($model, 'insurerLastName') ?><br>
            <?= Html::activeTextInput($model, 'insurerLastName', ['class' => 'form-control', 'placeholder' => $labels['insurerLastName']]); ?>
            <div class="js-error-msg js-error-osagoinguruform-insurerlastname"></div>
        </div>
        <div class="row">
            <?= Html::activeLabel($model, 'insurerFirstName') ?><br>
            <?= Html::activeTextInput($model, 'insurerFirstName', ['class' => 'form-control', 'placeholder' => $labels['insurerFirstName']]); ?>
            <div class="js-error-msg js-error-osagoinguruform-insurerfirstname"></div>
        </div>
        <div class="row">
            <?= Html::activeLabel($model, 'insurerMiddleName') ?><br>
            <?= Html::activeTextInput($model, 'insurerMiddleName', ['class' => 'form-control', 'placeholder' => $labels['insurerMiddleName']]); ?>
            <div class="js-error-msg js-error-osagoinguruform-insurermiddlename"></div>
        </div>
        <div class="row">
            <?= Html::activeLabel($model, 'insurerBirthDate') ?><br>
            <?php
            echo \yii\jui\DatePicker::widget([
                'model' => $model,
                'attribute' => 'insurerBirthDate',
                'language' => 'ru',
                'dateFormat' => 'dd.MM.yyyy',
                'options' => [
                    'id' => 'osagoinguruform-insurerbirthdate',
                    'placeholder' => $labels['insurerBirthDate'],
                    'class' => 'form-control',
                ],
            ]);
            ?>
            <div class="js-error-msg js-error-osagoinguruform-insurerbirthdate"></div>
        </div>
        <div class="row">
            <?= Html::activeLabel($model, 'insurerPassportType') ?><br>
            <?= Html::activeDropDownList($model, 'insurerPassportType', $model->getPassportTypes(), ['class' => 'form-control']); ?>
            <div class="js-error-msg js-error-osagoinguruform-insurerpassporttype"></div>
        </div>
        <div class="row">
            <?= Html::activeLabel($model, 'insurerPassportSerial') ?><br>
            <?= Html::activeTextInput($model, 'insurerPassportSerial', ['class' => 'form-control', 'placeholder' => $labels['insurerPassportSerial']]); ?>
            <div class="js-error-msg js-error-osagoinguruform-insurerpassportserial"></div>
        </div>
        <div class="row">
            <?= Html::activeLabel($model, 'insurerPassportNumber') ?><br>
            <?= Html::activeTextInput($model, 'insurerPassportNumber', ['class' => 'form-control', 'placeholder' => $labels['insurerPassportNumber']]); ?>
            <div class="js-error-msg js-error-osagoinguruform-insurerpassportnumber"></div>
        </div>
        <div class="row">
            <?= Html::activeLabel($model, 'insurerPassportDate') ?><br>
            <?php
            echo \yii\jui\DatePicker::widget([
                'model' => $model,
                'attribute' => 'insurerPassportDate',
                'language' => 'ru',
                'dateFormat' => 'dd.MM.yyyy',
                'options' => [
                    'id' => 'osagoinguruform-insurerpassportdate',
                    'placeholder' => $labels['insurerPassportDate'],
                    'class' => 'form-control',
                ],
            ]);
            ?>
            <div class="js-error-msg js-error-osagoinguruform-insurerpassportdate"></div>
        </div>
        <div class="row">
            <?= Html::activeLabel($model, 'insurerDadataAddress') ?><br>
            <?= \yii\jui\AutoComplete::widget([
                'model' => $model,
                'attribute' => 'insurerDadataAddress',
                'clientOptions' => [
                    'source' => \yii\helpers\Url::to(['/inguru/owner-info/', 'action' => 'searchAddress']),
                    'minLength'=>'3',
                    'select' => new \yii\web\JsExpression("function( event, ui ) {
                $(this).val(ui.item.value);
                }")
                ],
                'options'=>[
                    'class' => 'form-control',
                    'placeholder' => $labels['insurerDadataAddress']
                ]
            ]);
            ?>
            <div class="js-error-msg js-error-osagoinguruform-insurerdadataaddress"></div>
        </div>


    </div>
</div>
