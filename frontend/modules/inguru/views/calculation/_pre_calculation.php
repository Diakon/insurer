<?php
/**
 * @var $model \common\modules\inguru\forms\OsagoInguruForm
 * @var $order \common\modules\orders\models\Order
 * @var $response array
 * @var $error bool
 */

if ($error) { ?>
    <p>
        Во время расчета возникла ошибка. Пожалуйста, убедитесь в правильности введенных данных и повторите снова!
    </p>
<?php } else { ?>
    <div class="col-lg-4">
        <div class="row">
            <p><b>Предварительный расчет стоимости ОСАГО по введенным данным:</b></p>
            <p>Минимальная стоимость полиса:<?= Yii::$app->formatter->asPrice($response['results']['min']) ?></p>
            <p>Максимальная стоимость полиса:<?= Yii::$app->formatter->asPrice($response['results']['max']) ?></p>
        </div>
        <div class="row">
            <a href="#" style="display: none" class="btn btn-primary js-switch-to-final-calculation-btn">Перейти к полному расчету</a>
        </div>
    </div>
<?php } ?>