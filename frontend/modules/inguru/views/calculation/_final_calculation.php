<?php
/**
 * @var $model \common\modules\inguru\forms\OsagoInguruForm
 * @var $order \common\modules\orders\models\Order
 * @var $customBrandModelCar array
 * @var $response array
 * @var $error bool
 * @var $row integer
 */

use yii\helpers\Html;
use common\modules\inguru\dictionaries\FinalCalculationDictionary;
use common\modules\bonuses\services\BonusService;

?>

<div class="col-lg-12">
    <div class="row" align="center js-title-input-info-block"></div>
    <div class="row">
        <?php if (!empty($response['errors'])) { ?>
            <div class="alert alert-danger" role="alert">
                <div style="position: absolute; margin-left: -30000px; display:none">
                    <?= print_r($response) ?>
                </div>
                <?php foreach ($response['errors'] as $error) { ?>
                    <?php if (empty($error['sk'])) { ?>
                        Ошибка ввода данных, пожалйста, проверьте правильность введенных данных и повторите снова.
                    <?php } else { ?>
                        Предложение от компании "<?= FinalCalculationDictionary::INSURANCE_COMPANIES[$error['sk']] ?? "" ?>" не получено.
                        <?php if (!empty($error['error']) && is_string($error['error']) && strripos($error['error'], "Предложение от компании") === false) { ?>
                            <br>
                            Причина: <?= $error['error'] ?>
                        <?php } ?>
                        <?php // Если код ошибки 98 (код ошибки ИНГУРУ) - в страховой компании не знают что за марка модель ТС, вывожу возможность указать вручную ?>
                        <?php if (strripos($error['error'], "Указанная марка модель ТС не найдена в словаре страховой компании") !== false) { ?>
                            <div class="js-vehicle-brand-model-manual-input" style="display: none">
                                Пожалуйста, попробуйте указать бренд и модель ТС вручную:<br>
                                <?php
                                $serviceCarInfo = new \frontend\modules\inguru\services\CarInfoService();
                                echo Html::activeDropDownList(
                                    $model,
                                    'vehicleBrand',
                                    $serviceCarInfo->brandCarListInCompany((int)$error['sk']),
                                    [
                                        'style' => 'width:300px',
                                        'class' => 'form-control js-vehicle-brand-name-company',
                                        'id' => 'js-vehicle-brand-name-company-id-' . $error['sk'],
                                        'data-company' => $error['sk'],
                                        'prompt' => 'Выберите марку ТС из списка',
                                    ]);
                                ?>
                                <br>
                                <?php
                                echo Html::activeDropDownList(
                                    $model,
                                    'vehicleModel',
                                    !empty($customBrandModelCar) ? [$customBrandModelCar['vehicleModel']] : [],
                                    [
                                        'style' => 'width:300px',
                                        'class' => 'form-control',
                                        'id' => 'js-vehicle-model-name-company-id-' . $error['sk'],
                                        'data-company' => $error['sk'],
                                        'prompt' => 'Выберите модель ТС из списка',
                                    ]);
                                ?>
                                <?= \yii\helpers\Html::a('<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>', '#',
                                    [
                                        'style' => 'margin-top:5px;',
                                        'class' => 'btn btn-primary js-set-car-brand-model-for-company',
                                        'data-company' => $error['sk'],
                                    ]); ?>
                            </div>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
            </div>
        <?php } else if (!empty($response['results'])) { ?>
            <div class="alert alert-info" role="alert">
                <?php foreach ($response['results'] as $result) { ?>
                    <b>Предложение от компании
                        "<?= FinalCalculationDictionary::INSURANCE_COMPANIES[$result['sk']] ?? "" ?>":</b><br>
                    <div data-id="<?= $result['eId'] ?>" class="js-osago-prolongation-id"></div>
                    <div data-company="<?= $result['sk'] ?>" class="js-osago-prolongation-company"></div>
                    <ul>
                        <li>
                            Стоимость полиса ОСАГО: <?= Yii::$app->formatter->asPrice($result['total']) ?>
                        </li>
                        <li>
                            Сумма бонусов которые вы
                            получите: <?= Yii::$app->formatter->asPrice(BonusService::calculatePrice($result['commission'])) ?>
                        </li>
                    </ul>
                    <ul>
                        <?php if (!empty($result['coeffs'])) { ?>
                            <br>
                            <b>Дополнительные данные (коэфициенты вляющие на расчет):</b>
                            <?php foreach ($result['coeffs'] as $addInfoKey => $addInfoValue) { ?>
                                <?php switch ($addInfoKey) {
                                    case "Tb":
                                        echo Html::tag('li', 'Базовая стоимость полиса: ' . (Yii::$app->formatter->asPrice($addInfoValue)));
                                        break;
                                    case "Kt":
                                        echo Html::tag('li', 'Территориальный коэффициент: ' . $addInfoValue);
                                        break;
                                    case "Kbm":
                                        echo Html::tag('li', 'Коэффициент возникновения страховых случаев: ' . $addInfoValue);
                                        break;
                                    case "Kvs":
                                        echo Html::tag('li', 'Коэффициент водительского стажа: ' . $addInfoValue);
                                        break;
                                    case "Ks":
                                        echo Html::tag('li', 'Сезонный показатель: ' . $addInfoValue);
                                        break;
                                    case "Kp":
                                        echo Html::tag('li', 'Коэффициент применения в определенный период времени: ' . $addInfoValue);
                                        break;
                                    case "Km":
                                        echo Html::tag('li', 'Коэффициент мощности двигателя: ' . $addInfoValue);
                                        break;
                                    case "Kn":
                                        echo Html::tag('li', 'Ставка страховой компании (зависит от количества выявленных нарушений): ' . $addInfoValue);
                                        break;
                                    case "Ko":
                                        echo Html::tag('li', 'Коэффициент вычисляемый от количества страхуемых водителей: ' . $addInfoValue);
                                        break;
                                    case "Kpr":
                                        echo Html::tag('li', 'Коэффициент вычисляемый от факта наличие прицепного устройства: ' . $addInfoValue);
                                        break;
                                } ?>
                            <?php } ?>
                        <?php } ?>
                    </ul>
                    <div class="js-payment-btn-block" style="display: none">
                        <a
                                style="margin-top: 10px;"
                                href="#"
                                data-id="<?= $result['eId'] ?>"
                                data-company="<?= $result['sk'] ?>"
                                class="btn btn-primary js-prepare-payment-service"
                        >Перейти к оформления ОСАГО</a>
                        <a
                                style="margin-top: 10px;"
                                href="#"
                                data-id="<?= $result['eId'] ?>"
                                data-company="<?= $result['sk'] ?>"
                                class="btn btn-default js-save-to-awaiting-payment"
                        >Сохранить в раздел "Ожидают оплаты", что бы оплатить позднее</a>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
</div>


