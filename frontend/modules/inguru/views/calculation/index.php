<?php
/**
 * @var $model \common\modules\inguru\forms\OsagoInguruForm
 * @var $order \common\modules\orders\models\Order
 * @var $customBrandModelCar array
 * @var $response array
 * @var $error bool
 * @var $row integer
 */

echo Yii::$app->controller->renderPartial(
        $model::classNameShort() == "FinalCalculation" ? '_final_calculation' : '_pre_calculation',
        ['model' => $model, 'response' => $response, 'order' => $order, 'customBrandModelCar' => $customBrandModelCar, 'error' => $error, 'row' => $row]
)
?>
