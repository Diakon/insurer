<?php
/**
 * @var $orders Order[]
 */

use common\modules\orders\models\Order;

$service = new \common\modules\orders\services\OrderPropertyService();
?>
<?php if (!empty($orders)) { ?>
    <b>У Вас есть не завершенные ранее заказы.<br>Если Вы хотите продолжить оформление начатого ранее заказа, то выберите
        нужный:</b>
    <br>
    <ul>
        <?php foreach ($orders as $order) { ?>
            <?php
            $property = $order->properties;
            $brandCar = $service->getOrderPropertyValue($property, 'vehicleBrand');
            $modelCar = $service->getOrderPropertyValue($property, 'vehicleModel');
            ?>
            <li>
                <a href="<?= \yii\helpers\Url::to(['/', 'order_id' => $order->id]) ?>" class="btn btn-sm">
                    Заказ от <?= Yii::$app->formatter->asDate($order->created_at, 'php:d.m.Y H:i') ?>
                    <?php if (!empty($brandCar) && !empty($modelCar)) { ?>
                        для автомобиля <?= $brandCar . ' ' . $modelCar ?>
                    <?php } ?>
                </a>
            </li>
        <?php } ?>
    </ul>
    <br>
    <hr>
<?php } ?>