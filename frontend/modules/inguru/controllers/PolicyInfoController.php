<?php

namespace frontend\modules\inguru\controllers;

use common\modules\inguru\forms\OsagoInguruForm;
use Yii;
use frontend\controllers\SiteController;
use yii\widgets\ActiveForm;
use frontend\modules\orders\traits\OrderTrait;

/**
 * Class PolicyInfoController
 * @package frontend\modules\inguru\controllers
 */
class PolicyInfoController extends SiteController
{
    use OrderTrait;

    /**
     * @return mixed|string
     */
    public function actionIndex()
    {
        $action = Yii::$app->request->get('action');

        if (empty($action) && Yii::$app->request->isGet) {
            return $this->formHtml();
        }

        $post = Yii::$app->request->post();

        return $this->{$action}($post);
    }

    /**
     * Возвращает HTML кода формы
     *
     * @param array $params
     * @return string
     */
    private function formHtml($params = [])
    {
        $model = $this->setFormOrderModel($this->getModel(), $params);

        return $this->renderPartial('index', ['model' => $model]);
    }

    /**
     * Проверяет форму
     *
     * @param $params
     * @return array
     */
    private function validate($params)
    {
        $model = $this->getModel();
        $model->load($this->getOrderParams($params));
        if (!$model->validate()) {
            return $this->returnAjax(self::CODE_ERROR, ActiveForm::validate($model));
        }

        // Сохраняю шаг
        $order = $this->saveStep($model);
        if (!$order) {
            return $this->returnAjax(self::CODE_ERROR, "Ошибка АПИ");
        }

        return $this->returnAjax(self::CODE_SUCCESS, ['step' => 'calc', 'orderId' => $order->id]);
    }

    /**
     * @return OsagoInguruForm
     */
    private function getModel()
    {
        $model = new OsagoInguruForm();
        $model->setScenario(OsagoInguruForm::SCENARIO_STEP_POLICY);


        return $model;
    }
}