<?php

namespace frontend\modules\inguru\controllers;

use common\modules\inguru\services\KladrService;
use frontend\modules\inguru\models\Kladr;
use common\modules\inguru\forms\OsagoInguruForm;
use Yii;
use frontend\controllers\SiteController;
use yii\helpers\Json;
use yii\widgets\ActiveForm;
use common\modules\inguru\traits\AuthTrait;
use common\modules\inguru\traits\RequestApiTrait;
use frontend\modules\orders\traits\OrderTrait;

/**
 * Class OwnerController
 * @package frontend\modules\inguru\controllers
 */
class OwnerInfoController extends SiteController
{
    use AuthTrait;
    use RequestApiTrait;
    use OrderTrait;

    private $service;

    /**
     * OwnerInfoController constructor.
     * @param $id
     * @param $module
     * @param KladrService $service
     * @param array $config
     */
    public function __construct($id, $module, KladrService $service, $config = [])
    {
        $this->service = $service;
        parent::__construct($id, $module, $config);
    }

    /**
     * @return mixed|string
     */
    public function actionIndex()
    {
        $action = Yii::$app->request->get('action');

        if (empty($action) && Yii::$app->request->isGet) {
            return $this->formHtml();
        }
        $post = Yii::$app->request->post();

        return $this->{$action}($post);
    }

    /**
     * Возвращает HTML кода формы
     *
     * @param array $params
     * @return string
     */
    private function formHtml($params = [])
    {
        $model = $this->setFormOrderModel($this->getModel(), $params);

        return $this->renderPartial('index', ['model' => $model]);
    }

    /**
     * Возвращает список адресов получая их через запрос в АПИ dadata
     * @param array $params
     * @return string
     */
    private function searchAddress($params = [])
    {
        $term = $params['term'] ?? Yii::$app->request->get('term');
        return Json::encode(Yii::$app->api->getAddress($term));
    }

    /**
     * Поиск города по названию
     *
     * @param array $params
     * @return array|string
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    private function searchCity($params = [])
    {
        $result = [];

        $term = $params['term'] ?? Yii::$app->request->get('term');
        $term = trim($term);

        $model = new Kladr();
        $model->searchName = $term;

        $response = $this->request(
            $this->service->getApiUrl(),
            $this->getToken(),
            $this->service->getApiDataByModel($model),
            $this->service->getApiCurlFormat(),
            $this->service->getApiCurlMethod()
        );

        if ($response->isOk) {
            $response = $response->data;
            if (!empty($response['errors'])) {
                return $this->returnAjax(self::CODE_ERROR, "Ошибка АПИ");
            } else {
                foreach ($response['results']['found'] as $data) {
                    $cityName = trim($data['name']);
                    $label = trim($data['type']) . '. ' . $cityName;
                    if (!empty($data['regionName']) && !empty($data['regionType']) && !in_array($cityName, ['Москва', 'Санкт-Петербург'])) {
                        $label .= trim(', ' . $data['regionName'] . ' ' . $data['regionType']);
                    }
                    $result[] = [
                        'id' => trim($data['code']),
                        'value' => $label,
                        'label' => $label
                    ];
                }
            }
        }

        return Json::encode($result);
    }

    /**
     * Проверяет форму
     *
     * @param $params
     * @return array
     */
    private function validate($params)
    {
        $model = $this->getModel();
        $model->load($this->getOrderParams($params));
        if (!$model->validate()) {
            return $this->returnAjax(self::CODE_ERROR, ActiveForm::validate($model));
        }

        // Сохраняю шаг
        $order = $this->saveStep($model);
        if (!$order) {
            return $this->returnAjax(self::CODE_ERROR, "Ошибка АПИ");
        }

        return $this->returnAjax(self::CODE_SUCCESS, ['step' => 'policy', 'orderId' => $order->id]);
    }

    /**
     * @return OsagoInguruForm
     */
    private function getModel()
    {
        $model = new OsagoInguruForm();
        $model->setScenario(OsagoInguruForm::SCENARIO_STEP_OWNER);

        return $model;
    }
}