<?php

namespace frontend\modules\inguru\controllers;

use common\modules\cars\dictionaries\CarDictionary;
use common\modules\cars\services\CarService;
use common\modules\inguru\forms\OsagoInguruForm;
use common\modules\inguru\services\CarInfoService;
use common\modules\inguru\traits\AuthTrait;
use common\modules\inguru\traits\RequestApiTrait;
use Yii;
use frontend\controllers\SiteController;
use yii\helpers\Json;
use yii\widgets\ActiveForm;
use frontend\modules\orders\traits\OrderTrait;

/**
 * Class CarInfoController
 * @package frontend\modules\inguru\controllers
 */
class CarInfoController extends SiteController
{
    use AuthTrait;
    use RequestApiTrait;
    use OrderTrait;

    /**
     * @return mixed|string
     */
    public function actionIndex()
    {
        $action = Yii::$app->request->get('action');

        if (empty($action) && Yii::$app->request->isGet) {
            return $this->formHtml();
        }
        $post = Yii::$app->request->post();

        return $this->{$action}($post);
    }


    /**
     * Возвращает HTML кода формы
     * @param array $params
     * @return string
     */
    private function formHtml($params = [])
    {
        $model = $this->setFormOrderModel($this->getModel(), $params);

        return $this->renderPartial('index', ['model' => $model]);
    }

    /**
     * Получает данные об авто по гос. номеру. Если нет в БД - тянем по АПИ и сохраняем в БД у себя
     *
     * @param $params
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    private function carInfoByNumber($params)
    {
        $model = new OsagoInguruForm();
        $model->setScenario(OsagoInguruForm::SCENARIO_VALIDATE_CAR_NUMBER);
        $model->load($params);
        if (!$model->validate()) {
            return $this->returnAjax(self::CODE_ERROR, ActiveForm::validate($model));
        }

        $car = CarService::getCar($model->vehicleLicensePlate, CarDictionary::SOURCE_INGURU);

        if (empty($car)) {
            $model->addError('vehicleLicensePlate', 'Не удалось получить данные о машине по гос. номеру. Пожалуйста, введите данные вручную или попробуйте снова.');
            return $this->returnAjax(self::CODE_ERROR, ['osagoinguruform-vehiclelicenseplate' => $model->getErrorSummary(true)]);
        }

        return $this->returnAjax(self::CODE_SUCCESS, ['car' => $car->getAttributes()]);
    }

    /**
     * Проверяет форму
     *
     * @param $params
     * @return array
     */
    private function validate($params)
    {
        $model = $this->getModel();
        $model->load($this->getOrderParams($params));

        if (!$model->validate()) {
            return $this->returnAjax(self::CODE_ERROR, ActiveForm::validate($model));
        }

        // Сохраняю шаг
        $order = $this->saveStep($model);
        if (!$order) {
            return $this->returnAjax(self::CODE_ERROR, "Ошибка АПИ");
        }

        return $this->returnAjax(self::CODE_SUCCESS, ['step' => 'driver', 'orderId' => $order->id]);
    }

    /**
     * Поиск марки по названию
     *
     * @param array $params
     * @return array|string
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    private function searchBrand($params = [])
    {
        $term = $params['term'] ?? Yii::$app->request->get('term');
        $service = new \frontend\modules\inguru\services\CarInfoService();
        $brands = $service->searchBrandCarByName($term);

        if (!empty($brands)) {
            return Json::encode($brands);
        }

        return $this->returnAjax(self::CODE_ERROR, "Ошибка АПИ");
    }

    /**
     * Поиск модели по названию
     *
     * @param array $params
     * @return array|string
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    private function searchModel($params = [])
    {
        $term = $params['term'] ?? Yii::$app->request->get('term');
        $brandName = $params['brandName'] ?? Yii::$app->request->get('brandName');
        $companyId = $params['companyId'] ?? Yii::$app->request->get('companyId');

        $service = new \frontend\modules\inguru\services\CarInfoService();
        $brands = $service->searchModelCarByName($term, $brandName, $companyId);
        if (!empty($brands)) {
            return Json::encode($brands);
        }

        return $this->returnAjax(self::CODE_ERROR, "Ошибка АПИ");
    }

    /**
     * @return OsagoInguruForm
     */
    private function getModel()
    {
        $model = new OsagoInguruForm();
        $model->setScenario(OsagoInguruForm::SCENARIO_STEP_CAR_INFO);

        return $model;
    }
}