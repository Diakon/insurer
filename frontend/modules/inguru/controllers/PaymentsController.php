<?php

namespace frontend\modules\inguru\controllers;

use common\modules\inguru\dictionaries\OsagoInguruFormDictionary;
use common\modules\inguru\forms\OsagoInguruForm;
use common\modules\inguru\services\OsagoInguruFormService;
use common\modules\inguru\services\PaymentsService;
use common\modules\orders\dictionaries\OrderDictionary;
use common\modules\orders\services\OrderService;
use Yii;
use frontend\controllers\SiteController;
use frontend\modules\orders\traits\OrderTrait;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * Class PaymentsController
 * @package frontend\modules\inguru\controllers
 */
class PaymentsController extends SiteController
{
    use OrderTrait;

    /**
     * @return mixed|string
     */
    public function actionIndex()
    {
        if (!Yii::$app->request->isPost) {
            return $this->goHome();
        }

        set_time_limit(3 * 60); // Ответ от страховых может быть долгим - будем ждать до 3 минут

        $eId = Yii::$app->request->post('eId', 0);
        $company = Yii::$app->request->post('company');
        $awaiting = Yii::$app->request->post('awaiting');
        $awaiting = !empty($awaiting);

        // Получаю ID заказа
        $serviceOrder = new OrderService();
        $orderId = Yii::$app->request->get('order_id');
        $order = $serviceOrder->getOrdersByParams(['id' => $orderId, 'user_id' => Yii::$app->user->id], 'id', SORT_ASC, true);

        $model = $this->getModel();
        $model->eId = $eId;
        $model->company = $company;
        $model->successUrl = Yii::$app->request->hostInfo . '/payment-success/?token=' . $order->token;
        $model->failUrl = Yii::$app->request->hostInfo . '/payment-fail/?token=' . $order->token;

        // Если полный расчет - получаю стоимость и комиссию из сессии
        if ($model->preCalculation == OsagoInguruFormDictionary::PRE_CALCULATION_NO) {
            $sessionData = OsagoInguruFormService::getSessionInsureData($eId, $company);
            // Сохраняю выбранную стоимость и комиссию
            $order->price = $sessionData['price'] ?? null;
            $order->commission = $sessionData['commission'] ?? null;
            $serviceOrder->save($order);

            $model->paymentUrl = $order->generatePaymentLink(); // Генерирую ссылку на оплату
        }


        // Если нажали на кнопку сохранить в разделе "Ожилают оплаты", то не делаем запрос на оплату, а сохраняем заказ со статусом "Ожидает оплаты" и редиректим в этот раздел
        if ($awaiting) {
            // Сохраняю шаг как для "отложить оплату"
            if(!$this->saveStep($model, OrderDictionary::STATUS_AWAITING_PAYMENT)) {
                return $this->returnAjax(self::CODE_ERROR, 'Ошибка при сохранении данных. Пожалуйста, обратитесь в службу поддерхки!');
            }

            return $this->returnAjax(self::CODE_SUCCESS, ['url' => Url::to(['/awaiting-payment'])]);
        }

        $service = new PaymentsService();
        $responseResult = $service->getPaymentLinkResponse($model, $company); // Пробую получить ссылку на оплату

        if (!empty($responseResult)) {
            // Если пришел ответ и в ответе url - отдаю url оплаты
            if (empty($responseResult['errors']) && filter_var($responseResult['results'], FILTER_VALIDATE_URL, FILTER_FLAG_PATH_REQUIRED)) {
                // Сохраняю шаг
                if(!$this->saveStep($model)) {
                    return $this->returnAjax(self::CODE_ERROR, 'Ошибка при сохранении данных. Пожалуйста, обратитесь в службу поддерхки!');
                }

                return $this->returnAjax(self::CODE_SUCCESS, ['url' => $responseResult['results']]);
            } else {
                $msg = !empty($responseResult['errors'])
                    ? ArrayHelper::map($responseResult['errors'], 'code', 'error')
                    : "";
                return $this->returnAjax(self::CODE_ERROR, ['msg' => implode("\n", $msg)]);
            }
        }

        return $this->returnAjax(self::CODE_ERROR, 'Страховая компания отказала в выдаче полиса. Пожалуйста, проверьте данные и повторите снова.');
    }

    /**
     * @return OsagoInguruForm
     */
    private function getModel()
    {
        $model = new OsagoInguruForm();
        $model->setScenario(OsagoInguruForm::SCENARIO_STEP_PAYMENT);

        return $model;
    }
}