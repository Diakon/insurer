<?php

namespace frontend\modules\inguru\controllers;

use common\modules\orders\dictionaries\OrderDictionary;
use common\modules\orders\services\OrderService;
use Yii;
use frontend\controllers\SiteController;

/**
 * Class OrdersListController
 * @package frontend\modules\inguru\controllers
 */
class OrdersListController extends SiteController
{
    /**
     * @return mixed|string
     */
    public function actionIndex()
    {
        $status = Yii::$app->request->get('status', OrderDictionary::STATUS_NEW);
        $service = new OrderService();
        $orders = $service->getOrdersByUser(Yii::$app->user->id, $status);

        return $this->renderPartial('index', ['orders' => $orders]);
    }
}