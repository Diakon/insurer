<?php

namespace frontend\modules\inguru\controllers;

use common\modules\inguru\dictionaries\FinalCalculationDictionary;
use common\modules\inguru\dictionaries\OsagoInguruFormDictionary;
use common\modules\inguru\models\FinalCalculation;
use common\modules\inguru\models\PreCalculation;
use common\modules\inguru\forms\OsagoInguruForm;
use common\modules\inguru\services\FinalCalculationService;
use common\modules\inguru\services\OsagoInguruFormService;
use common\modules\inguru\services\PreCalculationService;
use common\modules\orders\models\CalculateProperty;
use common\modules\orders\dictionaries\OrderDictionary;
use common\modules\orders\models\OrderProperty;
use common\modules\orders\services\OrderPropertyService;
use Yii;
use frontend\controllers\SiteController;
use yii\base\BaseObject;
use yii\helpers\Json;
use yii\widgets\ActiveForm;
use common\modules\inguru\traits\AuthTrait;
use common\modules\inguru\traits\RequestApiTrait;
use frontend\modules\orders\traits\OrderTrait;

/**
 * Class CalculationController
 * @package frontend\modules\inguru\controllers
 */
class CalculationController extends SiteController
{
    use AuthTrait;
    use RequestApiTrait;
    use OrderTrait;
    /**
     * @return mixed|string
     */
    public function actionIndex()
    {
        if (!Yii::$app->request->isPost) {
            return $this->goHome();
        }

        $params = Yii::$app->request->post();
        $action = Yii::$app->request->get('action');
        if (!empty($action)) {
            return $this->{$action}($params);
        }

        $params = $this->getOrderParams($params);
        $modelForm = $this->setFormOrderModel(new OsagoInguruForm(), $params);
        $modelForm->setScenario(OsagoInguruForm::SCENARIO_STEP_CALCULATE);
        $modelForm->load($params);

        if (!$modelForm->validate()) {
            return $this->returnAjax(self::CODE_ERROR, ActiveForm::validate($modelForm));
        }

        // Валидация прошла - отправляю запрос в АПИ Ингуру
        $service = $modelForm->preCalculation == OsagoInguruFormDictionary::PRE_CALCULATION_YES ? new PreCalculationService() : new FinalCalculationService();
        $model = $modelForm->preCalculation == OsagoInguruFormDictionary::PRE_CALCULATION_YES ? new PreCalculation() : new FinalCalculation();
        $status = OrderDictionary::STATUS_NEW;
        if (!empty($params[$modelForm::classNameShort()])) {
            $model->load($params[$modelForm::classNameShort()], '');
        }
        $order = $this->getOrder();

        $customBrandModelCar = null;
        // Если полный расчет получаю выбранную страховую компанию, а так же тяну кастомный бренд/модель указанную для этой страховой компании (если быда указана)
        if ($modelForm->preCalculation == OsagoInguruFormDictionary::PRE_CALCULATION_NO) {
            $company = (int)Yii::$app->request->get('insuranceCompanyId', 0);
            $model->insuranceCompanies[] = $company;
            $customBrandModelCar = $this->getCustomCarBrandModelByCompany($order, $company);
        }

        // Сохраняю шаг, если это НЕ расчет для заказа из "ожидают оплаты" (нечего сохранять - уже все было сохранено ранее)
        if ($order->status != OrderDictionary::STATUS_AWAITING_PAYMENT) {
            $this->saveStep($modelForm, $status);
        }

        $response = $this->request(
            $service->getApiUrl(),
            $this->getToken(),
            $service->getApiDataByModel($model),
            $service->getApiCurlFormat(),
            $service->getApiCurlMethod()
        );

        $error = true;
        if ($response->isOk) {
            $response = $response->data;
            if (empty($response['errors'])) {
                $error = false;
            }
            // Если полный расчет - прохожу по предложениям от страховых компаний и пишу данные в сессию,
            // что бы после выбора предложения взять от туда цену, комиссию и тд
            if (!empty($response['results']) && $modelForm->preCalculation == OsagoInguruFormDictionary::PRE_CALCULATION_NO) {
                OsagoInguruFormService::setSessionInsureData($response['results']);
            }
        }

        return $this->renderPartial('index', [
            'model' => $model,
            'response' => $response,
            'error' => $error,
            'order' => $order,
            'customBrandModelCar' => $customBrandModelCar,
            'row' => Yii::$app->request->get('row', 1)
        ]);
    }

    /**
     * Выставляет кастомные значения бренда/модели ТС для компании
     * Некоторые страховые компании требуют название бренда/модели ТС указывать так, как заведено в справочнике конкретно у них,
     * а не так как одает ИНГУРУ по гос. номеру
     * @param $params
     * @return array
     */
    private function setCustomBrandModelCar($params)
    {
        $keyProperty = 'insurerCustomCarBrandModel';
        $order = $this->getOrder();
        // Получаю кастомные модель/бренд заданные для страховых компанй в этом заказе
        $service = new OrderPropertyService();
        $serviceModel = $service->getPropertyByParams($order->id, ['key' => $keyProperty], true);
        if (empty($serviceModel)) {
            $serviceModel = new CalculateProperty();
            $serviceModel->order_id = $order->id;
            $serviceModel->key = $keyProperty;
        }
        $serviceModelValue = !empty($serviceModel->value) ? Json::decode($serviceModel->value) : [];
        $serviceModelValue[$params['companyId']] = [
            'vehicleBrand' => $params['brand'],
            'vehicleModel' => $params['model'],
        ];
        $serviceModel->value = Json::encode($serviceModelValue);
        $service->save($serviceModel);

        return $this->returnAjax(self::CODE_SUCCESS);
    }
}