<?php
namespace frontend\modules\inguru;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\inguru\controllers';

    public function init()
    {
        parent::init();
    }
}
