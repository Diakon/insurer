<?php
namespace frontend\modules\inguru\services;

use api\modules\inguru\models\ModelBrandInfo;
use common\models\Service;
use common\modules\inguru\dictionaries\ModelBrandInfoDictionary;
use common\modules\inguru\services\ModelBrandInfoService;
use common\modules\inguru\traits\AuthTrait;
use common\modules\inguru\traits\RequestApiTrait;
use yii\base\BaseObject;

/**
 * Class CarInfoService
 * @package frontend\modules\inguru\services
 */
class CarInfoService extends Service
{
    use AuthTrait;
    use RequestApiTrait;

    /**
     * Поиск автомобиля по бренду (возвращает список брендов авто с таким названием).
     * Если передали $companyId - ищу в определенной компании
     * @param string $brandName
     * @param int|null $companyId
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function searchBrandCarByName(string $brandName, int $companyId = null)
    {
        $result = [];
        $service = new ModelBrandInfoService();
        $model = new ModelBrandInfo();
        $model->searchType = ModelBrandInfoDictionary::SEARCH_TYPE_1;
        $model->search = trim($brandName);
        $model->companyId = $companyId;

        $response = $this->request(
            $service->getApiUrl(),
            $this->getToken(),
            $service->getApiDataByModel($model),
            $service->getApiCurlFormat(),
            $service->getApiCurlMethod()
        );

        if ($response->isOk) {
            $response = $response->data;
            if (!empty($response['results'])) {
                foreach ($response['results'] as $brand) {
                    $result[] = [
                        'id' => trim($brand),
                        'value' => trim($brand),
                        'label' => trim($brand),
                    ];
                }
            }
        }

        return $result;
    }

    /**
     * Возвращает список авто в компании (ИНГУРУ возвращает весь список сразу)
     * @param int $companyId
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function brandCarListInCompany(int $companyId)
    {
        $result = [];
        $model = new ModelBrandInfo();
        $model->searchType = ModelBrandInfoDictionary::SEARCH_TYPE_1;
        $model->companyId = $companyId;
        $service = new ModelBrandInfoService();

        $response = $this->request(
            $service->getApiUrl(),
            $this->getToken(),
            $service->getApiDataByModel($model),
            $service->getApiCurlFormat(),
            $service->getApiCurlMethod()
        );

        if ($response->isOk) {
            $response = $response->data;
            if (!empty($response['results'])) {
                foreach ($response['results'] as $brand) {
                    $brand = trim($brand);
                    $result[$brand] = $brand;
                }
            }
        }

        return $result;
    }

    /**
     * Возвращает Модели авто по названию модели и бренду
     * Если передан так же $companyId - ищется только в указаной страховой компании
     * @param string $modelName
     * @param string $brandName
     * @param int|null $companyId
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function searchModelCarByName(string $modelName, string $brandName, int $companyId = null)
    {
        $searchType = ModelBrandInfoDictionary::SEARCH_TYPE_2;
        $result = [];
        $service = new ModelBrandInfoService();
        $model = new ModelBrandInfo();
        $model->searchType = $searchType;
        $model->search = trim($modelName);
        $model->brandName = trim($brandName);
        if (!empty($companyId)) {
            $model->companyId = $companyId;
        }

        $response = $this->request(
            $service->getApiUrl(),
            $this->getToken(),
            $service->getApiDataByModel($model),
            $service->getApiCurlFormat(),
            $service->getApiCurlMethod()
        );

        if ($response->isOk) {
            $response = $response->data;
            if (!empty($response['results'])) {
                foreach ($response['results'] as $brand) {
                    $result[] = [
                        'id' => trim($brand),
                        'value' => trim($brand),
                        'label' => trim($brand),
                    ];
                }
            }
        }

        return $result;
    }
}