<?php

namespace frontend\modules\orders\controllers;

use common\modules\bonuses\jobs\SendStoreBonus;
use common\modules\bonuses\services\BonusService;
use common\modules\cars\services\CarService;
use common\modules\orders\dictionaries\OrderDictionary;
use common\modules\orders\dictionaries\OrderPropertyDictionary;
use common\modules\orders\models\CarProperty;
use common\modules\orders\models\Order;
use common\modules\orders\services\OrderPropertyService;
use common\modules\orders\services\OrderService;
use Yii;
use frontend\controllers\SiteController;
use yii\base\BaseObject;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;

/**
 * Class PaymentController
 * @package frontend\modules\orders\controllers
 */
class PaymentController extends SiteController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['link', 'success', 'fail'],
                        'allow' => true,
                    ],
                ],
            ],
        ]);
    }
    /**
     * @var OrderService
     */
    private $service;

    /**
     * PaymentController constructor.
     * @param $id
     * @param $module
     * @param OrderService $service
     * @param array $config
     */
    public function __construct($id, $module, OrderService $service, $config = [])
    {
        $this->service = $service;
        parent::__construct($id, $module, $config);
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * Перенаправляет на страницу оплаты
     */
    public function actionLink()
    {
        $orderServiceProperty = new OrderPropertyService();
        /**
         * @var $order Order
         */
        $order = $this->getOrder();
        if (empty($order)) {
            throw new HttpException(404 ,'Заказ не найден');
        }
        if (in_array($order->status, [OrderDictionary::STATUS_COMPLETE, OrderDictionary::STATUS_CANCELED])) {
            throw new HttpException(404 ,'Заказ был ' . OrderDictionary::STATUSES[$order->status]);
        }

        // Если AJAX запрос - делаю запрос на получение ссылки на оплату
        if (Yii::$app->request->isAjax) {
            $responseResult = $this->service->getPaymentLink($order);

            if (!empty($responseResult)) {
                switch ($order->type) {
                    // Обработка ответа для перехода на страницу оплаты в ИНГУРУ
                    case OrderDictionary::TYPE_INGURU:
                        // Если пришел ответ и в ответе url - отдаю url оплаты
                        if (empty($responseResult['errors']) && filter_var($responseResult['results'], FILTER_VALIDATE_URL, FILTER_FLAG_PATH_REQUIRED)) {
                            $url = $responseResult['results'];
                            // Сохраняю url в OrderPropertyService (пока ссылка рабочая, надо использовать ее)
                            $orderServiceProperty->setProperty($order, OrderPropertyDictionary::TYPE_PAYMENT_PROPERTY_ID, ['tmpBankPaymentUrl' => $url]);
                            return $this->returnAjax(self::CODE_SUCCESS, ['url' => $url]);
                        }
                        else {
                            $msg = !empty($responseResult['errors'])
                                ? ArrayHelper::map($responseResult['errors'], 'code', 'error')
                                : "";
                            $msg = implode("\n", $msg);

                            // Смотрю, что если ошибка с кодом 109 (ингуру) - то значит уже есть ссылка созданая на оплату. Пробую вернуть ее (tmpBankPaymentUrl в OrderProperty)
                            if (
                                strcasecmp($msg, 'Создать дубль расчета.') == 0 ||
                                (!empty($responseResult['errors'][0]['code']) && $responseResult['errors'][0]['code'] == 109)
                            ) {
                                $url = $orderServiceProperty->getOrderPropertyValue($order->properties, 'tmpBankPaymentUrl');
                                if (!empty($url)) {
                                    return $this->returnAjax(self::CODE_SUCCESS, ['url' => $url]);
                                }
                            }
                            $msg = strcasecmp($msg, 'Создать дубль расчета.') == 0 ? "По указанным данным уже была получена ссылка на оплату ранее. Используйте ее или повторите попытку оплатить позднее." : $msg;
                            return $this->returnAjax(self::CODE_ERROR, ['msg' => $msg]);
                        }
                    // Обработка ответа для перехода на страницу оплаты в сравни.ру
                    case OrderDictionary::TYPE_SRAVNIRU:
                        return !empty($responseResult['url']) ?
                            $this->returnAjax(self::CODE_SUCCESS, ['url' => $responseResult['url']])
                            : $this->returnAjax(self::CODE_ERROR, ['msg' => 'Не удалось получить ссылку на оплату.']);
                }
            }
            return $this->returnAjax(self::CODE_ERROR, 'Не удалось получить ссылку на страницу оплаты. 
            Возможно, еще активна сгенерированная неданно ссылка. Пожалуйста, попробуйте позднее или сообщите в службу поддержки');
        }
        $properties = OrderPropertyService::propertyInfo($order);

        return $this->render('link', ['model' => $order, 'properties' => $properties]);
    }

    /**
     * Страница успешной оплаты
     * @return mixed|string
     */
    public function actionSuccess()
    {
        $order = $this->getOrder();
        if (empty($order)) {
            throw new HttpException(404 ,'Заказ не найден');
        }
        // Ставлю статус, что заказ успешно оплачен
        $order->status = OrderDictionary::STATUS_COMPLETE;
        $this->service->save($order);

        // Начисляю бонусы и ставлю задание в очередь на отправку в стор
        $bonusService = new BonusService();
        $bonusService->save($order);
        //Отправка задания в очередь
        \Yii::$app->queue->push(new SendStoreBonus([
            'order' => $order
        ]));


        return $this->render('success', ['model' => $order]);
    }

    /**
     * Страница не успешной оплаты
     * @return string
     * @throws HttpException
     */
    public function actionFail()
    {
        $token = Yii::$app->request->get('token');

        // Ищу заказ по токену
        $order = $this->service->getOrdersByParams(['token' => $token], 'id', SORT_ASC, true);
        if (empty($order)) {
            throw new HttpException(404 ,'Заказ не найден');
        }

        return $this->render('fail', ['model' => $order]);
    }

    /**
     * Вовзвращает заказ по токену
     * @return array|\yii\db\ActiveRecord|\yii\db\ActiveRecord[]|null
     */
    private function getOrder()
    {
        $token = Yii::$app->request->get('token');
        return $this->service->getOrdersByParams(['token' => $token], 'id', SORT_ASC, true);
    }
}