<?php

namespace frontend\modules\orders\controllers;

use common\modules\orders\dictionaries\OrderDictionary;
use common\modules\orders\services\OrderService;
use frontend\controllers\SiteController;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;

/**
 * Class OrderController
 * @package frontend\modules\orders\controllers
 */
class OrderController extends SiteController
{
    /**
     * @var OrderService
     */
    private $service;

    /**
     * PaymentController constructor.
     * @param $id
     * @param $module
     * @param OrderService $service
     * @param array $config
     */
    public function __construct($id, $module, OrderService $service, $config = [])
    {
        $this->service = $service;
        parent::__construct($id, $module, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['list', 'documents'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ]);
    }

    /**
     * Возвращает заказы пользователя
     * @param null $status
     * @return string
     */
    public function actionList($status = null)
    {
        // Получаю заказы пользователя
        $params = [];
        $params['user_id'] = \Yii::$app->user->id;
        if (!is_null($status)) {
            $params['status'] = $status;
        }
        $orders = $this->service->getOrdersByParams($params, 'id', SORT_DESC);

        switch ($status) {
            case OrderDictionary::STATUS_NEW:
                $view = '_new';
                break;
            case OrderDictionary::STATUS_AWAITING_PAYMENT:
                $view = '_awaiting';
                break;
            case OrderDictionary::STATUS_COMPLETE:
                $view = '_complete';
                break;
            default:
                $view = '_new';
        }

        return $this->render('list', ['orders' => $orders, 'view' => $view]);
    }

    /**
     * Возвращает документы об оплате
     * @param $id
     * @return array
     * @throws HttpException
     */
    public function actionDocuments($id)
    {
        $params = [];
        $params['id'] = $id;
        $params['user_id'] = \Yii::$app->user->id;
        /**
         * @var $order \common\modules\orders\models\Order
         */
        $order = $this->service->getOrdersByParams($params, 'id', SORT_DESC, true);
        if (empty($order)) {
            throw new HttpException(404 ,'Заказ не найден');
        }
        // Получаю ссылки на файл документов по заказу
        $documents = $this->service->getOsagoDocuments($order);

        return $documents
            ? $this->returnAjax(self::CODE_SUCCESS, ['documents' => $documents])
            : $this->returnAjax(self::CODE_ERROR, ['msg' => "Ошибка получения ссылок на документы оплаты"]);
    }
}