<?php

namespace frontend\modules\orders\traits;

use common\modules\inguru\forms\OsagoInguruForm;
use common\modules\orders\models\CalculateProperty;
use common\modules\orders\models\CarProperty;
use common\modules\orders\dictionaries\OrderDictionary;
use common\modules\orders\models\DriverProperty;
use common\modules\orders\models\OwnerProperty;
use common\modules\orders\models\PaymentProperty;
use common\modules\orders\models\PolicyProperty;
use common\modules\orders\services\OrderPropertyService;
use common\modules\orders\services\OrderService;
use common\modules\sravniru\forms\OsagoSravniRuForm;
use Yii;
use common\modules\orders\models\Order;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

trait OrderTrait
{
    /**
     * Сохраняет шаг заказа
     * @param OsagoInguruForm|OsagoSravniRuForm $form
     * @param int $status
     * @return bool|Order
     */
    public function saveStep($form, int $status = OrderDictionary::STATUS_NEW)
    {
        $step = $form->scenario;
        $params = $form->scenarios();
        $params = $params[$step] ?? [];
        $serviceOrder = $this->getOrderService();
        $serviceProperty = $this->getPropertyService();

        switch ($form::classNameShort()) {
            case "OsagoInguruForm":
                $type = OrderDictionary::TYPE_INGURU;
                break;
            case "OsagoSravniRuForm":
                $type = OrderDictionary::TYPE_SRAVNIRU;
                break;
            default:
                $type = OrderDictionary::TYPE_SRAVNIRU;
        }

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $needRollBack = false;
            $model = $this->getOrder();

            $model->step = $step;
            $model->type = $type;
            $model->status = $status;
            // Пользователь решил не оплачивать заказ, а отложить -
            // ставлю шаг на этап расчет (когда пользователь захочет оплатить - надо снова будет проверить введенные данные)
            if ($model->status == OrderDictionary::STATUS_AWAITING_PAYMENT) {
                $model->step = $form::SCENARIO_STEP_CALCULATE;
            }
            if ($serviceOrder->save($model)) {
                // Сохраняю выбранные параметры
                switch ($step) {
                    case "car":
                        $class = new CarProperty();
                        break;
                    case "driver":
                        $class = new DriverProperty();
                        break;
                    case "owner":
                        $class = new OwnerProperty();
                        break;
                    case "policy":
                        $class = new PolicyProperty();
                        break;
                    case "calc":
                        $class = new CalculateProperty();
                        break;
                    case "payment":
                        $class = new PaymentProperty();
                        break;
                    default:
                        $class = new CarProperty();
                }
                $serviceProperty->deleteByOrderId($model->id, $class::typeId()); // Очищаю от выбранных ранее параметров
                // Пишу новые параметры
                foreach ($form->attributes as $key => $value) {
                    if (!in_array($key, $params)) {
                        continue;
                    }
                    $className = get_class($class);
                    $modelProperty = new $className();
                    $modelProperty->order_id = $model->id;
                    $modelProperty->key = $key;
                    $modelProperty->value = is_array($value) ? Json::encode($value) : trim($value);
                    if (!$serviceProperty->save($modelProperty)) {
                        $needRollBack = true;
                        Yii::error('Ошибка при сохранении параметров заказа на шаге ' . $step . '. Сообщение валидатора: ' . Json::encode($modelProperty->errors));
                    }
                }

                if ($needRollBack) {
                    $transaction->rollBack();
                    return false;
                }

                $transaction->commit();
                return $model;
            }
        } catch (\Throwable $e) {
            $transaction->rollBack();
            Yii::error('Ошибка при сохранении шага ' . $step);
        }

        return false;
    }

    /**
     * Возвращает параметры заказа
     * @param array $params
     * @param string $paramsKey
     * @return array
     */
    public function getOrderParams(array $params = [], string $paramsKey = null)
    {
        if (empty($paramsKey)) {
            // Если имя ключа формы не передали - получаю
            foreach ([new OsagoInguruForm(), new OsagoSravniRuForm()] as $class) {
                $class = $class::classNameShort();
                if (!empty($params[$class])) {
                    $paramsKey = $class;
                    break;
                }
            }
        }

        $orderId = Yii::$app->request->get('order_id', 0);
        if (!empty($orderId)) {
            // Получаю параметры из заказа на шагах
            $serviceProperty = $this->getPropertyService();
            $property = $serviceProperty->getOrdersByParams(['order_id' => $orderId]);
            if (!empty($property)) {
                foreach (ArrayHelper::map($property, 'key', 'value') as $key => $value) {
                    $value = $this->isJson($value) ? Json::decode($value) : $value;
                    $params[$paramsKey][$key] = $params[$paramsKey][$key] ?? $value;
                }
            }
        }

        return $params;
    }

    /**
     * Возвращает заказ
     *
     * @return array|Order|\yii\db\ActiveRecord
     */
    public function getOrder()
    {
        $orderId = Yii::$app->request->get('order_id', 0);
        $serviceOrder = $this->getOrderService();
        // Если был передан ID заказа и он не завершен - продолжаю с выбранного заказа
        $model = !empty($orderId) ? $serviceOrder->getOrderById($orderId, Yii::$app->user->id) : null;

        // Если нет заказа - создаем новый
        if (empty($model)) {
            $model = new Order();
            $model->status = OrderDictionary::STATUS_NEW;
            $model->user_id = \Yii::$app->user->id;
        }
        $model->token = !empty($model->token) ? $model->token : $model->generateToken();

        return $model;
    }

    /**
     * Выставляет параметры для формы заказа ОСАГО
     *
     * @param OsagoInguruForm|OsagoSravniRuForm $model
     * @param array $params
     * @return OsagoInguruForm
     */
    public function setFormOrderModel($model, array $params = [])
    {
        $order = $this->getOrder();
        // Если это продолжение оформления не завершенного ранее заказа - проставляю на форму данные, которые пользоватиель вводил для этого заказа ранее
        if (!$order->isNewRecord) {
            $paramsProperty = [];
            foreach ($order->properties as $property) {
                $paramsProperty[$model::classNameShort()][$property->key] = $this->isJson($property->value) ? Json::decode($property->value) : $property->value;
            }
            $params = ArrayHelper::merge($paramsProperty, $params);
        }

        $model->load($params);

        return $model;
    }

    /**
     * Возвращает массив Бренд и Модель ТС указанный специально для страховой компании
     *
     * @param Order $order
     * @param $company
     * @return mixed|null
     */
    public function getCustomCarBrandModelByCompany(Order $order, $company)
    {
        $service = new OrderPropertyService();
        $customCarBrandModel = $service->getCarCustomBrandModel($order);

        return $customCarBrandModel[$company] ?? null;
    }

    /**
     * Возвращает сервис заказа
     * @return OrderService
     */
    private function getOrderService()
    {
        return new OrderService();
    }

    /**
     * Возвращает сервис характеристик заказа
     * @return OrderPropertyService
     */
    private function getPropertyService()
    {
        return new OrderPropertyService();
    }

    /**
     * Проверяет, является ли строка json
     *
     * @param $string
     * @return bool
     */
    public function isJson($string) {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}