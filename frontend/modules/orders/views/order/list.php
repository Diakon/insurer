<?php
/**
 * @var $orders \common\modules\orders\models\Order[]
 * @var $view string
 */
?>
<?= Yii::$app->controller->renderPartial($view, ['orders' => $orders]) ?>
