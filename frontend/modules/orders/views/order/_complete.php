<?php
/**
 * @var $orders \common\modules\orders\models\Order[]
 * @var $view string
 */

use yii\helpers\Url;
use common\modules\inguru\dictionaries\FinalCalculationDictionary;

$this->title = 'Страхование: Оплаченные';

$service = new \common\modules\orders\services\OrderPropertyService();
?>

<?php if (!empty($orders)) { ?>
    <div class="col-lg-12">
        <div class="row">
            <b>Оплаченные полисы ОСАГО:</b>
        </div>
        <?php foreach ($orders as $order) { ?>
            <?php
            $property = $order->properties;
            $brandCar = $service->getOrderPropertyValue($property, 'vehicleBrand');
            $modelCar = $service->getOrderPropertyValue($property, 'vehicleModel');
            $company = $service->getOrderPropertyValue($property, 'company');
            $paymentUrl = $service->getOrderPropertyValue($property, 'paymentUrl');
            $eId = $service->getOrderPropertyValue($property, 'eId');
            $bonuses = $order->bonuses;
            ?>
            <li>
                <div class="base-color-link">
                    <p>
                        Заказ полиса ОСАГО № <?= $order->id ?> от <?= Yii::$app->formatter->asDate($order->created_at, 'php:d.m.Y H:i') ?>
                        <?php if (!empty($brandCar) && !empty($modelCar)) { ?>
                            для автомобиля <?= $brandCar . ' ' . $modelCar ?>
                            в страховой компании "<?= FinalCalculationDictionary::INSURANCE_COMPANIES[$company] ?? "" ?>"
                            оплачен <?= Yii::$app->formatter->asDate($order->updated_at, 'php:d.m.Y H:i:s') ?>
                        <?php } ?>
                    </p>
                    <?php if (!empty($bonuses)) { ?>
                        <?php foreach ($bonuses as $bonus) { ?>
                            <div class="bonus-block-info">
                                <b>Бонусов получено: <?= round($bonus->bonus) ?></b>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <div
                            class="alert alert-info js-payment-document-block" role="alert"
                            style="display: none; margin-top: 10px; margin-bottom: 10px;"
                    ></div>
                    <br>
                    <a
                        href="#"
                        class="js-payment-document-link btn btn-sm btn-info" data-url="<?= Url::to(['/orders/order/documents', 'id' => $order->id]) ?>"
                    >Получить ссылки на документы</a>
                </div>
            </li>
        <?php } ?>
    </div>
<?php } else { ?>
    <div class="col-lg-12">
        <div class="row">
            <b>У Вас нет оплаченых заказов полиса ОСАГО.</b>
        </div>
    </div>
<?php } ?>
