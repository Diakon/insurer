<?php
/**
 * @var $order \common\modules\orders\models\Order
 * @var $view string
 */

use yii\helpers\Url;
use common\modules\inguru\dictionaries\FinalCalculationDictionary;

$service = new \common\modules\orders\services\OrderPropertyService();
$property = $order->properties;
$brandCar = $service->getOrderPropertyValue($property, 'vehicleBrand');
$modelCar = $service->getOrderPropertyValue($property, 'vehicleModel');
$company = $service->getOrderPropertyValue($property, 'company');
$paymentUrl = $service->getOrderPropertyValue($property, 'paymentUrl');
$eId = $service->getOrderPropertyValue($property, 'eId');
?>
<li>
    <div class="base-color-link">
        <p>
            Заказ полиса ОСАГО от <?= Yii::$app->formatter->asDate($order->created_at, 'php:d.m.Y H:i') ?>
            <?php if (!empty($brandCar) && !empty($modelCar)) { ?>
                для автомобиля <?= $brandCar . ' ' . $modelCar ?>
                в страховой компании "<?= FinalCalculationDictionary::INSURANCE_COMPANIES[$company] ?? "" ?>"
            <?php } ?>
        </p>
        <a href="<?= Url::to(['/calculation/', 'order_id' => $order->id, 'company_id' => $company]) ?>" class="btn btn-sm btn-info">Перейти в расчеты</a>
        <div style="margin-left: -20000px; position: absolute">
            <?= \yii\helpers\Html::textInput('url', $paymentUrl, ['id' => 'js-payment-url-link-input']) ?>
        </div>
        <a
            href="#"
            class="js-share-payment-link btn btn-sm btn-info"
        >Поделиться ссылкой</a>
    </div>
</li>
