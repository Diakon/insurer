<?php
/**
 * @var $orders \common\modules\orders\models\Order[]
 * @var $view string
 */

use common\modules\orders\dictionaries\OrderDictionary;
use yii\helpers\Url;

$this->title = 'Страхование: Ожидают оплаты';
?>

<?php if (!empty($orders)) { ?>
    <div class="col-lg-12">
        <div class="row">
            <b>Следующие полисы ОСАГО ожидают оплаты:</b>
        </div>
        <?php foreach ($orders as $order) {
            echo Yii::$app->controller->renderPartial($order->type == OrderDictionary::TYPE_INGURU ? '_awaiting_inguru' : '_awaiting_sravniru', ['order' => $order]);
        } ?>
    </div>
<?php } else { ?>
    <div class="col-lg-12">
        <div class="row">
            <b>У Вас нет ожидающих оплаты заказов полиса ОСАГО.</b>
        </div>
    </div>
<?php } ?>
