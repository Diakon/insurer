<?php
/**
 * @var $order \common\modules\orders\models\Order
 * @var $view string
 */

use yii\helpers\Url;

$service = new \common\modules\orders\services\OrderPropertyService();
$property = $order->properties;
$companyId = $service->getOrderPropertyValue($property, 'companyId');
$companyName = $service->getOrderPropertyValue($property, 'companyName');
$paymentUrl = $service->getOrderPropertyValue($property, 'paymentUrl');
$carNumber = $service->getOrderPropertyValue($property, 'vehicleLicensePlate');
$car = !empty($carNumber) ? \common\modules\cars\services\CarService::getCar($carNumber, $order->type) : null;
$modelCar = !empty($car) ? $car->model : "";
$brandCar = !empty($car) ? $car->brand : "";
?>
<li>
    <div class="base-color-link">
        <p>
            Заказ полиса ОСАГО от <?= Yii::$app->formatter->asDate($order->created_at, 'php:d.m.Y H:i') ?>
            <?php if (!empty($brandCar) && !empty($modelCar)) { ?>
                для автомобиля <?= $brandCar . ' ' . $modelCar ?>
                в страховой компании "<?= $companyName ?? "" ?>"
            <?php } ?>
        </p>
        <a href="<?= Url::to(['/calculation/', 'order_id' => $order->id, 'company_id' => $companyId]) ?>" class="btn btn-sm btn-info">Перейти в расчеты</a>
        <div style="margin-left: -20000px; position: absolute">
            <?= \yii\helpers\Html::textInput('url', $paymentUrl, ['id' => 'js-payment-url-link-input']) ?>
        </div>
        <a
            href="#"
            class="js-share-payment-link btn btn-sm btn-info"
        >Поделиться ссылкой</a>
    </div>
</li>
