<?php
/**
 * @var $orders \common\modules\orders\models\Order[]
 * @var $view string
 */
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Страхование: Мои расчеты';

$service = new \common\modules\orders\services\OrderPropertyService();
?>

<?php if (!empty($orders)) { ?>
    <div class="col-lg-12">
        <div class="row">
            <b>У Вас не оплачены следующие полисы ОСАГО:</b>
        </div>
        <?php foreach ($orders as $order) { ?>
            <?php
            $property = $order->properties;
            $brandCar = $service->getOrderPropertyValue($property, 'vehicleBrand');
            $modelCar = $service->getOrderPropertyValue($property, 'vehicleModel');
            $carNumber = $service->getOrderPropertyValue($property, 'vehicleLicensePlate');
            if ($order->type == \common\modules\orders\dictionaries\OrderDictionary::TYPE_SRAVNIRU) {
                $car = !empty($carNumber) ? \common\modules\cars\services\CarService::getCar($carNumber, $order->type) : null;
                $modelCar = !empty($car) ? $car->model : "";
                $brandCar = !empty($car) ? $car->brand : "";
            }
            ?>
            <li>
                <div class="base-color-link">
                    <a href="<?= Url::to(['/calculation/', 'order_id' => $order->id]) ?>" class="btn btn-sm">
                        <p>
                            Заказ от <?= Yii::$app->formatter->asDate($order->created_at, 'php:d.m.Y H:i') ?>
                            <?php if (!empty($brandCar) && !empty($modelCar)) { ?>
                                для автомобиля <?= $brandCar . ' ' . $modelCar ?>
                            <?php } ?>
                        </p>
                    </a>
                </div>
            </li>
        <?php } ?>
    </div>
<?php } else { ?>
    <div class="col-lg-12">
        <div class="row">
            <b>У Вас нет не законченых расчетов.</b>
        </div>
    </div>
<?php } ?>
