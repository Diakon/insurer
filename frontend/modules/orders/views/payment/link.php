<?php
/**
 * @var $model \common\modules\orders\models\Order
 * @var $properties \common\modules\orders\models\OrderProperty[]
 */
?>
<div class="col-lg-12">
    <div class="row">
        <div style="text-align: center">
            <?php
            switch ($model->type) {
                case \common\modules\orders\dictionaries\OrderDictionary::TYPE_INGURU:
                    echo Yii::$app->controller->renderPartial('_inguru_tab_info', ['model' => $model, 'properties' => $properties]);
                    break;
                case \common\modules\orders\dictionaries\OrderDictionary::TYPE_SRAVNIRU:
                    echo Yii::$app->controller->renderPartial('_sravniru_tab_info', ['model' => $model, 'properties' => $properties]);
                    break;
            }
            ?>
        </div>
    </div>
</div>
<div align="center">
    <div class="js-redirect-to-payment-order-loader-block">
        <div class="js-payment-link-page-error" style="padding: 20px; color: #FF6B6B"></div>
        <div style="padding: 20px; display: none" class="js-ajax-load-image">
            <img class="rot" src="/images/load-2.png" width="200px">
        </div>
    </div>
    <br>
    <a href="#" class="btn btn-primary js-redirect-to-payment-order-accept-btn">Перейти к оплате</a>
</div>