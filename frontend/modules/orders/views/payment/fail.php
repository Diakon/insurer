<?php
/**
 * @var $model \common\modules\orders\models\Order
 */
use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="col-lg-12">
    <div class="row">
        <div class="alert alert-danger" role="alert">
            <b>При оплате полиса ОСАГО возникла ошибка.</b>
            <br>
            Вы хотите
            <?= Html::a('повторить оплату', Url::to(['/', 'order_id' => $model->id])) ?>
            заказа № <?= $model->id ?>?
        </div>
    </div>
</div>