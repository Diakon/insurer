<?php
/**
 * @var $model \common\modules\orders\models\Order
 */

?>

<div class="col-lg-12">
    <div class="row">
        <div class="alert alert-info" role="alert">
            <b>Заказ №<?= $model->id ?> полиса ОСАГО успешно оплачен, спасибо!</b>
        </div>
    </div>
</div>