<?php

use common\modules\sravniru\dictionaries\FinalCalculationDictionary;

/**
 * @var $model \common\modules\orders\models\Order
 * @var $properties \common\modules\orders\models\OrderProperty[]
 */

$service = new \common\modules\orders\services\OrderPropertyService();
$property = $model->properties;
$brandCarId = $service->getOrderPropertyValue($property, 'vehicleBrand');
$modelCarId = $service->getOrderPropertyValue($property, 'vehicleModel');
$companyId = $service->getOrderPropertyValue($property, 'companyId');
$companyName = $service->getOrderPropertyValue($property, 'companyName');
$carNumber = $service->getOrderPropertyValue($property, 'vehicleLicensePlate');
$car = !empty($carNumber) ? \common\modules\cars\services\CarService::getCar($carNumber, $model->type) : null;
?>

<h4>
    Заказ полиса ОСАГО от <?= Yii::$app->formatter->asDate($model->created_at, 'php:d.m.Y H:i') ?>
    <?php if (!empty($car)) { ?>
        для автомобиля <?= $car->model . ' ' . $car->brand ?>
        в страховой компании "<?= $companyName ?? "" ?>"
        подготовлен для совершения оплаты.<br>
        Пожалуйста, проверьте правильность данных и, если все верно, нажмите на кнопку перехода к оплате.
    <?php } ?>
</h4>
<br>
<b>Подробная информация:</b>
<br>
<ul class="nav nav-tabs base-color-link">
    <li class="active" >
        <a data-toggle="tab" href="#cartab">Автомобиль</a>
    </li>
    <li>
        <a data-toggle="tab" href="#drivertab">Водители</a>
    </li>
    <li>
        <a data-toggle="tab" href="#ownertab">Собственник</a>
    </li>
    <li>
        <a data-toggle="tab" href="#policetab">Полис</a>
    </li>
</ul>
<div class="tab-content" style="text-align: left">
    <div class="tab-pane active" id="cartab">
        <ul style="padding-top: 10px;">
            <?php foreach ($properties[\common\modules\orders\models\CarProperty::typeId()] as $propertyInfo) { ?>
                <?php
                $value = trim($propertyInfo['value']);
                if (in_array($propertyInfo['key'], ['preCalculation', 'vehicleType']) || $value == "") {
                    continue;
                }
                if (in_array($propertyInfo['key'], ['vehicleIssueDate', 'vehicleDocDate', 'vehicleDcDate'])) {
                    $value = Yii::$app->formatter->asDate($value, 'php:d.m.Y');
                }
                if (in_array($propertyInfo['key'], ['vehicleDocType'])) {
                    $value = FinalCalculationDictionary::CAR_DOCS[$value] ?? "";
                }
                if (in_array($propertyInfo['key'], ['vehicleModel'])) {
                    $value = !empty($car) ? $car->model : null;
                }
                if (in_array($propertyInfo['key'], ['vehicleBrand'])) {
                    $value = !empty($car) ? $car->brand : null;
                }
                ?>
                <li><?= $propertyInfo['label'] ?>: <?= $value ?></li>
            <?php } ?>
        </ul>
    </div>
    <div class="tab-pane" id="drivertab">
        <ul style="padding-top: 10px;">
            <?php
            $driversInfo = [];
            $multiDrive = false;
            foreach ($properties[\common\modules\orders\models\DriverProperty::typeId()] as $propertyInfo) {
                if (in_array($propertyInfo['key'], ['preCalculation'])) {
                    continue;
                }
                if ($propertyInfo['key'] == 'multiDrive') {
                    $multiDrive = !empty($propertyInfo['value']);
                    continue;
                }
                if (!is_array($propertyInfo['value'])) {
                    continue;
                }
                foreach ($propertyInfo['value'] as $propertyInfoKey => $propertyInfoValue) {
                    $driversInfo[$propertyInfoKey][$propertyInfo['label']] = $propertyInfoValue;
                }
            }
            if ($multiDrive) {
                echo "<li>Мультидрайв</li>";
            } else {
                foreach ($driversInfo as $driverInfo) { ?>
                    <?php foreach ($driverInfo as $driverInfoLabel => $driverInfoValue) { ?>
                        <?php
                        if (empty($driverInfoLabel)) continue;
                        ?>
                        <li><?= $driverInfoLabel ?>
                            : <?= $driverInfoValue == "" ? "Не указано" : $driverInfoValue ?></li>
                    <?php } ?>
                    <li style="list-style-type: none;">
                        <hr>
                    </li>
                <?php } ?>
            <?php } ?>
        </ul>
    </div>
    <div class="tab-pane" id="ownertab">
        <?php
        $insurerIsOwner = false;
        $insurerList = $ownerList = [];
        foreach ($properties[\common\modules\orders\models\OwnerProperty::typeId()] as $propertyInfo) {
            $key = $propertyInfo['key'];
            $value = trim($propertyInfo['value']);
            if (in_array($key, ['ownerCity', 'preCalculation'])) {
                continue;
            }
            if (strripos($key, 'insurer') === false) {
                $ownerList[] = $propertyInfo;
            } else {
                if ($key == 'insurerIsOwner') {
                    $insurerIsOwner = true;
                    continue;
                }
                $insurerList[] = $propertyInfo;
            }
        }
        ?>
        <div style="padding-top: 10px;">
            <b>Владелец:</b>
            <br>
            <ul>
                <?php foreach ($ownerList as $owner) { ?>
                    <?php
                    $value = trim($owner['value']);
                    if (in_array($owner['key'], ['ownerBirthDate', 'ownerPassportDate', 'ownerDateOSAGOStart']) && !empty($value)) {
                        $value = Yii::$app->formatter->asDate($value, 'php:d.m.Y');
                    }
                    ?>
                    <li><?= $owner['label'] ?>: <?= $value ?></li>
                <?php } ?>
            </ul>
        </div>
        <?php if (!$insurerIsOwner) { ?>
            <div style="padding-top: 10px;">
                <b>Страхователь:</b>
                <br>
                <ul>
                    <?php foreach ($insurerList as $insurer) { ?>
                        <?php
                        $value = trim($insurer['value']);
                        if (in_array($insurer['key'], ['insurerPassportDate', 'insurerBirthDate']) && !empty($value)) {
                            $value = Yii::$app->formatter->asDate($value, 'php:d.m.Y');
                        }
                        ?>
                        <li><?= $insurer['label'] ?>: <?= $value ?></li>
                    <?php } ?>
                </ul>
            </div>
        <?php } ?>
    </div>
    <div class="tab-pane" id="policetab">
        <ul style="padding-top: 10px;">
            <?php foreach ($properties[\common\modules\orders\models\PolicyProperty::typeId()] as $propertyInfo) { ?>
                <?php
                $value = trim($propertyInfo['value']);
                if (in_array($propertyInfo['key'], ['preCalculation'])) {
                    continue;
                }
                ?>
                <li><?= $propertyInfo['label'] ?>: <?= $value != "" ? $value : "Не указано" ?></li>
            <?php } ?>
        </ul>
    </div>
</div>