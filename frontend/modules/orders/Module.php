<?php
namespace frontend\modules\orders;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\orders\controllers';

    public function init()
    {
        parent::init();
    }
}
