<?php
/**
 * @var $model \common\modules\sravniru\forms\OsagoSravniRuForm
 */

use common\modules\sravniru\dictionaries\PreCalculationDictionary;
use yii\helpers\Html;
$labels = $model->attributeLabels();
?>
<p>
    Вы можете заполнить данные по гос. номеру или указать вручную.
</p>
<div class="col-lg-4">
    <div class="row">
        <?= Html::activeLabel($model, 'vehicleLicensePlate') ?><br>
        <div class="col-lg-6">
            <div class="row">
                <?= Html::activeTextInput($model, 'vehicleLicensePlate', ['class' => 'form-control', 'placeholder' => 'Гос. номер']); ?>
            </div>
        </div>
        <div class="col-lg-2">
            <a href="#" class="btn btn-primary js-set-car-info-by-number">Заполнить</a>
        </div>
        <div style="clear: both;" class="js-error-msg js-error-osagosravniruform-vehiclelicenseplate"></div>
    </div>

    <br>

    <div class="row">
        <?= Html::activeLabel($model, 'vehicleBrand') ?><br>
        <?= Html::activeDropDownList($model, 'vehicleBrand', $model->getBrands(),
            ['class' => 'form-control js-vehicle-brand-id', 'prompt' => 'Выберите марку ТС']); ?>
        <div class="js-error-msg js-error-osagosravniruform-vehiclebrand"></div>
    </div>
    <div class="row">
        <?= Html::activeLabel($model, 'vehicleModel') ?><br>
        <?= Html::activeDropDownList($model, 'vehicleModel', $model->getModels(),
            ['class' => 'form-control js-vehicle-model-id', 'prompt' => 'Выберите модель ТС']); ?>
        <div class="js-error-msg js-error-osagosravniruform-vehiclemodel"></div>
    </div>
    <div class="row">
        <?= Html::activeLabel($model, 'vehicleYear') ?><br>
        <?= Html::activeTextInput($model, 'vehicleYear', ['class' => 'form-control js-mask-year', 'placeholder' => 'Год выпуска']); ?>
        <div class="js-error-msg js-error-osagosravniruform-vehicleyear"></div>
    </div>
    <div class="row">
        <?= Html::activeLabel($model, 'vehiclePower') ?><br>
        <?= Html::activeTextInput($model, 'vehiclePower', ['class' => 'form-control', 'placeholder' => 'Мощность']); ?>
        <div class="js-error-msg js-error-osagosravniruform-vehiclepower"></div>
    </div>
    <div class="row">
        <?= Html::activeLabel($model, 'vehicleIdentity') ?><br>
        <?= Html::activeDropDownList($model, 'vehicleIdentity', $model->getTypeCarIdentity(),
            ['class' => 'form-control js-vehicle-type-identity-select']); ?>
        <div class="js-error-msg js-error-osagosravniruform-vehicleidentity"></div>
    </div>
    <div class="row js-vehicle-type-identity js-vehicle-type-identity-id-<?= PreCalculationDictionary::CAR_HAVE_VIN ?>"
         style="<?= empty($model->vehicleIdentity) ? "" : "display:none" ?>">
        <?= Html::activeLabel($model, 'vehicleVin') ?><br>
        <?= Html::activeTextInput($model, 'vehicleVin', ['class' => 'form-control', 'placeholder' => $labels['vehicleVin']]); ?>
        <div class="js-error-msg js-error-osagosravniruform-vehiclevin"></div>
    </div>
    <div class="row js-vehicle-type-identity js-vehicle-type-identity-id-<?= PreCalculationDictionary::CAR_HAVE_BODY ?>"
         style="<?= $model->vehicleIdentity == PreCalculationDictionary::CAR_HAVE_BODY ? "" : "display:none" ?>">
        <?= Html::activeLabel($model, 'vehicleBodyNum') ?><br>
        <?= Html::activeTextInput($model, 'vehicleBodyNum', ['class' => 'form-control', 'placeholder' => $labels['vehicleBodyNum']]); ?>
        <div class="js-error-msg js-error-osagosravniruform-vehiclebodynum"></div>
    </div>
    <div class="row js-vehicle-type-identity js-vehicle-type-identity-id-<?= PreCalculationDictionary::CAR_HAVE_CHASSIS ?>"
         style="<?= $model->vehicleIdentity == PreCalculationDictionary::CAR_HAVE_CHASSIS ? "" : "display:none" ?>">
        <?= Html::activeLabel($model, 'vehicleChassisNum') ?><br>
        <?= Html::activeTextInput($model, 'vehicleChassisNum', ['class' => 'form-control', 'placeholder' => $labels['vehicleChassisNum']]); ?>
        <div class="js-error-msg js-error-osagosravniruform-vehiclechassisnum"></div>
    </div>

    <?= Yii::$app->controller->renderPartial('_extended_form', ['model' => $model]) ?>

</div>
