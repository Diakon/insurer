<?php
/**
 * Поля для расширенного расчета
 * @var $model \common\modules\sravniru\forms\OsagoSravniRuForm
 */

use common\modules\sravniru\dictionaries\FinalCalculationDictionary;
use yii\helpers\Html;
use common\modules\sravniru\dictionaries\OsagoSravniRuFormDictionary;

$labels = $model->attributeLabels();
?>

<div id="js-car-extended-info-block" style="<?= $model->preCalculation == OsagoSravniRuFormDictionary::PRE_CALCULATION_NO ? '' : 'display:none' ?>">
    <div class="row">
        <?= Html::activeLabel($model, 'vehicleDocType') ?><br>
        <?= Html::activeDropDownList($model, 'vehicleDocType', $model->getTypeCarDocs(),
            ['class' => 'form-control js-vehicle-doc-type', 'prompt' => $labels['vehicleDocType']]); ?>
        <div class="js-error-msg js-error-osagosravniruform-vehicledoctype"></div>
    </div>
    <div class="row">
        <?= Html::activeLabel($model, 'vehicleDocSerial') ?><br>
        <?= Html::activeTextInput($model, 'vehicleDocSerial', ['class' => 'form-control', 'placeholder' => $labels['vehicleDocSerial']]); ?>
        <div class="js-error-msg js-error-osagosravniruform-vehicledocserial"></div>
    </div>
    <div class="row">
        <?= Html::activeLabel($model, 'vehicleDocNumber') ?><br>
        <?= Html::activeTextInput($model, 'vehicleDocNumber', ['class' => 'form-control', 'placeholder' => $labels['vehicleDocNumber']]); ?>
        <div class="js-error-msg js-error-osagosravniruform-vehicledocnumber"></div>
    </div>
    <div class="row">
        <?= Html::activeLabel($model, 'vehicleDocDate') ?><br>
        <?php
        echo \yii\jui\DatePicker::widget([
            'model' => $model,
            'attribute' => 'vehicleDocDate',
            'language' => 'ru',
            'dateFormat' => 'dd.MM.yyyy',
            'options' => [
                'id' => 'osagosravniruform-vehicledocdate',
                'placeholder' => $labels['vehicleDocDate'],
                'class' => 'form-control js-mask-date js-date-picker-change-year-yes',
            ],
        ]);
        ?>
        <div class="js-error-msg js-error-osagosravniruform-vehicledocdate"></div>
    </div>

</div>
