<?php
/**
 * @var $model \common\modules\sravniru\forms\OsagoSravniRuForm
 * @var $order \common\modules\orders\models\Order
 * @var $response array
 * @var $success bool
 * @var $row integer
 * @var $selectedCompanyId integer
 */

use yii\helpers\Html;

$response = $response['response'] ?? $response;
if (!$success) { ?>
    <p>
        Во время расчета возникла ошибка. Пожалуйста, убедитесь в правильности введенных данных и повторите снова.
    </p>
<?php } else { ?>
    <div class="col-lg-12">
        <?php if (empty($response['osagoCalculateThroughOrderResults'])) { ?>
            <div class="row">
                <h4>Ни одна из страховых компаний не готова сделать предложение о заключении договора полиса ОСАГО</h4>
            </div>
        <?php } else { ?>
            <div class="row">
                <h4>Поступили следующие предложения от страховых компаний:</h4>
                <hr>
            </div>
            <?php
            $searchId = $response['searchId'] ?? null;
            foreach ($response['osagoCalculateThroughOrderResults'] as $orderResult) { ?>
                <?php
                $hash = $orderResult['hash'] ?? null;
                $companyId = $orderResult['companyId'] ?? null;
                $price = $orderResult['price'] ?? 0;
                $commission = $orderResult['partnerProfit']['profit'] ?? 0;
                $paymentUrl = $orderResult['paymentUrl'] ?? null;
                $companyName = $orderResult['provider']['name'] ?? "";
                $companyFullName = $orderResult['provider']['fullName'] ?? "";
                $rating = $orderResult['provider']['raRatingDescription'] ?? "";
                $coefficients = $orderResult['coefficients'];
                if (empty($paymentUrl) || (!empty($selectedCompanyId) && $selectedCompanyId != $companyId)) {
                    continue;
                }
                ?>
                <div class="alert alert-info" role="alert">
                    <b>Предложение от компании <?= !empty($companyFullName) ? $companyFullName : $companyName ?></b><br>
                    <ul>
                        <li>
                            Стоимость полиса ОСАГО: <?= Yii::$app->formatter->asPrice($price) ?>
                        </li>
                        <li>
                            Сумма бонусов которые Вы
                            получите: <?= Yii::$app->formatter->asPrice(\common\modules\bonuses\services\BonusService::calculatePrice($commission)) ?>
                        </li>
                    </ul>
                    <?php if (!empty($coefficients)) { ?>
                        <br>
                        <b>Дополнительные данные (коэфициенты вляющие на расчет):</b>
                        <ul>
                            <?php foreach ($coefficients as $coefficientKey => $coefficientValue) { ?>
                                <?php switch ($coefficientKey) {
                                    case "tb":
                                        echo Html::tag('li', 'Базовая стоимость полиса: ' . (Yii::$app->formatter->asPrice($coefficientValue)));
                                        break;
                                    case "kt":
                                        echo Html::tag('li', 'Территориальный коэффициент: ' . $coefficientValue);
                                        break;
                                    case "kbm":
                                        echo Html::tag('li', 'Коэффициент возникновения страховых случаев: ' . $coefficientValue);
                                        break;
                                    case "kvs":
                                        echo Html::tag('li', 'Коэффициент водительского стажа: ' . $coefficientValue);
                                        break;
                                    case "ks":
                                        echo Html::tag('li', 'Сезонный показатель: ' . $coefficientValue);
                                        break;
                                    case "kp":
                                        echo Html::tag('li', 'Коэффициент применения в определенный период времени: ' . $coefficientValue);
                                        break;
                                    case "km":
                                        echo Html::tag('li', 'Коэффициент мощности двигателя: ' . $coefficientValue);
                                        break;
                                    case "kn":
                                        echo Html::tag('li', 'Ставка страховой компании (зависит от количества выявленных нарушений): ' . $coefficientValue);
                                        break;
                                    case "ko":
                                        echo Html::tag('li', 'Коэффициент вычисляемый от количества страхуемых водителей: ' . $coefficientValue);
                                        break;
                                    case "kpr":
                                        echo Html::tag('li', 'Коэффициент вычисляемый от факта наличие прицепного устройства: ' . $coefficientValue);
                                        break;
                                } ?>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                    <div class="js-payment-btn-block">
                        <a
                                style="margin-top: 10px;"
                                href="<?= $paymentUrl ?>"
                                data-id="<?= $searchId ?>"
                                data-url="<?= $paymentUrl ?>"
                                data-hash="<?= $hash ?>"
                                data-company-id="<?= $companyId ?>"
                                data-company-name="<?= $companyName ?>"
                                class="btn btn-primary js-prepare-payment-service"
                        >Перейти к оформления ОСАГО</a>
                        <a
                                style="margin-top: 10px;"
                                href="#"
                                data-id="<?= $searchId ?>"
                                data-url="<?= $paymentUrl ?>"
                                data-hash="<?= $hash ?>"
                                data-company-id="<?= $companyId ?>"
                                data-company-name="<?= $companyName ?>"
                                class="btn btn-default js-save-to-awaiting-payment"
                        >Сохранить в раздел "Ожидают оплаты", что бы оплатить позднее</a>
                    </div>
                </div>
            <?php } ?>
        <?php } ?>
    </div>
<?php } ?>
<div style="display: none; position:absolute; margin-left: -40000px;">
    <pre>
        <?php print_r($response) ?>
    </pre>
</div>
