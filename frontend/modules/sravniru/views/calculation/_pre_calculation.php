<?php
/**
 * @var $model \common\modules\sravniru\forms\OsagoSravniRuForm
 * @var $order \common\modules\orders\models\Order
 * @var $response array
 * @var $success bool
 * @var $row integer
 */
if (!$success) { ?>
    <p>
        Во время расчета возникла ошибка. Пожалуйста, убедитесь в правильности введенных данных и повторите снова.
    </p>
<?php } else { ?>
    <?php
    // Прохожу по результатам - получаю максимальную и минимальную стоимость
    $results = [];
    foreach ($response['propositions'] as $result) {
        $price = $result['price'];
        if ($price < 1) {
            continue;
        }
        $results[$price] = $result;
    }
    ksort($results);
    $prices = array_keys($results);
    $priceMin = current($prices);
    $priceMax = array_pop($prices);
    ?>
    <div class="col-lg-4">
        <div class="row">
            <p><b>Предварительный расчет стоимости ОСАГО по введенным данным:</b></p>
            <p>Минимальная стоимость полиса: <?= Yii::$app->formatter->asPrice((float)$priceMin) ?></p>
            <p>Максимальная стоимость полиса: <?= Yii::$app->formatter->asPrice((float)$priceMax) ?></p>
        </div>
        <div class="row">
            <a href="#" style="display: none" class="btn btn-primary js-switch-to-final-calculation-btn">Перейти к
                полному расчету</a>
        </div>
    </div>
<?php } ?>
<div style="display: none; position:absolute; margin-left: -40000px;">
    <pre>
        <?php print_r($response) ?>
    </pre>
</div>
