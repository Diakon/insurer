<?php
/**
 * @var $model \common\modules\sravniru\forms\OsagoSravniRuForm
 * @var $order \common\modules\orders\models\Order
 * @var $response array
 * @var $success bool
 * @var $row integer
 * @var $selectedCompanyId integer
 */

echo Yii::$app->controller->renderPartial(
        $model::classNameShort() == "FinalCalculation" ? '_final_calculation' : '_pre_calculation',
        ['model' => $model, 'response' => $response, 'order' => $order, 'success' => $success, 'selectedCompanyId' => $selectedCompanyId, 'row' => $row]
)
?>
