<?php
/**
 * @var $model \common\modules\sravniru\forms\OsagoSravniRuForm
 */
use yii\helpers\Html;
use common\modules\sravniru\dictionaries\OsagoSravniRuFormDictionary;

$label = $model->attributeLabels();
?>
<div class="row">
    <h4>Информация о действующем полисе</h4>
    <hr>
</div>
<div class="col-lg-4">
    <div class="js-isset-prev-policy-block" style="<?= $model->prevPolicyIsset == OsagoSravniRuFormDictionary::PREV_POLICY_ISSET_NO ? "display:none" : "" ?>">
        <div class="row">
            <div class="row">
                <?= Html::activeLabel($model, 'prevPolicySerial') ?><br>
                <?= Html::activeTextInput($model, 'prevPolicySerial', ['class' => 'form-control', 'placeholder' => $label['prevPolicySerial']]); ?>
                <div class="js-error-msg js-error-osagosravniruform-prevpolicyserial"></div>
            </div>
            <br>
        </div>

        <div class="row">
            <div class="row">
                <?= Html::activeLabel($model, 'prevPolicyNumber') ?><br>
                <?= Html::activeTextInput($model, 'prevPolicyNumber', ['class' => 'form-control', 'placeholder' => $label['prevPolicyNumber']]); ?>
                <div class="js-error-msg js-error-osagosravniruform-prevpolicynumber"></div>
            </div>
            <br>
        </div>
        <div class="row">
            <div class="row">
                <?= Html::activeLabel($model, 'prevPolicyEndDate') ?><br>
                <?php
                echo \yii\jui\DatePicker::widget([
                    'model' => $model,
                    'attribute' => 'prevPolicyEndDate',
                    'language' => 'ru',
                    'dateFormat' => 'dd.MM.yyyy',
                    'options' => [
                        'id' => 'osagosravniruform-prevpolicyenddate',
                        'placeholder' => $label['prevPolicyEndDate'],
                        'class' => 'form-control js-mask-date js-date-picker-change-year-yes',
                    ],
                ]);
                ?>
                <div class="js-error-msg js-error-osagosravniruform-prevpolicyenddate"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="row">
            <div class="custom-control custom-switch">
                <input type="checkbox" class="custom-control-input" <?= $model->prevPolicyIsset == OsagoSravniRuFormDictionary::PREV_POLICY_ISSET_NO ? "" : "checked" ?> id="js-prev-policy-isset-switch">
                <label class="custom-control-label" for="js-prev-policy-isset-switch">Есть предыдущий полис</label>
                <?= Html::activeHiddenInput($model, 'prevPolicyIsset', ['class' => 'js-prev-policy-isset-input']) ?>
            </div>
        </div>
    </div>
</div>

