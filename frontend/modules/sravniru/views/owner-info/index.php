<?php
/**
 * @var $model \common\modules\sravniru\forms\OsagoSravniRuForm
 */
use yii\helpers\Html;
$label = $model->attributeLabels();
?>
<div class="col-lg-4">
    <div class="row">
        <h4>Информация о собственнике автомобиля</h4>
        <hr>
    </div>
    <div class="row">
        <?= Html::activeLabel($model, 'ownerCity') ?><br>
        <?= \yii\jui\AutoComplete::widget([
            'model' => $model,
            'attribute' => 'ownerCity',
            'clientOptions' => [
                'source' => \yii\helpers\Url::to(['/sravniru/owner-info/', 'action' => 'searchCity']),
                'minLength'=>'3',
                'select' => new \yii\web\JsExpression("function( event, ui ) {
                $(this).val(ui.item.value);
                }")
            ],
            'options'=>[
                'class' => 'form-control',
                'placeholder' => $label['ownerCity']
            ]
        ]);
        ?>
        <div class="js-error-msg js-error-osagosravniruform-ownercity"></div>
    </div>

    <div class="row">
        <?= Html::activeLabel($model, 'ownerDateOSAGOStart') ?><br>
        <?php
        echo \yii\jui\DatePicker::widget([
            'model' => $model,
            'attribute' => 'ownerDateOSAGOStart',
            'language' => 'ru',
            'dateFormat' => 'dd.MM.yyyy',
            'options' => [
                'id' => 'osagosravniruform-ownerdateosagostart',
                'placeholder' => $label['ownerDateOSAGOStart'],
                'style' => 'width:45%;',
                'class' => 'form-control js-date-picker-change-year-no-mindate js-mask-date js-osago-start-date-input',
            ],
        ]);
        ?>
        <?= Html::activeTextInput($model, 'ownerDateOSAGOEnd',
            [
                'class' => 'form-control js-osago-end-date-input',
                'disabled' => 'disabled',
                'style' => 'width: 45%; position: absolute; margin-top:-33px; margin-left:55%;',
                'value' => !empty($model->ownerDateOSAGOEnd) ? Yii::$app->formatter->asDate($model->ownerDateOSAGOEnd, 'php:d.m.Y') : ""
            ]) ?>
        <div class="js-error-msg js-error-osagosravniruform-ownerdateosagostart"></div>
        <i>Если указана дата выдачи полиса менее 4 дней от сегодняшнего дня, то количество предложений будет значительно ниже (всего две страховых компаний готовы расматривать обращения, где желаемая дата выдчи полиса менее четырех дней от текущей)</i>
    </div>

    <?php echo Yii::$app->controller->renderPartial('_extended_form', ['model' => $model]) ?>
</div>
