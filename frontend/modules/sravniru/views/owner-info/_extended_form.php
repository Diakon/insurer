<?php
/**
 * Поля для расширенного расчета
 * @var $model \common\modules\sravniru\forms\OsagoSravniRuForm
 */
use common\modules\sravniru\dictionaries\OsagoSravniRuFormDictionary;
use yii\helpers\Html;

$labels = $model->attributeLabels();
$showOwner = $model->preCalculation == OsagoSravniRuFormDictionary::PRE_CALCULATION_NO || $model->multiDrive == OsagoSravniRuFormDictionary::IS_MULTI_DRIVE_YES;
?>

<div id="js-owner-info-block" style="<?= $showOwner ? '' : 'display:none' ?>">
    <div class="row">
        <?= Html::activeLabel($model, 'ownerLastName') ?><br>
        <?= Html::activeTextInput($model, 'ownerLastName', ['class' => 'form-control js-fio-input', 'placeholder' => $labels['ownerLastName']]); ?>
        <div class="js-error-msg js-error-osagosravniruform-ownerlastname"></div>
    </div>
    <div class="row">
        <?= Html::activeLabel($model, 'ownerFirstName') ?><br>
        <?= Html::activeTextInput($model, 'ownerFirstName', ['class' => 'form-control js-fio-input', 'placeholder' => $labels['ownerFirstName']]); ?>
        <div class="js-error-msg js-error-osagosravniruform-ownerfirstname"></div>
    </div>
    <div class="row">
        <?= Html::activeLabel($model, 'ownerMiddleName') ?><br>
        <?= Html::activeTextInput($model, 'ownerMiddleName', ['class' => 'form-control js-fio-input', 'placeholder' => $labels['ownerMiddleName']]); ?>
        <div class="js-error-msg js-error-osagosravniruform-ownermiddlename"></div>
    </div>
    <div class="row">
        <?= Html::activeLabel($model, 'ownerBirthDate') ?><br>
        <?php
        echo \yii\jui\DatePicker::widget([
            'model' => $model,
            'attribute' => 'ownerBirthDate',
            'language' => 'ru',
            'dateFormat' => 'dd.MM.yyyy',
            'options' => [
                'id' => 'osagosravniruform-ownerbirthdate',
                'placeholder' => $labels['ownerBirthDate'],
                'class' => 'form-control js-mask-date js-date-picker-change-year-yes',
            ],
        ]);
        ?>
        <div class="js-error-msg js-error-osagosravniruform-ownerbirthdate"></div>
    </div>
    <div class="row">
        <?= Html::activeLabel($model, 'ownerEmail') ?><br>
        <?= Html::activeTextInput($model, 'ownerEmail', ['class' => 'form-control', 'placeholder' => $labels['ownerEmail']]); ?>
        <div class="js-error-msg js-error-osagosravniruform-owneremail"></div>
    </div>
    <div class="row">
        <?= Html::activeLabel($model, 'ownerPhone') ?><br>
        <?= Html::activeTextInput($model, 'ownerPhone', ['class' => 'form-control js-mask-phone', 'placeholder' => $labels['ownerPhone']]); ?>
        <div class="js-error-msg js-error-osagosravniruform-ownerphone"></div>
    </div>
    <div class="row">
        <?= Html::activeLabel($model, 'ownerDadataAddress') ?><br>
        <?= \yii\jui\AutoComplete::widget([
            'model' => $model,
            'attribute' => 'ownerDadataAddress',
            'clientOptions' => [
                'source' => \yii\helpers\Url::to(['/sravniru/owner-info/', 'action' => 'searchAddress']),
                'minLength'=>'3',
                'select' => new \yii\web\JsExpression("function( event, ui ) {
                $(this).val(ui.item.value);
                }")
            ],
            'options'=>[
                'class' => 'form-control',
                'placeholder' => $labels['ownerDadataAddress']
            ]
        ]);
        ?>
        <div class="js-error-msg js-error-osagosravniruform-insurerdadataaddress"></div>
    </div>
    <div class="row">
        <?= Html::activeLabel($model, 'ownerPassportSerial') ?><br>
        <?= Html::activeTextInput($model, 'ownerPassportSerial', ['class' => 'form-control js-mask-pass-serial', 'placeholder' => $labels['ownerPassportSerial']]); ?>
        <div class="js-error-msg js-error-osagosravniruform-ownerpassportserial"></div>
    </div>
    <div class="row">
        <?= Html::activeLabel($model, 'ownerPassportNumber') ?><br>
        <?= Html::activeTextInput($model, 'ownerPassportNumber', ['class' => 'form-control js-mask-pass-number', 'placeholder' => $labels['ownerPassportNumber']]); ?>
        <div class="js-error-msg js-error-osagosravniruform-ownerpassportnumber"></div>
    </div>
    <div class="row">
        <?= Html::activeLabel($model, 'ownerPassportIssuedBy') ?><br>
        <?= Html::activeTextInput($model, 'ownerPassportIssuedBy', ['class' => 'form-control', 'placeholder' => $labels['ownerPassportIssuedBy']]); ?>
        <div class="js-error-msg js-error-osagosravniruform-ownerpassportissuedby"></div>
    </div>
    <div class="row">
        <?= Html::activeLabel($model, 'ownerPassportDate') ?><br>
        <?php
        echo \yii\jui\DatePicker::widget([
            'model' => $model,
            'attribute' => 'ownerPassportDate',
            'language' => 'ru',
            'dateFormat' => 'dd.MM.yyyy',
            'options' => [
                'id' => 'osagosravniruform-ownerpassportdate',
                'placeholder' => $labels['ownerPassportDate'],
                'class' => 'form-control js-mask-date js-date-picker-change-year-yes',
            ],
        ]);
        ?>
        <div class="js-error-msg js-error-osagosravniruform-ownerpassportdate"></div>
    </div>
</div>

<div id="js-insurer-info-block" style="<?= $model->preCalculation == OsagoSravniRuFormDictionary::PRE_CALCULATION_NO ? '' : 'display:none' ?>">
    <div class="row">
        <div class="custom-control custom-switch">
            <input type="checkbox" class="custom-control-input"
                <?= $model->insurerIsOwner == \common\modules\sravniru\dictionaries\FinalCalculationDictionary::ONE_FACE_NO ? "" : "checked" ?>
                   id="js-insurer-is-owner-switch">
            <label class="custom-control-label" for="js-insurer-is-owner-switch">Страхователь и собственник одно лицо</label>
            <?= Html::activeHiddenInput($model, 'insurerIsOwner', ['class' => 'js-insurer-is-owner-input']) ?>
        </div>
    </div>

    <div class="js-insurer-is-owner-yes" style="<?= $model->insurerIsOwner == \common\modules\sravniru\dictionaries\FinalCalculationDictionary::ONE_FACE_NO ? "" : "display:none" ?>">
        <div class="row">
            <h4>Информация о страхователе</h4>
            <hr>
        </div>

        <div class="row">
            <?= Html::activeLabel($model, 'insurerLastName') ?><br>
            <?= Html::activeTextInput($model, 'insurerLastName', ['class' => 'form-control js-fio-input', 'placeholder' => $labels['insurerLastName']]); ?>
            <div class="js-error-msg js-error-osagosravniruform-insurerlastname"></div>
        </div>
        <div class="row">
            <?= Html::activeLabel($model, 'insurerFirstName') ?><br>
            <?= Html::activeTextInput($model, 'insurerFirstName', ['class' => 'form-control js-fio-input', 'placeholder' => $labels['insurerFirstName']]); ?>
            <div class="js-error-msg js-error-osagosravniruform-insurerfirstname"></div>
        </div>
        <div class="row">
            <?= Html::activeLabel($model, 'insurerMiddleName') ?><br>
            <?= Html::activeTextInput($model, 'insurerMiddleName', ['class' => 'form-control js-fio-input', 'placeholder' => $labels['insurerMiddleName']]); ?>
            <div class="js-error-msg js-error-osagosravniruform-insurermiddlename"></div>
        </div>
        <div class="row">
            <?= Html::activeLabel($model, 'insurerBirthDate') ?><br>
            <?php
            echo \yii\jui\DatePicker::widget([
                'model' => $model,
                'attribute' => 'insurerBirthDate',
                'language' => 'ru',
                'dateFormat' => 'dd.MM.yyyy',
                'options' => [
                    'id' => 'osagosravniruform-insurerbirthdate',
                    'placeholder' => $labels['insurerBirthDate'],
                    'class' => 'form-control js-mask-date js-date-picker-change-year-yes',
                ],
            ]);
            ?>
            <div class="js-error-msg js-error-osagosravniruform-insurerbirthdate"></div>
        </div>
        <div class="row">
            <?= Html::activeLabel($model, 'insurerEmail') ?><br>
            <?= Html::activeTextInput($model, 'insurerEmail', ['class' => 'form-control', 'placeholder' => $labels['insurerEmail']]); ?>
            <div class="js-error-msg js-error-osagosravniruform-insureremail"></div>
        </div>
        <div class="row">
            <?= Html::activeLabel($model, 'insurerPhone') ?><br>
            <?= Html::activeTextInput($model, 'insurerPhone', ['class' => 'form-control js-mask-phone', 'placeholder' => $labels['insurerPhone']]); ?>
            <div class="js-error-msg js-error-osagosravniruform-insurerphone"></div>
        </div>
        <div class="row">
            <?= Html::activeLabel($model, 'insurerDadataAddress') ?><br>
            <?= \yii\jui\AutoComplete::widget([
                'model' => $model,
                'attribute' => 'insurerDadataAddress',
                'clientOptions' => [
                    'source' => \yii\helpers\Url::to(['/sravniru/owner-info/', 'action' => 'searchAddress']),
                    'minLength'=>'3',
                    'select' => new \yii\web\JsExpression("function( event, ui ) {
                $(this).val(ui.item.value);
                }")
                ],
                'options'=>[
                    'class' => 'form-control',
                    'placeholder' => $labels['insurerDadataAddress']
                ]
            ]);
            ?>
            <div class="js-error-msg js-error-osagosravniruform-insurerdadataaddress"></div>
        </div>
        <div class="row">
            <?= Html::activeLabel($model, 'insurerPassportSerial') ?><br>
            <?= Html::activeTextInput($model, 'insurerPassportSerial', ['class' => 'form-control js-mask-pass-serial', 'placeholder' => $labels['insurerPassportSerial']]); ?>
            <div class="js-error-msg js-error-osagosravniruform-insurerpassportserial"></div>
        </div>
        <div class="row">
            <?= Html::activeLabel($model, 'insurerPassportNumber') ?><br>
            <?= Html::activeTextInput($model, 'insurerPassportNumber', ['class' => 'form-control js-mask-pass-number', 'placeholder' => $labels['insurerPassportNumber']]); ?>
            <div class="js-error-msg js-error-osagosravniruform-insurerpassportnumber"></div>
        </div>
        <div class="row">
            <?= Html::activeLabel($model, 'insurerPassportIssuedBy') ?><br>
            <?= Html::activeTextInput($model, 'insurerPassportIssuedBy', ['class' => 'form-control', 'placeholder' => $labels['insurerPassportIssuedBy']]); ?>
            <div class="js-error-msg js-error-osagosravniruform-insurerpassportissuedby"></div>
        </div>
        <div class="row">
            <?= Html::activeLabel($model, 'insurerPassportDate') ?><br>
            <?php
            echo \yii\jui\DatePicker::widget([
                'model' => $model,
                'attribute' => 'insurerPassportDate',
                'language' => 'ru',
                'dateFormat' => 'dd.MM.yyyy',
                'options' => [
                    'id' => 'osagosravniruform-insurerpassportdate',
                    'placeholder' => $labels['insurerPassportDate'],
                    'class' => 'form-control js-mask-date js-date-picker-change-year-yes',
                ],
            ]);
            ?>
            <div class="js-error-msg js-error-osagosravniruform-insurerpassportdate"></div>
        </div>
    </div>
</div>
