<?php
/**
 * @var $model \common\modules\sravniru\forms\OsagoSravniRuForm
 * @var $row integer
 */

use yii\helpers\Html;
use common\modules\sravniru\dictionaries\OsagoSravniRuFormDictionary;

$labels = $model->attributeLabels();
?>
<div class="col-lg-4" style="clear: both;">
    <div class="js-driver-block js-driver-block-id-<?= $row ?>">
        <?php if ($row > 0) { ?>
            <hr>
        <?php } ?>
        <div class="row">
            <div class="row">
                <?= Html::activeLabel($model, 'driverLastName') ?><br>
                <?= Html::activeTextInput($model, 'driverLastName[' . $row . ']',
                    [
                        'id' => 'osagosravniruform-driverlastname_' . $row,
                        'class' => 'form-control js-fio-input',
                        'placeholder' => $labels['driverLastName']
                    ]); ?>
                <div class="js-error-msg js-error-osagosravniruform-driverlastname_<?= $row ?>"></div>
            </div>
        </div>
        <div class="row">
            <div class="row">
                <?= Html::activeLabel($model, 'driverFirstName') ?><br>
                <?= Html::activeTextInput($model, 'driverFirstName[' . $row . ']',
                    [
                        'id' => 'osagosravniruform-driverfirstname_' . $row,
                        'class' => 'form-control js-fio-input',
                        'placeholder' => $labels['driverFirstName']
                    ]); ?>
                <div class="js-error-msg js-error-osagosravniruform-driverfirstname_<?= $row ?>"></div>
            </div>
        </div>
        <div class="row">
            <div class="row">
                <?= Html::activeLabel($model, 'driverMiddleName') ?><br>
                <?= Html::activeTextInput($model, 'driverMiddleName[' . $row . ']',
                    [
                        'id' => 'osagosravniruform-drivermiddlename_' . $row,
                        'class' => 'form-control js-fio-input',
                        'placeholder' => $labels['driverMiddleName']
                    ]); ?>
                <div class="js-error-msg js-error-osagosravniruform-drivermiddlename_<?= $row ?>"></div>
            </div>
        </div>
        <div class="row">
            <div class="row">
                <?= Html::activeLabel($model, 'driverLicenseSerial') ?><br>
                <?= Html::activeTextInput($model, 'driverLicenseSerial[' . $row . ']',
                    [
                        'id' => 'osagosravniruform-driverlicenseserial_' . $row,
                        'class' => 'form-control js-mask-pass-serial',
                        'placeholder' => $labels['driverLicenseSerial']
                    ]); ?>
                <div class="js-error-msg js-error-osagosravniruform-driverlicenseserial_<?= $row ?>"></div>
            </div>
        </div>
        <div class="row">
            <div class="row">
                <?= Html::activeLabel($model, 'driverLicenseNumber') ?><br>
                <?= Html::activeTextInput($model, 'driverLicenseNumber[' . $row . ']',
                    [
                        'id' => 'osagosravniruform-driverlicensenumber_' . $row,
                        'class' => 'form-control js-mask-pass-number',
                        'placeholder' => $labels['driverLicenseNumber']
                    ]); ?>
                <div class="js-error-msg js-error-osagosravniruform-driverlicensenumber_<?= $row ?>"></div>
            </div>
        </div>
        <div class="row">
            <div class="row">
                <?= Html::activeLabel($model, 'driverBirthDate') ?><br>
                <?php
                echo \yii\jui\DatePicker::widget([
                    'model' => $model,
                    'attribute' => 'driverBirthDate[' . $row . ']',
                    'language' => 'ru',
                    'dateFormat' => 'dd.MM.yyyy',
                    'options' => [
                        'id' => 'osagosravniruform-driverbirthdate' . $row,
                        'placeholder' => $labels['driverBirthDate'],
                        'class' => 'form-control js-mask-date js-date-picker-change-year-yes',
                    ],
                ]);
                ?>
                <div class="js-error-msg js-error-osagosravniruform-driverbirthdate_<?= $row ?>"></div>
            </div>
        </div>
        <div class="row">
            <div class="row">
                <?= Html::activeLabel($model, 'driverExpDate') ?><br>
                <?php
                echo \yii\jui\DatePicker::widget([
                    'model' => $model,
                    'attribute' => 'driverExpDate[' . $row . ']',
                    'language' => 'ru',
                    'dateFormat' => 'dd.MM.yyyy',
                    'options' => [
                        'id' => 'osagosravniruform-driverexpdate' . $row,
                        'placeholder' => $labels['driverExpDate'],
                        'class' => 'form-control js-mask-date js-date-picker-change-year-yes',
                    ],
                ]);
                ?>
                <div class="js-error-msg js-error-osagosravniruform-driverexpdate_<?= $row ?>"></div>
            </div>
        </div>

        <div class="row">
            <div class="row" style="padding-bottom: 10px;">
                <?= Html::activeHiddenInput($model, 'driverPrevAddInfo[' . $row . ']',
                    [
                        'id' => 'osagosravniruform-driverprevaddinfo_' . $row,
                        'class' => 'js-driver-prev-add-info-input'
                    ]) ?>
                <a href="#" class="btn bg-info" onclick="
                        let parentBlock = $(this).closest('.js-driver-block');
                        let input = parentBlock.find('.js-driver-prev-add-info-input');
                        let block = parentBlock.find('.js-driver-info-prev-info-form');
                        let addInfoVal = block.val();
                        input.val(parseInt(input.val()) == <?= OsagoSravniRuFormDictionary::PRE_CALCULATION_YES ?> ? <?= OsagoSravniRuFormDictionary::PRE_CALCULATION_NO ?> : <?= OsagoSravniRuFormDictionary::PRE_CALCULATION_YES ?>);
                        block.slideToggle();
                        return false;
                        ">
                    Указать предыдущее ВУ
                </a>
            </div>
        </div>

        <div
                class="js-driver-info-prev-info-form"
                style="<?=
                !empty($model->driverPrevLastName[$row]) ||
                !empty($model->driverPrevFirstName[$row]) ||
                !empty($model->driverPrevMiddleName[$row]) ||
                !empty($model->driverPrevLicenseSerial[$row]) ||
                !empty($model->driverPrevLicenseNumber[$row]) ||
                !empty($model->driverPrevLicenseDate[$row])
                    ? ""
                    : "display: none"
                ?>"
        >
            <?= Yii::$app->controller->renderPartial('_prev_driver_info', ['model' => $model, 'row' => $row]) ?>
        </div>

        <?php if ($row > 0) { ?>
            <div class="row">
                <div class="row" style="padding-bottom: 10px;">
                    <a href="#" class="btn btn-danger js-add-remove-driver-btn" data-type="remove" data-row="<?= $row ?>">Удалить
                        водителя</a>
                </div>
            </div>
        <?php } ?>
    </div>
</div>