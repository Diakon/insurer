<?php
/**
 * @var $model \common\modules\sravniru\forms\OsagoSravniRuForm
 * @var $row integer
 */

use yii\helpers\Html;

$labels = $model->attributeLabels();
?>
<div class="row">
    <?= Html::activeLabel($model, 'driverPrevLastName') ?><br>
    <?= Html::activeTextInput($model, 'driverPrevLastName[' . $row . ']',
        [
            'id' => 'osagosravniruform-driverprevlastname_' . $row,
            'class' => 'form-control js-fio-input',
            'placeholder' => $labels['driverPrevLastName']
        ]); ?>
    <div class="js-error-msg js-error-osagosravniruform-driverprevlastname_<?= $row ?>"></div>
</div>
<div class="row">
    <?= Html::activeLabel($model, 'driverPrevFirstName') ?><br>
    <?= Html::activeTextInput($model, 'driverPrevFirstName[' . $row . ']',
        [
            'id' => 'osagosravniruform-driverprevfirstname_' . $row,
            'class' => 'form-control js-fio-input',
            'placeholder' => $labels['driverPrevFirstName']
        ]); ?>
    <div class="js-error-msg js-error-osagosravniruform-driverprevfirstname_<?= $row ?>"></div>
</div>
<div class="row">
    <?= Html::activeLabel($model, 'driverPrevMiddleName') ?><br>
    <?= Html::activeTextInput($model, 'driverPrevMiddleName[' . $row . ']',
        [
            'id' => 'osagosravniruform-driverprevmiddlename_' . $row,
            'class' => 'form-control js-fio-input',
            'placeholder' => $labels['driverPrevMiddleName']
        ]); ?>
    <div class="js-error-msg js-error-osagosravniruform-driverprevmiddlename_<?= $row ?>"></div>
</div>
<div class="row">
    <?= Html::activeLabel($model, 'driverPrevLicenseSerial') ?><br>
    <?= Html::activeTextInput($model, 'driverPrevLicenseSerial[' . $row . ']',
        [
            'id' => 'osagosravniruform-driverprevlicenseserial_' . $row,
            'class' => 'form-control js-driver-license-serial-mask',
            'placeholder' => $labels['driverPrevLicenseSerial']
        ]); ?>
    <div class="js-error-msg js-error-osagosravniruform-driverprevlicenseserial_<?= $row ?>"></div>
</div>
<div class="row">
    <?= Html::activeLabel($model, 'driverPrevLicenseNumber') ?><br>
    <?= Html::activeTextInput($model, 'driverPrevLicenseNumber[' . $row . ']',
        [
            'id' => 'osagosravniruform-driverlicensenumber_' . $row,
            'class' => 'form-control js-driver-license-number-mask',
            'placeholder' => $labels['driverPrevLicenseNumber']
        ]); ?>
    <div class="js-error-msg js-error-osagosravniruform-driverprevlicensenumber_<?= $row ?>"></div>
</div>
<div class="row">
    <?= Html::activeLabel($model, 'driverPrevLicenseDate') ?><br>
    <?php
    echo \yii\jui\DatePicker::widget([
        'model' => $model,
        'attribute' => 'driverPrevLicenseDate[' . $row . ']',
        'language' => 'ru',
        'dateFormat' => 'dd.MM.yyyy',
        'options' => [
            'id' => 'osagosravniruform-driverprevlicensedate_' . $row,
            'placeholder' => $labels['driverPrevLicenseDate'],
            'class' => 'form-control js-date-picker-change-year-yes js-mask-date',
        ],
    ]);
    ?>
    <div class="js-error-msg js-error-osagosravniruform-driverprevlicensedate_<?= $row ?>"></div>
</div>