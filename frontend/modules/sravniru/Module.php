<?php
namespace frontend\modules\sravniru;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\sravniru\controllers';

    public function init()
    {
        parent::init();
    }
}
