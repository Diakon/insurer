<?php

namespace frontend\modules\sravniru\controllers;

use common\modules\inguru\services\KladrService;
use common\modules\orders\models\Order;
use common\modules\sravniru\dictionaries\FinalCalculationDictionary;
use common\modules\sravniru\dictionaries\OsagoSravniRuFormDictionary;
use common\modules\sravniru\forms\OsagoSravniRuForm;
use Yii;
use frontend\controllers\SiteController;
use yii\helpers\Json;
use yii\widgets\ActiveForm;
use frontend\modules\orders\traits\OrderTrait;

/**
 * Class OwnerController
 * @package frontend\modules\sravniru\controllers
 */
class OwnerInfoController extends SiteController
{
    use OrderTrait;

    /**
     * @return mixed|string
     */
    public function actionIndex()
    {
        $action = Yii::$app->request->get('action');

        if (empty($action) && Yii::$app->request->isGet) {
            return $this->formHtml();
        }
        $post = Yii::$app->request->post();

        return $this->{$action}($post);
    }

    /**
     * Возвращает HTML кода формы
     *
     * @param array $params
     * @return string
     */
    private function formHtml($params = [])
    {
        $model = $this->setFormOrderModel($this->getModel(), $params);
        $model->multiDrive = $this->getMultiDrive(); // Для валидации данных о владельце требуется знать значение выбранное в multiDrive
        if (is_null($model->insurerIsOwner)) {
            $model->insurerIsOwner = FinalCalculationDictionary::ONE_FACE_YES;
        }

        return $this->renderPartial('index', ['model' => $model]);
    }

    /**
     * Возвращает список городов с областью (так как требует сравни.ру)
     * @param array $params
     * @return string
     */
    private function searchCity($params = [])
    {
        $result = [];
        $term = $params['term'] ?? Yii::$app->request->get('term');
        $term = trim($term);

        foreach (KladrService::getCityByName($term) as $city) {
            $regionName = $city['regionName'];
            $regionType = $city['regionType'];
            $cityName = $city['name'];
            $cityType = $city['type'];
            $label = '';
            if (!empty($regionName) && !in_array($city['code'], [7700000000000, 9200000000000, 7800000000000])) { // города ведерального значения возвращаю без указания региона (у них это название самого города)
                $label .= trim("$regionName $regionType") . ", ";
            }
            $label .= "$cityType. $cityName";
            $label = trim($label);
            $result[] = [
                'id' => $label,
                'value' => $label,
                'label' => $label,
            ];
        }

        return Json::encode($result);
    }

    /**
     * Возвращает список адресов получая их через запрос в АПИ dadata
     * @param array $params
     * @return string
     */
    private function searchAddress($params = [])
    {
        $term = $params['term'] ?? Yii::$app->request->get('term');
        return Json::encode(Yii::$app->api->getAddress($term));
    }

    /**
     * Проверяет форму
     *
     * @param $params
     * @return array
     */
    private function validate($params)
    {
        $model = $this->getModel();
        $model->load($this->getOrderParams($params));
        $model->multiDrive = $this->getMultiDrive(); // Для валидации данных о владельце требуется знать значение выбранное в multiDrive
        $model->insurerIsOwner = !empty($model->insurerIsOwner) ? FinalCalculationDictionary::ONE_FACE_YES : FinalCalculationDictionary::ONE_FACE_NO;

        if (!$model->validate()) {
            return $this->returnAjax(self::CODE_ERROR, ActiveForm::validate($model));
        }

        // Сохраняю шаг
        $order = $this->saveStep($model);
        if (!$order) {
            return $this->returnAjax(self::CODE_ERROR, "Ошибка АПИ");
        }

        return $this->returnAjax(self::CODE_SUCCESS, ['step' => OsagoSravniRuForm::SCENARIO_STEP_POLICY, 'orderId' => $order->id]);
    }

    /**
     * @return OsagoSravniRuForm
     */
    private function getModel()
    {
        $model = new OsagoSravniRuForm();
        $model->setScenario(OsagoSravniRuForm::SCENARIO_STEP_OWNER);

        return $model;
    }

    /**
     * Возвращает данные о мультидрайве
     * @return int
     */
    private function getMultiDrive()
    {
        $order = $this->getOrder();
        $serviceProperty = new \common\modules\orders\services\OrderPropertyService();
        $multiDrive = $serviceProperty->getOrderPropertyValue($order->properties, 'multiDrive');

        return !empty($multiDrive) ? OsagoSravniRuFormDictionary::IS_MULTI_DRIVE_YES : OsagoSravniRuFormDictionary::IS_MULTI_DRIVE_NO;
    }
}