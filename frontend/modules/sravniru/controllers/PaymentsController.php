<?php

namespace frontend\modules\sravniru\controllers;

use common\modules\orders\models\Order;
use common\modules\sravniru\dictionaries\OsagoSravniRuFormDictionary;
use common\modules\sravniru\forms\OsagoSravniRuForm;
use common\modules\sravniru\services\OsagoSravniRuFormService;
use frontend\controllers\SiteController;
use frontend\modules\orders\traits\OrderTrait;
use common\modules\orders\dictionaries\OrderDictionary;
use common\modules\orders\services\OrderService;
use Yii;
use yii\helpers\Url;

/**
 * Class PaymentsController
 * @package frontend\modules\sravniru\controllers
 */
class PaymentsController extends SiteController
{
    use OrderTrait;
    /**
     * @return mixed|string
     */
    public function actionIndex()
    {
        if (!Yii::$app->request->isPost) {
            return $this->goHome();
        }

        $searchId = Yii::$app->request->post('id', 0);
        $companyId = Yii::$app->request->post('companyId');
        $companyName = Yii::$app->request->post('companyName');
        $redirectPaymentUrlSravniRu = Yii::$app->request->post('url');
        $hash = Yii::$app->request->post('hash');
        $awaiting = Yii::$app->request->post('awaiting');
        $awaiting = !empty($awaiting);

        // Получаю ID заказа
        $serviceOrder = new OrderService();
        $orderId = Yii::$app->request->get('order_id');
        /**
         * @var $order Order
         */
        $order = $serviceOrder->getOrdersByParams(['id' => $orderId, 'user_id' => Yii::$app->user->id], 'id', SORT_ASC, true);

        $model = $this->getModel();
        $model->searchId = $searchId;
        $model->companyId = $companyId;
        $model->companyName = $companyName;
        $model->redirectPaymentUrlSravniRu = $redirectPaymentUrlSravniRu;
        $model->hash = $hash;

        // Получаю стоимость и комиссию из сессии
        if ($model->preCalculation == OsagoSravniRuFormDictionary::PRE_CALCULATION_NO) {
            $sessionData = OsagoSravniRuFormService::getSessionInsureData($searchId, $companyId);
            // Сохраняю выбранную стоимость и комиссию
            $order->price = $sessionData['price'] ?? null;
            $order->commission = $sessionData['commission'] ?? null;
            $serviceOrder->save($order);

            $model->paymentUrl = $order->generatePaymentLink(); // Генерирую ссылку на оплату (наш сайт)
        }

        // Если нажали на кнопку сохранить в разделе "Ожилают оплаты", то не делаем запрос на оплату, а сохраняем заказ со статусом "Ожидает оплаты" и редиректим в этот раздел
        if ($awaiting) {
            // Сохраняю шаг как для "отложить оплату"
            if(!$this->saveStep($model, OrderDictionary::STATUS_AWAITING_PAYMENT)) {
                return $this->returnAjax(self::CODE_ERROR, 'Ошибка при сохранении данных. Пожалуйста, обратитесь в службу поддерхки!');
            }

            return $this->returnAjax(self::CODE_SUCCESS, ['url' => Url::to(['/awaiting-payment'])]);
        }

        return $this->returnAjax(self::CODE_SUCCESS, ['url' => $paymentUrl]);
    }

    /**
     * @return OsagoSravniRuForm
     */
    private function getModel()
    {
        $model = new OsagoSravniRuForm();
        $model->setScenario(OsagoSravniRuForm::SCENARIO_STEP_PAYMENT);

        return $model;
    }
}