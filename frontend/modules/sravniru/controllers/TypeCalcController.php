<?php

namespace frontend\modules\sravniru\controllers;

use common\modules\orders\services\OrderPropertyService;
use common\modules\orders\services\OrderService;
use common\modules\sravniru\dictionaries\OsagoSravniRuFormDictionary;
use common\modules\sravniru\forms\OsagoSravniRuForm;
use Yii;
use frontend\controllers\SiteController;
use frontend\modules\orders\traits\OrderTrait;
use yii\widgets\ActiveForm;

/**
 * Class TypeCalcController
 * @package frontend\modules\sravniru\controllers
 */
class TypeCalcController extends SiteController
{
    use OrderTrait;

    /**
     * @return mixed|string
     */
    public function actionIndex()
    {
        $action = Yii::$app->request->get('action');
        if (empty($action)) {
            return $this->formHtml();
        }

        $post = Yii::$app->request->post();

        $action = Yii::$app->request->get('action');

        return $this->{$action}($post);
    }

    /**
     * Возвращает HTML кода формы
     *
     * @param array $params
     * @return string
     */
    private function formHtml($params = [])
    {
        $model = $this->getModel();
        $model->load($params);

        // Если это редактирование заказа - получаю тип расчета, который был выбран ранее
        $orderId = Yii::$app->request->get('order_id');
        if (!empty($orderId)) {
            $service = new OrderService();
            $order = $service->getOrderById($orderId, Yii::$app->user->id);
            if (!empty($order)) {
                $serviceProperty = new \common\modules\orders\services\OrderPropertyService();
                $model->preCalculation = $serviceProperty->getOrderPropertyValue($order->properties, 'preCalculation');
            }
        }


        return $this->renderPartial('index', ['model' => $model]);
    }

    /**
     * Проверяет форму
     * @param $params
     * @return array
     */
    private function validate($params)
    {
        $model = $this->getModel();
        $model->load($this->getOrderParams($params));
        if (!$model->validate()) {
            return $this->returnAjax(self::CODE_ERROR, ActiveForm::validate($model));
        }

        return $this->returnAjax(self::CODE_SUCCESS, ['step' => OsagoSravniRuForm::SCENARIO_STEP_CAR_INFO, 'orderId' => 0]);
    }

    /**
     * @return OsagoSravniRuForm
     */
    private function getModel()
    {
        $model = new OsagoSravniRuForm();
        $model->setScenario(OsagoSravniRuForm::SCENARIO_STEP_TYPE_CALC);

        return $model;
    }

    /**
     * Возвращает тип расчета
     * @param array $params
     * @return array
     */
    private function typeCalc(array $params = [])
    {
        $order = $this->getOrder();
        $serviceProperty = new OrderPropertyService();
        $property = $serviceProperty->getOrdersByParams(['order_id' => $order->id, 'key' => 'preCalculation'], true);

        return $this->returnAjax(self::CODE_SUCCESS, $property->value);
    }

    /**
     * Переводит тип заказа в полный расчет
     * @param array $params
     * @return array
     */
    private function changeToFinalCalculation(array $params = [])
    {
        $order = $this->getOrder();
        if ($order->isNewRecord) {
            return $this->returnAjax(self::CODE_ERROR, 'Заказ не найден');
        }
        // Ставлю шаг на "Автомобиль"
        $serviceOrder = new OrderService();
        $order->step = OsagoSravniRuForm::SCENARIO_STEP_CAR_INFO;
        $serviceOrder->save($order);
        // Выставляю тип расчета, как полный расчет для заказа
        $serviceProperty = new OrderPropertyService();
        $serviceProperty->update(
            ['value' => OsagoSravniRuFormDictionary::PRE_CALCULATION_NO],
            ['AND', ['=', 'order_id', $order->id],['=', 'key', 'preCalculation']]
        );

        return $this->returnAjax(self::CODE_SUCCESS, 'Заказ успешно переведен в полный расчет');
    }
}