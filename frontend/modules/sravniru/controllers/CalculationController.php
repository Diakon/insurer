<?php

namespace frontend\modules\sravniru\controllers;

use common\modules\orders\models\Order;
use common\modules\sravniru\dictionaries\OsagoSravniRuFormDictionary;
use common\modules\sravniru\forms\OsagoSravniRuForm;
use common\modules\orders\models\CalculateProperty;
use common\modules\orders\dictionaries\OrderDictionary;
use common\modules\orders\models\OrderProperty;
use common\modules\orders\services\OrderPropertyService;
use common\modules\sravniru\models\FinalCalculation;
use common\modules\sravniru\models\PreCalculation;
use common\modules\sravniru\services\FinalCalculationService;
use common\modules\sravniru\services\OsagoSravniRuFormService;
use common\modules\sravniru\services\PreCalculationService;
use DeepCopyTest\Matcher\Y;
use Yii;
use frontend\controllers\SiteController;
use yii\base\BaseObject;
use yii\helpers\Json;
use yii\widgets\ActiveForm;
use frontend\modules\orders\traits\OrderTrait;

/**
 * Class CalculationController
 * @package frontend\modules\sravniru\controllers
 */
class CalculationController extends SiteController
{
    use OrderTrait;

    /**
     * @return mixed|string
     */
    public function actionIndex()
    {
        if (!Yii::$app->request->isPost) {
            return $this->goHome();
        }

        $params = Yii::$app->request->post();
        $action = Yii::$app->request->get('action');
        $selectedCompanyId = Yii::$app->request->get('company_id'); // Если выбрали конкретную страховую для расчета
        if (!empty($action)) {
            return $this->{$action}($params);
        }

        $params = $this->getOrderParams($params, OsagoSravniRuForm::classNameShort());
        $modelForm = $this->setFormOrderModel(new OsagoSravniRuForm(), $params);
        $modelForm->setScenario(OsagoSravniRuForm::SCENARIO_STEP_CALCULATE);
        $modelForm->load($params);

        if (!$modelForm->validate()) {
            return $this->returnAjax(self::CODE_ERROR, ActiveForm::validate($modelForm));
        }

        // Валидация прошла - отправляю запрос в АПИ сравни.ру
        $model = $modelForm->preCalculation == OsagoSravniRuFormDictionary::PRE_CALCULATION_YES ? new PreCalculation() : new FinalCalculation();
        $model->load($params[$modelForm::classNameShort()], '');
        $service = $modelForm->preCalculation == OsagoSravniRuFormDictionary::PRE_CALCULATION_YES ? new PreCalculationService($model) : new FinalCalculationService($model);
        $status = OrderDictionary::STATUS_NEW;
        /**
         * @var Order
         */
        $order = $this->getOrder();

        $response = $service->getCalculateResult($order->token);


        // Сохраняю шаг, если это НЕ расчет для заказа из "ожидают оплаты" (нечего сохранять - уже все было сохранено ранее)
        if ($order->status != OrderDictionary::STATUS_AWAITING_PAYMENT) {
            $this->saveStep($modelForm, $status);
        }

        // Если полный расчет - прохожу по предложениям от страховых компаний и пишу данные в сессию,
        // что бы после выбора предложения взять от туда цену, комиссию и тд
        if (
            !empty($response['response']['searchId']) &&
            !empty($response['response']['osagoCalculateThroughOrderResults']) &&
            $modelForm->preCalculation == OsagoSravniRuFormDictionary::PRE_CALCULATION_NO
        ) {
            OsagoSravniRuFormService::setSessionInsureData($response['response']['searchId'], $response['response']['osagoCalculateThroughOrderResults']);
        }

        return $this->renderPartial('index', [
            'model' => $model,
            'response' => $response['response'] ?? [],
            'success' => $response['success'] ?? false,
            'order' => $order,
            'row' => Yii::$app->request->get('row', 1),
            'selectedCompanyId' => $selectedCompanyId
        ]);
    }
}