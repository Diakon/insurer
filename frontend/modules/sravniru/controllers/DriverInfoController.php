<?php

namespace frontend\modules\sravniru\controllers;

use common\modules\sravniru\forms\OsagoSravniRuForm;
use Yii;
use frontend\controllers\SiteController;
use yii\widgets\ActiveForm;
use frontend\modules\orders\traits\OrderTrait;

/**
 * Class DriverInfoController
 * @package frontend\modules\sravniru\controllers
 */
class DriverInfoController extends SiteController
{
    use OrderTrait;

    /**
     * @return mixed|string
     */
    public function actionIndex()
    {
        if (Yii::$app->request->isGet) {
            return $this->formHtml();
        }

        $post = Yii::$app->request->post();
        $action = Yii::$app->request->get('action');

        return $this->{$action}($post);
    }

    /**
     * Возвращает HTML кода формы
     *
     * @param array $params
     * @return string
     */
    private function formHtml($params = [])
    {
        $isNew = Yii::$app->request->get('add', false);
        $model = $this->getModel();
        $model = $isNew ? $model : $this->setFormOrderModel($model, $params);
        $row = Yii::$app->request->get('row', 0);

        return $this->renderPartial('index', ['model' => $model, 'row' => $row]);
    }

    /**
     * Проверяет форму
     *
     * @param $params
     * @return array
     */
    private function validate($params)
    {
        $model = $this->getModel();
        $model->load($this->getOrderParams($params, $model::classNameShort()));

        if (!$model->validate()) {
            return $this->returnAjax(self::CODE_ERROR, ActiveForm::validate($model));
        }

        // Сохраняю шаг
        $order = $this->saveStep($model);
        if (!$order) {
            return $this->returnAjax(self::CODE_ERROR, "Ошибка АПИ");
        }

        return $this->returnAjax(self::CODE_SUCCESS, ['step' => OsagoSravniRuForm::SCENARIO_STEP_OWNER, 'orderId' => $order->id]);
    }

    /**
     * @return OsagoSravniRuForm
     */
    private function getModel()
    {
        $model = new OsagoSravniRuForm();
        $model->setScenario(OsagoSravniRuForm::SCENARIO_STEP_DRIVER);

        return $model;
    }
}