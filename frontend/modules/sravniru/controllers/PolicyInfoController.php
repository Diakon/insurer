<?php

namespace frontend\modules\sravniru\controllers;

use common\modules\sravniru\dictionaries\OsagoSravniRuFormDictionary;
use common\modules\sravniru\forms\OsagoSravniRuForm;
use Yii;
use frontend\controllers\SiteController;
use yii\widgets\ActiveForm;
use frontend\modules\orders\traits\OrderTrait;

/**
 * Class PolicyInfoController
 * @package frontend\modules\sravniru\controllers
 */
class PolicyInfoController extends SiteController
{
    use OrderTrait;

    /**
     * @return mixed|string
     */
    public function actionIndex()
    {
        $action = Yii::$app->request->get('action');

        if (empty($action) && Yii::$app->request->isGet) {
            return $this->formHtml();
        }

        $post = Yii::$app->request->post();

        return $this->{$action}($post);
    }

    /**
     * Возвращает HTML кода формы
     *
     * @param array $params
     * @return string
     */
    private function formHtml($params = [])
    {
        $model = $this->setFormOrderModel($this->getModel(), $params);
        if (is_null($model->prevPolicyIsset)) {
            $model->prevPolicyIsset = OsagoSravniRuFormDictionary::PREV_POLICY_ISSET_YES;
        }

        return $this->renderPartial('index', ['model' => $model]);
    }

    /**
     * Проверяет форму
     *
     * @param $params
     * @return array
     */
    private function validate($params)
    {
        $model = $this->getModel();
        $model->load($this->getOrderParams($params));
        if (!$model->validate()) {
            return $this->returnAjax(self::CODE_ERROR, ActiveForm::validate($model));
        }

        // Сохраняю шаг
        $order = $this->saveStep($model);
        if (!$order) {
            return $this->returnAjax(self::CODE_ERROR, "Ошибка АПИ");
        }

        return $this->returnAjax(self::CODE_SUCCESS, ['step' => OsagoSravniRuForm::SCENARIO_STEP_CALCULATE, 'orderId' => $order->id]);
    }

    /**
     * @return OsagoSravniRuForm
     */
    private function getModel()
    {
        $model = new OsagoSravniRuForm();
        $model->setScenario(OsagoSravniRuForm::SCENARIO_STEP_POLICY);

        return $model;
    }
}