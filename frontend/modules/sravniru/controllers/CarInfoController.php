<?php

namespace frontend\modules\sravniru\controllers;

use common\modules\cars\dictionaries\CarDictionary;
use common\modules\cars\services\CarService;
use common\modules\sravniru\dictionaries\FinalCalculationDictionary;
use common\modules\sravniru\forms\OsagoSravniRuForm;
use common\modules\sravniru\services\CarBrandModelService;
use common\modules\sravniru\services\CarNumberService;
use Yii;
use frontend\controllers\SiteController;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\widgets\ActiveForm;
use frontend\modules\orders\traits\OrderTrait;

/**
 * Class CarInfoController
 * @package frontend\modules\sravniru\controllers
 */
class CarInfoController extends SiteController
{
    use OrderTrait;

    /**
     * @return mixed|string
     */
    public function actionIndex()
    {
        $action = Yii::$app->request->get('action');

        if (empty($action) && Yii::$app->request->isGet) {
            return $this->formHtml();
        }
        $post = Yii::$app->request->post();

        return $this->{$action}($post);
    }


    /**
     * Возвращает HTML кода формы
     * @param array $params
     * @return string
     */
    private function formHtml($params = [])
    {
        $model = $this->setFormOrderModel($this->getModel(), $params);

        return $this->renderPartial('index', ['model' => $model]);
    }

    /**
     * Проверяет форму
     *
     * @param $params
     * @return array
     */
    private function validate($params)
    {
        $model = $this->getModel();
        $model->load($this->getOrderParams($params, $model::classNameShort()));

        if (!$model->validate()) {
            return $this->returnAjax(self::CODE_ERROR, ActiveForm::validate($model));
        }

        // Сохраняю шаг
        $order = $this->saveStep($model);
        if (!$order) {
            return $this->returnAjax(self::CODE_ERROR, "Ошибка АПИ");
        }

        return $this->returnAjax(self::CODE_SUCCESS, ['step' => 'driver', 'orderId' => $order->id]);
    }


    /**
     * Поиск модели по бренду
     *
     * @param array $params
     * @return array|string
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    private function searchModel($params = [])
    {
        $brandId = $params['brandId'] ?? 0;
        $models = \common\modules\sravniru\services\CarBrandModelService::getModels((int)$brandId);
        if (!empty($models)) {
            return Json::encode(ArrayHelper::map($models, 'id', 'name'));
        }

        return $this->returnAjax(self::CODE_ERROR, "Ошибка АПИ");
    }

    /**
     * @return OsagoSravniRuForm
     */
    private function getModel()
    {
        $model = new OsagoSravniRuForm();
        $model->setScenario(OsagoSravniRuForm::SCENARIO_STEP_CAR_INFO);

        return $model;
    }

    /**
     * Получает данные об авто по гос. номеру. Если нет в БД - тянем по АПИ и сохраняем в БД у себя
     * @param $params
     * @return array
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\HttpException
     */
    private function carInfoByNumber($params)
    {
        $model = new OsagoSravniRuForm();
        $model->setScenario(OsagoSravniRuForm::SCENARIO_VALIDATE_CAR_NUMBER);
        $model->load($params);
        if (!$model->validate()) {
            return $this->returnAjax(self::CODE_ERROR, ActiveForm::validate($model));
        }
        $car = CarService::getCar($model->vehicleLicensePlate, CarDictionary::SOURCE_SRAVNIRU);
        if (empty($car)) {
            $model->addError('vehicleLicensePlate', 'Не удалось получить данные о машине по гос. номеру. Пожалуйста, введите данные вручную или попробуйте снова.');
            return $this->returnAjax(self::CODE_ERROR, ['osagosravniruform-vehiclelicenseplate' => $model->getErrorSummary(true)]);
        }
        $car = $car->getAttributes();
        //Преобразую ID типа документа на ТС с констант в car на соответствующие им id в сравни.ру
        if (!is_null($car['doc_type'])) {
            $car['doc_type'] = FinalCalculationDictionary::CAR_TYPE_TO_SRAVNIRU[(int)$car['doc_type']];
        }

        return $this->returnAjax(self::CODE_SUCCESS, ['car' => $car]);
    }
}