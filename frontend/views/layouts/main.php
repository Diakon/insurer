<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Html;
use frontend\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<!-- Chatra {literal} -->
<script>
    (function(d, w, c) {
        w.ChatraID = 'KiTo6mPMbGHBjAPgW';
        var s = d.createElement('script');
        w[c] = w[c] || function() {
            (w[c].q = w[c].q || []).push(arguments);
        };
        s.async = true;
        s.src = 'https://call.chatra.io/chatra.js';
        if (d.head) d.head.appendChild(s);
    })(document, window, 'Chatra');
</script>
<!-- /Chatra {/literal} -->
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon">
    <?php $this->head() ?>
    <?php $this->registerJsFile('/js/script.js', ['depends' => [\yii\web\JqueryAsset::class]]); ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?= Html::a('<img src="/images/logo.png" style="width: 200px">', \yii\helpers\Url::to(['/'])) ?>

    <div class="container">
        <?= $content ?>

        <?php if (!Yii::$app->user->isGuest) { ?>
            <div class="main-menu-block">
                <div class="stick-menu__menu-row">
                    <?= Html::a('<span class="glyphicon glyphicon-plus" aria-hidden="true"><br>Новый расчет</span>', \yii\helpers\Url::to(['/new-calculation']), ['class' => 'stick-menu__menu-item btn btn-default']) ?>
                    <?= Html::a('<span class="glyphicon glyphicon-ruble" aria-hidden="true"><br>Мои расчеты</span>', \yii\helpers\Url::to(['/my-calculations']), ['class' => 'stick-menu__menu-item btn btn-default']) ?>
                    <?= Html::a('<span class="glyphicon glyphicon-time" aria-hidden="true"><br>Ожидают<br>оплаты</span>', \yii\helpers\Url::to(['/awaiting-payment']), ['class' => 'stick-menu__menu-item btn btn-default']) ?>
                    <?= Html::a('<span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"><br>Оплаченные</span>', \yii\helpers\Url::to(['/paid']), ['class' => 'stick-menu__menu-item btn btn-default']) ?>
                    <?= Html::a('<span class="glyphicon glyphicon-user" aria-hidden="true"><br>Профиль</span>', \yii\helpers\Url::to(['/profile']), ['class' => 'stick-menu__menu-item btn btn-default']) ?>
                    <?= Html::a('<span class="glyphicon glyphicon-wrench" aria-hidden="true"><br>Поддержка</span>', \yii\helpers\Url::to(['/faq']), ['class' => 'stick-menu__menu-item btn btn-default']) ?>
                    <?= Html::a('<span class="glyphicon glyphicon-random" aria-hidden="true"><br>Перейти в<br>стор</span>', 'https://store.bezlimit.ru', ['class' => 'stick-menu__menu-item btn btn-default']) ?>
                </div>
            </div>
        <?php } ?>
    </div>
</div>

<div id="chatra-wrapper"></div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
