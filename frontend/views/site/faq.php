<?php
/**
 * @var $faqs array
 */
$this->title = 'Страхование: Поддержка';
?>
<div align="center">
    <div class="chat-support-btn js-support-chat-button">
        <p></p>
        <h3>Чат с поддержкой</h3>
    </div>
</div>

<div class="col-md-3"></div>
<div class="col-md-6">
    <?php foreach ($faqs as $faqId => $faq) { ?>
        <div class="row js-faq-block js-faq-block-id-<?= $faqId ?>">
            <span style="text-align: center"><h2><?= $faq['question'] ?></h2></span>
            <br>
            <div style="display: none" class="js-faq-answer">
                <?= $faq['answer'] ?>
            </div>
        </div>
    <?php } ?>
</div>
<div class="col-md-3"></div>
