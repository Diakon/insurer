<?php
/**
 * @var $this yii\web\View
 * @var $order \common\modules\orders\models\Order|null
 * @var string $viewName;
 * @var integer|null $companyId
 */

use common\modules\inguru\forms\OsagoInguruForm;
use yii\widgets\ActiveForm;
use yii\helpers\Html;


$step = !empty($order) ? $order->step : 'typecalc';
?>
<?php $this->registerJsFile('/js/script_inguru.js', ['depends' => [\yii\web\JqueryAsset::class]]); ?>
<?php $this->registerJsFile('/js/jquery-ui.js', ['depends' => [\yii\web\JqueryAsset::class]]); ?>
<?php $this->registerJsFile('/js/jquery.inputmask.js', ['depends' => [\yii\web\JqueryAsset::class]]); ?>


<?php $this->registerCssFile("/css/jquery-ui.css", ['depends' => [\yii\bootstrap\BootstrapAsset::class]]); ?>

<?php
$form = ActiveForm::begin([
    'options' => [
        'id' => 'js-osago-form',
        'method' => 'post'
    ]
]) ?>
<?= Html::hiddenInput('orderId', !empty($order) ? $order->id : 0, ['class' => 'js-order-id']) ?>

    <div class="body-content">
        <div class="row">
            <div class="col-lg-12">
                <ul class="navbar-nav  nav js-menu-block">
                    <li><p class="js-menu-item js-menu-item-typecalc" style="margin-left: 20px">Тип расчета</p></li>
                    <li><p class="js-menu-item js-menu-item-car" style="margin-left: 20px">Автомобиль</p></li>
                    <li><p class="js-menu-item js-menu-item-driver" style="margin-left: 20px">Водители</p></li>
                    <li><p class="js-menu-item js-menu-item-owner" style="margin-left: 20px">Собственник</p></li>
                    <li><p class="js-menu-item js-menu-item-policy" style="margin-left: 20px">Полис</p></li>
                    <li><p class="js-menu-item js-menu-item-calc" style="margin-left: 20px">Расчет</p></li>
                </ul>
            </div>
            <br>
            <hr>
            <div class="col-lg-12">
                <div class="js-step js-step-typecalc" data-step="typecalc"></div>
                <div class="js-step js-step-car" data-step="car"></div>
                <div class="js-step js-step-driver" data-step="driver"></div>
                <div class="js-step js-step-owner" data-step="owner"></div>
                <div class="js-step js-step-policy" data-step="policy"></div>
                <div class="js-step js-step-calc" data-step="calc"></div>
            </div>

            <div class="col-lg-12">
                <div class="js-driver-btn" style="display: none">
                    <?= Html::hiddenInput('driverCount', 0, ['class' => 'js-input-driver-count']) ?>
                    <a href="#" class="btn btn-info js-add-remove-driver-btn" data-type="add">Добавить водителя</a>
                </div>
            </div>

            <br>
            <?= Html::hiddenInput('step', $step, ['class' => 'js-input-step']) ?>
            <hr>
            <div align="center">
                <div style="padding: 20px; display: none" class="js-ajax-load-image">
                    <img class="rot" src="/images/load-2.png" width="200px">
                </div>

                <div class="js-action-reload-page-block" style="padding:5px; display: none" align="center">
                    <?= \yii\helpers\Html::a('Обновить', '#',
                        [
                            'class' => 'btn btn-primary js-action-reload-page-btn',
                            'onclick' => 'location.reload(); return false;'
                    ]); ?>
                </div>

                <a href="#" class="btn btn-info js-prev-step-btn" style="display: none">Назад</a>
                <a href="#" class="btn btn-info js-next-step-btn">Продолжить</a>
            </div>

        </div>

    </div>

<?php ActiveForm::end() ?>

<?php // Блок со списком страховых компаний. Если пользователь уже выбрал конкретную компанию в $companyId - в список вывожу только ее ?>
<div style="display: none">
    <?php foreach (OsagoInguruForm::insuranceCompaniesList() as $id => $name) { ?>
        <?php if (!empty($companyId) && $id != $companyId) { continue; } ?>
        <div data-id="<?= $id ?>" class="js-insurance-companies-list-id"></div>
    <?php } ?>
    <?= Html::hiddenInput('selectedCompany', !empty($companyId) ? 1 : 0, ['class' => 'js-input-selected-company']) ?>
</div>
