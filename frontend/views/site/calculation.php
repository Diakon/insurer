<?php
/**
 * @var $this yii\web\View
 * @var $order \common\modules\orders\models\Order|null
 * @var string $viewName;
 * @var integer|null $companyId
 */

$this->title = 'Страхование: Новый расчет';
?>
<?= Yii::$app->controller->renderPartial($viewName, ['order' => $order, 'companyId' => $companyId]); ?>