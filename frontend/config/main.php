<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'language' => 'ru-RU',
    'components' => [
        'formatter' => 'common\components\Formatter',
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        
        'user' => $params['userIdentityParams'],
        'session' => $params['sessionIdentityParams'],
        'redis' => $params['redisIdentityParams'],

        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
                // Логирование запросов отпраляемых через курл
                [
                    'class' => 'yii\log\FileTarget',
                    'logFile' => '@runtime/logs/http-request.log',
                    'categories' => ['yii\httpclient\*'],
                ]
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'suffix' => '/',
            'rules' => [
                ['pattern' => '/signup', 'route' => 'users/user/signup'],
                ['pattern' => '/logout', 'route' => 'users/user/logout'],
                ['pattern' => '/login', 'route' => 'users/user/login'],
                ['pattern' => '/site/login', 'route' => 'users/user/login'],
                ['pattern' => '/payment-success', 'route' => 'orders/payment/success'],
                ['pattern' => '/payment-fail', 'route' => 'orders/payment/fail'],
                ['pattern' => '/payment-link', 'route' => 'orders/payment/link'],
                ['pattern' => '/new-calculation', 'route' => 'site/calculation'],
                ['pattern' => '/calculation', 'route' => 'site/calculation'],
                ['pattern' => '/my-calculations', 'route' => 'orders/order/list', 'defaults' => ['status' => \common\modules\orders\dictionaries\OrderDictionary::STATUS_NEW]],
                ['pattern' => '/awaiting-payment', 'route' => 'orders/order/list', 'defaults' => ['status' => \common\modules\orders\dictionaries\OrderDictionary::STATUS_AWAITING_PAYMENT]],
                ['pattern' => '/paid', 'route' => 'orders/order/list', 'defaults' => ['status' => \common\modules\orders\dictionaries\OrderDictionary::STATUS_COMPLETE]],
                ['pattern' => '/profile', 'route' => 'users/user/profile'],
                ['pattern' => '/faq', 'route' => 'site/faq'],

                '<action>' => 'frontend/<action>',
                '<controler>/<action>' => '<controler>/<action>',
                '<module>/<controler>/<action>' => '<module>/<controler>/<action>',
            ],
        ],
    ],
    'modules' => [
        'users' => [
            'class' => frontend\modules\users\Module::class,
        ],
        'inguru' => [
            'class' => frontend\modules\inguru\Module::class,
        ],
        'sravniru' => [
            'class' => frontend\modules\sravniru\Module::class,
        ],
        'orders' => [
            'class' => frontend\modules\orders\Module::class,
        ],
    ],
    'params' => $params,
];
